#!/usr/bin/gawk -f
BEGIN { cv = 0; }
/^<VERBATIM>$/{cv=1; next}
/^<\/VERBATIM>$/{cv=0; next}
{
	if (cv) {
		out = $0;
		gsub(/&/, "\\&amp;", out);
		gsub(/"/, "\\&quot;", out);
		gsub(/</, "\\&lt;", out);
		gsub(/>/, "\\&gt;", out);
		print(out, "<br/>");
	} else print;
}
