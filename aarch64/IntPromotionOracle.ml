(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Benjamin Bonneau   ENS, PSL                       *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

(** Oracle to promote integers into long (RISC-V only) *)

open BTL_Invariants
open Registers

let int_promotion_oracle (btl : BTL.code) (entry : RTL.node) (params : reg list)
  : BTL.code * gluemap =
  failwith "not implemented"
