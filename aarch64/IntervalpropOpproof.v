(* *********************************************************************)
(*                                                                     *)
(*              The Compcert verified compiler                         *)
(*                                                                     *)
(*                 Xavier Leroy, INRIA Paris                           *)
(*                                                                     *)
(*  Copyright Institut National de Recherche en Informatique et en     *)
(*  Automatique.  All rights reserved.  This file is distributed       *)
(*  under the terms of the INRIA Non-Commercial License Agreement.     *)
(*                                                                     *)
(* *********************************************************************)

(** Correctness proof for operator strength reduction. *)

Require Import Coqlib Compopts.
Require Import Integers Floats Values Memory Globalenvs Events Values.
Require Import Op Registers RTL ZIntervalDomain.
Require Import IntervalpropOp.

Section STRENGTH_REDUCTION.

Variable ge: genv.
Variable sp: block.
Variable ae: AE.t.
Variable e: regset.
Variable m: mem.
Hypothesis MATCH: ematch e ae.

Lemma const_for_result_correct:
  forall a op v,
  const_for_result a = Some op ->
  vmatch v a ->
  exists v', eval_operation ge (Vptr sp Ptrofs.zero) op nil m = Some v' /\ Val.lessdef v v'.
Proof.
  unfold const_for_result. intros.
  destruct (option_val_of_ival a) as [v' | ] eqn:OPTION. 2: discriminate.
  pose proof (option_val_of_ival_sound _ _ H0 _ OPTION).
  subst v'.
  exists v.
  destruct v; try discriminate; inv H; cbn.
  all: split; [reflexivity | apply Val.lessdef_refl].
Qed.

Lemma zero_ext_eq_below:
  forall n x (NONNEG : 0 <=n) (SMALL: n<=Int.zwordsize) (BELOW: Int.unsigned x < 2^n),
    (Int.unsigned (Int.zero_ext n x)) = (Int.unsigned x).
Proof.
  intros.
  pose proof (Int.unsigned_range x) as RANGE.
  pose proof (Zbits.eqmod_Zzero_ext n (Int.unsigned x) NONNEG) as EQMOD.
  assert (0 < 2 ^ n) as MODULUS_POS.
  { apply Z.pow_pos_nonneg. lia. assumption. }
  assert (2 ^ n > 0) as MODULUS_POS' by lia.
  rewrite <- two_p_equiv in MODULUS_POS'.
  pose proof (Zbits.eqmod_mod_eq _ MODULUS_POS' _ _ EQMOD) as ZEXT.
  clear MODULUS_POS'.
  rewrite two_p_equiv in ZEXT.
  rewrite (Zmod_small (Int.unsigned x)) in ZEXT by lia.
  unfold Int.zero_ext.
  pose proof (Zbits.Zzero_ext_range n (Int.unsigned x) NONNEG) as RANGE'.
  rewrite two_p_equiv in RANGE'.
  rewrite Int.unsigned_repr; cycle 1.
  { assert (2^n <= 2^Int.zwordsize) as LE.
    { apply Z.pow_le_mono_r. lia. assumption. }
    unfold Int.max_unsigned, Int.modulus.
    rewrite two_power_nat_two_p. fold Int.zwordsize.
    rewrite two_p_equiv. lia.
  }
  rewrite Zmod_small in ZEXT by lia.
  exact ZEXT.
Qed.

Lemma zero_ext_eq_below64:
  forall n x (NONNEG : 0 <=n) (SMALL: n<=Int64.zwordsize) (BELOW: Int64.unsigned x < 2^n),
    (Int64.unsigned (Int64.zero_ext n x)) = (Int64.unsigned x).
Proof.
  intros.
  pose proof (Int64.unsigned_range x) as RANGE.
  pose proof (Zbits.eqmod_Zzero_ext n (Int64.unsigned x) NONNEG) as EQMOD.
  assert (0 < 2 ^ n) as MODULUS_POS.
  { apply Z.pow_pos_nonneg. lia. assumption. }
  assert (2 ^ n > 0) as MODULUS_POS' by lia.
  rewrite <- two_p_equiv in MODULUS_POS'.
  pose proof (Zbits.eqmod_mod_eq _ MODULUS_POS' _ _ EQMOD) as ZEXT.
  clear MODULUS_POS'.
  rewrite two_p_equiv in ZEXT.
  rewrite (Zmod_small (Int64.unsigned x)) in ZEXT by lia.
  unfold Int64.zero_ext.
  pose proof (Zbits.Zzero_ext_range n (Int64.unsigned x) NONNEG) as RANGE'.
  rewrite two_p_equiv in RANGE'.
  rewrite Int64.unsigned_repr; cycle 1.
  { assert (2^n <= 2^Int64.zwordsize) as LE.
    { apply Z.pow_le_mono_r. lia. assumption. }
    unfold Int64.max_unsigned, Int64.modulus.
    rewrite two_power_nat_two_p. fold Int64.zwordsize.
    rewrite two_p_equiv. lia.
  }
  rewrite Zmod_small in ZEXT by lia.
  exact ZEXT.
Qed.

Lemma op_strength_reduction_correct:
  forall op args vl v,
  vl = map (fun r => AE.get r ae) args ->
  eval_operation ge (Vptr sp Ptrofs.zero) op e##args m = Some v ->
  let (op', args') := op_strength_reduction op args vl in
  exists w, eval_operation ge (Vptr sp Ptrofs.zero) op' e##args' m = Some w /\ Val.lessdef v w.
Proof.
  intros until v. intros VL EVAL.
  unfold op_strength_reduction.
  destruct op_strength_reduction_match.
  
  - cbn in VL. inv VL.
    destruct (_ && _) eqn:ACTIVE.
    + rewrite andb_true_iff in ACTIVE.
      destruct ACTIVE as (NONNEG & nMAX).
      cbn in EVAL. inv EVAL. cbn.
      pose proof (MATCH a1) as MATCHa1.
      destruct (e # a1); cbn.
      all: try (exists Vundef; auto; fail).
      pose proof (int_is_nonnegative_sound _ _ MATCHa1 NONNEG) as RANGE.
      unfold Int.ltu, Int.shru in *.
      change (Int.unsigned Int.iwordsize) with 32 in *.
      change (Int.unsigned (Int.repr 31)) with 31.
      destruct zlt as [LT | GE] in nMAX. 2: discriminate.
      rewrite zero_ext_eq_below by (change Int.zwordsize with 32 in *; lia).
      rewrite zlt_true by lia.
      destruct zlt; cbn.
      * fold (Int.shru i n).
        rewrite Int.nonneg_shrx_is_shru by assumption.
        eexists. split. reflexivity. apply Val.lessdef_refl.
      * eexists. split. reflexivity. apply Val.lessdef_undef.
    + eexists. split. eassumption. constructor.

  - cbn in VL. inv VL.
    destruct (_ && _) eqn:ACTIVE.
    + rewrite andb_true_iff in ACTIVE.
      destruct ACTIVE as (NONNEG & nMAX).
      cbn in EVAL. inv EVAL. cbn.
      pose proof (MATCH a1) as MATCHa1.
      destruct (e # a1); cbn.
      all: try (exists Vundef; auto; fail).
      pose proof (long_is_nonnegative_sound _ _ MATCHa1 NONNEG) as RANGE.
      unfold Int.ltu, Int64.shru' in *.
      change (Int.unsigned Int64.iwordsize') with 64 in *.
      change (Int.unsigned (Int.repr 63)) with 63.
      destruct zlt as [LT | GE] in nMAX. 2: discriminate.
      rewrite zero_ext_eq_below by (change Int.zwordsize with 32; lia).
      rewrite zlt_true by lia.
      destruct zlt; cbn.
      * fold (Int64.shru' i n).
        rewrite Int64.nonneg_shrx'_is_shru' by assumption.
        eexists. split. reflexivity. apply Val.lessdef_refl.
      * eexists. split. reflexivity. apply Val.lessdef_undef.
    + eexists. split. eassumption. constructor.

  - eexists. split. eassumption. constructor.
Qed.

Lemma addr_strength_reduction_correct:
  forall addr args vl res,
  vl = map (fun r => AE.get r ae) args ->
  eval_addressing ge (Vptr sp Ptrofs.zero) addr e##args = Some res ->
  let (addr', args') := addr_strength_reduction addr args vl in
  exists res', eval_addressing ge (Vptr sp Ptrofs.zero) addr' e##args' = Some res' /\ Val.lessdef res res'.
Proof.
  intros until res. intros VL EVAL.
  unfold addr_strength_reduction.
  destruct addr_strength_reduction_match.
  - destruct is_long_zero eqn:ZERO.
    + cbn in VL. inv VL.
      cbn in EVAL. inv EVAL. cbn.
      pose proof (MATCH a1) as MATCHa1.
      pose proof (MATCH a2) as MATCHa2.
      rewrite (is_long_zero_sound _ _ MATCHa2 ZERO).
      destruct (e # a1); cbn.
      all: try (exists Vundef; auto; fail).
      * eexists. split. reflexivity. constructor.
      * destruct Archi.ptr64.
        -- eexists. split. reflexivity. constructor.
        -- eexists. split. reflexivity. constructor.
    + eexists. split. eassumption. constructor.
  - eexists. split. eassumption. constructor.
Qed.

Lemma cond_strength_reduction_correct:
  forall cond args vl b,
    vl = map (fun r => AE.get r ae) args ->
    eval_condition cond e##args m = Some b ->
    let (cond', args') := cond_strength_reduction cond args vl in
    eval_condition cond' e##args' m = Some b.
Proof.
  intros until b. intros VL EVAL. cbn.
  destruct cond; assumption.
Qed.

End STRENGTH_REDUCTION.
