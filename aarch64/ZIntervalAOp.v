(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           David Monniaux     CNRS, VERIMAG                  *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.  *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

Require Import Coqlib Compopts.
Require Import AST Integers Floats Values Memory Globalenvs.
Require Import Op RTL ZIntervalDomain.

Definition eval_static_shift (s: shift) (v: ival) (n: amount32) : ival :=
  match s with
  | Slsl => shl v (intconst n)
  | Slsr => shru v (intconst n)
  | Sasr => Itop (* shr v (intconst n) *)
  | Sror => Itop (* ror v (intconst n) *)
  end.

Lemma eval_static_shift_sound: forall v av s n,
  vmatch v av -> vmatch (eval_shift s v n) (eval_static_shift s av n).
Proof.
  intros. unfold eval_shift, eval_static_shift; destruct s; auto with ival.
Qed.

Definition eval_static_shiftl (s: shift) (v: ival) (n: amount64) : ival :=
  match s with
  | Slsl => shll v (intconst n)
  | Slsr => shrlu v (intconst n)
  | Sasr => Itop (* shrl v (intconst n) *)
  | Sror => Itop (* rorl v (intconst n) *)
  end.

Lemma eval_static_shiftl_sound: forall v av s n,
  vmatch v av -> vmatch (eval_shiftl s v n) (eval_static_shiftl s av n).
Proof.
  intros. unfold eval_shiftl, eval_static_shiftl; destruct s; auto with ival.
Qed.

Definition eval_static_extend (x: extension) (v: ival) (n: amount64) : ival :=
  shll (match x with
        | Xsgn32 => Itop (* TODO *)
        | Xuns32 => longofintu v end) (intconst n).

Lemma eval_static_extend_sound: forall v av x n,
  vmatch v av -> vmatch (eval_extend x v n) (eval_static_extend x av n).
Proof.
  intros. unfold eval_extend, eval_static_extend; destruct x;
  auto with ival.
Qed.

#[global] Hint Resolve eval_static_shift_sound eval_static_shiftl_sound eval_static_extend_sound : ival.

Definition filter_static_condition (cond: condition) (vl: list ival): list ival :=
  match cond, vl with
  | (Ccomp cmp), v1 :: v2 :: nil =>
      let v' := filter_cmp cmp v1 v2 in
      (fst v') :: (snd v') :: nil
  | (Ccompimm cmp imm), v1 :: nil =>
      let v' := filter_cmp cmp v1 (intconst imm) in
      (fst v') :: nil
  | (Ccompu cmp), v1 :: v2 :: nil =>
      let v' := filter_cmpu cmp v1 v2 in
      (fst v') :: (snd v') :: nil
  | (Ccompuimm cmp imm), v1 :: nil =>
      let v' := filter_cmpu cmp v1 (intconst imm) in
      (fst v') :: nil
  | (Ccompl cmp), v1 :: v2 :: nil =>
      let v' := filter_cmpl cmp v1 v2 in
      (fst v') :: (snd v') :: nil
  | (Ccomplimm cmp imm), v1 :: nil =>
      let v' := filter_cmpl cmp v1 (longconst imm) in
      (fst v') :: nil
  | (Ccomplu cmp), v1 :: v2 :: nil =>
      let v' := filter_cmplu cmp v1 v2 in
      (fst v') :: (snd v') :: nil
  | (Ccompluimm cmp imm), v1 :: nil =>
      let v' := filter_cmplu cmp v1 (longconst imm) in
      (fst v') :: nil
  | _, _ => vl
  end.

Definition eval_static_condition 
 (cond: condition) (vl: list ival): ibool :=
  match cond, vl with
  | (Ccomp cmp), v1 :: v2 :: nil => Btop
  | (Ccompimm cmp imm), v1 :: nil => Btop
  | (Ccompu cmp), v1 :: v2 :: nil =>
      cmpu_bool cmp v1 v2
  | (Ccompuimm cmp imm), v1 :: nil =>
      cmpu_bool cmp v1 (intconst imm)
  | (Ccompl cmp), v1 :: v2 :: nil => Btop
  | (Ccomplimm cmp imm), v1 :: nil => Btop
  | (Ccomplu cmp), v1 :: v2 :: nil =>
      cmplu_bool cmp v1 v2
  | (Ccompluimm cmp imm), v1 :: nil =>
      cmplu_bool cmp v1 (longconst imm)
  | _, _ => Btop
  end.

Definition eval_static_operation (op: operation) (vl: list ival): ival :=
  match op, vl with
  | Omove, v1::nil => v1
  | Ocopy, v1::v2::nil => Itop
  | Ocopyimm uid, v1::nil => Itop
  | Ointconst n, nil => intconst n
  | Olongconst n, nil => longconst n
  | Ofloatconst n, nil => Itop
  | Osingleconst n, nil => Itop
  | Oaddrsymbol id ofs, nil => Itop
  | Oaddrstack ofs, nil => Itop

  | Oshift s a, v1::nil => eval_static_shift s v1 a
  | Oadd, v1::v2::nil => add v1 v2
  | Oaddshift s a, v1::v2::nil => add v1 (eval_static_shift s v2 a)
  | Oaddimm n, v1::nil => add v1 (intconst n)
  | Oneg, v1::nil => neg v1
  | Onegshift s a, v1::nil => neg (eval_static_shift s v1 a)
  | Osub, v1::v2::nil => sub v1 v2
  | Osubshift s a, v1::v2::nil =>  sub v1 (eval_static_shift s v2 a)
  | Omul, v1::v2::nil => mul v1 v2
  | Omuladd, v1::v2::v3::nil => add v1 (mul v2 v3)
  | Omulsub, v1::v2::v3::nil => sub v1 (mul v2 v3)
  | Odiv, v1::v2::nil => Itop
  | Odivu, v1::v2::nil => Itop
  | Oand, v1::v2::nil => Itop
  | Oandshift s a, v1::v2::nil => Itop
  | Oandimm n, v1::nil => Itop
  | Oor, v1::v2::nil => Itop
  | Oorshift s a, v1::v2::nil => Itop
  | Oorimm n, v1::nil => Itop
  | Oxor, v1::v2::nil => Itop
  | Oxorshift s a, v1::v2::nil => Itop
  | Oxorimm n, v1::nil => Itop
  | Onot, v1::nil => Itop
  | Onotshift s a, v1::nil => Itop
  | Obic, v1::v2::nil => Itop
  | Obicshift s a, v1::v2::nil => Itop
  | Oorn, v1::v2::nil => Itop
  | Oornshift s a, v1::v2::nil => Itop
  | Oeqv, v1::v2::nil => Itop
  | Oeqvshift s a, v1::v2::nil => Itop
  | Oshl, v1::v2::nil => shl v1 v2
  | Oshr, v1::v2::nil => Itop
  | Oshru, v1::v2::nil => shru v1 v2
  | Oshrximm n, v1::nil => Itop
  | Ozext s, v1::nil => Itop (* TODO *)
  | Osext s, v1::nil => Itop (* TODO *)
  | Oshlzext s a, v1::nil => Itop
  | Oshlsext s a, v1::nil => Itop
  | Ozextshr a s, v1::nil => Itop
  | Osextshr a s, v1::nil => Itop

  | Oshiftl s a, v1::nil => eval_static_shiftl s v1 a
  | Oextend x a, v1::nil => eval_static_extend x v1 a
  | Omakelong, v1::v2::nil => Itop (* TODO *)
  | Olowlong, v1::nil => loword v1
  | Ohighlong, v1::nil => Itop
  | Oaddl, v1::v2::nil => addl v1 v2
  | Oaddlshift s a, v1::v2::nil => addl v1 (eval_static_shiftl s v2 a)
  | Oaddlext x a, v1::v2::nil => addl v1 (eval_static_extend x v2 a)
  | Oaddlimm n, v1::nil => addl v1 (longconst n)
  | Onegl, v1::nil => negl v1
  | Oneglshift s a, v1::nil => negl (eval_static_shiftl s v1 a)
  | Osubl, v1::v2::nil => subl v1 v2
  | Osublshift s a, v1::v2::nil => subl v1 (eval_static_shiftl s v2 a)
  | Osublext x a, v1::v2::nil => subl v1 (eval_static_extend x v2 a)
  | Omull, v1::v2::nil => mull v1 v2
  | Omulladd, v1::v2::v3::nil => addl v1 (mull v2 v3)
  | Omullsub, v1::v2::v3::nil => subl v1 (mull v2 v3)
  | Omullhs, v1::v2::nil => Itop
  | Omullhu, v1::v2::nil => Itop
  | Odivl, v1::v2::nil => Itop
  | Odivlu, v1::v2::nil => Itop
  | Oandl, v1::v2::nil => Itop
  | Oandlshift s a, v1::v2::nil => Itop
  | Oandlimm n, v1::nil => Itop
  | Oorl, v1::v2::nil => Itop
  | Oorlshift s a, v1::v2::nil => Itop
  | Oorlimm n, v1::nil => Itop
  | Oxorl, v1::v2::nil => Itop
  | Oxorlshift s a, v1::v2::nil => Itop
  | Oxorlimm n, v1::nil => Itop
  | Onotl, v1::nil => Itop
  | Onotlshift s a, v1::nil => Itop
  | Obicl, v1::v2::nil => Itop
  | Obiclshift s a, v1::v2::nil => Itop
  | Oornl, v1::v2::nil => Itop
  | Oornlshift s a, v1::v2::nil => Itop
  | Oeqvl, v1::v2::nil => Itop
  | Oeqvlshift s a, v1::v2::nil => Itop
  | Oshll, v1::v2::nil => shll v1 v2
  | Oshrl, v1::v2::nil => Itop
  | Oshrlu, v1::v2::nil => shrlu v1 v2
  | Oshrlximm n, v1::nil => Itop
  | Ozextl s, v1::nil => Itop
  | Osextl s, v1::nil => Itop
  | Oshllzext s a, v1::nil => Itop
  | Oshllsext s a, v1::nil => Itop
  | Ozextshrl a s, v1::nil => Itop
  | Osextshrl a s, v1::nil => Itop

  | Onegf, v1::nil => Itop
  | Oabsf, v1::nil => Itop
  | Oaddf, v1::v2::nil => Itop
  | Osubf, v1::v2::nil => Itop
  | Omulf, v1::v2::nil => Itop
  | Odivf, v1::v2::nil => Itop

  | Onegfs, v1::nil => Itop
  | Oabsfs, v1::nil => Itop
  | Oaddfs, v1::v2::nil => Itop
  | Osubfs, v1::v2::nil => Itop
  | Omulfs, v1::v2::nil => Itop
  | Odivfs, v1::v2::nil => Itop

  | Osingleoffloat, v1::nil => Itop
  | Ofloatofsingle, v1::nil => Itop
  | Ointoffloat, v1::nil => Itop
  | Ointuoffloat, v1::nil => Itop
  | Ofloatofint, v1::nil => Itop
  | Ofloatofintu, v1::nil => Itop
  | Ointofsingle, v1::nil => Itop
  | Ointuofsingle, v1::nil => Itop
  | Osingleofint, v1::nil => Itop
  | Osingleofintu, v1::nil => Itop
  | Olongoffloat, v1::nil => Itop
  | Olonguoffloat, v1::nil => Itop
  | Ofloatoflong, v1::nil => Itop
  | Ofloatoflongu, v1::nil => Itop
  | Olongofsingle, v1::nil => Itop
  | Olonguofsingle, v1::nil => Itop
  | Osingleoflong, v1::nil => Itop
  | Osingleoflongu, v1::nil => Itop

  | Ocmp c, vl => of_optbool (eval_static_condition c vl)
  | Osel c, v1::v2::vl => select (eval_static_condition c vl) v1 v2

  | Obits_of_single, v1::nil => Itop
  | Obits_of_float, v1::nil => Itop
  | Osingle_of_bits, v1::nil => Itop
  | Ofloat_of_bits, v1::nil => Itop

  | Ogetcanary, nil => Itop

  | _, _ => Ibot
  end.

Section SOUNDNESS.
(* Variable bc: block_classification. *)
Variable genF genV: Type.  
Variable ge: Genv.t genF genV.
(* Hypothesis GENV: genv_match bc ge. *)
Variable sp: block.
(* Hypothesis STACK: bc sp = BCstack. *)

Theorem filter_static_condition_sound:
  forall cond vargs m aargs
         (MATCH : list_forall2 vmatch vargs aargs)
         (COND : (eval_condition cond vargs m) = Some true),
     (list_forall2 vmatch vargs (filter_static_condition cond aargs)).
Proof.
  destruct cond; intros; cbn; trivial.
  - destruct aargs ; [assumption | idtac];
    destruct aargs ; [assumption | idtac];
    destruct aargs ; [idtac | assumption].
    inv MATCH. inv H3. inv H5. cbn in COND.
    pose proof (filter_cmp_sound _ _ _ _ _ H2 H4 COND) as SOUND.
    destruct filter_cmp as (a1' & a0').
    destruct SOUND as (SOUND1 & SOUND0).
    repeat (constructor; cbn; trivial).
  - destruct aargs ; [assumption | idtac];
    destruct aargs ; [assumption | idtac];
    destruct aargs ; [idtac | assumption].
    inv MATCH. inv H3. inv H5. cbn in COND.
    pose proof (filter_cmpu_sound _ _ _ _ _ _ H2 H4 COND) as SOUND.
    destruct filter_cmpu as (a1' & a0').
    destruct SOUND as (SOUND1 & SOUND0).
    repeat (constructor; cbn; trivial).
  - destruct aargs ; [assumption | idtac];
    destruct aargs ; [idtac | assumption].
    inv MATCH. inv H3. cbn in COND.
    pose proof (intconst_sound n) as CONST.
    pose proof (filter_cmp_sound _ _ _ _ _ H2 CONST COND) as SOUND.
    destruct filter_cmp as (a1' & a0').
    destruct SOUND as (SOUND1 & SOUND0).
    repeat (constructor; cbn; trivial).
  - destruct aargs ; [assumption | idtac];
    destruct aargs ; [idtac | assumption].
    inv MATCH. inv H3. cbn in COND.
    pose proof (intconst_sound n) as CONST.
    pose proof (filter_cmpu_sound _ _ _ _ _ _ H2 CONST COND) as SOUND.
    destruct filter_cmpu as (a1' & a0').
    destruct SOUND as (SOUND1 & SOUND0).
    repeat (constructor; cbn; trivial).
  - destruct aargs ; [assumption | idtac];
    destruct aargs ; [assumption | idtac];
    destruct aargs ; [idtac | assumption].
    inv MATCH. inv H3. inv H5. cbn in COND.
    pose proof (filter_cmpl_sound _ _ _ _ _ H2 H4 COND) as SOUND.
    destruct filter_cmpl as (a1' & a0').
    destruct SOUND as (SOUND1 & SOUND0).
    repeat (constructor; cbn; trivial).
  - destruct aargs ; [assumption | idtac];
    destruct aargs ; [assumption | idtac];
    destruct aargs ; [idtac | assumption].
    inv MATCH. inv H3. inv H5. cbn in COND.
    pose proof (filter_cmplu_sound _ _ _ _ _ _ H2 H4 COND) as SOUND.
    destruct filter_cmplu as (a1' & a0').
    destruct SOUND as (SOUND1 & SOUND0).
    repeat (constructor; cbn; trivial).
  - destruct aargs ; [assumption | idtac];
    destruct aargs ; [idtac | assumption].
    inv MATCH. inv H3. cbn in COND.
    pose proof (longconst_sound n) as CONST.
    pose proof (filter_cmpl_sound _ _ _ _ _ H2 CONST COND) as SOUND.
    destruct filter_cmpl as (a1' & a0').
    destruct SOUND as (SOUND1 & SOUND0).
    repeat (constructor; cbn; trivial).
  - destruct aargs ; [assumption | idtac];
    destruct aargs ; [idtac | assumption].
    inv MATCH. inv H3. cbn in COND.
    pose proof (longconst_sound n) as CONST.
    pose proof (filter_cmplu_sound _ _ _ _ _ _ H2 CONST COND) as SOUND.
    destruct filter_cmplu as (a1' & a0').
    destruct SOUND as (SOUND1 & SOUND0).
    repeat (constructor; cbn; trivial).
Qed.

Ltac InvHyps :=
  match goal with
  | [H: None = Some _ |- _ ] => discriminate
  | [H: Some _ = Some _ |- _] => inv H
  | [H1: match ?vl with nil => _ | _ :: _ => _ end = Some _ ,
     H2: list_forall2 _ ?vl _ |- _ ] => inv H2; InvHyps
  | _ => idtac
  end.

Theorem eval_static_condition_sound:
  forall cond vargs m aargs,
  list_forall2 vmatch vargs aargs ->
  cmatch (eval_condition cond vargs m) (eval_static_condition cond aargs).
Proof.
  intros until aargs; intros VM. inv VM.
  destruct cond; cbn; try constructor.
  inv H0.
  { destruct cond; cbn; auto with ival. }
  inv H2.
  { destruct cond; cbn; auto with ival. }
  destruct cond; cbn; constructor.
Qed.

Hint Resolve eval_static_condition_sound : ival.

Theorem eval_static_operation_sound:
  forall op vargs m vres aargs,
  eval_operation ge (Vptr sp Ptrofs.zero) op vargs m = Some vres ->
  list_forall2 vmatch vargs aargs ->
  vmatch vres (eval_static_operation op aargs).
Proof.
  unfold eval_operation, eval_static_operation; intros;
    destruct op; InvHyps; eauto with ival.
Qed.
End SOUNDNESS.

Arguments eval_static_operation_sound {genF genV}.
