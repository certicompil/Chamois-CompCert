(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Sylvain Boulmé     Grenoble-INP, VERIMAG          *)
(*           David Monniaux     CNRS, VERIMAG                  *)
(*           Cyril Six          Kalray                         *)
(*                                                             *)
(*  Copyright Kalray. Copyright VERIMAG. All rights reserved.  *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

Require Import AST.
Require Import Values.
Require Import Integers.
Require Import Coq.ZArith.BinIntDef.
Require Import BinNums.

Local Open Scope Z_scope.

Definition zscale_of_chunk (chunk: memory_chunk) : Z :=
  match chunk with
  | Mbool
  | Mint8signed
  | Mint8unsigned => 0
  | Mint16signed 
  | Mint16unsigned => 1
  | Mint32
  | Mfloat32
  | Many32 => 2
  | Mint64 
  | Mfloat64
  | Many64 => 3
  end.
Definition scale_of_chunk chunk := Vint (Int.repr (zscale_of_chunk chunk)).
