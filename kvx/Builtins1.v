(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Sylvain Boulmé     Grenoble-INP, VERIMAG          *)
(*           Xavier Leroy       INRIA Paris-Rocquencourt       *)
(*           David Monniaux     CNRS, VERIMAG                  *)
(*           Cyril Six          Kalray                         *)
(*                                                             *)
(*  Copyright Kalray. Copyright VERIMAG. All rights reserved.  *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

(** Platform-specific built-in functions *)

Require Import String Coqlib.
Require Import AST Integers Floats Values ExtFloats ExtValues.
Require Import Builtins0.

Inductive platform_builtin : Type :=
| BI_fmin
| BI_fmax
| BI_fminf
| BI_fmaxf
| BI_fma
| BI_fmaf
| BI_lround_ne
| BI_luround_ne
| BI_fp_udiv32
| BI_fp_udiv64
| BI_fp_umod32
| BI_fp_umod64
| BI_fp_sdiv32
| BI_fp_sdiv64
| BI_fp_smod32
| BI_fp_smod64
| BI_abs
| BI_absl.

Local Open Scope string_scope.

Definition platform_builtin_table : list (string * platform_builtin) :=
     ("__builtin_fmin", BI_fmin)
  :: ("__builtin_fmax", BI_fmax)
  :: ("__builtin_fminf", BI_fminf)
  :: ("__builtin_fmaxf", BI_fmaxf)
  :: ("__builtin_fma", BI_fma)
  :: ("__builtin_fmaf", BI_fmaf)
  :: ("__builtin_lround_ne", BI_lround_ne)
  :: ("__builtin_luround_ne", BI_luround_ne)
  :: ("__builtin_fp_udiv32", BI_fp_udiv32)
  :: ("__builtin_fp_udiv64", BI_fp_udiv64)
  :: ("__builtin_fp_umod32", BI_fp_umod32)
  :: ("__builtin_fp_umod64", BI_fp_umod64)
  :: ("__builtin_fp_sdiv32", BI_fp_sdiv32)
  :: ("__builtin_fp_sdiv64", BI_fp_sdiv64)
  :: ("__builtin_fp_smod32", BI_fp_smod32)
  :: ("__builtin_fp_smod64", BI_fp_smod64)
  :: ("__builtin_abs", BI_abs)
  :: ("__builtin_absl", BI_absl)
  :: nil.

Local Open Scope asttyp_scope.


Definition platform_builtin_sig (b: platform_builtin) : signature :=
  match b with
  | BI_fmin | BI_fmax => [Xfloat ; Xfloat ---> Xfloat ]
  | BI_fminf | BI_fmaxf => [Xsingle ; Xsingle ---> Xsingle]
  | BI_fma => [Xfloat ; Xfloat ; Xfloat ---> Xfloat]
  | BI_fmaf => [Xsingle ; Xsingle ; Xsingle ---> Xsingle]
  | BI_lround_ne | BI_luround_ne => [Xfloat ---> Xlong ]
  | BI_fp_udiv32
  | BI_fp_sdiv32
  | BI_fp_umod32
  | BI_fp_smod32 => [Xint ; Xint ---> Xint]
  | BI_fp_udiv64
  | BI_fp_sdiv64
  | BI_fp_umod64
  | BI_fp_smod64 => [Xlong ; Xlong ---> Xlong]
  | BI_abs => [Xint ---> Xint]
  | BI_absl => [Xlong ---> Xlong]
  end.

Definition platform_builtin_sem (b: platform_builtin) : builtin_sem (sig_res (platform_builtin_sig b)) :=
  match b with
  | BI_fmin => mkbuiltin_n2t Tfloat Tfloat Xfloat ExtFloat.min
  | BI_fmax => mkbuiltin_n2t Tfloat Tfloat Xfloat ExtFloat.max
  | BI_fminf => mkbuiltin_n2t Tsingle Tsingle Xsingle ExtFloat32.min
  | BI_fmaxf => mkbuiltin_n2t Tsingle Tsingle Xsingle ExtFloat32.max
  | BI_fma => mkbuiltin_n3t Tfloat Tfloat Tfloat Xfloat Float.fma
  | BI_fmaf => mkbuiltin_n3t Tsingle Tsingle Tsingle Xsingle Float32.fma
  | BI_lround_ne => mkbuiltin_n1p Tfloat Xlong Float.to_long_ne
  | BI_luround_ne => mkbuiltin_n1p Tfloat Xlong Float.to_longu_ne
  | BI_fp_udiv32 => mkbuiltin_n2p Tint Tint Xint
       (fun n1 n2 => if Int.eq n2 Int.zero
                     then None
                     else Some (Int.divu n1 n2))
  | BI_fp_udiv64 => mkbuiltin_n2p Tlong Tlong Xlong
       (fun n1 n2 => if Int64.eq n2 Int64.zero
                     then None
                     else Some (Int64.divu n1 n2))
  | BI_fp_umod32 => mkbuiltin_n2p Tint Tint Xint
       (fun n1 n2 => if Int.eq n2 Int.zero
                     then None
                     else Some (Int.modu n1 n2))
  | BI_fp_umod64 => mkbuiltin_n2p Tlong Tlong Xlong
       (fun n1 n2 => if Int64.eq n2 Int64.zero
                     then None
                     else Some (Int64.modu n1 n2))
  | BI_fp_sdiv32 => mkbuiltin_n2p Tint Tint Xint
       (fun n1 n2 => if Int.eq n2 Int.zero
                     then None
                     else Some (Int.divs n1 n2))
  | BI_fp_sdiv64 => mkbuiltin_n2p Tlong Tlong Xlong
       (fun n1 n2 => if Int64.eq n2 Int64.zero
                     then None
                     else Some (Int64.divs n1 n2))
  | BI_fp_smod32 => mkbuiltin_n2p Tint Tint Xint
       (fun n1 n2 => if Int.eq n2 Int.zero
                     then None
                     else Some (Int.mods n1 n2))
  | BI_fp_smod64 => mkbuiltin_n2p Tlong Tlong Xlong
       (fun n1 n2 => if Int64.eq n2 Int64.zero
                     then None
                     else Some (Int64.mods n1 n2))
  | BI_abs => mkbuiltin_n1t Tint Xint ExtValues.int_abs
  | BI_absl => mkbuiltin_n1t Tlong Xlong ExtValues.long_abs
  end.
