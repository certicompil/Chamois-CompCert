#######################################################################
#                                                                     #
#              The Compcert verified compiler                         #
#                                                                     #
#          Xavier Leroy, INRIA Paris-Rocquencourt                     #
#                                                                     #
#  Copyright Institut National de Recherche en Informatique et en     #
#  Automatique.  All rights reserved.  This file is distributed       #
#  under the terms of the GNU Lesser General Public License as        #
#  published by the Free Software Foundation, either version 2.1 of   #
#  the License, or (at your option) any later version.                #
#  This file is also distributed under the terms of the               #
#  INRIA Non-Commercial License Agreement.                            #
#                                                                     #
#######################################################################

# Second-stage Makefile, after Coq extraction

include Makefile.config

#
# Variables from Makefile.config:
# -OCAML_NATIVE_COMP: native-code compilation is supported
# -OCAML_OPT_COMP: can we use the natively-compiled compilers
# -COMPFLAGS: compile options
# -LINK_OPT: additional linker flags for the native binary
# -OCAML_LM_PROF, OCAML_BS_PROF, and OCAML_INSTRUMENT: booleans for
#  landmarks, bisect, and the disjunction between both
#

# Menhir configuration.

include Makefile.menhir

#
# Variables from Makefile.menhir:
# -MENHIR_INCLUDES: additional menhir include paths
# -MENHIR_LIBS: additional menhir libraries
#

# The pre-parser's error message database is compiled as follows.

cparser/pre_parser_messages.ml:
	$(MAKE) -C cparser correct

# Directories containing plain Caml code

DIRS=extraction \
  lib common $(ARCH) scheduling backend cfrontend cparser driver \
  export debug kvx/unittest lib/Impure/ocaml

INCLUDES=$(patsubst %,-I %, $(DIRS))

# Control of warnings

# For hand-written code:
WARNINGS=-w +a \
  -w -fragile-match \
  -w -missing-record-field-pattern \
  -w -disambiguated-name \
  -w -unused-var-strict \
  -w -missing-mli

# For Menhir-generated code: add
cparser/pre_parser.cmx: WARNINGS += -w -ambiguous-name
cparser/pre_parser.cmo: WARNINGS += -w -ambiguous-name

# For Coq-generated code: add
EXTRWARNINGS=$(WARNINGS) \
  -w -ignored-extra-argument \
  -w -unused-value-declaration \
  -w -unused-open \
  -w -unused-type-declaration \
  -w -unused-rec-flag \
  -w -ambiguous-name \
  -w -open-shadow-identifier \
  -w -open-shadow-label-constructor \
  -w -unused-module \
  -w -unused-functor-parameter

# Turn warnings into errors, but not for released tarballs
ifeq ($(wildcard .git),.git)
WARN_ERRORS=-warn-error +a
else
WARN_ERRORS=
endif

COMPFLAGS+=-g -strict-sequence -safe-string \
  $(INCLUDES) -I "$(MENHIR_DIR)" -I +str -I +unix \
  $(WARN_ERRORS) 

# Using .opt compilers if available

ifeq ($(OCAML_OPT_COMP),true)
DOTOPT=.opt
else
DOTOPT=
endif

OCAMLC=ocamlc$(DOTOPT) $(COMPFLAGS)
OCAMLOPT=ocamlopt$(DOTOPT) $(COMPFLAGS)
OCAMLDEP=ocamldep$(DOTOPT) -slash $(INCLUDES)
OCAMLFIND=ocamlfind

LMFLAGS_LINK=-package landmarks -linkpkg
LMFLAGS_COMP=-package landmarks -package landmarks-ppx -ppxopt landmarks-ppx,--auto
BSFLAGS_LINK=-package bisect_ppx -linkpkg
BSFLAGS_COMP=-package bisect_ppx -ppxopt bisect_ppx,"--exclusions ccomp_profiling/bisect/bisect.exclude"
ifeq ($(OCAML_LM_PROF),true)
PPXFLAGS_LINK+=$(LMFLAGS_LINK)
PPXFLAGS_COMP+=$(LMFLAGS_COMP)
endif
ifeq ($(OCAML_BS_PROF),true)
PPXFLAGS_LINK+=$(BSFLAGS_LINK)
PPXFLAGS_COMP+=$(BSFLAGS_COMP)
endif


OCAMLLEX=ocamllex -q
MODORDER=tools/modorder .depend.extr
COPY=cp

PARSERS=cparser/pre_parser.mly
LEXERS=cparser/Lexer.mll lib/Tokenize.mll \
	     lib/Readconfig.mll lib/Responsefile.mll

ifeq ($(OCAML_BS_PROF), true)
LIBS=str.cmxa $(MENHIR_LIBS)
else
LIBS=str.cmxa unix.cmxa $(MENHIR_LIBS)
endif
LIBS_BYTE=$(patsubst %.cmxa,%.cma,$(patsubst %.cmx,%.cmo,$(LIBS)))

EXECUTABLES=ccomp ccomp.byte cchecklink cchecklink.byte clightgen clightgen.byte
GENERATED=$(PARSERS:.mly=.mli) $(PARSERS:.mly=.ml) $(LEXERS:.mll=.ml) cparser/pre_parser_messages.ml

# Beginning of part that assumes .depend.extr already exists

ifeq ($(wildcard .depend.extr),.depend.extr)

CCOMP_OBJS:=$(shell $(MODORDER) driver/Driver.cmx)

ifeq ($(OCAML_NATIVE_COMP),true)
ifeq ($(OCAML_INSTRUMENT), true)
ccomp: $(CCOMP_OBJS)
	@echo "Linking $@ with instrumentation"
	$(OCAMLFIND) opt $(COMPFLAGS) -o $@ $(PPXFLAGS_LINK) $(LIBS) $(LINK_OPT) $+
else
ccomp: $(CCOMP_OBJS)
	@echo "Linking $@"
	@$(OCAMLOPT) -o $@ $(LIBS) $(LINK_OPT) $+
endif
else
ccomp: ccomp.byte
	@echo "Copying to $@"
	@$(COPY) $+ $@
endif

# DM force compilation without checking dependencies
ccomp.force:
	$(OCAMLOPT) -o $@ $(LIBS) $(LINK_OPT) $(CCOMP_OBJS)
	
ifeq ($(OCAML_INSTRUMENT),true)
ccomp.byte: $(CCOMP_OBJS:.cmx=.cmo)
	@echo "Linking $@ with instrumentation"
	@$(OCAMLFIND) ocamlc $(COMPFLAGS) -o $@ $(PPXFLAGS_LINK) $(LIBS_BYTE) $+
else
ccomp.byte: $(CCOMP_OBJS:.cmx=.cmo)
	@echo "Linking $@"
	@$(OCAMLC) -o $@ $(LIBS_BYTE) $+
endif

CLIGHTGEN_OBJS:=$(shell $(MODORDER) export/ExportDriver.cmx)

ifeq ($(OCAML_NATIVE_COMP),true)
ifeq ($(OCAML_INSTRUMENT),true)
clightgen: $(CLIGHTGEN_OBJS)
	@echo "Linking $@ with instrumentation"
	@$(OCAMLFIND) opt $(COMPFLAGS) -o $@ $(PPXFLAGS_LINK) $(LIBS) $(LINK_OPT) $+
else
clightgen: $(CLIGHTGEN_OBJS)
	@echo "Linking $@"
	@$(OCAMLOPT) -o $@ $(LIBS) $(LINK_OPT) $+
endif
else
clightgen: clightgen.byte
	@echo "Copying to $@"
	@$(COPY) $+ $@
endif

clightgen.byte: $(CLIGHTGEN_OBJS:.cmx=.cmo)
	@echo "Linking $@"
	@$(OCAMLC) -o $@ $(LIBS_BYTE) $+

include .depend.extr

endif

# End of part that assumes .depend.extr already exists

# DM: port this
# ifeq ($(OCAML_INSTRUMENT), true)
# %.cmi: %.mli
# 	@echo "OCAMLC $< with instrumentation"
# 	@$(OCAMLFIND) ocamlc $(COMPFLAGS) -c $(PPXFLAGS_COMP) $<
# else
# %.cmi: %.mli
# 	@echo "OCAMLC $<"
# 	@$(OCAMLC) -c $<
# endif
# ifeq ($(OCAML_INSTRUMENT), true)
# %.cmo: %.ml
# 	@echo "OCAMLC $< with instrumentation"
# 	@$(OCAMLFIND) ocamlc $(COMPFLAGS) -c $(PPXFLAGS_COMP) $<
# else
# %.cmo: %.ml
# 	@echo "OCAMLC $<"
# 	@$(OCAMLC) -c $<
# endif
# ifeq ($(OCAML_INSTRUMENT), true)
# %.cmx: %.ml
# 	@echo "OCAMLOPT $< with instrumentation"
# 	@$(OCAMLFIND) opt $(COMPFLAGS) -c $(PPXFLAGS_COMP) $<
# else
# %.cmx: %.ml
# 	@echo "OCAMLOPT $<"
# 	@$(OCAMLOPT) -c $<
# endif

extraction/%.cmi: extraction/%.mli
	@echo "OCAMLC   $<"
	@$(OCAMLC) $(EXTRWARNINGS) -c $<
extraction/%.cmo: extraction/%.ml
	@echo "OCAMLC   $<"
	@$(OCAMLC) $(EXTRWARNINGS) -c $<
extraction/%.cmx: extraction/%.ml
	@echo "OCAMLOPT $<"
	@$(OCAMLOPT) $(EXTRWARNINGS) -c $<

%.cmi: %.mli
	@echo "OCAMLC   $<"
	@$(OCAMLC) $(WARNINGS) -c $<
%.cmo: %.ml
	@echo "OCAMLC   $<"
	@$(OCAMLC) $(WARNINGS) -c $<
%.cmx: %.ml
	@echo "OCAMLOPT $<"
	@$(OCAMLOPT) $(WARNINGS) -c $<

%.ml: %.mll
	$(OCAMLLEX) $<

clean:
	rm -f $(EXECUTABLES)
	rm -f $(GENERATED)
	for d in $(DIRS); do rm -f $$d/*.cm[iotx] $$d/*cmti $$d/*.o; done
	$(MAKE) -C cparser clean

# Generation of .depend.extr

depend: $(GENERATED)
	@echo "Analyzing OCaml dependencies"
	@$(OCAMLDEP) $(foreach d,$(DIRS),$(wildcard $(d)/*.ml)) $(GENERATED) >.depend.extr || { rm -f .depend.extr; exit 2; }
	@$(OCAMLDEP) $(foreach d,$(DIRS),$(wildcard $(d)/*.mli)) $(GENERATED) >>.depend.extr || { rm -f .depend.extr; exit 2; }
