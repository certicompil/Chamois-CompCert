FROM compcert_kvx
USER root
RUN apt-get -y install gcc && apt-get -y remove opam && apt-get -y autoremove
RUN rm -rf /home/appuser/.opam /home/appuser/CompCert
USER appuser
