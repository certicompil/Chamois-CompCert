(* *********************************************************************)
(*                                                                     *)
(*              The Compcert verified compiler                         *)
(*                                                                     *)
(*                 Xavier Leroy, INRIA Paris                           *)
(*                                                                     *)
(*  Copyright Institut National de Recherche en Informatique et en     *)
(*  Automatique.  All rights reserved.  This file is distributed       *)
(*  under the terms of the INRIA Non-Commercial License Agreement.     *)
(*                                                                     *)
(* *********************************************************************)

(** Correctness proof for operator strength reduction. *)

Require Import Coqlib Compopts.
Require Import Integers Floats Values Memory Globalenvs Events.
Require Import Op Registers RTL ZIntervalDomain.
Require Import IntervalpropOp.

Section STRENGTH_REDUCTION.

Variable ge: genv.
Variable sp: block.
Variable ae: AE.t.
Variable e: regset.
Variable m: mem.
Hypothesis MATCH: ematch e ae.

Lemma const_for_result_correct:
  forall a op v,
  const_for_result a = Some op ->
  vmatch v a ->
  exists v', eval_operation ge (Vptr sp Ptrofs.zero) op nil m = Some v' /\ Val.lessdef v v'.
Proof.
  unfold const_for_result. intros.
  destruct (option_val_of_ival a) as [v' | ] eqn:OPTION. 2: discriminate.
  pose proof (option_val_of_ival_sound _ _ H0 _ OPTION).
  subst v'.
  exists v.
  destruct v; try discriminate; inv H; cbn.
  all: split; [try reflexivity | apply Val.lessdef_refl].
  destruct Archi.ptr64. 2: discriminate.
  inv H2. reflexivity.
Qed.

Lemma op_strength_reduction_correct:
  forall op args vl v,
  vl = map (fun r => AE.get r ae) args ->
  eval_operation ge (Vptr sp Ptrofs.zero) op e##args m = Some v ->
  let (op', args') := op_strength_reduction op args vl in
  exists w, eval_operation ge (Vptr sp Ptrofs.zero) op' e##args' m = Some w /\ Val.lessdef v w.
Proof.
  intros until v. intros VL EVAL.
  unfold op_strength_reduction.
  destruct op_strength_reduction_match.
  - cbn in VL. inv VL.
    destruct int_is_nonnegative eqn:NONNEG.
    + cbn in EVAL. inv EVAL. cbn.
      pose proof (MATCH a1) as MATCHa1.
      destruct (e # a1); cbn.
      all: try (exists Vundef; auto; fail).
      cbn. eexists. split. reflexivity.
      pose proof (int_is_nonnegative_sound _ _ MATCHa1 NONNEG) as RANGE.
      unfold Int.ltu.
      change (Int.unsigned Int.iwordsize) with 32.
      change (Int.unsigned (Int.repr 31)) with 31.
      destruct (zlt (Int.unsigned n) 31).
      { rewrite zlt_true by lia.
        rewrite Int.nonneg_shrx_is_shru by assumption.
        cbn. apply Val.lessdef_refl. }
      destruct zlt; cbn; apply Val.lessdef_undef.
    + eexists. split. eassumption. constructor.

  - cbn in VL. inv VL.
    destruct long_is_nonnegative eqn:NONNEG.
    + cbn in EVAL. inv EVAL. cbn.
      pose proof (MATCH a1) as MATCHa1.
      destruct (e # a1); cbn.
      all: try (exists Vundef; auto; fail).
      cbn. eexists. split. reflexivity.
      pose proof (long_is_nonnegative_sound _ _ MATCHa1 NONNEG) as RANGE.
      unfold Int.ltu.
      change (Int.unsigned (Int.repr 63)) with 63.
      change (Int.unsigned Int64.iwordsize') with 64.
      destruct (zlt (Int.unsigned n) 63).
      { rewrite zlt_true by lia.
        rewrite Int64.nonneg_shrx'_is_shru' by assumption.
        cbn. apply Val.lessdef_refl. }
      destruct zlt; cbn; apply Val.lessdef_undef.
    + eexists. split. eassumption. constructor.

  - destruct int_is_nonnegative eqn:NONNEG.
    + cbn in EVAL. inv EVAL. cbn. cbn in VL. inv VL.
      pose proof (MATCH a1) as MATCHa1.
      destruct (e # a1); cbn.
      all: try (exists Vundef; auto; fail).
      pose proof (int_is_nonnegative_sound _ _ MATCHa1 NONNEG) as RANGE.
      eexists. split. reflexivity.
      rewrite Int.nonneg_signed_is_unsigned by assumption.
      apply Val.lessdef_refl.
    + eexists. split. eassumption. constructor.
  - eexists. split. eassumption. constructor.
Qed.

Lemma addr_strength_reduction_correct:
  forall addr args vl res,
  vl = map (fun r => AE.get r ae) args ->
  eval_addressing ge (Vptr sp Ptrofs.zero) addr e##args = Some res ->
  let (addr', args') := addr_strength_reduction addr args vl in
  exists res', eval_addressing ge (Vptr sp Ptrofs.zero) addr' e##args' = Some res' /\ Val.lessdef res res'.
Proof.
  intros until res. intros VL EVAL. rewrite VL.
  unfold addr_strength_reduction.
  destruct addr_strength_reduction_match.
  - eexists. split. eassumption. constructor.
  - eexists. split. eassumption. constructor.
Qed.

Lemma cond_strength_reduction_correct:
  forall cond args vl b,
    vl = map (fun r => AE.get r ae) args ->
    eval_condition cond e##args m = Some b ->
    let (cond', args') := cond_strength_reduction cond args vl in
    eval_condition cond' e##args' m = Some b.
Proof.
  intros until b. intros VL EVAL. cbn.
  unfold cond_strength_reduction.
  destruct cond_strength_reduction_match.
  - cbn in VL. inv VL.
    destruct (_ && _) eqn:ACTIVE.
    + rewrite andb_true_iff in ACTIVE.
      destruct ACTIVE as (ACTIVE1 & ACTIVE2).
      pose proof (MATCH a1) as MATCHa1.
      pose proof (MATCH a2) as MATCHa2.
      cbn. cbn in EVAL. inv EVAL.
      destruct (e # a1); cbn; try reflexivity.
      all: destruct (e # a2); cbn; try reflexivity.
      * pose proof (int_is_nonnegative_sound _ _ MATCHa1 ACTIVE1) as NONNEG1.
        pose proof (int_is_nonnegative_sound _ _ MATCHa2 ACTIVE2) as NONNEG2.
        unfold Int.cmpu, Int.cmp. f_equal. f_equal.
        unfold Int.ltu, Int.lt.
        rewrite Int.nonneg_signed_is_unsigned by assumption.
        rewrite Int.nonneg_signed_is_unsigned by assumption.
        reflexivity.
      * destruct Archi.ptr64. reflexivity.
        rewrite if_same. reflexivity.
      * destruct Archi.ptr64. reflexivity.
        rewrite if_same. reflexivity.
      * destruct Archi.ptr64. reflexivity.
        destruct (eq_block b0 b1); cycle 1.
        { rewrite if_same. reflexivity. }
        destruct (_ && _). 2: reflexivity.
        subst. cbn in H0. discriminate.
    + congruence.
  - congruence.
Qed.

End STRENGTH_REDUCTION.
