(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Benjamin Bonneau   ENS, PSL                       *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

(** Oracle to promote integers into long (RISC-V only) *)

open Maps
open BTL_Invariants
open Registers
open BTL
open BTLtypes
open BTLcommonaux
open IntPromotionCommon
open Printf
open Camlcoq

(* On riscV, the signed promotion is a no-op.
   When possible, we use the signed promotion instead of the unsigned one. *)
let prefer_signed_promotion = true
(* We can even insert freely signed promotions. *)
let free_signed_promotion = true

type pnode =
  | Nparam  of reg
  | Nblock  of RTL.node * reg
  | Ninstr  of int
  | Nresult of int
  | Nnoprom of int

let print_pnode pp = function
  | Nparam r      -> fprintf pp "Nparam %d" (P.to_int r)
  | Nblock (b, r) -> fprintf pp "Nblock %d:%d" (P.to_int b) (P.to_int r)
  | Ninstr  i     -> fprintf pp "Ninstr %d"  i
  | Nresult i     -> fprintf pp "Nresult %d" i
  | Nnoprom i     -> fprintf pp "Nnoprom %d" i

type promotion_sgn =
  | PSany
  | PSsgn of bool

let print_promotion_sgn pp = function
  | PSany       -> fprintf pp "any sgn"
  | PSsgn false -> fprintf pp "usg"
  | PSsgn true  -> fprintf pp "sgn"

type promotion =
  | PRable of (promotion_sgn * bool(*want*))
  | PRconvertible
  | PRimpossible

let print_promotion pp = function
  | PRable (sgn, false) -> fprintf pp "PRable(%a)" print_promotion_sgn sgn
  | PRable (sgn,  true) -> fprintf pp "PRable(%a, want)" print_promotion_sgn sgn
  | PRconvertible -> fprintf pp "PRconvertible"
  | PRimpossible  -> fprintf pp "PRimpossible"

type edgek =
  | Ejoin
  | Esrc of bool (* true iff allow different signs *)

let print_edgek pp = function
  | Ejoin  -> fprintf pp "Ejoin"
  | Esrc b -> fprintf pp "Esrc%s" (if b then "~" else "=")

let print_edge_dir pp (ek, dir) =
  fprintf pp "%a%c" print_edgek ek (if dir then '>' else '<')

type propagate_phase =
  | PHconvertible
  | PHpromotions
  | PHwant

let promotion_rank p =
  match p with
  | PRable _ -> 0 | PRconvertible -> 1 | PRimpossible -> 2

let promotion_join0 (ph : propagate_phase) (ek : edgek) (p0 : promotion) (p1 : promotion) : promotion option =
  match p0, p1 with
  | PRable (sgn0, want0), PRable (sgn1, want1) ->
      let want = match ph with
                 | PHwant -> want0 || want1
                 | _ -> want1                           in
      let res = if ek = Esrc true then PRable (sgn1, want)
                else match sgn0, sgn1 with
                | PSany, sgn | sgn, PSany -> PRable (sgn, want)
                | PSsgn sgn0, PSsgn sgn1 when sgn0 = sgn1 -> PRable (PSsgn sgn0, want)
                | PSsgn _, PSsgn _ -> PRimpossible      in
      if res = p1 then None else Some res
  | _, _ -> None

(* Transfer the promotion status from a node of status [p0] to a node of status [p1] across an edge of kind [ek].
   For termination, the result must be > p1. *)
let promotion_join_edge (ph : propagate_phase) (ek : edgek) (dir : bool)
                        (p0 : promotion) (p1 : promotion) : promotion option =
  if ph = PHconvertible && p0 <> PRconvertible then None
  else match ek, dir, p0, p1 with
  | _,     false, _, PRconvertible -> None
  | Esrc _, true, PRconvertible, _ -> None
        (* If an instruction has a convertible register as source, we keep its promotion status
           since we allow ourselves to insert a cast to promote it. *)
  | Esrc ceq, true, PRimpossible, PRable (sgn, _)
     when free_signed_promotion && (match sgn with PSany -> true | PSsgn sgn -> sgn || ceq) -> None
        (* If we allow ourselves to insert freely signed conversion, we can promote an instruction that has a
           non-promoted register as source. *)
  | _, _, _, _ ->
     let rk0 = promotion_rank p0 in
     let rk1 = promotion_rank p1 in
     if rk0 = rk1 then promotion_join0 ph ek p0 p1
     else if rk0 > rk1 then Some p0 else None

let is_promoted : promotion -> bool option = function
  | PRable (PSany,   true) -> Some true (* signed promotion if unconstrained *)
  | PRable (PSsgn b, true) -> Some b
  | _ -> None

module PromotionGraph : sig
  type t
  val make      : unit -> t
  val add_node  : t -> pnode -> promotion -> unit
  val add_edge  : t -> edgek -> pnode -> pnode -> unit
  val set_state : t -> pnode -> promotion -> unit
  val propagate : propagate_phase -> t -> unit
  val get_state : t -> pnode -> promotion
  val print     : out_channel -> t -> unit
end = struct

  type gnode = {
    mutable nd_state : promotion;
    mutable nd_edges : (edgek * bool * pnode) list;
    mutable nd_done  : bool;
  }

  type t = (pnode, gnode) Hashtbl.t

  let make () = Hashtbl.create 64

  let add_node (g : t) (n : pnode) (p : promotion) =
      assert (Hashtbl.find_opt g n = None);
    Hashtbl.add g n {nd_state = p; nd_edges = []; nd_done = false}

  let add_edge1 g ek dir n0 n1 =
    let nd0 = Hashtbl.find g n0 in
    nd0.nd_edges <- (ek, dir, n1) :: nd0.nd_edges

  let add_edge (g : t) (ek : edgek) (n0 : pnode) (n1 : pnode) =
    add_edge1 g ek true  n0 n1;
    add_edge1 g ek false n1 n0

  let set_state (g : t) (n : pnode) (p : promotion) =
    let nd = Hashtbl.find g n in
    nd.nd_state <- p

  let propagate (ph : propagate_phase) (g : t) =
    let rec aux (wl : gnode list) =
      match wl with
      | [] -> ()
      | nd :: wl ->
         if nd.nd_done then aux wl
         else begin
           nd.nd_done <- true;
           aux (List.fold_left (fun wl (ek, dir, n') ->
             let nd' = Hashtbl.find g n' in
             match promotion_join_edge ph ek dir nd.nd_state nd'.nd_state with
             | None    -> wl
             | Some p' -> nd'.nd_done  <- false;
                          nd'.nd_state <- p';
                          nd' :: wl
             ) wl nd.nd_edges)
         end
    in
    aux (Hashtbl.fold (fun _ nd wl -> nd.nd_done <- false; nd :: wl) g [])

  let get_state (g : t) (n : pnode) =
    (Hashtbl.find g n).nd_state
  
  let print (pp : out_channel) : t -> unit =
    Hashtbl.iter (fun n nd ->
      fprintf pp "%a [%a] :" print_pnode n print_promotion nd.nd_state;
      List.iter (fun (ek, dir, n') -> fprintf pp " %a%a;" print_edge_dir (ek, dir) print_pnode n') nd.nd_edges;
      fprintf pp "\n")

end

(** Step 1 : building the promotion graph *)

let op_want_prom = function
  | Op.Ocast32signed | Op.Ocast32unsigned -> true
  | _ -> false

let op_promotion_sgn : inst_promotion_info -> (promotion_sgn * op_promotion_ceq) option = function
  | IPromotableOp prom ->
      begin match prom.op_prom0.op_prom_usg, prom.op_prom0.op_prom_sgn with
      | Some _, Some _ -> Some (PSany, prom)
      | Some _, None   -> Some (PSsgn false, prom)
      | None,   Some _ -> Some (PSsgn true, prom)
      | _ -> None
      end
  | _ -> None

let cond_promotion_sgn : inst_promotion_info -> (promotion_sgn * cond_promotion_ceq) option = function
  | IPromotableCond prom ->
      begin match prom.cond_prom0.cond_prom_usg, prom.cond_prom0.cond_prom_sgn with
      | Some _, Some _ -> Some (PSany, prom)
      | Some _, None   -> Some (PSsgn false, prom)
      | None,   Some _ -> Some (PSsgn true, prom)
      | _ -> None
      end
  | _ -> None

let extract_promotion (g : PromotionGraph.t) (n : pnode)
  : (promotion_sgn * 'a) option -> (bool * 'a) option = function
  | Some (_, prom) ->
      begin match is_promoted (PromotionGraph.get_state g n) with
      | Some sgn -> Some (sgn, prom)
      | None     -> None
      end
  | None -> None

let partition_snd rbs = List.partition_map Either.(fun (r, b) -> if b then Left r else Right r) rbs

let build_graph (btl : BTL.code) (entry : RTL.node) (params : reg list) : PromotionGraph.t =
begin

let g : PromotionGraph.t = PromotionGraph.make () in

let init_iblock_graph (ibnum : RTL.node) (ib : BTL.iblock_info) : pnode PTree.t =
  Regset.fold
    (fun r rs ->
       let n = Nblock (ibnum, r) in
       PromotionGraph.add_node g n (PRable (PSany, false));
       PTree.set r n rs)
    ib.binfo.input_regs PTree.empty
in
let join_iblock_entry (ibnum : RTL.node) (rs : pnode PTree.t) : unit =
  let ib = match PTree.get ibnum btl with
           | None -> failwith "unknow iblock"
           | Some ib -> ib in
  Regset.fold
    (fun r () ->
       let n1 = Nblock (ibnum, r) in
       match PTree.get r rs with
       | Some n0 -> PromotionGraph.add_edge g Ejoin n0 n1
       | None    -> PromotionGraph.set_state g n1 PRimpossible
    ) ib.binfo.input_regs ()
in
let make_instr_node (src_regs : pnode PTree.t) (dst_regs : pnode PTree.t)
                    (n : pnode) (p : promotion) (src : (reg * bool) list) (dst : reg list)
  : pnode PTree.t =
  PromotionGraph.add_node g n p;
  List.iter (fun (r, ceq) ->
    let n0 = PTree.get r src_regs in
    match n0 with
    | None    -> failwith "get src, should be live"
    | Some n0 -> PromotionGraph.add_edge g (Esrc ceq) n0 n) src;
  List.fold_left (fun regs r -> PTree.set r n regs) dst_regs dst
in
let make_noprom_node regs instr args =
  make_instr_node regs regs (Nnoprom instr) PRimpossible (List.map (fun r -> (r, false)) args)
in
let rec build_iblock_graph (regs : pnode PTree.t) (ib : BTL.iblock) : pnode PTree.t option =
  match ib with
  | BF (_, iinfo) ->
      let (src, dst) = instr_all_deps ib in
      let regs = make_noprom_node regs iinfo.iinfo_shadow.inumb src dst in
      List.iter (fun ibnum -> join_iblock_entry ibnum regs) (successors_instr_btl ib);
      None
  | Bnop _ -> Some regs
  | Bop (op, args, dst, iinfo) ->
      let instr = iinfo.iinfo_shadow.inumb in
      begin match op_promotion_sgn iinfo.inst_promotion with
      | Some (sgn, prom) ->
           let argsb = List.combine (List.combine args prom.op_prom_args_eq) prom.op_prom0.op_prom_args in
           let resb  = [dst, prom.op_prom0.op_prom_res] in
           let args_p, args_n = partition_snd argsb in
           let res_p,  res_n  = partition_snd resb  in
           let   _   = make_instr_node regs regs (Ninstr instr) (PRable (sgn, op_want_prom op)) args_p [] in
           let regs1 = make_instr_node regs regs (Nresult instr) (PRable (PSany, false)) [] res_p in
           PromotionGraph.add_edge g (Esrc prom.op_prom_res_eq) (Ninstr instr) (Nresult instr);
           Some (make_instr_node regs regs1 (Nnoprom instr) PRimpossible args_n res_n)
      | None -> Some (make_noprom_node regs instr args [dst])
      end
  | Bload (_, _, _, _, _, iinfo) | Bstore (_, _, _, _, iinfo) ->
      let (src, dst) = instr_all_deps ib in
      Some (make_noprom_node regs iinfo.iinfo_shadow.inumb src dst)
  | Bseq (ib0, ib1) ->
      begin match build_iblock_graph regs ib0 with
      | Some regs1 -> build_iblock_graph regs1 ib1
      | None -> None (* ib1 is unreachable *)
      end
  | Bcond (cond, args, ib0, ib1, iinfo) ->
      let _(*=regs*) =
        match cond_promotion_sgn iinfo.inst_promotion with
        | Some (sgn, prom) ->
            make_instr_node regs regs (Ninstr iinfo.iinfo_shadow.inumb) (PRable (sgn, false))
              (List.combine args prom.cond_prom_args_eq) []
        | None ->
            make_noprom_node regs iinfo.iinfo_shadow.inumb args []
      in
      match build_iblock_graph regs ib0, build_iblock_graph regs ib1 with
      | None, regs | regs, None -> regs
      | Some _, Some _ -> failwith "BTL Bcond shape"
in
  (* initialisation *)
  let ib_regs = List.map (fun (ibnum, ib) -> (ib.entry, init_iblock_graph ibnum ib)) (PTree.elements btl) in
  (* parameters *)
  let param_regs = List.fold_left (fun regs r ->
      let n = Nparam r in
      (* This will fail if a register is used twice as parameter. *)
      PromotionGraph.add_node g n PRconvertible;
      PTree.set r n regs) PTree.empty params
  in join_iblock_entry entry param_regs;
  (* iblocks *)
  List.iter (fun (ib, regs) -> let _ = build_iblock_graph regs ib in ()) ib_regs;
  g
end (* build_graph *)

(** Step 2 : building the target program *)

let promote_btl (g : PromotionGraph.t) (btl : BTL.code) (params : reg list) : BTL.code * gluemap =
begin
(* Since for each register, we keep its original value xor its promoted value,
   it seems that we could always reuse the same register in the target program.
   However, this generates programs that are rejected by the type-checker when a register is promoted
   only in a fragment of the program.
   Instead, we use fresh registers [prom_reg] to store the promoted values. *)
let reg_shift = btl_max_reg btl params in
let prom_reg (r : reg) : reg = P.of_int (P.to_int r + reg_shift) in
let opcast (sgn : bool) = if sgn then Op.Ocast32signed else Op.Ocast32unsigned in
let bopcast (sgn : bool) (ceq : bool) r =
  Bop (opcast (sgn || (prefer_signed_promotion && ceq)), [r], prom_reg r, def_iinfo ()) in

let init_iblock_regs (ibnum : RTL.node) (ib : BTL.iblock_info) : bool PTree.t * invariants =
  let regs, inv = Regset.fold
    (fun r (regs, inv) ->
       let n    = Nblock (ibnum, r) in
       let prom = is_promoted (PromotionGraph.get_state g n) in
       (PTree.set r (prom <> None) regs,
        match prom with
        | Some sgn -> { aseq    = (prom_reg r, Iop (Rop (opcast sgn), [BTL_Invariants.input r])) :: inv.aseq;
                        outputs = Regset.add (prom_reg r) inv.outputs }
        | None     -> { aseq    = inv.aseq;
                        outputs =  Regset.add r inv.outputs }))
    ib.binfo.input_regs (PTree.empty, {aseq = []; outputs = Regset.empty})
  in (regs, {history = csi_empty; glue = inv; meminv = [] })
in
let rec promote_iblock (regs : bool PTree.t) (ib : BTL.iblock) : (bool PTree.t option * BTL.iblock) =
  match ib with
  | BF _ -> (None, ib)
  | Bnop _ -> (Some regs, ib)
  | Bop (op, args, dst, iinfo) ->
      let inb = iinfo.iinfo_shadow.inumb in
      let pres, instr = 
        match extract_promotion g (Ninstr inb) (op_promotion_sgn iinfo.inst_promotion) with
        | Some (sgn, prom) ->
            let op' = match op_prom_sgb sgn prom.op_prom0 with
                      | Some op' -> op'
                      | None -> failwith "bad promotion" in
            let argsb = List.combine (List.combine args prom.op_prom_args_eq) prom.op_prom0.op_prom_args in
            let pres  = prom.op_prom0.op_prom_res in
            let (args', casts) = List.fold_right (fun ((r, ceq), b) (args', casts) ->
                     if not b
                     then (r :: args', casts)
                     else if Option.get (PTree.get r regs)
                     then (prom_reg r :: args', casts)
                     else (prom_reg r :: args', (r, ceq) :: casts))
                   argsb ([], []) in
            let ret_sgn = if pres
                          then match is_promoted (PromotionGraph.get_state g (Nresult inb)) with
                               | Some sgn -> sgn
                               | None     -> failwith "result not promoted"
                          else sgn in
            (pres,
             List.fold_right (fun (r, ceq) rhs -> Bseq (bopcast sgn ceq r, rhs)) casts
               (Bop (op', args', (if pres then prom_reg dst else dst),
                     set_inst_promotion (IPromotedOp (op, prom, sgn, ret_sgn)) iinfo)))
        | None ->
            (false, Bop (op, args, dst, set_inst_promotion IPromNone iinfo))
      in
      let pres, instr =
        if pres then true, instr
        else match op_promotion_sgn iinfo.inst_promotion with
             | Some (_, prom) when prom.op_prom0.op_prom_res ->
                 begin match is_promoted (PromotionGraph.get_state g (Nresult inb)) with
                 | Some sgn ->
                     (* Case where the instruction is not promoted but its result is (free signed promotion) *)
                     true, Bseq (instr, bopcast sgn false dst)
                 | None -> false, instr
                 end
             | _ -> false, instr
      in (Some (PTree.set dst pres regs), instr)
  | Bload (_, _, _, _, dst, _) | Bstore (_, _, _, dst, _) ->
      (Some (PTree.set dst false regs), ib)
  | Bseq (ib0, ib1) ->
      begin match promote_iblock regs ib0 with
      | (Some regs1, ib0') -> let (regs2, ib1') = promote_iblock regs1 ib1 in
                              (regs2, Bseq (ib0', ib1'))
      | (None, ib0') -> (None, Bseq (ib0', ib1))
      end
  | Bcond (cond, args, ib0, ib1, iinfo) ->
      let regs0', ib0' = promote_iblock regs ib0 in
      let regs1', ib1' = promote_iblock regs ib1 in
      let regs' = match regs0', regs1' with
                  | None, regs | regs, None -> regs
                  | Some _, Some _ -> failwith "BTL Bcond shape"
      in
      (regs',
      begin match extract_promotion g (Ninstr iinfo.iinfo_shadow.inumb)
                                      (cond_promotion_sgn iinfo.inst_promotion) with
      | Some (sgn, prom) ->
          let cond' = match cond_prom_sgb sgn prom.cond_prom0 with
                    | Some op' -> op'
                    | None -> failwith "bad promotion" in
          let (args', casts) = List.fold_right (fun (r, ceq) (args', casts) ->
                  if Option.get (PTree.get r regs)
                  then (prom_reg r :: args', casts)
                  else (prom_reg r :: args', (r, ceq) :: casts))
                (List.combine args prom.cond_prom_args_eq) ([], []) in
         List.fold_right (fun (r, ceq) rhs -> Bseq (bopcast sgn ceq r, rhs)) casts
           (Bcond (cond', args', ib0', ib1',
                   set_inst_promotion (IPromotedCond (cond, prom, sgn)) iinfo))
      | None ->
          Bcond (cond, args, ib0', ib1', set_inst_promotion IPromNone iinfo)
      end)
in
  let res = PTree.map (fun ibnum ib ->
                let (regs, inv) = init_iblock_regs ibnum ib in
                let (_, ib') = promote_iblock regs ib.entry in
                ({ib with entry = flatten_iblock ib'}, inv))
              btl
  in (PTree.map (fun _ -> fst) res, PTree.map (fun _ -> snd) res)
end (* promote_btl *)

let int_promotion_oracle (btl : BTL.code) (entry : RTL.node) (params : reg list)
  : BTL.code * gluemap =
  let need_dce = BTL_Liveness.update_liveness_btl btl in
  let btl = if need_dce then BTL_Liveness.btl_simple_dead_code_elimination btl else btl in
  BTL_Renumber.recompute_inumbs btl entry;
  let g = build_graph btl entry params in
  PromotionGraph.propagate PHconvertible g;
  PromotionGraph.propagate PHpromotions  g;
  PromotionGraph.propagate PHwant        g;
  let (btl', gm) = promote_btl g btl params in
  (btl', gm)
