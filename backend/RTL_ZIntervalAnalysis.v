(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           David Monniaux     CNRS, VERIMAG                  *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

Require Import Coqlib.
Require Import RTL AnalysisDomain RTL_AnalysisLib ZIntervalAnalysis ZIntervalDomain ZIntervalAOp Maps.
Require Import Op Registers Integers Values Memory IntPromotionCommon IntPromotion.
Require Liveness.

Module AAD := ZIntervalAADomain RTL_IRspec.
Module AA  := RTL_AbstractAnalysis AAD.
(*
Definition relax_ae kills ae :=
  match kills with
  | None => ae
  | Some regs => eforget regs ae
  end.


Lemma relax_ae_ge :
  forall kills ae va
         (GE : AA.VA.ge va (Some (relax_ae kills ae))),
    AA.VA.ge va (Some ae).
Proof.
  unfold relax_ae. intros.
  destruct kills. 2: assumption.
  eapply AA.VA.ge_trans. exact GE.
  apply eforget_ge.
Qed.

Definition relax_next kills (va : node * AA.VA.t) :=
  ((fst va),
           match snd va with
           | None => None
           | Some ae' => Some (relax_ae kills ae')
           end).

Definition transfer' (pi : AA.prog_info) (f: function) (lastuses: PTree.t (list reg))
                     (pc: node) (before: AA.VA.t) : list (node * AA.VA.t) :=
  List.map (relax_next (PTree.get pc lastuses)) (AA.transfer pi f pc before).

Definition widen_node (f : function) (n : node) := true.

Definition analyze (f: function): PMap.t AA.VA.t :=
  let last_uses := Liveness.last_uses f in
  match AA.DS.solution_opt (widen_node f) (transfer' tt f last_uses) ((f.(fn_entrypoint), AA.entry_state f) :: nil) with
  | Some solution => solution
  | None => PMap.init AA.default_top
  end.

Lemma analyze_entrypoint:
  forall f vl,
  exists ae,
     (analyze f)!!(fn_entrypoint f) = Some ae
  /\ ematch (init_regs vl (fn_params f)) ae.
Proof.
  intros.
  unfold analyze.
  set (lu := Liveness.last_uses f).
  destruct AA.DS.solution_opt as [res | ] eqn: SOLUTION.
  set (entry := Some (einit_regs f.(fn_params))).
  - assert (A: AA.VA.ge res!!(fn_entrypoint f) entry).
    { apply (AA.DS.solution_includes_initial _ _ _ _ _ _ SOLUTION).
      constructor. reflexivity.
    }
    destruct (res!!(fn_entrypoint f)) as [ae | ]; simpl in A. 2:contradiction.
    exists ae.
    split. reflexivity.
    eapply ematch_ge; eauto. apply ematch_init; auto.
    intros. constructor.
  - exists AE.top.
    split. apply PMap.gi.
    apply ematch_ge with (einit_regs (fn_params f)).
    + apply ematch_init; auto.
      intros. constructor.
    + apply AE.ge_top.
Qed.

Lemma analyze_successor:
  forall f n ae s ae'
  (CURRENT : (analyze f)!!n = ae),
  In (s, Some ae') (AA.transfer tt f n ae) ->
  AA.VA.ge (analyze f)!!s (Some ae').
Proof.
  unfold analyze; intros.
  set (entry := Some (einit_regs f.(fn_params))) in *.
  destruct AA.DS.solution_opt as [res | ] eqn:SOLUTION.
  - assert (AA.DS.is_stable (transfer' tt f (Liveness.last_uses f)) res) as STABLE.
    { eapply (AA.DS.solution_stable _ _ _ _ _ SOLUTION).
      Unshelve. reflexivity. }
    unfold AA.DS.is_stable in STABLE.
    apply relax_ae_ge with (kills := (Liveness.last_uses f) ! n). 
    apply STABLE with (n := n).
    rewrite CURRENT.
    unfold transfer'.
    change (s, (Some (relax_ae (Liveness.last_uses f) ! n ae'))) with
      (relax_next ((Liveness.last_uses f) ! n) (s, (Some ae'))).
    apply in_map. assumption.
  - rewrite PMap.gi. constructor.
Qed.

Lemma analyze_succ:
  forall e f n ae s ae'
  (ANALYZE : (analyze f)!!n = ae)
  (IN : In (s, (Some ae')) (AA.transfer tt f n ae)),
  ematch e ae' ->
  exists ae'',
     (analyze f)!!s = Some ae''
  /\ ematch e ae''.
Proof.
  intros.
  pose proof (analyze_successor _ _ _ _ _ ANALYZE IN) as GE.
  destruct ((analyze f) # s) as [ae'' | ].
  - exists ae''. split. reflexivity.
    eapply ematch_ge; eauto.
  - destruct GE.
Qed.
*)
