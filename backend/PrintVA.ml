(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           David Monniaux     CNRS, VERIMAG                  *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

open ValueDomain
open Printf
open Maps
open Camlcoq
open RTL
open BinPos
open Datatypes

let print_ident oc id =
  output_string oc (extern_atom id);;

let print_aptr oc ap =
  match ap with
  | Pbot -> output_string oc "$bot"
  | Gl(id, ofs) ->
     fprintf oc "%s[%Ld]" (PrintAST.name_of_qualident id) (camlint64_of_ptrofs ofs)
  | Glo(id) ->
     fprintf oc "%s[?]" (PrintAST.name_of_qualident id)
  | Glob ->
     output_string oc "$glob"
  | Stk(ofs) ->
     fprintf oc "$stack[%Ld]" (camlint64_of_ptrofs ofs)
  | Stack ->
     output_string oc "$stack"
  | Nonstack ->
     output_string oc "$nonstack"
  | Ptop ->
     output_string oc "$top";;

let print_aval oc av =
  match av with
  | Vbot -> output_string oc "$bot"
  | I x -> fprintf oc "I%ld" (camlint_of_coqint x)
  | IU x -> fprintf oc "IU%ld" (camlint_of_coqint x)
  | L x -> fprintf oc "L%Ld" (camlint64_of_coqint x)
  | F x -> fprintf oc "F%f" (camlfloat_of_coqfloat x)
  | FS x -> fprintf oc "FS%f" (camlfloat_of_coqfloat32 x)
  | Num p -> fprintf oc "Num(%a)" print_aptr p
  | Ptr p -> fprintf oc "Ptr(%a)" print_aptr p
  | Ifptr p -> fprintf oc "Ifptr(%a)" print_aptr p
  | Uns(p, n) -> fprintf oc "Uns(%a, %d)" print_aptr p (Z.to_int n)
  | Sgn(p, n) -> fprintf oc "Sgn(%a, %d)" print_aptr p (Z.to_int n);;

let print_list oc opening separator closing pp l =
  output_string oc opening;
  let is_first = ref true in
  List.iter (fun item ->
      (if not !is_first then output_string oc separator);
      is_first := false;
      pp item) l;
  output_string oc closing;;

let print_aenv oc ae =
  match ae with
  | AE.Bot -> output_string oc "eBOT"
  | AE.Top_except tr ->
     let items =
       PTree.elements tr |>
       List.map (fun (n, v) -> ((P.to_int n), v)) |>
       List.sort (fun (n1, _) (n2, _) -> n1 - n2) in
     print_list oc "{ " "; " " }"
       (fun (n, av) ->
         fprintf oc "v%d: %a"n print_aval av) items;;

let print_rm oc rm =
   let items =
       PTree.elements rm |>
       List.map (fun (n, v) -> ((P.to_int n), v)) |>
       List.sort (fun (n1, _) (n2, _) -> n1 - n2) in
     print_list oc "{ " "; " " }"
       (fun (n, av) ->
         match av with
         | Some _ -> fprintf oc "v%d: Some" n
         | None -> fprintf oc "v%d: None"n) items;;
        
let print_va oc va =
  match va with
  | VA.State(ae, am) -> print_aenv oc ae
  | VA.Bot -> output_string oc "BOT" ;;

let print_va_fn oc f va =
  let pcs = List.sort
              (fun x y ->
                match Pos.compare x y with
                | Lt -> 1
                | Eq -> 0
                | Gt -> -1)
              (List.map fst (PTree.elements f.fn_code)) in
  List.iter (fun pc -> Printf.fprintf oc "%d: %a\n" (Camlcoq.P.to_int pc)
            print_va  (PMap.get pc va)) pcs;;
