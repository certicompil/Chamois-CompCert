(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           David Monniaux     CNRS, VERIMAG                  *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.  *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

open ZIntervalDomain
open Camlcoq
open Maps

let print_interval oc (itv : Interval.t) =
  let low = Z.to_int64 itv.Interval.itv_lo
  and high = Z.to_int64 itv.Interval.itv_hi in
  if low=high then
    Printf.fprintf oc "%Lu" low
  else
    Printf.fprintf oc "[%Lu, %Lu]" low high;;

let print_ifundef oc b =
  if b then output_string oc " +undef";;

let print_ival oc (x : ival) =
  match x with
  | Iint(b, i) -> Printf.fprintf oc "I%a%a" print_interval i print_ifundef b
  | Ilong(b, l) -> Printf.fprintf oc "L%a%a" print_interval l print_ifundef b
  | Itop -> output_string oc "T"
  | Ibot -> output_string oc "B"
  | Iundef -> output_string oc "undef";;

let print_ae oc (ae : AE.t) =
  match AE.elements ae with
  | None -> Printf.fprintf oc "Bot"
  | Some ae' ->
     let ae'' =
       ae' |> Array.of_list |>
         (Array.map (fun (k, v) -> (P.to_int64 k), v)) in
     Array.sort (fun (k, v) (k', v') -> Int64.compare k k') ae'';
     Array.iteri (fun i (k, v) ->
         (if i>0 then output_string oc "; ");
         Printf.fprintf oc "x%Ld=%a" k print_ival v) ae'';;

let print_ae_bot oc (x : Interval_Analysis.t) =
  match x with
  | None -> Printf.fprintf oc "Unreach"
  | Some ae -> print_ae oc ae;;

let print_analysis_results oc (control_points : P.t list)
      (pmap : Interval_Analysis.t PMap.t) =
  let control_points' = Array.of_list control_points |>
    Array.map P.to_int64 in
  Array.sort Int64.compare control_points';
  Array.iter (fun k ->
      Printf.fprintf oc "P%Ld: %a\n" k print_ae_bot
        (PMap.get (P.of_int64 k) pmap)) control_points';;

