open Camlcoq
open LTL
open Maps

let spill_analysis f =
  let is_getstack = function
    | LTL.Lgetstack(_, _, _, _) -> true
    | _ -> false in
  let rec count_getstack = function
    | [] -> 0
    | i :: il -> (if is_getstack i then 1 else 0) + (count_getstack il) in
  let is_setstack = function
    | LTL.Lsetstack(_, _, _, _) -> true
    | _ -> false in
  let rec count_setstack = function
    | [] -> 0
    | i :: il -> (if is_setstack i then 1 else 0) + (count_setstack il) in
  let instrs =
    List.sort
      (fun (pc1, _) (pc2, _) -> compare pc2 pc1)
      (List.rev_map
        (fun (pc, i) -> (P.to_int pc, i))
        (PTree.elements f.fn_code)) in
  let get_cnt = List.fold_left (fun cnt (_, blk) ->
    cnt + (count_getstack blk)) 0 instrs in
  let set_cnt = List.fold_left (fun cnt (_, blk) ->
    cnt + (count_setstack blk)) 0 instrs in
  (get_cnt, set_cnt)
