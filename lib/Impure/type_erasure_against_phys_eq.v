(** Two Rocq values that are provably distinct are extracted on the same value in the well-typed extracted OCaml code 

This file provides an example that two logically distinct values can be extracted on a single OCaml value,
because of type erasure. Therefore, this makes incorrect Jourdan's Physical Equality on any [A:Type], as axiomatized in:

  https://hal.science/tel-01327023v1/preview/thesis.pdf#page=194

The below example is inspired by the [any] type of Clément Allain (https://jfla.inria.fr/static/slides/jfla2025-Allain.pdf#page=17)
which illustrates this consequence of type erasure on GADT within the OCaml compiler itself.

In order to "run" this example, just do:
 - coqc type_erasure_against_phys_eq.v
 - ocaml type_erasure_against_phys_eq.ml
NB: tested with coq 8.19.1 and ocaml 4.14.1

See a workaround in Impure axioms for [phys_eq].
*)

Require Import Extraction.
Require Import ExtrOcamlBasic.
Require Import Program.

Definition is_single (A: Type): Prop := forall (x y:A), x=y.

Lemma is_single_unit: is_single unit.
Proof.
  unfold is_single. destruct x, y. auto.
Qed.

Lemma not_single_bool: ~(is_single bool).
Proof.
  unfold is_single. intros H. generalize (H false true). congruence.
Qed.

Record any: Type := Any { type: Set }.

Definition b2a (b: bool): any := if b then Any bool else Any unit.

Lemma b2a_diff: b2a true <> b2a false.
Proof.
  simpl. intros H.
  apply not_single_bool.
  change (is_single (type (Any bool))).
  rewrite H.
  apply is_single_unit.
Qed.

Axiom jourdan_phys_eq: forall {A} {B} (x y: A)
  (fast_path: {_:unit | x=y } -> B)
  (slow_path: unit -> B),
  (forall (Heq: x=y), fast_path (exist _ () Heq) = slow_path ()) -> B.
Extract Constant jourdan_phys_eq => "(fun x y fast_path slow_path -> if x==y then fast_path () else slow_path ())".

Axiom jourdan_phys_eq_correct: forall {A} {B} (x y: A)
  (fast_path: {_:unit | x=y } -> B)
  (slow_path: unit -> B)
  (Hdet: forall (Heq: x=y), fast_path (exist _ () Heq) = slow_path ()),
  jourdan_phys_eq x y fast_path slow_path Hdet = slow_path tt.

Definition b2a_test: bool := jourdan_phys_eq (b2a true) (b2a false) (fun _ => true) (fun _ => false) 
  (fun Heq => match b2a_diff Heq return true = false with end).

Theorem b2a_test_is_false: b2a_test = false.
Proof.
  apply jourdan_phys_eq_correct.
Qed.

(** Here we observe that at extraction [b2a_test=true] on the contrary to the theorem proved above *)
Axiom check_false: bool -> bool.
Extract Constant check_false => "(fun b -> assert (not b); b)".

Definition r := check_false b2a_test.

Extraction "type_erasure_against_phys_eq.ml" r.
