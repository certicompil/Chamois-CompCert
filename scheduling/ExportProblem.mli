open InstructionSchedulerTypes

val id : int ref
val adv_id : unit -> int
val reset_id : unit -> unit
val to_DOT : string -> problem -> unit
val to_json : string -> problem -> int array -> unit

val to_solution : int array -> solution
val check_schedule : problem -> solution -> unit