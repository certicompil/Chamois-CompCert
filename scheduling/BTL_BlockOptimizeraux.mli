(** An instruction's register usage type:
    [Def]    - The register that is defined in the instruction
    [Arg]    - The register used as an argument in the instruction
    [ExtArg] - The register is also an argument, but comes from pointed block's
               live variables. Can be seen as an external [Arg] *)
type mentions_element = Def | Arg | ExtArg;;

(** information about a register
    [multiplicity]  - multiplicity of the register type accoring to [multiplicity_of_type]
    [nb_read]       - number of time the register is referenced as an argument
    [rg_class]      - class of the register accorfing to [typing]
*)
type register_info = {
  multiplicity : int; (**)
  nb_read : int;
  rg_class : int;
};;

(** Returns a hashtable with bindings of shape [(r,info], where [r] is a
    pseudo-register (Registers.reg) and [info] are information about the pseudo-register ;
    and an array containing the list of regs referenced by each
    instruction, with a [mentions_element] to know whether it's as [Arg],
    [Dest] or [Live] *)
val reference_counting : (BTL.iblock * Registers.Regset.t) array -> BTL.iblock option -> Registers.Regset.t -> 
                         RTLtyping.regenv ->
                         ((Registers.reg, register_info) Hashtbl.t * (Registers.reg * mentions_element) list array);;

(** Takes a list of BTL instructions, the last instruction of the block (both
    given by [flatten_blk_basics]), and an integer array of new positions of
    scheduled instructions, and returns the BTL tree of rescheduled
    instructions *)
val apply_schedule : BTL.iblock array -> BTL.iblock option -> int array -> BTL.iblock;;

(** Takes a BTL tree and returns a list of instructions and the last instruction
    of the BTL block *)
val flatten_blk_basics : BTL.iblock_info -> BTL.iblock array * BTL.iblock option;;
