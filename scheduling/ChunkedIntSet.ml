module IntMap=Map.Make(Int)
type chunk = int
let chunk_size = Sys.int_size

type elt = int

type t = chunk IntMap.t

let empty : t = IntMap.empty

let empty_chunk : chunk = 0

let singleton_chunk k = Int.shift_left 1 k
let union_chunk = Int.logor
let inter_chunk = Int.logand
let diff_chunk c c' = Int.logand c (Int.lognot c')
let disjoint_chunk c c' = (inter_chunk c c') = empty_chunk
let add_to_chunk k (c : chunk) : chunk = union_chunk c (singleton_chunk k)
let remove_from_chunk k (c : chunk) : chunk = diff_chunk c (singleton_chunk k)
let mem_chunk k (c : chunk) = not (disjoint_chunk c (singleton_chunk k))

let rec elements_chunk_append (c : chunk) (n : int) (acc : int list) : int list =
  if c = empty_chunk
  then acc (*change from [] to acc*)
  else elements_chunk_append (Int.shift_right_logical c 1) (Int.succ n)
         (if (Int.logand c 1) <> 0 then n :: acc else acc)

let idiv x y =
  if x < 0
  then (x-(y-1)) / y
  else x / y

let decompose n =
  let nchunk = idiv n chunk_size in
  (nchunk, n - chunk_size * nchunk)

let expand_ochunk (ochunk : chunk option) =
  match ochunk with
  | None -> empty_chunk
  | Some x -> x

let unexpand_chunk (chunk : chunk) : chunk option =
  if chunk = empty_chunk
  then None
  else Some chunk

let get_chunk nchunk (s : t) : chunk =
  expand_ochunk (IntMap.find_opt nchunk s)

let add n (s : t) : t =
  let (nchunk, k) = decompose n in
  IntMap.add nchunk (add_to_chunk k (get_chunk nchunk s)) s

let singleton n : t =
  let (nchunk, k) = decompose n in
  IntMap.singleton nchunk (singleton_chunk k)

let remove n (s : t) : t =
  let (nchunk, k) = decompose n in
  let chunk' = remove_from_chunk k (get_chunk nchunk s) in
  if chunk' = empty_chunk
  then IntMap.remove nchunk s
  else IntMap.add nchunk chunk' s

let union (s : t) (s': t) : t =
  IntMap.union
    (fun _ chunk chunk' ->
      Some (union_chunk chunk chunk'))
  s s'

let inter (s : t) (s' : t) : t =
  IntMap.merge
    (fun _ ochunk ochunk' ->
      unexpand_chunk (inter_chunk (expand_ochunk ochunk)
                                  (expand_ochunk ochunk')))
    s s'

let disjoint (s : t) (s' : t) : bool =
  try
    IntMap.iter (fun nchunk chunk ->
        match IntMap.find_opt nchunk s' with
        | None -> ()
        | Some chunk' -> if not (disjoint_chunk chunk chunk')
                         then raise Exit) s;
    true
  with Exit -> false

let diff (s : t) (s' : t) : t =
  IntMap.merge
    (fun _ ochunk ochunk' ->
      match ochunk with
      | None -> None
      | Some chunk ->
         match ochunk' with
         | None -> ochunk
         | Some chunk' -> unexpand_chunk (diff_chunk chunk chunk'))
  s s'

let elements (s : t) : int list = IntMap.fold (fun nchunk chunk acc ->
  elements_chunk_append chunk (nchunk * chunk_size) acc) s [] 
                            
(* would need popcount to be efficient *)
let cardinal s = List.length (elements s)

let is_empty : t -> bool = IntMap.is_empty

let mem (n : elt) (s : t) =
  let (nchunk, k) = decompose n in
  match IntMap.find_opt nchunk s with
  | None -> false
  | Some chunk -> mem_chunk k chunk
