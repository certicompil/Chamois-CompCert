(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           David Monniaux     CNRS, VERIMAG                  *)
(*           Nicolas Nardino    ENS-Lyon, VERIMAG              *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

open InstructionSchedulerTypes
open DebugPrint
open RTLcommonaux
open BTL_BlockOptimizeraux
open Registers
open Camlcoq
open Op

module IS = Set.Make (Int)
module IM = Map.Make (Int)
module PM = Map.Make (P)

let with_destructor dtor stuff f =
  try let ret = f stuff in
      dtor stuff;
      ret
  with exn -> dtor stuff;
              raise exn;;

let with_out_channel chan f = with_destructor close_out chan f;;
let with_in_channel chan f = with_destructor close_in chan f;;

(** Schedule instructions on a synchronized pipeline
@author David Monniaux, CNRS, VERIMAG *)

let print_problem channel problem =
  (if problem.max_latency >= 0
   then Printf.fprintf channel "max makespan: %d\n" problem.max_latency);
  output_string channel "resource bounds:";
  (Array.iter (fun b -> Printf.fprintf channel " %d" b) problem.resource_bounds);
  output_string channel ";\n";
  (Array.iteri (fun i v ->
       Printf.fprintf channel "instr%d:" i;
       (Array.iter (fun b -> Printf.fprintf channel " %d" b) v);
       output_string channel ";\n") problem.instruction_usages);
  List.iter (fun instr ->
      Printf.fprintf channel "t%d - t%d >= %d;\n"
        instr.instr_to instr.instr_from instr.latency)
    problem.latency_constraints;;

let get_nr_instructions problem = Array.length problem.instruction_usages;;
let get_nr_resources problem = Array.length problem.resource_bounds;;

(* type solution = int array
type scheduler = problem -> solution option *)

(* DISABLED				    
(** Schedule the problem optimally by constraint solving using the Gecode solver. *)
external gecode_scheduler : problem -> solution option =
  "caml_gecode_schedule_instr";;
 *)
				     
let maximum_slot_used times =
  let maxi = ref (-1) in
  for i=0 to (Array.length times)-2
  do
    maxi := max !maxi times.(i)
  done;
  !maxi;;

let check_schedule (problem : problem) (solution : solution) =
  let nr_instructions = get_nr_instructions problem in
  let times, _ = solution in
  (if Array.length times <> nr_instructions+1
   then failwith
          (Printf.sprintf "check_schedule: %d times expected, got %d"
            (nr_instructions+1) (Array.length times)));
  (if problem.max_latency >= 0 && times.(nr_instructions)> problem.max_latency
   then failwith "check_schedule: max_latency exceeded");
  (Array.iteri (fun i time ->
       (if time < 0
        then failwith (Printf.sprintf "time[%d] < 0" i))) times);
  let slot_resources = Array.init ((maximum_slot_used times)+1)
                         (fun _ -> Array.copy problem.resource_bounds) in
  for i=0 to nr_instructions -1
  do
    let remaining_resources = slot_resources.(times.(i))
    and used_resources = problem.instruction_usages.(i) in
    for resource=0 to (Array.length used_resources)-1
    do
      let after = remaining_resources.(resource) - used_resources.(resource) in
      (if after < 0
       then failwith (Printf.sprintf "check_schedule: instruction %d exceeds resource %d at slot %d" i resource times.(i)));
      remaining_resources.(resource) <- after
    done
  done;
  List.iter (fun ctr ->
      if times.(ctr.instr_to) - times.(ctr.instr_from) < ctr.latency
      then failwith (Printf.sprintf "check_schedule: time[%d]=%d - time[%d]=%d < %d"
                       ctr.instr_to times.(ctr.instr_to)
                       ctr.instr_from times.(ctr.instr_from)
                       ctr.latency)
    ) problem.latency_constraints;;

let bound_max_time problem =
  let total = ref(Array.length problem.instruction_usages) in
  List.iter (fun ctr -> total := !total + ctr.latency) problem.latency_constraints;
  !total;;

let vector_less_equal a b =
  try
    Array.iter2 (fun x y ->
        if x>y
        then raise Exit) a b;
    true
  with Exit -> false;;

(* let vector_add a b =
 *   assert ((Array.length a) = (Array.length b));
 *   for i=0 to (Array.length a)-1
 *   do
 *     b.(i) <- b.(i) + a.(i)
 *   done;; *)

let vector_subtract a b =
  assert ((Array.length a) = (Array.length b));
  for i=0 to (Array.length a)-1
  do
    b.(i) <- b.(i) - a.(i)
  done;;

(* The version with critical path ordering is much better! *)
type list_scheduler_order =
  | INSTRUCTION_ORDER
  | CRITICAL_PATH_ORDER;;

let int_max (x : int) (y : int) =
  if x > y then x else y;;

let int_min (x : int) (y : int) =
  if x < y then x else y;;

let get_predecessors problem =
  let nr_instructions = get_nr_instructions problem in
  let predecessors = Array.make (nr_instructions+1) [] in
  List.iter (fun ctr ->
      predecessors.(ctr.instr_to) <-
        (ctr.instr_from, ctr.latency)::predecessors.(ctr.instr_to))
    problem.latency_constraints;
  predecessors;;

let get_successors problem =
  let nr_instructions = get_nr_instructions problem in
  let successors = Array.make nr_instructions [] in
  List.iter (fun ctr ->
      successors.(ctr.instr_from) <-
             (ctr.instr_to, ctr.latency)::successors.(ctr.instr_from))
    problem.latency_constraints;
  successors;;

let critical_paths successors =
  let nr_instructions = Array.length successors in
  let path_lengths =  Array.make nr_instructions (-1) in
  let rec compute i =
    if i=nr_instructions then 0 else
      match path_lengths.(i) with
      | -2 -> failwith "InstructionScheduler: the dependency graph has cycles"
      | -1 -> path_lengths.(i) <- -2;
              let x = List.fold_left
                        (fun cur (j, latency)-> int_max cur (latency+(compute j)))
                        1 successors.(i)
              in path_lengths.(i) <- x; x
      | x -> x
  in for i = nr_instructions-1 downto 0
     do
       ignore (compute i)
     done;
     path_lengths;;

let maximum_critical_path problem =
  let paths = critical_paths (get_successors problem) in
  Array.fold_left int_max 0 paths;;

let get_earliest_dates predecessors =
  let nr_instructions = (Array.length predecessors)-1 in
  let path_lengths =  Array.make (nr_instructions+1) (-1) in
  let rec compute i =
    match path_lengths.(i) with
    | -2 -> failwith "InstructionScheduler: the dependency graph has cycles"
    | -1 -> path_lengths.(i) <- -2;
            let x = List.fold_left
                      (fun cur (j, latency)-> int_max cur (latency+(compute j)))
                      0 predecessors.(i)
            in path_lengths.(i) <- x; x
    | x -> x
  in for i = 0 to nr_instructions
     do
       ignore (compute i)
     done;
     for i = 0 to nr_instructions - 1
     do
       path_lengths.(nr_instructions) <- int_max
	 path_lengths.(nr_instructions) (1 + path_lengths.(i))
     done;
     path_lengths;;

exception Unschedulable
        
let get_latest_dates deadline successors =
  let nr_instructions = Array.length successors
  and path_lengths = critical_paths successors in
  Array.init (nr_instructions + 1)
	     (fun i ->
	      if i < nr_instructions then
		let path_length = path_lengths.(i) in
		assert (path_length >= 1);
		(if path_length > deadline
                 then raise Unschedulable);
		deadline - path_length
	      else deadline);;
  
let priority_list_scheduler (order : list_scheduler_order)
      (problem : problem) :
      solution option =
  let nr_instructions = get_nr_instructions problem in
  let successors = get_successors problem
  and predecessors = get_predecessors problem
  and times = Array.make (nr_instructions+1) (-1) in

  let priorities = match order with
    | INSTRUCTION_ORDER -> None
    | CRITICAL_PATH_ORDER -> Some (critical_paths successors) in
  
  let module InstrSet =
    Set.Make (struct type t=int
                     let compare = match priorities with
                       | None -> (fun x y -> x - y)
                       | Some p -> (fun x y ->
                         (match p.(y)-p.(x) with
                          | 0 -> x - y
                          | z -> z))
              end) in

  let max_time = bound_max_time problem in
  let ready = Array.make max_time InstrSet.empty in
  Array.iteri (fun i preds ->
      if i<nr_instructions && preds=[]
      then ready.(0) <- InstrSet.add i ready.(0)) predecessors;
  
  let current_time = ref 0
  and current_resources = Array.copy problem.resource_bounds
  and earliest_time i =
    try
      let time = ref (-1) in
      List.iter (fun (j, latency) ->
          if times.(j) < 0
          then raise Exit
          else let t = times.(j) + latency in
               if t > !time
               then time := t) predecessors.(i);
      assert(!time >= 0);
      !time
    with Exit -> -1
  in
  
  let advance_time() =
    begin
      (if !current_time < max_time-1
      then
        begin
          Array.blit problem.resource_bounds 0 current_resources 0
            (Array.length current_resources);
          ready.(!current_time + 1) <-
            InstrSet.union (ready.(!current_time))
              (ready.(!current_time + 1));
          ready.(!current_time) <- InstrSet.empty;
        end);
      incr current_time
    end in

  let attempt_scheduling ready usages =
    let result = ref (-1) in
    try
      InstrSet.iter (fun i ->
          (* Printf.printf "trying scheduling %d\n" i;
        pr   int_vector usages.(i);
        print           _vector current_resources; *)
          if vector_less_equal usages.(i) current_resources
          then
            begin
              vector_subtract usages.(i) current_resources;
              result := i;
              raise Exit
            end) ready;
      -1
    with Exit -> !result in
  
  while !current_time < max_time
  do
    if (InstrSet.is_empty ready.(!current_time))
     then advance_time()
    else
      match attempt_scheduling ready.(!current_time)
              problem.instruction_usages with
    | -1 -> advance_time()
    | i ->
       begin
         assert(times.(i) < 0);
         times.(i) <- !current_time;
         ready.(!current_time) <- InstrSet.remove i (ready.(!current_time));
         List.iter (fun (instr_to, _) ->
             if instr_to < nr_instructions then
               match earliest_time instr_to with
               | -1 -> ()
               | to_time ->
                  ready.(to_time) <- InstrSet.add instr_to ready.(to_time))
           successors.(i);
         successors.(i) <- []
       end
  done;
  try
    let final_time = ref (-1) in
    for i=0 to nr_instructions-1
    do
      (if times.(i) < 0 then raise Exit);
      (if !final_time < times.(i)+1 then final_time := times.(i)+1)
    done;
    List.iter (fun (i, latency) ->
        let target_time = latency + times.(i) in
        if target_time > !final_time
        then final_time := target_time
      ) predecessors.(nr_instructions);
    times.(nr_instructions) <- !final_time;
    Some (times, None)
  with Exit -> None;;

let list_scheduler = priority_list_scheduler CRITICAL_PATH_ORDER;;

(* dummy code for placating ocaml's warnings *)
let _ = fun x -> priority_list_scheduler INSTRUCTION_ORDER x;;

(* an algorithm that gives a sequence with near optimally minimal
 * register usage *)
module Lineage =
  struct
    type lineage = { list: int list; back: int; reg_class: AST.typ }
    let make list reg_class =
      match List.rev list with
      | [] -> assert false
      | x :: _ -> { list; back = x; reg_class }
    let to_list x = x.list
    let front x = List.hd x.list
    let back x = x.back
    let get_reg_class x = x.reg_class
  end

let mris_scheduler (problem : problem) rename_opt lazy_opt: solution option =
  let nr_instructions = get_nr_instructions problem in
  let live_regs_entry = problem.live_regs_entry in
  let successors = get_successors problem in
  let times = Array.make (nr_instructions + 1) None
  and class_r r = problem.typing r in

  (*debug_flag := true;*)
  let _, mentions =
    match problem.reference_counting with
    | Some (l, r) -> l, r
    | None -> assert false in

  debug "mentions:\n";
  Array.iteri (fun i l ->
    debug " .(%d):" i;
    List.iter (fun (reg, arg) ->
      debug " (%d, %s)" (RTLcommonaux.p2i reg)
      (match arg with 
       | Arg -> "arg" 
       | Def -> "dst" 
       | ExtArg -> "extarg")) l;
    debug "\n") mentions;

  (* filtering useless liveins (it happens) *)
  let live_regs_entry = Regset.fold (fun r rs ->
    let nrel_inst = Seq.(take_while (fun l ->
      not (List.mem (r, Def) l)) (Array.to_seq mentions) |> length) + 1 in 
    let rel_inst = Seq.take nrel_inst (Array.to_seq mentions) in
    if Seq.exists (List.exists (fun (r', ndef) -> r = r' && ndef <> Def)) rel_inst then
      Regset.add r rs
    else rs) live_regs_entry Regset.empty in
  let nr_liveins = regset_len live_regs_entry in

  (* add block liveness info to mentions *)
  let mentions' = Array.make (nr_instructions + 1 + nr_liveins) [] in
  Array.blit mentions 0 mentions' 0 (nr_instructions + 1);
  List.iteri (fun i r ->
    mentions'.(nr_instructions + 1 + i) <- [(r, Def)]) (Regset.elements live_regs_entry);
  let mentions = mentions' in

  debug "mentions (after liveness):\n";
  Array.iteri (fun i l ->
    debug " .(%d):" i;
    List.iter (fun (reg, arg) ->
      debug " (%d, %s)" (RTLcommonaux.p2i reg)
      (match arg with 
       | Arg -> "arg" 
       | Def -> "dst" 
       | ExtArg -> "extarg")) l;
    debug "\n") mentions;

  (* map of instructions that define new registers *)
  let def_reg_map = Hashtbl.create 42 in
  let update_def_reg_map i r =
    Hashtbl.replace def_reg_map i r in
  (* fill def map *)
  Array.iteri (fun i l ->
    match List.find_opt (fun (_, arg) -> arg = Def) l with
    | Some (r, _) ->
        update_def_reg_map i r;
    | _ -> ()) mentions;

  (* ddg utilities *)
  let topological_order ddg =
    let visited = Array.make (Array.length ddg) false in
    let tl = ref [] in
    let rec dfs i =
      visited.(i) <- true;
      List.iter (fun (j, _, _) ->
        if not visited.(j) then
          dfs j) ddg.(i);
      tl := i :: !tl in
    Array.iteri (fun i _ ->
      if not visited.(i) then
        dfs i) ddg;
    Array.of_list !tl in

  let compute_height ddg =
    let ht = Array.make (Array.length ddg) (-1) in
    let rec height i =
      match ht.(i) with
      | -2 -> failwith "InstructionScheduler: the dependency graph has cycles"
      | -1 -> (ht.(i) <- -2;
        let x = List.fold_left (fun cur (j, lat, _) ->
          int_max cur (lat + 1 + (height j))) 0 ddg.(i) in
        ht.(i) <- x; x)
      | x -> x in
    Array.mapi (fun i _ -> height i) ht in

  let factorize_ddg ddg =
    (* removes node duplicates *)
    let rec factorize_node l =
      match l with
      | (i, lat, reg) :: l' ->
          let repr, next = List.fold_left (fun (r, n) (i', lat', reg') ->
            if i = i' then
              ((i, max lat lat',
                match reg, reg' with
                | Some r, None 
                | None, Some r -> Some r
                | Some r, Some r' when r = r' -> Some r
                | None, None -> None
                | _ -> assert false), n)
            else
              (r, (i', lat', reg') :: n)) ((i, lat, reg), []) l' in
          repr :: (factorize_node next)
      | [] -> [] in
    let f_ddg = Array.map factorize_node ddg in
    (* removing useless sequencing edges *)
    let order = topological_order f_ddg |> Array.to_list |> List.rev in
    let reach_ddg = Array.make (Array.length ddg) IS.empty in
    List.iter (fun n ->
      reach_ddg.(n) <- IS.add n reach_ddg.(n);
      List.iter (fun (c, _, _) ->
        reach_ddg.(n) <- IS.union reach_ddg.(n) reach_ddg.(c)) ddg.(n)) order;
    let reach_ddg = Array.mapi (fun i s -> IS.remove i s) reach_ddg in
    Array.map (fun succ ->
      let rnodes, snset = List.fold_left (fun (rn, sn) n ->
        match n with
        | (i, _, None) -> (rn, IS.add i sn)
        | _ -> (n :: rn, sn)) ([], IS.empty) succ in
      let succ_sset = IS.fold (fun i ss -> IS.union ss reach_ddg.(i)) IS.empty snset in
      let snodes = IS.(fold (fun i l -> (i, 0, None) :: l) (inter succ_sset snset) []) in
      rnodes @ snodes) f_ddg in

  (* ddg will contain all dependencies and raw+reg def info 
   * liveouts: at index nr_instructions
   * liveins: at index (nr_instructions+1).. *)
  let ddg_liveout = nr_instructions in
  let ddg_is_liveout i = i = ddg_liveout
  and ddg_is_livein i = i > nr_instructions
  and ddg_is_node i = i < nr_instructions in
  let build_pred desc =
    let pred = Array.make (Array.length desc + 1) [] in
    Array.iteri (fun instr_from dsc ->
      List.iter (fun (instr_to, lat) ->
        pred.(instr_to) <- (instr_from, lat) :: pred.(instr_to)) dsc) desc;
    pred in
  let livein_seq = Seq.(ints (ddg_liveout + 1) |> take nr_liveins) in
  let mentions_seq = Seq.(append livein_seq (ints 0 |> take (nr_instructions + 1))) in
  let build_ddg mentions successors =
    let ddg = Array.make (nr_instructions + 1 + nr_liveins) [] in
    Array.blit (Array.map (List.map (fun (a, b) -> (a, b, None))) successors) 0 ddg 0 nr_instructions;
    let populate_deps i s =
      match List.find_opt (fun (_, b) -> b = Def) mentions.(i) with
      | Some (r, _) ->
        (let rec loop s =
           match Seq.uncons s with
           | Some (i', s) ->
               let l = mentions.(i') in
               List.iter (fun e ->
                 if e = (r, Arg) || e = (r, ExtArg) then
                   ddg.(i) <- (i', 1, Some r) :: ddg.(i)) l;
               if List.exists ((=) (r, Def)) l then raise Exit;
               loop s
           | None -> () in
         try loop s with Exit -> ())
      | None -> () in
    let rec loop s =
      match Seq.uncons s with
      | Some (i, s) -> populate_deps i s; loop s
      | None -> () in
    loop mentions_seq;
    (* adding dependencies to nodes that have no descendent -> sink (forcing lineage fusion) *)
    let ddg = Array.mapi (fun i l ->
      if l = [] && ddg_is_node i then [(nr_instructions, 0, None)]
      else l) ddg in
    (* adding sequencing edges from liveins to DAG roots (they are already executed before schedule) *)
    let pred = build_pred successors in
    let no_pred = snd (Array.fold_left (fun (i, np) l ->
      (i + 1, if l = [] && not (ddg_is_liveout i) then i :: np else np)) (0, []) pred) in
    Seq.iter (fun i ->
      ddg.(i) <- List.(append ddg.(i) (filter_map (fun j ->
        if i <> j then Some (j, 0, None) else None) no_pred))) livein_seq;
    ddg in

  let ddg = build_ddg mentions successors in
  let ddg = factorize_ddg ddg in

  debug "\nddg(after creation):\n";
  Array.iteri (fun i l ->
    debug " .(%d):" i;
    List.iter (fun (i, l, r) ->
      debug " (%d, %d, %s)" i l 
        (match r with
         | Some r -> Printf.sprintf "Some %d" (RTLcommonaux.p2i r)
         | None -> "None")) l;
         debug "\n") ddg;

  let lineage_formation ddg ht =
    let module DDGSet = Set.Make (struct
      type t = int
      let compare x y =
        match !ht.(x) - !ht.(y) with
        | 0 -> y - x
        | z -> z
      end) in
    let nr = Array.length ddg in
    let get_nodes () = DDGSet.add_seq Seq.(ints 0 |> take nr) DDGSet.empty in
    let get_raw_succ n ddg =
      List.filter_map (fun (n', _, reg) ->
        if Option.is_some reg then Some n' else None) ddg.(n) |> 
      List.to_seq |> IS.of_seq in
    let rec compute_lineages nodes in_lin ht lineages =
      match DDGSet.max_elt_opt (DDGSet.filter (fun n -> not (IS.mem n in_lin)) nodes) with
      | None -> lineages
      | Some vi ->
          let cur_lin = [vi]
          and in_lin = IS.add vi in_lin in
          let rec continue_lineage vi' cur_lin recomp in_lin =
            let children = get_raw_succ vi' ddg in
            match DDGSet.min_elt_opt (DDGSet.filter (fun n -> IS.mem n children) nodes) with
            | Some vj ->
                IS.iter (fun vk ->
                  if vk <> vj then
                    ddg.(vk) <- (vj, 0, None) :: ddg.(vk)) children;
                let recomp = recomp || (IS.cardinal children > 1)
                and cur_lin = vj :: cur_lin in
                if IS.mem vj in_lin then
                  cur_lin, recomp, in_lin
                else 
                  let regi = Hashtbl.find_opt def_reg_map vi'
                  and regj = Hashtbl.find_opt def_reg_map vj in
                  (match regi, regj with
                   | Some regi, Some regj when (class_r regi) <> (class_r regj) ->
                       cur_lin, recomp, in_lin
                   | _ -> continue_lineage vj cur_lin recomp (IS.add vj in_lin))
            | _ -> cur_lin, recomp, in_lin in
          let cur_lin, recomp, in_lin = continue_lineage vi cur_lin false in_lin in
          let nodes = if recomp
            then (ht := compute_height ddg; get_nodes ())
            else nodes in
          let lineages = cur_lin :: lineages in
          compute_lineages nodes in_lin ht lineages in
    let lineages = compute_lineages (get_nodes ()) IS.empty ht [] in 
    (* reversing order due to list append *)
    let ret = List.map (fun l ->
      let l = List.rev l in
      let reg_class =
        match List.find_map (fun n ->
          Hashtbl.find_opt def_reg_map n) l with
        | Some rc -> (class_r rc)
        | None -> RTLtypes.default in
      Lineage.make l reg_class) lineages in
    ret in

  let ht = compute_height ddg in
  let lineages = lineage_formation ddg (ref ht) in
  let ddg = factorize_ddg ddg in

  debug "\nddg(after lineage formation):\n";
  Array.iteri (fun i l ->
    debug " .(%d):" i;
    List.iter (fun (i, l, r) ->
      debug " (%d, %d, %s)" i l 
        (match r with
         | Some r -> Printf.sprintf "Some %d" (RTLcommonaux.p2i r)
         | None -> "None")) l;
         debug "\n") ddg;

  debug "\nlineages:";
  List.iteri (fun i l ->
    debug "\n .(%d):" i;
    List.iter (debug " %d") (Lineage.to_list l);
    debug " %s"
      (match Lineage.get_reg_class l with
       | AST.Tint -> "Tint"
       | AST.Tfloat -> "Tfloat"
       | AST.Tlong -> "Tlong"
       | AST.Tsingle -> "Tsingle"
       | AST.Tany32 -> "Tany32"
       | AST.Tany64 -> "Tany64")) lineages;
  debug "\n";

  (* removing lineages that start with no def *)
  let lineages = List.filter (fun l ->
    Hashtbl.mem def_reg_map (Lineage.front l))
    lineages in

  (* reachability relation *)
  let order = topological_order ddg in

  (* S and E sets *)
  let s_set, e_set = List.fold_left (fun (s1, s2) l ->
    let x1, x2 = Lineage.front l, Lineage.back l in
    (IS.add x1 s1, IS.add x2 s2)) (IS.empty, IS.empty) lineages in

  (* this array contains all the predecessors nodes in S *)
  let reach_ddg = Array.make (Array.length ddg) IS.empty in
  Array.iter (fun n ->
    if IS.mem n s_set
    then reach_ddg.(n) <- IS.add n reach_ddg.(n);
    List.iter (fun (c, _, _) ->
      reach_ddg.(c) <- IS.union reach_ddg.(n) reach_ddg.(c)) ddg.(n)) order;

  (* optimized reachability structure
   * must be used when:
   * - x is a lineage first
   * - y is a lineage back *)
  let reach = Hashtbl.create 42 in
  let reaches_memo x y =
    match Hashtbl.find_opt reach (x, y) with
    | None -> 
        let b = IS.mem x reach_ddg.(y) in
        Hashtbl.add reach (x, y) b; b
    | Some b -> b in

  (* classic non-optimized reachability function *)
  let reaches x y ddg =
    let rec dfs visited s =
      let visited = IS.add s visited in
      List.fold_left (fun visited (s', _, _) ->
        if IS.mem s' visited then visited
        else dfs visited s') visited ddg.(s) in
    let visited = dfs IS.empty x in
    IS.mem y visited in

  (* lineage fusion *)
  (* lazy option: stop fusion if lineages satisfies machine needs *)
  let is_candidate_param strong (d, g) (c, f) ddg =
    not (ddg_is_livein c) &&
    not (reaches c g ddg) && (if strong then Fun.id else not) (reaches d f ddg) in
  let ll_get_class ll =
    Lineage.get_reg_class @@ List.hd ll in
  let ll_same_class dg cf =
    (ll_get_class dg = ll_get_class cf) in
  let enough_class ll cnt_arr =
    cnt_arr.(Machregsaux.class_of_type (ll_get_class ll)) >= 0 in
  let enough_lineages cnt_arr =
    Array.for_all (fun a -> a >= 0) cnt_arr in
  let lineage_fusion lineages ddg ht =
    let rec loop lineages ddg ht is_candidate =
      let cnt_arr = Array.copy Machregsaux.nr_regs in
      if lazy_opt &&
        (List.iter (fun l ->
           let typ = Lineage.get_reg_class (List.hd (fst l)) in
           let rc = Machregsaux.class_of_type typ in
           cnt_arr.(rc) <- cnt_arr.(rc) - multiplicity_of_type typ) lineages;
           enough_lineages cnt_arr) then
             (debug "\nstopping fusion due to aggressive opt.";
              lineages) else
      let extract_lin = List.(concat (map fst lineages)) in
      let s_set, e_set = List.fold_left (fun (s1, s2) l ->
        let x1, x2 = Lineage.front l, Lineage.back l in
        (IS.add x1 s1, IS.add x2 s2)) (IS.empty, IS.empty) extract_lin in
      (* reordering lineage so that the fusion is done top-down *)
      let lineages = List.fast_sort (fun (l1, _) (l2, _) ->
        let concat_lin l =
          List.concat (List.map Lineage.to_list l) in
        let l1, l2 = concat_lin l1, concat_lin l2 in
        List.compare (fun a b -> ht.(b) - ht.(a)) l1 l2) lineages in
      let lineages = Array.of_list lineages in
      let nb_lin = Array.length lineages in
      let top_down = Seq.(ints 0 |> map (fun x -> 
        map (fun y -> (x, y)) (ints 0 |> take x)) |> concat |> map (fun (x, y) -> 
          List.to_seq [(x, y); (y, x)]) |> concat |> 
          take_while (fun (ix, iy) -> ix < nb_lin && iy < nb_lin)) in
      match Seq.find (fun (ix, iy) ->
        let (dg, dgc), (cf, cfc) = lineages.(ix), lineages.(iy) in
        debug "\nis_candidate (%d, %d) (%d, %d): %b" (fst dgc) (snd dgc) (fst cfc) (snd cfc) (is_candidate dgc cfc ddg);
        debug "\nll_same_class (%d, %d) (%d, %d): %b" (fst dgc) (snd dgc) (fst cfc) (snd cfc) (ll_same_class dg cf);
        debug "\nenough_class (%d, %d): %b" (fst dgc) (snd dgc) (enough_class dg cnt_arr);
        ll_same_class dg cf && not (enough_class dg cnt_arr) && is_candidate dgc cfc ddg) top_down with
      | Some (ix, iy) ->
          let ldg, lcf = lineages.(ix), lineages.(iy) in
          let (dg, (d, g)), (cf, (c, f)) = ldg, lcf in
          (* found R(c, g) = 0 & R(d, f) = 1 *)
          debug "\nfound that R(%d, %d) = %b and R(%d, %d) = %b" c g (reaches_memo c g) d f (reaches_memo d f);
          (* change DDG, height table and reachability *)
          debug "\nadding a sequencing edge from %d to %d" g c;
          ddg.(g) <- (c, 0, None) :: ddg.(g);
          let ht = compute_height ddg in
          (* R(x,y) = 1 for all x in S that verify R(x,g) = 1, y in E that verify R(c,y) = 1 *)
          let all_x = IS.filter (fun x ->
            reaches_memo x g) s_set in
          let all_y = IS.filter (fun y ->
            reaches_memo c y) e_set in
          IS.iter (fun e ->
            reach_ddg.(e) <- IS.union reach_ddg.(e) all_x;
            IS.iter (fun s ->
              Hashtbl.replace reach (s, e) true) all_x) all_y;
          let filtered = List.filter (fun l -> l <> ldg && l <> lcf) (Array.to_list lineages) in
          let lineages = ((List.concat [dg; cf]), (d, f)) :: filtered in
          loop lineages ddg ht is_candidate
      | _ -> Array.to_list lineages in
    let lineages = List.map (fun l ->
      let f, b = Lineage.front l, Lineage.back l in
      ([l], (f, b))) lineages in
    debug "\nfusing lineages with strong condition";
    let lineages = loop lineages ddg ht (is_candidate_param true) in
    if lazy_opt then List.map fst lineages else
    let ht = compute_height ddg in
    debug "\nfusing lineages with weak condition";
    List.map fst (loop lineages ddg ht (is_candidate_param false)) in

  let lineages = lineage_fusion lineages ddg ht in
  let ddg = factorize_ddg ddg in

  debug "\nddg(after fusion):\n";
  Array.iteri (fun i l ->
    debug " .(%d):" i;
    List.iter (fun (i, l, r) ->
      debug " (%d, %d, %s)" i l 
        (match r with
         | Some r -> Printf.sprintf "Some %d" (RTLcommonaux.p2i r)
         | None -> "None")) l;
         debug "\n") ddg;

  let get_front_back ll =
    match ll, List.rev ll with
    | lf :: _, lb :: _ -> Lineage.front lf, Lineage.back lb
    | _ -> assert false in

  (* generating LIG *)
  let neighbors i_l lineages =
    List.filter (fun (_, l') ->
      let l = lineages.(i_l) in
      let lf, lb = get_front_back l in
      let l'f, l'b = get_front_back l' in
      (lf <> l'f) &&
      (((reaches_memo lf l'b) &&
        (reaches_memo l'f lb)) ||
       (not (ll_same_class l l')) ||
       (ddg_is_livein lf && ddg_is_livein l'f)))
    (Array.mapi (fun i x -> (i, x)) lineages |> Array.to_list) |> List.map fst in

  (* maximum cardinality search (MCS) *)
  (* gives a good ordering for greedy graph coloring *)
  let mcs lig =
    let n = Array.length lig in
    let lig = Array.copy lig in
    let lambda = Hashtbl.create 42 in
    Hashtbl.add_seq lambda (Seq.(ints 0 |> take n |> map (fun i -> (i, 0))));
    let lambda_max () =
      let start = 
        match Hashtbl.to_seq lambda |> Seq.find (fun (k, _) -> lig.(k) <> []) with
        | None -> Hashtbl.to_seq lambda |> Seq.uncons |> get_some |> fst
        | Some (k, v) -> (k, v) in
      Hashtbl.fold (fun k v (max_k, max_v) ->
        if v > max_v && lig.(k) <> [] then (k, v) else (max_k, max_v)) lambda start |> fst in
    let delete_node v =
      lig.(v) <- [];
      Array.iteri (fun i l -> lig.(i) <- List.filter (fun u -> u <> v) l) lig; in
    Seq.fold_left (fun acc i ->
      let v = lambda_max () in
      Hashtbl.remove lambda v;
      List.iter (fun u -> 
        let k = Hashtbl.find lambda u in
        Hashtbl.replace lambda u (k + 1)) lig.(v);
      delete_node v;
      v::acc) [] (Seq.(ints 0 |> take n)) |> List.rev in

  let color_lig_greedy lig ordering =
    let first_freg = r2pi () in
    let n = Array.length lig in
    let coloring = Array.make n None in
    List.iter (fun v ->
      let c_n = List.fold_left (fun s u -> 
        match coloring.(u) with
        | Some c -> Regset.add c s
        | None -> s) Regset.empty lig.(v) in
      let c = Seq.find (fun c -> not (Regset.mem c c_n)) (Seq.ints (p2i first_freg) |> Seq.map i2p) in 
      coloring.(v) <- c) ordering;
    let ret = Array.map get_some coloring in
    (* updating synthetic register's name *)
    if Array.length ret > 0 then reg := Array.fold_left max 1 (Array.map p2i ret);
    ret in
  
  (* transforming lineage to array for easier graph manipulation *)
  let lineages = Array.of_list lineages in
       
  let colors = 
    (* creating & coloring the lig *)
    let lig = Array.mapi (fun i l -> neighbors i lineages) lineages in
    let ordering = mcs lig in
    color_lig_greedy lig ordering in

  (* apply color to lineages *)
  let lineages_colors = Array.combine lineages colors in
  debug "\nlineages_color:\n";
  Array.iteri (fun i (l, c) ->
    debug " .(%d):" i;
    List.iter (fun l ->
      debug " ["; List.iter (debug " %d") (Lineage.to_list l); debug " )") l;
    debug ", color: %d\n" (p2i c)) lineages_colors;

  (* slightly increasing register usage [lazy] *)
  let lineages_colors =
    if lazy_opt then
      let color_map = Hashtbl.create 42 in
      let class_of_assq ll =
        let typ = Lineage.get_reg_class (List.hd (List.hd ll)) in
        let rc = Machregsaux.class_of_type typ in
        (typ, rc) in
      let get_crit_class color_assq =
        let cnt_arr = Array.copy Machregsaux.nr_regs in
        List.iter (fun (_, l) ->
          let (typ, rc) = class_of_assq l in
          cnt_arr.(rc) <- cnt_arr.(rc) - (multiplicity_of_type typ)) color_assq;
        Array.map ((>=) 0) cnt_arr in
      let lin_len ll =
        List.(fold_left (fun acc l -> length (Lineage.to_list l) + acc) 0 ll) in
      let reorder_assq color_assq =
        let crit_class = get_crit_class color_assq in
        let put_last l =
          let (_, rc) = class_of_assq l in
          List.length l = 1 || crit_class.(rc) in
        let last =  List.filter (fun (_, l) -> put_last l) color_assq in
        let first = List.filter (fun (_, l) -> not (put_last l)) color_assq in
        (List.fast_sort (fun (_, la) (_, lb) ->
           let l_sum l =
             List.fold_left (fun s l ->
               s + lin_len l) 0 l in
           l_sum lb - l_sum la) first) @ last in
      let rec reduce_loop color_assq =
        let flat =
          List.fold_left (fun a (c, l) ->
            List.map (fun l -> (l, c)) l @ a) [] in
        let cant_reduce =
          let crit_class = get_crit_class color_assq in
          List.for_all (fun (_, l) ->
            let (_, rc) = class_of_assq l in
            List.length l == 1 || crit_class.(rc)) in
        if cant_reduce color_assq then
          flat color_assq
        else 
          let (c, l), ca = List.hd color_assq, List.tl color_assq in
          let hl, tl = List.hd l, List.tl l in
          let color_assq = ca @ [(c, tl)] @ [(r2pi (), [hl])] in
          reduce_loop (reorder_assq color_assq) in
      Array.iter (fun (l, c) ->
        match Hashtbl.find_opt color_map c with
        | Some l' -> Hashtbl.replace color_map c (l :: l')
        | None -> Hashtbl.add color_map c [l]) lineages_colors;
      let color_assq = Hashtbl.fold (fun c l ca ->
        let sl = List.fast_sort (fun a b ->
          (lin_len b) - (lin_len a)) l in
        (c, sl) :: ca) color_map [] in
      Array.of_list (reduce_loop (reorder_assq color_assq))
    else lineages_colors in

  debug "\nlineages_color (after recoloration):\n";
  Array.iteri (fun i (l, c) ->
    debug " .(%d):" i;
    List.iter (fun l ->
      debug " ["; List.iter (debug " %d") (Lineage.to_list l); debug " )") l;
    debug ", color: %d\n" (p2i c)) lineages_colors;

  let s_colors = Hashtbl.create 42
  and e_colors = Hashtbl.create 42 in
  Array.iter (fun (l, c) ->
    Hashtbl.add s_colors (Lineage.front (List.hd l)) c) lineages_colors;
  Array.iter (fun (l, c) ->
    let x = Lineage.back List.(rev l |> hd) in
    match Hashtbl.find_opt e_colors x with
    | None ->
        Hashtbl.add e_colors x (Regset.singleton c)
    | Some cs ->
        Hashtbl.replace e_colors x (Regset.add c cs)) lineages_colors;

  (* register constraints *)
  let all_regs = Array.fold_left (fun s (_, c) -> Regset.add c s) Regset.empty lineages_colors in
  let livein_regs = 
    Seq.fold_left (fun s i ->
      if Hashtbl.mem e_colors i then s 
      else Regset.add (Hashtbl.find s_colors i) s) Regset.empty livein_seq in
  let available_regs = ref (Regset.diff all_regs livein_regs) in

  let find_inst_lin_col i =
    Array.find_opt (fun (ll, c) ->
      let l = List.(concat @@ map (fun ll ->
        tl @@ rev @@ Lineage.to_list ll) ll) in
      List.mem i l) lineages_colors in

  debug "\ne_colors:";
  Hashtbl.iter (fun i cs ->
    debug "\n .(%d):" i;
    List.iter (fun r -> debug " %d" (p2i r)) (Regset.elements cs)) e_colors;

  (* restore scheduling variables *)
  Array.blit (Array.map (List.map (fun (inst, lat, _) -> (inst, lat))) ddg) 0 successors 0 nr_instructions;
  let predecessors = build_pred successors in
  let priorities = critical_paths successors in

  let module InstrSet =
    Set.Make (struct type t=int
      let compare = (fun x y ->
        (match priorities.(y) - priorities.(x) with
         | 0 -> x - y
         | z -> z))
         end) in

  let max_time = bound_max_time problem in
  let ready = Array.make max_time InstrSet.empty in
  Array.iteri (fun i preds ->
      if ddg_is_node i && preds = []
      then ready.(0) <- InstrSet.add i ready.(0)) predecessors;

  let def_nreg_map = Hashtbl.create 42 in
  let find_new_reg i =
    find_inst_lin_col i |> Option.map snd in
  (* fill new def map *)
  Hashtbl.iter (fun i reg ->
    match find_new_reg i with
    | Some r -> Hashtbl.replace def_nreg_map i r
    | None -> ()) def_reg_map;
  let regs_cycle = ref Regset.empty in
  let reg_nreg_map = ref PM.empty in
  Seq.iter (fun i ->
    reg_nreg_map := 
      PM.add (Hashtbl.find def_reg_map i)
             (Hashtbl.find def_nreg_map i)
             !reg_nreg_map) livein_seq;
  let get_nregs_mentions mentions i =
    List.fold_left (fun rs (reg, arg) ->
      Regset.add
      (if arg <> Def then (PM.find reg !reg_nreg_map)
       else (Hashtbl.find def_nreg_map i)) rs) Regset.empty mentions.(i) in
  let current_time = ref 0
  and current_resources = Array.copy problem.resource_bounds in
  let earliest_time i =
    try List.fold_left (fun t (j, latency) ->
      match t, times.(j) with
      | _, None -> raise Exit
      | None, Some tj -> Some (tj + latency)
      | Some t, Some tj -> Some (max t (tj + latency))) None predecessors.(i)
    with Exit -> None
  and advance_time () =
    if !current_time < max_time - 1 then
      (Array.blit problem.resource_bounds 0 current_resources 0
        (Array.length current_resources);
      ready.(!current_time + 1) <-
        InstrSet.union (ready.(!current_time))
          (ready.(!current_time + 1));
      ready.(!current_time) <- InstrSet.empty);
    regs_cycle := Regset.empty;
    incr current_time
  (* use_i has been precomputed *)
  and update_resources i usages =
    (* update hardware resources *)
    vector_subtract usages.(i) current_resources;
    (* update scheduled instructions for cycle *)
    regs_cycle := Regset.union !regs_cycle (get_nregs_mentions mentions i);
    (* update other variables *)
    (match Hashtbl.find_opt def_reg_map i, Hashtbl.find_opt def_nreg_map i with
     | Some r, Some rs ->
         reg_nreg_map := PM.add r rs !reg_nreg_map;
     | _ -> ());
    (* update register availability *)
    (match Hashtbl.find_opt s_colors i with
     | Some reg_i ->
         available_regs := Regset.remove reg_i !available_regs
     | None -> ());
    (match Hashtbl.find_opt e_colors i with
     | Some to_free ->
         available_regs := Regset.union to_free !available_regs
     | None -> ()) in
  let allow_instruction_regs mentions i =
    let used_regs_i = get_nregs_mentions mentions i in
    Regset.is_empty (Regset.inter !regs_cycle used_regs_i) in
  let instruction_lineage_valid i =
    match Hashtbl.find_opt s_colors i with
    | Some reg_i ->
        (* if reg is currently available, schedule is possible *)
        Regset.mem reg_i !available_regs
    | None ->
        (* if instruction does not start a lineage, it can be scheduled *)
        true in
  let simu_deadlock ready =
    let sched_seq = Seq.(ints !current_time |> take (max_time - !current_time)) in
    let full_ready = Seq.fold_left (fun s i -> 
      InstrSet.union s ready.(i)) InstrSet.empty sched_seq in
    if not (InstrSet.exists instruction_lineage_valid full_ready)
    then InstrSet.find_first_opt (fun i -> not (instruction_lineage_valid i)) full_ready
    else None in
  let new_color i =
    (* assert lazy_opt; *)
    let nr = r2pi () in
    let l, c = get_some @@ find_inst_lin_col i in
    let f, b = get_front_back l in
    debug "\nchanging color of lineage (%d, %d) with %d\n\n" f b (p2i nr);
    (* changing s and e sets *)
    Hashtbl.replace s_colors i nr;
    Hashtbl.replace e_colors b (Regset.(remove c (Hashtbl.find e_colors b) |> add nr));
    (* changing def_nreg map for rename *)
    List.iter (fun i ->
      if Hashtbl.mem def_nreg_map i then
      Hashtbl.replace def_nreg_map i nr) List.(tl @@ rev @@ concat @@ map Lineage.to_list l);
    (* adding the new register to list *)
    available_regs := Regset.add nr !available_regs in
  let attempt_scheduling ready usages =
    debug "\n\ntime: %d" !current_time;
    debug "\nready set: ";
    InstrSet.iter (fun i -> debug "%d " i) ready.(!current_time);
    debug "\navailable regs: ";
    Regset.elements !available_regs |>
    List.iter (fun i -> debug "%d " (p2i i));
    debug "\n";
    let result = ref None in
    (* if a deadlock is detected, continue with a new color *)
    (match simu_deadlock ready with
     | Some i -> new_color i
     | None -> ());
    try
      (InstrSet.iter (fun i ->
        debug "vector_less_equal(%d)        :%b\n" i (vector_less_equal usages.(i) current_resources);
        debug "allow_instruction_regs(%d)   :%b\n" i (allow_instruction_regs mentions i);
        debug "instruction_lineage_valid(%d):%b\n" i (instruction_lineage_valid i);
        if vector_less_equal usages.(i) current_resources &&
           allow_instruction_regs mentions i &&
           instruction_lineage_valid i then
          (update_resources i usages;
           result := Some i;
           raise Exit)) ready.(!current_time);
       None)
    with Exit -> !result in
  while !current_time < max_time do
    if (InstrSet.is_empty ready.(!current_time))
    then advance_time ()
    else
      match attempt_scheduling ready
        problem.instruction_usages
      with
      | Some i ->
        debug "\nissuing instruction %d" i;
        assert(times.(i) = None);
        (* registering order *)
        times.(i) <- Some !current_time;
        ready.(!current_time) <- InstrSet.remove i ready.(!current_time);
        List.iter (fun (instr_to, _) ->
          if ddg_is_node instr_to || ddg_is_liveout instr_to then
            match earliest_time instr_to with
            | None -> ()
            (* freeing liveout registers *)
            | Some _ when ddg_is_liveout instr_to ->
                debug "\nfreeing liveout registers";
                (match Hashtbl.find_opt e_colors instr_to with
                 | Some free_regs -> available_regs := Regset.union !available_regs free_regs
                 | None -> ())
            (* forwarding intruction to earliest time *)
            | Some to_time ->
                debug "\nforwarding instruction %d to time %d" instr_to to_time;
                ready.(to_time) <- InstrSet.add instr_to ready.(to_time))
          successors.(i);
          successors.(i) <- []
      | None -> advance_time ()
  done;
  (*debug_flag := false;*)

  match try
    let final_time = ref (-1) in
    for i=0 to nr_instructions-1
    do
      match times.(i) with
      | None -> raise Exit
      | Some itime ->
          if !final_time < itime + 1 then final_time := itime + 1;
    done;
    List.iter (fun (i, latency) ->
      let itime = get_some @@ times.(i) in
      let target_time = latency + itime in
      if target_time > !final_time
      then final_time := target_time) predecessors.(nr_instructions);
    times.(nr_instructions) <- Some !final_time;
    Some (Array.map get_some times)
  with Exit -> None with
  | None -> assert false
  | Some times when (not rename_opt) -> Some (times, None)
  | Some times -> (* ... *)

  let last_def_liveout =
    Seq.(map fst (List.to_seq predecessors.(ddg_liveout)) |> 
         filter (Hashtbl.mem def_reg_map)) |> IS.of_seq in

  let ren_map = Seq.fold_left (fun rm i ->
    if ddg_is_liveout i || ddg_is_livein i then rm else
    let r =  Hashtbl.find def_reg_map i in
    if IS.mem i last_def_liveout then IM.add i (r, r) rm else
    let nr = Hashtbl.find def_nreg_map i in
    IM.add i (r, nr) rm) IM.empty (Hashtbl.to_seq_keys def_reg_map) in

  Some (times, Some ren_map)

let sort_mentions l =
  let args = List.filter (fun (_, arg) -> arg = Arg) l in
  let defs = List.filter (fun (_, arg) -> arg = Def) l in
  let eargs = List.filter (fun (_, arg) -> arg = ExtArg) l in
  List.concat [args; defs; eargs]

(* Creates a mapping of the form (int -> reg) where a value is attached to a
 * register, in a way that a new value is assigned when a register is
 * (re)defined.
 * This function returns the (int -> reg) mapping [vr_arr] and updated
 * reference_counting structures ([counts'] and [mentions']) *)
let value_numbering mentions live_regs_entry =
  let cval = ref 0 in
  let rv_map = Hashtbl.create 42 in
  let counts' = Hashtbl.create 42 in
  debug "\nlive_regs_entry:";
  List.iter (fun r ->
    debug " %d" (p2i r);
    Hashtbl.add rv_map r !cval; cval := !cval + 1)
    (Regset.elements live_regs_entry);
  let apply_mapping l =
    (* for mention evaluation, first comes Arg, then Def, then ExtArg *)
    let l = sort_mentions l in
    List.map (fun (r, b) ->
      if b <> Def then
        let v = Hashtbl.find rv_map r in
        match Hashtbl.find_opt counts' v with
        | None -> Hashtbl.add counts' v 1
        | Some i -> Hashtbl.replace counts' v (i + 1)
      else (Hashtbl.add rv_map r !cval; cval := !cval + 1);
      (Hashtbl.find rv_map r, b)) l in
  let mentions' = Array.map apply_mapping mentions in
  let vr_arr = Array.make !cval None in
  Hashtbl.iter (fun r i ->
    vr_arr.(i) <- Some r) rv_map;
  (Array.map get_some vr_arr, counts', mentions')

(* list scheduling that is register pressure aware *)
(* register liveness computed through value numbering *)
let regpres_scheduler (problem : problem) : solution option =
  (*debug_flag := true;*)
  let nr_instructions = get_nr_instructions problem in
  let successors = get_successors problem
  and predecessors = get_predecessors problem
  and times = Array.make (nr_instructions + 1) None in
  let priorities = critical_paths successors in

  let module InstrSet =
    Set.Make (struct type t=int
      let compare =  (fun x y ->
        (match priorities.(y)-priorities.(x) with
        | 0 -> x - y
        | z -> z))
        end) in

  let max_time = bound_max_time problem in
  let ready = Array.make max_time InstrSet.empty in
  Array.iteri (fun i preds ->
      if i<nr_instructions && preds=[]
      then ready.(0) <- InstrSet.add i ready.(0)) predecessors;

  (* regpres specific variables *)
  let live_regs_entry = problem.live_regs_entry in
  let available_regs = Array.copy Machregsaux.nr_regs in
  let nr_types_regs = Array.length available_regs
  and thres = Array.fold_left min
    (max !(Clflags.option_regpres_threshold) 0)
    Machregsaux.nr_regs in
  let regs_thresholds = Array.make nr_types_regs thres in

  let counts, mentions =
    match problem.reference_counting with
    | Some (l, r) -> l, r
    | None -> assert false in
  let class_r r =
    (Hashtbl.find counts r).rg_class
  and reg_mult r =
    (Hashtbl.find counts r).multiplicity in
  List.iter (fun r ->
    let c = class_r r in
    let n = reg_mult r in
    available_regs.(c) <- available_regs.(c) - n)
    (Registers.Regset.elements live_regs_entry);

  debug "\n\ncounts:";
  Hashtbl.iter (fun r ri ->
    debug "\n %d -> typ[%d, %d], %d" (RTLcommonaux.p2i r) ri.rg_class ri.multiplicity ri.nb_read) counts;
  debug "\n\nmentions:";
  Array.iteri (fun i l -> debug "\n %d -" i;
    List.iter (fun (r, b) -> debug " (%d, %s)" (RTLcommonaux.p2i r)
    (match b with | Arg -> "arg" | Def -> "def" | ExtArg -> "extarg")) l) mentions;
  let vr_arr, counts', mentions' = value_numbering mentions live_regs_entry in
  let live_vregs_entry = Seq.(ints 0 |> take (List.length (Regset.elements live_regs_entry))) |> List.of_seq in
  debug "\n\ncounts':";
  Hashtbl.iter (fun v n ->
    debug "\n %d -> %d" v n) counts';
  debug "\n\nmentions':";
  Array.iteri (fun i l -> debug "\n %d -" i;
    List.iter (fun (v, b) -> debug " (%d, %s)" v
    (match b with | Arg -> "arg" | Def -> "def" | ExtArg -> "extarg")) l) mentions';
  debug "\nvr_arr:";
  Array.iteri (fun i r ->
    debug "\n.(%d): %d" i (p2i r)) vr_arr;

  let vals_to_free = ref (Hashtbl.fold
    (fun v n is -> if n = 1 then IS.add v is else is) counts' IS.empty)
  and use_of_inst i =
    List.filter_map (fun (r, b) -> if b <> Def then Some r else None) mentions'.(i) 
  and current_time = ref 0
  and current_resources = Array.copy problem.resource_bounds in
  let earliest_time i =
    try List.fold_left (fun t (j, latency) ->
      match t, times.(j) with
      | _, None -> raise Exit
      | None, Some tj -> Some (tj + latency)
      | Some t, Some tj -> Some (max t (tj + latency))) None predecessors.(i)
    with Exit -> None
  and advance_time () =
    if !current_time < max_time - 1 then
      (Array.blit problem.resource_bounds 0 current_resources 0
        (Array.length current_resources);
      ready.(!current_time + 1) <-
        InstrSet.union (ready.(!current_time))
          (ready.(!current_time + 1));
      ready.(!current_time) <- InstrSet.empty);
    incr current_time

  and n_def = Hashtbl.create 42
  and n_kill = Hashtbl.create 42 in
  let account_def i vr =
    let r = vr_arr.(vr) in
    let n =
      match Hashtbl.find_opt n_def r with
      | None -> 0
      | Some n -> n in
    Hashtbl.replace n_def r (n + 1);
    (* vregister also killed when it has no usage in the block *)
    if not (Hashtbl.mem counts' vr) then
      (let n =
         match Hashtbl.find_opt n_kill r with
         | None -> 0
         | Some n -> n in
       Hashtbl.replace n_kill r (n + 1)) in
  (* populating n_def by adding live variables at entry *)
  List.iter (fun vr -> account_def 0 vr) live_vregs_entry;

  debug "\nn_def:";
  Hashtbl.iter (fun r n -> debug "\n .(%d): %d" (p2i r) n) n_def;
  debug "\nn_kill:";
  Hashtbl.iter (fun r n -> debug "\n .(%d): %d" (p2i r) n) n_kill;
  (* report changes to n_def and n_kill counters *)
  let report_usage i =
    let n_loc_args = Hashtbl.create 42 in
    let li = sort_mentions mentions'.(i) in
    debug "\nn_def (before report):";
    Hashtbl.iter (fun r n -> debug "\n .(%d): %d" (p2i r) n) n_def;
    debug "\nn_kill (before report):";
    Hashtbl.iter (fun r n -> debug "\n .(%d): %d" (p2i r) n) n_kill;
    List.iter (fun (vr, arg) ->
      match arg with
      | Def -> account_def i vr
      | _ ->
          let n =
            match Hashtbl.find_opt n_loc_args vr with
            | None -> 0
            | Some n -> n in
          Hashtbl.replace n_loc_args vr (n + 1)) li;
    (* register is kill after instruction when it is used *)
    (* the same amount of times that is is available *)
    Hashtbl.iter (fun vr n_arg ->
      let r = vr_arr.(vr) in
      if n_arg = (Hashtbl.find counts' vr) then
      let n =
        match Hashtbl.find_opt n_kill r with
        | None -> 0
        | Some n -> n in
      Hashtbl.replace n_kill r (n + 1)) n_loc_args;
    debug "\nn_def (after report):";
    Hashtbl.iter (fun r n -> debug "\n .(%d): %d" (p2i r) n) n_def;
    debug "\nn_kill (after report):";
    Hashtbl.iter (fun r n -> debug "\n .(%d): %d" (p2i r) n) n_kill in

  let update_resources i use_i usages =
    (* update n_def and n_kill *)
    report_usage i;
    (* recompute available_regs *)
    Array.iteri (fun i n -> available_regs.(i) <- n) Machregsaux.nr_regs;
    Hashtbl.iter (fun r nr_def ->
      let rc = class_r r in
      let rs = reg_mult r in
      (* nr_def: counting the number of times register r was defined *)
      (* nr_kill: counting the number of times register r was killed *)
      let nr_kill =
        match Hashtbl.find_opt n_kill r with
        | None -> 0
        | Some n -> n in 
      (* the difference: register is alive *)
      let diff = nr_def - nr_kill in
      debug "\ndiff for inst %d reg %d: %d" i (p2i r) diff;
      assert(diff >= 0 && diff <= 1);
      if diff > 0 then debug "\nreg %d is used" (p2i r);
      available_regs.(rc) <- available_regs.(rc) - (diff * rs)) n_def;
    (* update register count *)
    List.iter (fun v ->
      let n = Hashtbl.find counts' v in
      if n - 1 = 1 then
        vals_to_free := IS.add v !vals_to_free;
      Hashtbl.replace counts' v (n - 1))
      use_i;
    (* update hardware resources *)
    vector_subtract usages.(i) current_resources in

  let attempt_scheduling ready usages =
    debug "\n\ntime: %d" !current_time;
    debug "\nready set: ";
    InstrSet.iter (fun i -> debug "%d " i) ready;
    let critical_reg_class =
      snd (Array.fold_left (fun (rc, s) nr ->
        debug "\nreg_class: %d, %d available" rc nr;
        rc + 1,
        if nr <= regs_thresholds.(rc)
        then (debug " [!]"; IS.add rc s)
        else s) (0, IS.empty) available_regs) in
    let result = ref None in
    try
      (* first, try to free values *)
      (* here we get the instruction that frees the maximum amount of values *)
      if IS.cardinal critical_reg_class > 0 then
      (let instr_to_free = InstrSet.fold (fun i a ->
         (* if instruction is schedulable *)
         if vector_less_equal usages.(i) current_resources then
           let use_i = use_of_inst i in
           let vals_free = IS.inter (IS.of_seq (List.to_seq use_i)) !vals_to_free in
           (* filters values to free that are critical values *)
           let n_free_crit = IS.cardinal (IS.filter (fun v ->
             IS.mem (class_r vr_arr.(v)) critical_reg_class) vals_free) in
           let ret = (Some i, n_free_crit, Some use_i, Some vals_free) in
           match a with
           | (None, _, _, _) -> ret
           | (Some _, max_free_crit, _, _) ->
             if max_free_crit < n_free_crit then ret else a
         else a)
       ready (None, 0, None, None) in
       match instr_to_free with
       | (Some i, _, Some use_i, Some vals_free) ->
           update_resources i use_i usages;
           result := Some i;
           debug "\nstrategy: free values:";
           IS.iter (debug " %d") vals_free;
           raise Exit
       | _ -> ());
      if IS.cardinal critical_reg_class > 0 then debug "\nfailed to free values";
      (* second, issue the most interesting instruction *)
      InstrSet.iter (fun i ->
        if vector_less_equal usages.(i) current_resources then
          (let use_i = use_of_inst i in
           let vals_free = IS.inter (IS.of_seq (List.to_seq use_i)) !vals_to_free in
           update_resources i use_i usages;
           result := Some i;
           debug "\nstrategy: issuing most interesting instruction. values freed:";
           IS.iter (debug " %d") vals_free;
           raise Exit)) ready;
      None
    with Exit -> !result in

  while !current_time < max_time do
    if (InstrSet.is_empty ready.(!current_time))
    then advance_time ()
    else
      match attempt_scheduling ready.(!current_time)
        problem.instruction_usages
      with
      | None -> advance_time ()
      | Some i ->
        (debug "\nissuing instruction: %d" i;
        assert(times.(i) = None);
        times.(i) <- Some !current_time;
        ready.(!current_time) <- InstrSet.remove i ready.(!current_time);
        List.iter (fun (instr_to, _) ->
          if instr_to < nr_instructions then
            match earliest_time instr_to with
            | None -> ()
            | Some to_time ->
                ready.(to_time) <- InstrSet.add instr_to ready.(to_time))
          successors.(i);
          successors.(i) <- [])
  done;
  
  debug "\nn_def (before olast):\n";
  Hashtbl.iter (fun r n ->
    debug " .(%d): %d\n" (p2i r) n) n_def;
  debug "n_kill (before olast):\n";
  Hashtbl.iter (fun r n ->
    debug " .(%d): %d\n" (p2i r) n) n_kill;

  report_usage nr_instructions;

  debug "\nn_def (after olast):\n";
  Hashtbl.iter (fun r n ->
    debug " .(%d): %d\n" (p2i r) n) n_def;
  debug "n_kill (after olast):\n";
  Hashtbl.iter (fun r n ->
    debug " .(%d): %d\n" (p2i r) n) n_kill;

  (* assert n_kill = n_def *)
  assert (Hashtbl.fold (fun r n b ->
    match Hashtbl.find_opt n_kill r with
    | None -> false
    | Some n' -> (n = n') && b) n_def true);

  (*debug_flag := false;*)

  try
    let final_time = ref (-1) in
    for i=0 to nr_instructions-1
    do
      match times.(i) with
      | None -> raise Exit
      | Some itime ->
          if !final_time < itime + 1 then final_time := itime + 1;
    done;
    List.iter (fun (i, latency) -> 
      let itime = get_some @@ times.(i) in
      let target_time = latency + itime in
      if target_time > !final_time
      then final_time := target_time) predecessors.(nr_instructions);
    times.(nr_instructions) <- Some !final_time;
    Some (Array.map get_some times, None)
  with Exit -> None;;

(* A scheduler sensitive to register pressure *)
let reg_pres_scheduler_old (problem : problem) : solution option = 
  (*debug_flag := true;*)
  let nr_instructions = get_nr_instructions problem in

  if !Clflags.option_debug_compcert > 6 then
    (Printf.eprintf "\nSCHEDULING_SUPERBLOCK %d\n" nr_instructions;
     flush stderr);
  
  let successors = get_successors problem
  and predecessors = get_predecessors problem
  and times = Array.make (nr_instructions+1) (-1) in
  let live_regs_entry = problem.live_regs_entry in

  let available_regs = Array.copy Machregsaux.nr_regs in

  let nr_types_regs = Array.length available_regs in

  let thres = Array.fold_left (min)
                (max !(Clflags.option_regpres_threshold) 0)
                Machregsaux.nr_regs
  in
  
  let regs_thresholds = Array.make nr_types_regs thres in
  (* placeholder value *)
  
  let live_regs = Hashtbl.create 42 in
  
  let counts, mentions =
    match problem.reference_counting with
    | Some (l, r) -> l, r
    | None -> failwith "called reg_pres_scheduler_old with reference_counting = None"
  in
  List.iter (fun r ->
      (* let ty = failwith "TODO in reg_pres_scheduler_old in InstructionScheduler" (* problem.typing r *) in
      let classe = Machregsaux.class_of_type ty in *)
      let r_info = Hashtbl.find counts r in
      
      available_regs.(r_info.rg_class) <- available_regs.(r_info.rg_class) - (r_info.multiplicity);
      Hashtbl.add live_regs r (r_info.rg_class))
    (Registers.Regset.elements live_regs_entry);

  let csr_b = ref false in

  let counts, mentions =
    match problem.reference_counting with
    | Some (l, r) -> l, r
    | None -> assert false
  in

  let class_r r =
    (Hashtbl.find counts r).rg_class in

  (*debug_flag := true;*)
  debug "\n\ncounts:";
  Hashtbl.iter (fun r ri ->
    debug "\n %d -> typ[%d, %d], %d" (RTLcommonaux.p2i r) ri.rg_class ri.multiplicity ri.nb_read) counts;
  debug "\n\nmentions:";
  Array.iteri (fun i l -> debug "\n %d -" i;
    List.iter (fun (r, b) -> debug " (%d, %s)" (RTLcommonaux.p2i r)
    (match b with | Arg -> "arg" | Def -> "def" | ExtArg -> "extarg")) l) mentions;
  (*debug_flag := false;*)
  
  let fold_delta i  = (fun a (r, b) ->
    a +
      if (class_r r) <> i then 0 else
        (if b = Arg then
           let ri = Hashtbl.find counts r in
           if (ri.rg_class = i && ri.nb_read = 1)
           then 1 else 0
         else
           match Hashtbl.find_opt live_regs r with
           | None -> -1
           | Some t -> 0
  )) in
  
  let priorities = critical_paths successors in
  
  let current_resources = Array.copy problem.resource_bounds in

  let module InstrSet =
    struct 
      module MSet = 
        Set.Make (struct
            type t=int
            let compare x y =
              match priorities.(y) - priorities.(x) with
              | 0 -> x - y
              | z -> z
          end)

      let empty = MSet.empty
      let is_empty = MSet.is_empty
      let add = MSet.add
      let remove = MSet.remove
      let union = MSet.union
      let iter = MSet.iter

      let compare_regs i x y =
        let pyi = List.fold_left (fold_delta i) 0 mentions.(y) in
        (* print_int y;
         * print_string " ";
         * print_int pyi;
         * print_newline ();
         * flush stdout; *)
        let pxi = List.fold_left (fold_delta i) 0 mentions.(x) in
        match pyi - pxi with
        | 0 -> (match priorities.(y) - priorities.(x) with
               | 0 -> x - y
               | z -> z)
        | z -> z

      (** t is the register class *)
      let sched_CSR t ready usages =
        (* print_string "looking for max delta";
         * print_newline ();
         * flush stdout; *)
        let result = ref (-1) in
        iter (fun i ->
            if vector_less_equal usages.(i) current_resources
            then if !result = -1 || (compare_regs t !result i > 0)
                 then result := i) ready;
        !result
    end
  in

  let max_time = bound_max_time problem + 5*nr_instructions in
  let ready = Array.make max_time InstrSet.empty in

  Array.iteri (fun i preds ->
      if i < nr_instructions && preds = []
      then ready.(0) <- InstrSet.add i ready.(0)) predecessors;

  let current_time = ref 0
  and earliest_time i =
    try
      let time = ref (-1) in
      List.iter (fun (j, latency) ->
          if times.(j) < 0
          then raise Exit
          else let t = times.(j) + latency in
               if t > !time
               then time := t) predecessors.(i);
      assert (!time >= 0);
      !time
    with Exit -> -1
  in

  let advance_time () =
    (if !current_time < max_time-1
     then (
       Array.blit problem.resource_bounds 0 current_resources 0
         (Array.length current_resources);
       ready.(!current_time + 1) <-
         InstrSet.union (ready.(!current_time))
           (ready.(!current_time +1));
       ready.(!current_time) <- InstrSet.empty));
    incr current_time
  in

  (* ALL MENTIONS TO cnt ARE PLACEHOLDERS *)
  let cnt = ref 0 in

  let attempt_scheduling ready usages =
    let result = ref (-1) in
    DebugPrint.debug "\n\nREADY: ";
    InstrSet.iter (fun i -> DebugPrint.debug "%d " i) ready;
    DebugPrint.debug "\n\ntime: %d" !current_time;
    DebugPrint.debug "\n\n";
    try
      Array.iteri (fun i avlregs ->
          DebugPrint.debug "avlregs: %d %d\nlive regs: %d\n"
            i avlregs (Hashtbl.length live_regs); 
          if !cnt < 5 && avlregs <= regs_thresholds.(i)
          then (
            csr_b := true;
            let maybe = InstrSet.sched_CSR i ready usages in
            DebugPrint.debug "maybe %d\n" maybe;
            (if maybe >= 0 &&
                  let delta = 
                    List.fold_left (fold_delta i) 0 mentions.(maybe) in
                  DebugPrint.debug "delta %d\n" delta;
                  delta > 0
             then
               (vector_subtract usages.(maybe) current_resources;
                result := maybe)
             else
               if not !Clflags.option_regpres_wait_window
               then
                 (InstrSet.iter (fun ins ->
                      if vector_less_equal usages.(ins) current_resources 
                         && List.fold_left (fold_delta i) 0 mentions.(maybe) >= 0
                      then (result := ins;
                            vector_subtract usages.(!result) current_resources;
                            raise Exit)
                    ) ready;
                  if !result <> -1 then
                    vector_subtract usages.(!result) current_resources
                  else incr cnt)
               else
                 (incr cnt)
            );
            raise Exit)) available_regs;
      InstrSet.iter (fun i ->
          if vector_less_equal usages.(i) current_resources
          then (
            vector_subtract usages.(i) current_resources;
            result := i;
            raise Exit)) ready;
      -1
    with Exit ->
      !result in
  
  while !current_time < max_time
  do
    if (InstrSet.is_empty ready.(!current_time))
    then advance_time ()
    else
      match attempt_scheduling ready.(!current_time)
              problem.instruction_usages with
      | -1 -> advance_time()
      | i -> (assert(times.(i) < 0);
             (DebugPrint.debug "INSTR ISSUED: %d\n" i;
              if !csr_b && !Clflags.option_debug_compcert > 6 then
                (Printf.eprintf "REGPRES: high pres class %d\n" i;
                 flush stderr);
              csr_b := false;
              (* if !Clflags.option_regpres_wait_window then *)
              cnt := 0;
              List.iter (fun (r,b) ->
                match b with
                | Arg ->
                    (match Hashtbl.find_opt counts r with
                     | None -> assert false
                     | Some ri ->
                        Hashtbl.remove counts r;
                        if ri.nb_read = 1 then
                          (Hashtbl.remove live_regs r;
                           available_regs.(ri.rg_class) <- available_regs.(ri.rg_class) + 1))
                | Def ->
                    let r_info = Hashtbl.find counts r in
                    let t = r_info.rg_class in
                    (match Hashtbl.find_opt live_regs r with
                     | None ->
                         (Hashtbl.add live_regs r t;
                          available_regs.(t) <- available_regs.(t) - 1)
                     | Some i -> ())
                | _ -> ()) mentions.(i));
              times.(i) <- !current_time;
              ready.(!current_time) <- InstrSet.remove i (ready.(!current_time));
              List.iter (fun (instr_to, latency) ->
                if instr_to < nr_instructions then
                  match earliest_time instr_to with
                  | -1 -> ()
                  | to_time ->
                    ((* DebugPrint.debug "TO TIME %d : %d\n" to_time
                      *   (Array.length ready); *)
                     ready.(to_time)
                     <- InstrSet.add instr_to ready.(to_time))) successors.(i);
              successors.(i) <- [])
  done;
  (*debug_flag := true;*)

  (*debug_flag := true;*)
  try
    let final_time = ref (-1) in
    debug "\n\nreg_pres schedule:\n";
    for i = 0 to nr_instructions - 1 do
      debug "%d: %d\n" i times.(i);
      (if times.(i) < 0 then raise Exit);
      (if !final_time < times.(i) + 1 then final_time := times.(i) + 1)
    done;
    List.iter (fun (i, latency) ->
        let target_time = latency + times.(i) in
        if target_time > !final_time then
          final_time := target_time) predecessors.(nr_instructions);
    times.(nr_instructions) <- !final_time;
    (*debug_flag := false;*)
    Some (times, None)
  with Exit ->
    debug "reg_pres_sched failed\n";
    (*debug_flag := false;*)
    (*None;;*)
    assert false;;

(********************************************************************)

type bundle = int list;;

let rec extract_deps_to index = function
  | [] -> []
  | dep :: deps -> let extracts = extract_deps_to index deps in
      if (dep.instr_to == index) then 
        dep :: extracts
      else
        extracts

exception InvalidBundle;;

let dependency_check problem bundle index =
  let index_deps = extract_deps_to index problem.latency_constraints in
  List.iter (fun i -> 
    List.iter (fun dep ->
      if (dep.instr_from == i) then raise InvalidBundle
    ) index_deps
  ) bundle;;

let rec make_bundle problem resources bundle index =
  let resources_copy = Array.copy resources in
  let nr_instructions = get_nr_instructions problem in
  if (index >= nr_instructions) then (bundle, index+1) else
  let inst_usage = problem.instruction_usages.(index) in
  try match vector_less_equal inst_usage resources with
  | false -> raise InvalidBundle
  | true -> (
      dependency_check problem bundle index;
      vector_subtract problem.instruction_usages.(index) resources_copy;
      make_bundle problem resources_copy (index::bundle) (index+1)
      )
  with InvalidBundle -> (bundle, index);;

let rec make_bundles problem index : bundle list =
  if index >= get_nr_instructions problem then
    []
  else
    let (bundle, new_index) = make_bundle problem problem.resource_bounds [] index in
    bundle :: (make_bundles problem new_index);;

let bundles_to_schedule problem bundles : solution =
  let nr_instructions = get_nr_instructions problem in
  let schedule = Array.make (nr_instructions+1) (nr_instructions+4) in
  let time = ref 0 in
  List.iter (fun bundle ->
    begin
    List.iter (fun i ->
      schedule.(i) <- !time
    ) bundle;
    time := !time + 1
    end
  ) bundles; (schedule, None);;

let greedy_scheduler (problem : problem) : solution option =
  let bundles = make_bundles problem 0 in
  Some (bundles_to_schedule problem bundles);;
  
(* alternate implementation
let swap_array_elements a i j =
  let x = a.(i) in
  a.(i) <- a.(j);
  a.(j) <- x;;

let array_reverse_slice a first last =
  let i = ref first and j = ref last in
  while i < j
  do
    swap_array_elements a !i !j;
    incr i;
    decr j
  done;;

let array_reverse a =
  let a' = Array.copy a in
  array_reverse_slice a' 0 ((Array.length a)-1);
  a';;
 *)

(* unneeded
let array_reverse a =
  let n=Array.length a in
  Array.init n (fun i -> a.(n-1-i));;
 *)

let reverse_constraint nr_instructions ctr =
  { instr_to = nr_instructions -ctr.instr_from;
    instr_from =  nr_instructions - ctr.instr_to;
    latency = ctr.latency };;

(* unneeded
let rec list_map_filter f = function
  | [] ->  []                                      
  | h::t ->
     (match f h with
      | None ->  list_map_filter f t
      | Some x -> x :: (list_map_filter f t));;
 *)

let reverse_problem problem =
  let nr_instructions = get_nr_instructions problem in
  {
    max_latency = problem.max_latency;
    resource_bounds = problem.resource_bounds;
    live_regs_entry = Registers.Regset.empty; (* PLACEHOLDER *)
    typing = problem.typing;
    (* Not needed for the revlist sched, and for now we wont bother
       with creating a reverse scheduler aware of reg press *)
    reference_counting = problem.reference_counting;
    instruction_usages = Array.init (nr_instructions + 1)
      (fun i ->
        if i=0
        then Array.map (fun _ -> 0) problem.resource_bounds                             else problem.instruction_usages.(nr_instructions - i));
    latency_constraints = List.map (reverse_constraint nr_instructions)
                            problem.latency_constraints
  };;

let max_scheduled_time solution =
  let time = ref (-1) in
  for i = 0 to ((Array.length solution) - 2)
  do
    time := max !time solution.(i)
  done;
  !time;;

(* dubious
let recompute_makespan problem solution =
  let n = (Array.length solution) - 1 and ms = ref 0 in
  List.iter (fun cstr ->
      if cstr.instr_to = n
      then ms := max !ms (solution.(cstr.instr_from) + cstr.latency) 
    ) problem.latency_constraints;
  !ms;;
 *)

let recompute_makespan problem solution =
  let predecessors = get_predecessors problem
  and nr_instructions = Array.length solution - 1 in
  try
    let final_time = ref (-1) in
    for i=0 to nr_instructions-1
    do
      (if solution.(i) < 0 then raise Exit);
      (if !final_time < solution.(i)+1 then final_time := solution.(i)+1)
    done;
    List.iter (fun (i, latency) ->
        let target_time = latency + solution.(i) in
        if target_time > !final_time
        then final_time := target_time
      ) predecessors.(nr_instructions);
    Some !final_time
  with Exit -> None

let schedule_reversed (scheduler : problem -> solution option)
    (problem : problem) : solution option =
  match scheduler (reverse_problem problem) with
  | None -> None
  | Some (solution, _) ->
     let nr_instructions = get_nr_instructions problem in
     let makespan = max_scheduled_time solution in
     let ret = Array.init (nr_instructions + 1)
                 (fun i -> makespan-solution.(nr_instructions-i)) in
     ret.(nr_instructions) <- max ((max_scheduled_time ret) + 1)
                                (ret.(nr_instructions));
     Some (ret, None);;

(** Schedule the problem using a greedy list scheduling algorithm, from the end. *)
let reverse_list_scheduler = schedule_reversed list_scheduler;;

let check_problem problem =
  (if (Array.length problem.instruction_usages) < 1
   then failwith "length(problem.instruction_usages) < 1");;

let validated_scheduler (scheduler : problem -> solution option)
      (problem : problem) =
  check_problem problem;
  match scheduler problem with
  | None -> None
  | (Some solution) as ret -> check_schedule problem solution; ret;;

let get_max_latency solution =
  solution.((Array.length solution)-1);;
  
let show_date_ranges problem =
  let deadline = problem.max_latency in
  assert(deadline >= 0);
  let successors = get_successors problem
  and predecessors = get_predecessors problem in
  let earliest_dates : int array = get_earliest_dates predecessors
  and latest_dates : int array = get_latest_dates deadline successors in
  assert ((Array.length earliest_dates) =
            (Array.length latest_dates));
  Array.iteri (fun i early ->
      let late = latest_dates.(i) in
      Printf.printf "t[%d] in %d..%d\n" i early late)
    earliest_dates;;

type pseudo_boolean_problem_type =
  | SATISFIABILITY
  | OPTIMIZATION;;

type pseudo_boolean_mapper = {
  mapper_pb_type : pseudo_boolean_problem_type;
  mapper_nr_instructions : int;
  mapper_nr_pb_variables : int;
  mapper_earliest_dates : int array;
  mapper_latest_dates : int array;
  mapper_var_offsets : int array;
  mapper_final_predecessors : (int * int) list
};;

(* Latency constraints are:
  presence of instr-to at each t <= sum of presences of instr-from at compatible times
  
  if reverse_encoding
  presence of instr-from at each t <= sum of presences of instr-to at compatible times *)

(* Experiments show reverse_encoding=true multiplies time by 2 in sat4j
   without making hard instances easier *)
let direct_encoding = false
and reverse_encoding = false
and delta_encoding = true
                   
let pseudo_boolean_print_problem channel problem pb_type =
  let deadline = problem.max_latency in
  assert (deadline > 0);
  let nr_instructions = get_nr_instructions problem
  and nr_resources = get_nr_resources problem
  and successors = get_successors problem
  and predecessors = get_predecessors problem in
  let earliest_dates = get_earliest_dates predecessors
  and latest_dates = get_latest_dates deadline successors in
  let var_offsets = Array.make
                      (match pb_type with
                       | OPTIMIZATION -> nr_instructions+1
                       | SATISFIABILITY -> nr_instructions) 0 in
  let nr_pb_variables =
    (let nr = ref 0 in
     for i=0 to (match pb_type with
                 | OPTIMIZATION -> nr_instructions
                 | SATISFIABILITY -> nr_instructions-1)
     do
        var_offsets.(i) <- !nr;
        nr := !nr + latest_dates.(i) - earliest_dates.(i) + 1
     done;
     !nr)
  and nr_pb_constraints =
    (match pb_type with
     | OPTIMIZATION -> nr_instructions+1
     | SATISFIABILITY -> nr_instructions) +

    (let count = ref 0 in
     for t=0 to deadline-1
     do
       for j=0 to nr_resources-1
       do
         try
           for i=0 to nr_instructions-1
           do
             let usage = problem.instruction_usages.(i).(j) in
             if t >= earliest_dates.(i) && t <= latest_dates.(i)
                && usage > 0 then raise Exit
           done
         with Exit -> incr count
       done
     done;
     !count) +
      
     (let count=ref 0 in
      List.iter
        (fun ctr ->
          if ctr.instr_to < nr_instructions
          then count := !count + 1 + latest_dates.(ctr.instr_to)
                                   - earliest_dates.(ctr.instr_to)
                    + (if reverse_encoding
                       then 1 + latest_dates.(ctr.instr_from)
                -      earliest_dates.(ctr.instr_from)
                       else 0)
        )
        problem.latency_constraints;
      !count) +
      
      (match pb_type with
       | OPTIMIZATION -> (1 + deadline - earliest_dates.(nr_instructions)) * nr_instructions
       | SATISFIABILITY -> 0)
  and measured_nr_constraints = ref 0 in

  let pb_var i t =
    assert(t >= earliest_dates.(i));
    assert(t <= latest_dates.(i));
    let v = 1+var_offsets.(i)+t-earliest_dates.(i) in
    assert(v <= nr_pb_variables);
    Printf.sprintf "x%d" v in
  
  let end_constraint () =
    begin
      output_string channel ";\n";
      incr measured_nr_constraints
    end in

  let gen_latency_constraint i_to i_from latency t_to =
    Printf.fprintf channel "* t[%d] - t[%d] >= %d when t[%d]=%d\n"
		   i_to i_from latency i_to t_to;
        for t_from=earliest_dates.(i_from) to
              int_min latest_dates.(i_from) (t_to - latency)
        do
          Printf.fprintf channel "+1 %s " (pb_var i_from t_from)
        done;
        Printf.fprintf channel "-1 %s " (pb_var i_to t_to);
        Printf.fprintf channel ">= 0";
        end_constraint()

  and gen_dual_latency_constraint i_to i_from latency t_from =
    Printf.fprintf channel "* t[%d] - t[%d] >= %d when t[%d]=%d\n"
		   i_to i_from latency i_to t_from;
        for t_to=int_max earliest_dates.(i_to) (t_from + latency)
            to latest_dates.(i_to)
        do
          Printf.fprintf channel "+1 %s " (pb_var i_to t_to)
        done;
        Printf.fprintf channel "-1 %s " (pb_var i_from t_from);
        Printf.fprintf channel ">= 0";
        end_constraint()
  in
  
  Printf.fprintf channel "* #variable= %d #constraint= %d\n" nr_pb_variables nr_pb_constraints;
  Printf.fprintf channel "* nr_instructions=%d deadline=%d\n" nr_instructions deadline;
  begin
    match pb_type with
    | SATISFIABILITY -> ()
    | OPTIMIZATION ->
       output_string channel "min:";
       for t=earliest_dates.(nr_instructions) to deadline
       do
         Printf.fprintf channel " %+d %s" t (pb_var nr_instructions t)
       done;
       output_string channel ";\n";
  end;
  for i=0 to (match pb_type with
              | OPTIMIZATION -> nr_instructions
              | SATISFIABILITY -> nr_instructions-1)
  do
    let early = earliest_dates.(i) and late= latest_dates.(i) in
    Printf.fprintf channel "* t[%d] in %d..%d\n" i early late;
    for t=early to late
    do
      Printf.fprintf channel "+1 %s " (pb_var i t)
    done;
    Printf.fprintf channel "= 1";
    end_constraint()
  done;

  for t=0 to deadline-1
  do
    for j=0 to nr_resources-1
    do
      let bound = problem.resource_bounds.(j)
      and coeffs = ref [] in
      for i=0 to nr_instructions-1
      do
        let usage = problem.instruction_usages.(i).(j) in
        if t >= earliest_dates.(i) && t <= latest_dates.(i)
           && usage > 0
        then coeffs := (i, usage) :: !coeffs 
      done;
      if !coeffs <> [] then
        begin
          Printf.fprintf channel "* resource #%d at t=%d <= %d\n" j t bound;
          List.iter (fun (i, usage) ->
              Printf.fprintf channel "%+d %s " (-usage) (pb_var i t)) !coeffs;
          Printf.fprintf channel ">= %d" (-bound);
          end_constraint();
        end
    done
  done;
  
  List.iter
    (fun ctr ->
      if ctr.instr_to < nr_instructions then
        begin
          for t_to=earliest_dates.(ctr.instr_to) to latest_dates.(ctr.instr_to)
          do
	    gen_latency_constraint ctr.instr_to ctr.instr_from ctr.latency t_to
          done;
          if reverse_encoding
          then
            for t_from=earliest_dates.(ctr.instr_from) to latest_dates.(ctr.instr_from)
            do
	      gen_dual_latency_constraint ctr.instr_to ctr.instr_from ctr.latency t_from
            done
        end
    ) problem.latency_constraints;
  
  begin
    match pb_type with
    | SATISFIABILITY -> ()
    | OPTIMIZATION ->
       let final_latencies = Array.make nr_instructions 1 in
       List.iter (fun (i, latency) ->
	   final_latencies.(i) <- int_max final_latencies.(i) latency)
	 predecessors.(nr_instructions);
       for t_to=earliest_dates.(nr_instructions) to deadline
       do
         for i_from = 0 to nr_instructions -1
         do
	   gen_latency_constraint nr_instructions i_from final_latencies.(i_from) t_to
         done
       done
  end;
  assert (!measured_nr_constraints = nr_pb_constraints);
  {
    mapper_pb_type = pb_type;
    mapper_nr_instructions = nr_instructions;
    mapper_nr_pb_variables = nr_pb_variables;
    mapper_earliest_dates = earliest_dates;
    mapper_latest_dates = latest_dates;
    mapper_var_offsets = var_offsets;
    mapper_final_predecessors = predecessors.(nr_instructions)
  };;

type pb_answer =
  | Positive
  | Negative
  | Unknown
      
let line_to_pb_solution sol line nr_pb_variables =
  let assign s v =
    begin
      let i = int_of_string s in
      sol.(i-1) <- v
    end in
  List.iter
    begin
      function "" -> ()
	     | item ->
		(match String.get item 0 with
		 | '+' ->
		    assert ((String.length item) >= 3);
		    assert ((String.get item 1) = 'x');
		    assign (String.sub item 2 ((String.length item)-2)) Positive
		 | '-' ->
		    assert ((String.length item) >= 3);
		    assert ((String.get item 1) = 'x');
		    assign (String.sub item 2 ((String.length item)-2)) Negative
		 | 'x' ->
		    assert ((String.length item) >= 2);
		    assign (String.sub item 1 ((String.length item)-1)) Positive
                 | _ -> failwith "syntax error in pseudo Boolean solution: epected + - or x" 
		)
    end
    (String.split_on_char ' ' (String.sub line 2 ((String.length line)-2)));;

let pb_solution_to_schedule mapper pb_solution =
  Array.mapi (fun i offset ->
	      let first = mapper.mapper_earliest_dates.(i)
	      and last = mapper.mapper_latest_dates.(i)
	      and time = ref (-1) in
	      for t=first to last
	      do
		match pb_solution.(t - first + offset) with
		| Positive ->
		   (if !time = -1
		    then time:=t
		    else failwith "duplicate time in pseudo boolean solution")
		| Negative -> ()
		| Unknown -> failwith "unknown value in pseudo boolean solution"
	      done;
	      (if !time = -1
	       then failwith "no time in pseudo boolean solution");
	      !time
	    ) mapper.mapper_var_offsets;;
  
let pseudo_boolean_read_solution mapper channel =
  let optimum = ref (-1)
  and optimum_found = ref false
  and solution = Array.make mapper.mapper_nr_pb_variables Unknown in
  try
    while true do
      match input_line channel with
      | "" -> ()
      | line ->
	 begin
	   match String.get line 0 with
	   | 'c' -> ()
	   | 'o' ->
	      assert ((String.length line) >= 2);
	      assert ((String.get line 1) = ' ');
	      optimum := int_of_string (String.sub line 2 ((String.length line)-2))
	   | 's' -> (match line with
		     | "s OPTIMUM FOUND" -> optimum_found := true
		     | "s SATISFIABLE" -> ()
		     | "s UNSATISFIABLE" -> close_in channel;
		                            raise Unschedulable
                     | _ -> failwith line)
	   | 'v' -> line_to_pb_solution solution line mapper.mapper_nr_pb_variables
	   | x -> Printf.printf "unknown: %s\n" line
	 end
    done;
    assert false
  with End_of_file ->
       close_in channel;
       begin
	 let sol = pb_solution_to_schedule mapper solution in
	 sol
       end;;

let recompute_max_latency mapper solution =
  let maxi = ref (-1) in
  for i=0 to (mapper.mapper_nr_instructions-1)
  do
    maxi := int_max !maxi (1+solution.(i))
  done;
  List.iter (fun (i, latency) ->
      maxi := int_max !maxi (solution.(i) + latency)) mapper.mapper_final_predecessors;
  !maxi;;
  
let adjust_check_solution mapper solution =
  match mapper.mapper_pb_type with
  | OPTIMIZATION ->
     let max_latency = recompute_max_latency mapper solution in
     assert (max_latency = solution.(mapper.mapper_nr_instructions));
     solution
  | SATISFIABILITY ->
     let max_latency = recompute_max_latency mapper solution in
     Array.init (mapper.mapper_nr_instructions+1)
       (fun i -> if i < mapper.mapper_nr_instructions
                 then solution.(i)
                 else max_latency);;

(* let pseudo_boolean_solver = ref "/local/monniaux/progs/naps/naps" *)
(* let pseudo_boolean_solver = ref "/local/monniaux/packages/sat4j/org.sat4j.pb.jar CuttingPlanes" *)

(* let pseudo_boolean_solver = ref "java -jar /usr/share/java/org.sat4j.pb.jar CuttingPlanes" *)
(* let pseudo_boolean_solver = ref "java -jar /usr/share/java/org.sat4j.pb.jar" *)
(* let pseudo_boolean_solver = ref "clasp" *)
(* let pseudo_boolean_solver = ref "/home/monniaux/progs/CP/open-wbo/open-wbo_static -formula=1" *)
(* let pseudo_boolean_solver = ref "/home/monniaux/progs/CP/naps/naps" *)
(* let pseudo_boolean_solver = ref "/home/monniaux/progs/CP/minisatp/build/release/bin/minisatp" *)
(* let pseudo_boolean_solver = ref "java -jar sat4j-pb.jar CuttingPlanesStar" *)
let pseudo_boolean_solver = ref "pb_solver"

let pseudo_boolean_scheduler pb_type problem : solution option =
  try
    let filename_in = "problem.opb" in
    (* needed only if not using stdout and filename_out = "problem.sol" *)
    let mapper =
      with_out_channel (open_out filename_in)
        (fun opb_problem ->
          pseudo_boolean_print_problem opb_problem problem pb_type) in
    Some (with_in_channel
      (Unix.open_process_in (!pseudo_boolean_solver ^ " " ^ filename_in))
      (fun opb_solution -> adjust_check_solution mapper (pseudo_boolean_read_solution mapper opb_solution)), None)
  with
  | Unschedulable -> None;;
				 
let rec reoptimizing_scheduler (scheduler : scheduler) (previous_solution : solution) (problem : problem) =
  let previous_times, _ = previous_solution in
  if (get_max_latency previous_times)>1 then
    begin
      Printf.printf "reoptimizing < %d\n" (get_max_latency previous_times);
      flush stdout;
      match scheduler
              { problem with max_latency = (get_max_latency previous_times)-1 }
      with
      | None -> previous_solution
      | Some solution -> reoptimizing_scheduler scheduler solution problem
    end
  else previous_solution;;

let smt_var i = Printf.sprintf "t%d" i

let is_resource_used problem j =
  try
    Array.iter (fun usages ->
        if usages.(j) > 0
        then raise Exit) problem.instruction_usages;
    false
  with Exit -> true;;

let smt_use_quantifiers = false
                        
let smt_print_problem channel problem =
  let nr_instructions = get_nr_instructions problem in
  let gen_smt_resource_constraint time j =
        output_string channel "(<= (+";
        Array.iteri
          (fun i usages ->
            let usage=usages.(j) in
            if usage > 0
            then Printf.fprintf channel " (ite (= %s %s) %d 0)"
                   time (smt_var i) usage)
          problem.instruction_usages;
        Printf.fprintf channel ") %d)" problem.resource_bounds.(j)
  in
  output_string channel "(set-option :produce-models true)\n"; 
  for i=0 to nr_instructions
  do
    Printf.fprintf channel "(declare-const %s Int)\n" (smt_var i);
    Printf.fprintf channel "(assert (>= %s 0))\n" (smt_var i)
  done;
  for i=0 to nr_instructions-1
  do
    Printf.fprintf channel "(assert (< %s %s))\n"
      (smt_var i) (smt_var nr_instructions)
  done;
  (if problem.max_latency > 0
   then Printf.fprintf channel "(assert (<= %s %d))\n"
          (smt_var nr_instructions) problem.max_latency);
  List.iter (fun ctr ->
      Printf.fprintf channel "(assert (>= (- %s %s) %d))\n"
        (smt_var ctr.instr_to)
        (smt_var ctr.instr_from)
        ctr.latency) problem.latency_constraints;
  for j=0 to (Array.length problem.resource_bounds)-1
  do
    if is_resource_used problem j
    then
      begin
        if smt_use_quantifiers
        then
          begin
            Printf.fprintf channel
              "; resource #%d <= %d\n(assert (forall ((t Int)) "
              j problem.resource_bounds.(j);
            gen_smt_resource_constraint "t" j;
            output_string channel "))\n"
          end
        else
          begin
            (if problem.max_latency < 0
             then failwith "quantifier explosion needs max latency");
            for t=0 to problem.max_latency
            do
              Printf.fprintf channel
                "; resource #%d <= %d at t=%d\n(assert "
                j problem.resource_bounds.(j) t;
              gen_smt_resource_constraint (string_of_int t) j;
              output_string channel ")\n"
            done
          end
      end
  done;
  output_string channel "(check-sat)(get-model)\n";;
      
                  
let ilp_print_problem channel problem pb_type =
  let deadline = problem.max_latency in
  assert (deadline > 0);
  let nr_instructions = get_nr_instructions problem
  and nr_resources = get_nr_resources problem
  and successors = get_successors problem
  and predecessors = get_predecessors problem in
  let earliest_dates = get_earliest_dates predecessors
  and latest_dates = get_latest_dates deadline successors in

  let pb_var i t =
    Printf.sprintf "x%d_%d" i t in

  let gen_latency_constraint i_to i_from latency t_to =
    Printf.fprintf channel "\\ t[%d] - t[%d] >= %d when t[%d]=%d\n"
		   i_to i_from latency i_to t_to;
    Printf.fprintf channel "c_%d_%d_%d_%d: "
		   i_to i_from latency t_to;
        for t_from=earliest_dates.(i_from) to
              int_min latest_dates.(i_from) (t_to - latency)
        do
          Printf.fprintf channel "+1 %s " (pb_var i_from t_from)
        done;
        Printf.fprintf channel "-1 %s " (pb_var i_to t_to);
        output_string channel ">= 0\n"

  and gen_dual_latency_constraint i_to i_from latency t_from =
    Printf.fprintf channel "\\ t[%d] - t[%d] >= %d when t[%d]=%d\n"
      i_to i_from latency i_to t_from;
    Printf.fprintf channel "d_%d_%d_%d_%d: "
      i_to i_from latency t_from;
        for t_to=int_max earliest_dates.(i_to) (t_from + latency)
            to latest_dates.(i_to)
        do
          Printf.fprintf channel "+1 %s " (pb_var i_to t_to)
        done;
        Printf.fprintf channel "-1 %s " (pb_var i_from t_from);
        Printf.fprintf channel ">= 0\n"

  and gen_delta_constraint i_from i_to latency =
    if delta_encoding
    then  Printf.fprintf channel "l_%d_%d_%d: +1 t%d -1 t%d >= %d\n"
            i_from i_to latency i_to i_from latency

  in
  
  Printf.fprintf channel "\\ nr_instructions=%d deadline=%d\n" nr_instructions deadline;
  begin
    match pb_type with
    | SATISFIABILITY -> output_string channel "Minimize dummy: 0\n"
    | OPTIMIZATION ->
       Printf.fprintf channel "Minimize\nmakespan: t%d\n" nr_instructions
  end;
  output_string channel "Subject To\n";
  for i=0 to (match pb_type with
              | OPTIMIZATION -> nr_instructions
              | SATISFIABILITY -> nr_instructions-1)
  do
    let early = earliest_dates.(i) and late= latest_dates.(i) in
    Printf.fprintf channel "\\ t[%d] in %d..%d\ntimes%d: " i early late i;
    for t=early to late
    do
      Printf.fprintf channel "+1 %s " (pb_var i t)
    done;
    Printf.fprintf channel "= 1\n"
  done;

  for t=0 to deadline-1
  do
    for j=0 to nr_resources-1
    do
      let bound = problem.resource_bounds.(j)
      and coeffs = ref [] in
      for i=0 to nr_instructions-1
      do
        let usage = problem.instruction_usages.(i).(j) in
        if t >= earliest_dates.(i) && t <= latest_dates.(i)
           && usage > 0
        then coeffs := (i, usage) :: !coeffs 
      done;
      if !coeffs <> [] then
        begin
          Printf.fprintf channel "\\ resource #%d at t=%d <= %d\nr%d_%d: " j t bound j t;
          List.iter (fun (i, usage) ->
              Printf.fprintf channel "%+d %s " (-usage) (pb_var i t)) !coeffs;
          Printf.fprintf channel ">= %d\n" (-bound)
        end
    done
  done;
  
  List.iter
    (fun ctr ->
      if ctr.instr_to < nr_instructions then
        begin
          gen_delta_constraint ctr.instr_from ctr.instr_to ctr.latency;
          begin
            if direct_encoding
            then
              for t_to=earliest_dates.(ctr.instr_to) to latest_dates.(ctr.instr_to)
              do
	        gen_latency_constraint ctr.instr_to ctr.instr_from ctr.latency t_to
              done
          end;
          begin
            if reverse_encoding
            then
              for t_from=earliest_dates.(ctr.instr_from) to latest_dates.(ctr.instr_from)
              do
	        gen_dual_latency_constraint ctr.instr_to ctr.instr_from ctr.latency t_from
              done
          end
        end
    ) problem.latency_constraints;
  
  begin
    match pb_type with
    | SATISFIABILITY -> ()
    | OPTIMIZATION ->
       let final_latencies = Array.make nr_instructions 1 in
       List.iter (fun (i, latency) ->
	   final_latencies.(i) <- int_max final_latencies.(i) latency)
	 predecessors.(nr_instructions);
       for i_from = 0 to nr_instructions -1
       do
	 gen_delta_constraint i_from nr_instructions final_latencies.(i_from)
       done;
       for t_to=earliest_dates.(nr_instructions) to deadline
       do
         for i_from = 0 to nr_instructions -1
         do
	   gen_latency_constraint nr_instructions i_from final_latencies.(i_from) t_to
         done
       done
  end;
  for i=0 to (match pb_type with
              | OPTIMIZATION -> nr_instructions
              | SATISFIABILITY -> nr_instructions-1)
  do
    Printf.fprintf channel "ct%d : -1 t%d" i i;
    let early = earliest_dates.(i) and late= latest_dates.(i) in
    for t=early to late do
      Printf.fprintf channel " +%d %s" t (pb_var i t)
    done;
    output_string channel " = 0\n"
  done;
  output_string channel "Bounds\n";
  for i=0 to (match pb_type with
              | OPTIMIZATION -> nr_instructions
              | SATISFIABILITY -> nr_instructions-1)
  do
    let early = earliest_dates.(i) and late= latest_dates.(i) in
    begin
      Printf.fprintf channel "%d <= t%d <= %d\n" early i late;
      if true then
        for t=early to late do
          Printf.fprintf channel "0 <= %s <= 1\n" (pb_var i t)
        done
    end
  done;
  output_string channel "Integer\n";
  for i=0 to (match pb_type with
              | OPTIMIZATION -> nr_instructions
              | SATISFIABILITY -> nr_instructions-1)
  do
    Printf.fprintf channel "t%d " i
  done;
  output_string channel "\nBinary\n";
  for i=0 to (match pb_type with
              | OPTIMIZATION -> nr_instructions
              | SATISFIABILITY -> nr_instructions-1)
  do
    let early = earliest_dates.(i) and late= latest_dates.(i) in
    for t=early to late do
      output_string channel (pb_var i t);
      output_string channel " "
    done;
    output_string channel "\n"
  done;
  output_string channel "End\n";
  {
    mapper_pb_type = pb_type;
    mapper_nr_instructions = nr_instructions;
    mapper_nr_pb_variables = 0;
    mapper_earliest_dates = earliest_dates;
    mapper_latest_dates = latest_dates;
    mapper_var_offsets = [| |];
    mapper_final_predecessors = predecessors.(nr_instructions)
  };;

(* Guess what? Cplex sometimes outputs 11.000000004 instead of integer 11 *)

let positive_float_round x = truncate (x +. 0.5)
                           
let float_round (x : float) : int =
  if x > 0.0
  then positive_float_round x
  else - (positive_float_round (-. x))
  
let rounded_int_of_string x =  float_round (float_of_string x)
                             
let ilp_read_solution mapper channel =
  let times = Array.make
                (match mapper.mapper_pb_type with
                 | OPTIMIZATION -> 1+mapper.mapper_nr_instructions
                 | SATISFIABILITY -> mapper.mapper_nr_instructions) (-1) in
  try
    while true do
      let line = input_line channel in
      ( if (String.length line) < 3
        then failwith (Printf.sprintf "bad ilp output: length(line) < 3: %s" line));
      match String.get line 0 with
      | 'x' -> ()
      | 't' -> let space =
                 try String.index line ' '
                 with Not_found ->
                   failwith "bad ilp output: no t variable number"
               in
               let tnumber =
                 try int_of_string (String.sub line 1 (space-1))
                 with Failure _ ->
                   failwith "bad ilp output: not a variable number"
               in
               (if tnumber < 0 || tnumber >= (Array.length times)
                then failwith (Printf.sprintf "bad ilp output: not a correct variable number: %d (%d)" tnumber (Array.length times)));
               let value =
                 let s = String.sub line (space+1) ((String.length line)-space-1) in
                 try rounded_int_of_string s
                 with Failure _ ->
                   failwith (Printf.sprintf "bad ilp output: not a time number (%s)" s)
               in
               (if value < 0
                then failwith "bad ilp output: negative time");
               times.(tnumber) <- value
      | '#' -> ()
      | '0' -> ()
      | _ -> failwith (Printf.sprintf "bad ilp output: bad variable initial, line = %s" line)
    done;
    assert false
  with End_of_file ->
    Array.iteri (fun i x ->
        if i<(Array.length times)-1
           && x<0 then raise Unschedulable) times;
    times;;

let ilp_solver = ref "ilp_solver"

let problem_nr = ref 0

let ilp_scheduler pb_type problem : solution option =
  try
    let filename_in = Printf.sprintf  "problem%05d.lp" !problem_nr
    and filename_out = Printf.sprintf "problem%05d.sol" !problem_nr in
    incr problem_nr;
    let mapper = with_out_channel (open_out filename_in)
      (fun opb_problem -> ilp_print_problem opb_problem problem pb_type) in

    begin
      match Unix.system (!ilp_solver ^ " " ^ filename_in ^ " " ^ filename_out) with
      | Unix.WEXITED 0 ->
         Some (with_in_channel
                 (open_in filename_out)
                 (fun opb_solution ->
                   adjust_check_solution mapper
                     (ilp_read_solution mapper opb_solution)), None)
      | Unix.WEXITED _ -> failwith "failed to start ilp solver"
      | _ -> None
    end
  with
  | Unschedulable -> None;;

let current_utime_all () =
  let t = Unix.times() in
  t.Unix.tms_cutime +. t.Unix.tms_utime;;

let utime_all_fn fn arg =
  let utime_start = current_utime_all () in
  let output = fn arg in
  let utime_end = current_utime_all () in
  (output, utime_end -. utime_start);;
    
let cascaded_scheduler (problem : problem) =
  let (some_initial_solution, list_scheduler_time) =
    utime_all_fn (validated_scheduler list_scheduler) problem in
  match some_initial_solution with
  | None -> None
  | Some (initial_solution) ->
     let (solution, reoptimizing_time) = utime_all_fn (reoptimizing_scheduler (validated_scheduler
       (ilp_scheduler SATISFIABILITY)) initial_solution) problem in
     begin
       let latency2 = get_max_latency (fst solution)
       and latency1 = get_max_latency (fst initial_solution) in
       Printf.printf "postpass %s: %d, %d, %d, %g, %g\n"
		     (if latency2 < latency1 then "REOPTIMIZED" else "unchanged")
		     (get_nr_instructions problem)
		     latency1 latency2
		     list_scheduler_time reoptimizing_time;
       flush stdout
     end;
     Some solution;;

(**
fail if the given schedulers give different awnsers
*)
let _debug_comp_scheduler sh1 sh2 problem : solution option =
  let res1,_ = Option.get (sh1 problem) in
  let res2,_ = Option.get (sh2 problem) in
  if Array.for_all2 (fun a b -> a=b) res1 res2 then
    Some (res1,None)
  else
    (print_problem Out_channel.stdout problem;
    let res1_str = Array.fold_left (fun acc v -> acc^" "^(string_of_int v)) "" (res1) in
    let res2_str = Array.fold_left (fun acc v -> acc^" "^(string_of_int v)) "" (res2) in
    failwith (res1_str^" | "^res2_str))


(** make sure registers that are written but no read appear in the Hashtable *)
(*
let complet_reference_counting (problem: problem) (typing: regenv) = 
  match problem.reference_counting with
  | None -> ()
  | Some (by_r,by_i) ->
    Array.iteri (fun i rl -> 
      List.iter (fun (r,_) -> 
        match Hashtbl.find_opt by_r r with
        | Some _ -> ()
        | None -> 
            Hashtbl.add by_r r { rg_class = Machregsaux.class_of_type (typing r);
                                 multiplicity = multiplicity_of_type (typing r);
                                 nb_read =  0 }
      ) rl
    ) by_i
*)

let bnb_heuristic_scheduler (module Config:BNB_Config.T) (initial_problem:problem) = 
  let module H = Greedy.Make(Config) in 
  (* initialise problem *)
  let nr_regs = Machregsaux.nr_regs in
  (* complet_reference_counting initial_problem (fun _ -> failwith "TODO"); *)
  let (problem,postprocess) = PreprocessProblem.compute Config.preprocess_steps initial_problem in


  (* compute list scheduling makespan *)
  let cp = H.init problem nr_regs in
  let cp_raw_schedule = H.compute_schedule cp in

  (* postprocess solution *)
  let schedule = PreprocessProblem.postprocess cp_raw_schedule postprocess in
  let solution = ExportProblem.to_solution (schedule) in
  ExportProblem.check_schedule initial_problem solution;
  Some solution


let bnb_scheduler (module Config:BNB_Config.T) (initial_problem:problem) = 
  let module BnB = BranchAndBound.Make(Config) in
  let module CPH = Greedy.Make(Config) in 
  let module Tracker = BranchAndBoundTracker in

  (* initialise problem *)
  let nr_regs = Machregsaux.nr_regs in
  (* complet_reference_counting initial_problem (fun _ -> failwith "TODO"); *)
  let (problem,postprocess) = PreprocessProblem.compute Config.preprocess_steps initial_problem in

  (*get instressting values*)
  let nb_instructions = Array.length problem.instruction_usages in

  (* compute list scheduling makespan *)
  let cp = CPH.init problem nr_regs in
  let (_,cp_makespan) = CPH.compute_bounds cp in
  let cp_raw_schedule = CPH.compute_schedule cp in

  (* initialise bnb *)
  let tracker = Tracker.start () in
  let interupter = BranchAndBound.Interupter.node (100000/nb_instructions) in 
  let latency_constraint = if Config.Scheduler.do_regpress then (cp_makespan - 1) else cp_makespan in
  let problem = {problem with max_latency = latency_constraint } in (*look for a better or equivalent schedule than the greedy one*)
  let bnb = BnB.init ~tracker ~interupter problem nr_regs in (*respect nr_regs as register constraint*)

  (* launch bnb solver *)
  let raw_schedule = match BnB.compute bnb with
  | BnB.Proven_Optimal (schedule,_makespan,_regpress_max) -> schedule
  | BnB.Proven_Impossible -> cp_raw_schedule
  | BnB.Unkown_Possible (schedule,_makespan,_regpress_max) -> schedule
  | BnB.Unkown -> cp_raw_schedule in

  (* postprocess solution *)
  let schedule = PreprocessProblem.postprocess raw_schedule postprocess in
  let solution = ExportProblem.to_solution (schedule) in
  (* Printf.printf "schedule : [%s]\n" (Tools.Adv_string_conversion.string_of_int_array (fst solution));
  print_problem stdout initial_problem;
  flush stdout; *)
  ExportProblem.check_schedule initial_problem solution;
  Some solution

let scheduler_by_name name =
  match name with
  | "ilp" -> validated_scheduler cascaded_scheduler
  | "list" -> flush stdout;validated_scheduler list_scheduler
  | "revlist" -> validated_scheduler reverse_list_scheduler
  | "regpres" -> validated_scheduler regpres_scheduler
  | "regpres_old" -> validated_scheduler reg_pres_scheduler_old
  | "greedy" -> Printf.fprintf stderr "WARNING ! greedy does not compute correct makespan !\n"; greedy_scheduler
  | "mris" -> failwith "mris has been selected in postpass while it is not supported."
  (* | "critical_path" -> InstructionSchedulerOR.CriticalPathHeuristic.scheduler *)
  (* | "bnb" -> InstructionSchedulerOR.BranchAndBound.scheduler *)
  | "DEBUG_critical_path" -> 
    BNB_configs.(
      let@*@ (module Config) = "raw" in
      let module CPH = Greedy.Make(Config) in

      let cph_scheduler = (fun initial_problem -> 
        let nr_regs = Machregsaux.nr_regs in
        (* complet_reference_counting initial_problem; *)
        let (problem,postprocess) = PreprocessProblem.compute Config.preprocess_steps initial_problem in

        let cp = CPH.init problem nr_regs in
        let solution = ExportProblem.to_solution (PreprocessProblem.postprocess (CPH.compute_schedule cp) postprocess) in
        ExportProblem.check_schedule initial_problem solution;
        Some solution
      ) in       
      _debug_comp_scheduler cph_scheduler list_scheduler
    )
    
  | "bnb" -> fun problem -> 
      BNB_configs.(
      let@*@ config = match problem.reference_counting with 
      | None ->  "postpass" 
      | Some _ -> "prepass"
      in
      bnb_scheduler config problem
    )
    
  | _ when String.starts_with ~prefix:"bnb_" name -> BNB_configs.(
      let@*@ config = String.sub name 4 (String.length name - 4) in
      bnb_scheduler config
  )

  (*
  bnbheuristic_prepass
  bnbheuristic_postpass
  *)
  | "bnbheuristic" -> fun problem -> 
    BNB_configs.(
    let@*@ config = match problem.reference_counting with 
    | None ->  "ref" 
    | Some _ -> "ref"
    in
    bnb_heuristic_scheduler config problem
  )
    
  | _ when String.starts_with ~prefix:"bnbheuristic_" name -> BNB_configs.(
      let@*@ config = String.sub name 13 (String.length name - 13) in
      bnb_heuristic_scheduler config
  )

  | "EXPORT_TO_JSON" -> (fun problem -> 
    (*save the problem*)
    let id = ExportProblem.adv_id () in
    Printf.printf "%d " id;
    
    (* make sure registers that are written but no read appear in the Hashtable *)
    (* complet_reference_counting problem (fun _ -> failwith "TODO !"); *)
    (* let (problem,_) = PreprocessProblem.add_register_latency problem in *)
    ExportProblem.to_json ("bench/benchmark_" ^ (string_of_int id)) problem Machregsaux.nr_regs;
    list_scheduler problem
  )
      
  | s -> failwith ("unknown scheduler: " ^ s);;
