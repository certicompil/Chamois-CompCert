(*need to implement :
   - cardinality
   - add
   - intersection
assume non mutable
*)

module InterSet = struct
  module Intern = (* Ptset *) (* Set.Make(struct type t = int let compare = (-) end) *) ChunkedIntSet
  type t = (Intern.t * int)
  let empty () = (Intern.empty,0)
  (* let is_empty (_,c) = c=0 *)
  (* let cardinal (_,c) = c *)
  let add e (s,c) = (Intern.add e s,c+1) (*assume no duplicate !*)

  let is_full_inter_empty (set_seq : t Seq.t) : bool = 
    let set_tab = Array.of_seq set_seq in
    Array.sort (fun a b -> Int.compare (snd a) (snd b)) set_tab;
    let set_inter = Array.fold_left (fun acc (s,_) -> Intern.inter acc s) (fst set_tab.(0)) set_tab in
    Intern.is_empty set_inter

  let is_full_inter_not_unitary (set_seq : t Seq.t) : bool = 
    let set_tab = Array.of_seq set_seq in
    Array.sort (fun a b -> Int.compare (snd a) (snd b)) set_tab;
    let set_inter = Array.fold_left (fun acc (s,_) -> Intern.inter acc s) (fst set_tab.(0)) set_tab in
    Intern.cardinal set_inter > 1

  let mem (value:int) ((set,_):t) : bool = 
    Intern.mem value set

  let remove (value:int) ((set,size):t) : t =
    let new_set = Intern.remove value set in
    match set = new_set with
    | true -> (set,size)
    | false -> (new_set,size-1)
end

(*mode paranoyakeyakeyake*)
(* module InterSet = struct
  module Intern1 = (* Ptset *) Set.Make(struct type t = int let compare = (-) end) (* ChunkedIntSet *)
  module Intern2 = (* Ptset *)(*  Set.Make(struct type t = int let compare = (-) end) *) ChunkedIntSet
  type t = (Intern1.t * Intern2.t * int)
  let empty () = (Intern1.empty,Intern2.empty,0)
  let is_empty (_,_,c) = c=0
  let cardinal (_,_,c) = c
  let add e (s1,s2,c) = (Intern1.add e s1,Intern2.add e s2,c+1) (*assume no duplicate !*)

  let is_full_inter_empty (set_seq : t Seq.t) : bool = 
    let set_tab = Array.of_seq set_seq in
    Array.sort (fun a b -> Int.compare (cardinal a) (cardinal b)) set_tab;
    let (set_tab_i1,set_tab_i2,_) = set_tab.(0) in
    let set_inter_1 = Array.fold_left (fun acc (s,_,_) -> Intern1.inter acc s) (set_tab_i1) set_tab in
    let set_inter_2 = Array.fold_left (fun acc (_,s,_) -> Intern2.inter acc s) (set_tab_i2) set_tab in
    assert ((List.sort (-) @@ Intern1.elements set_inter_1) = (List.sort (-) @@ Intern2.elements set_inter_2));
    Intern1.is_empty set_inter_1
end *)


type t = {
  (*last used id*)
  mutable current_id : int;
  (*for each possible value of each coordinate, contain the set of memorised array with a lower value on this coorinate*)
  lower : InterSet.t array array;
  bounds:(int*int) array;
}

(*a pofiner*)
let next_id (id:int) : int = 
  id + 1
  (* if id > 0 then 
    -id 
  else 
    (0-id) + 1
 *)

 (*
   create the structure using the given bounds
*)
let create (bounds_seq:(int*int) Seq.t) : t = 
  let bounds = Array.of_seq bounds_seq in
  let lower = Array.map (fun (min_size,max_size) -> Array.init (max_size + 1 - min_size) (fun _ -> InterSet.empty ())) bounds in
  {
  current_id = 0;
  lower;
  bounds
  }

  (*
    Return true if a memorise array is lower or equal for all value
*)
let check_dominance {lower;bounds;_} seq: bool =
  let lower_than_array = Seq.mapi (fun coord value -> lower.(coord).(value - fst bounds.(coord))) seq in 
  not (InterSet.is_full_inter_empty lower_than_array)

(*
    memorise the array
*)
let add_dominance intern seq: unit =
  let {lower;bounds;current_id} = intern in
  intern.current_id <- next_id current_id;
  Seq.iteri (fun coord value -> 
    for s = (value - fst bounds.(coord)) to Array.length lower.(coord) - 1 do
      lower.(coord).(s) <- InterSet.add current_id lower.(coord).(s)
    done
  ) seq


let copy intern = {
  lower = Array.init (Array.length intern.lower) (fun i -> Array.copy intern.lower.(i));
  bounds = Array.copy intern.bounds;
  current_id = intern.current_id
  }

let get_seq intern id = 
  let {lower;bounds;_} = intern in
  Array.to_seq @@
  Array.mapi (fun coord a -> 
    (* find the first indice containing the value *)
    let i = ref 0 in
    let flag = ref true in
    while !flag && !i < Array.length lower.(coord) do
      if InterSet.mem id a.(!i) then
        flag := false
      else 
        i := !i + 1
    done;
    if !flag then failwith "useless id";
    !i + fst bounds.(coord)
  ) lower

let clean intern = 
  let {lower;bounds;current_id;} = intern in
  for id = 0 to current_id - 1 do
    let lower_than_array = Seq.mapi (fun coord value -> lower.(coord).(value - fst bounds.(coord))) (get_seq intern id) in 
    match InterSet.is_full_inter_not_unitary lower_than_array with
    | false -> ()
    | true -> 
      Array.iteri (fun i1 a ->
        Array.iteri (fun i2 set ->
          lower.(i1).(i2) <- (InterSet.remove id set)
        ) a
      ) lower
  done

(********************************************************************************)

let _test_binary_array = (
  let dom1  = create (Array.to_seq [|(0,2);(0,2);(0,2);(0,2);(0,2)|]) in

  add_dominance dom1 (Array.to_seq [|0;1;1;1;0|]);
  assert (List.rev @@ Seq.fold_left (fun acc e -> e::acc) [] (get_seq dom1 0) = [0;1;1;1;0]);
  assert (    check_dominance dom1 (Array.to_seq [|1;1;1;1;1|]) );
  assert (    check_dominance dom1 (Array.to_seq [|1;1;1;1;0|]) );
  assert (not(check_dominance dom1 (Array.to_seq [|0;0;0;0;0|])));
  assert (not(check_dominance dom1 (Array.to_seq [|0;1;0;1;0|])));
  assert (not(check_dominance dom1 (Array.to_seq [|1;1;0;1;1|])));

  

  add_dominance dom1 (Array.to_seq [|1;0;0;1;1|]);
  assert (List.rev @@ Seq.fold_left (fun acc e -> e::acc) [] (get_seq dom1 1) = [1;0;0;1;1]);
  assert (check_dominance dom1 (Array.to_seq [|1;1;0;1;1|]));
  assert (check_dominance dom1 (Array.to_seq [|1;1;1;1;1|]));
  assert (not(check_dominance dom1 (Array.to_seq [|0;1;0;1;0|])));
)

let _test_clean = (
  let dom = create (Array.to_seq [|(0,2);(5,7);(0,3);(0,2);(0,2)|]) in
  add_dominance dom (Array.to_seq [|0;6;2;1;0|]);
  assert (    check_dominance dom (Array.to_seq [|0;7;2;1;0|]) );
  assert (    check_dominance dom (Array.to_seq [|1;6;2;1;0|]) );
  assert (not(check_dominance dom (Array.to_seq [|1;7;1;1;1|])));
  assert (not(check_dominance dom (Array.to_seq [|1;7;1;0;1|])));
  add_dominance dom (Array.to_seq [|0;6;1;1;0|]);
  assert (    check_dominance dom (Array.to_seq [|0;7;2;1;0|]) );
  assert (    check_dominance dom (Array.to_seq [|1;6;2;1;0|]) );
  assert (   (check_dominance dom (Array.to_seq [|1;7;1;1;1|])));
  assert (not(check_dominance dom (Array.to_seq [|1;7;1;0;1|])));
  clean dom;
  assert (    check_dominance dom (Array.to_seq [|0;7;2;1;0|]) );
  assert (    check_dominance dom (Array.to_seq [|1;6;2;1;0|]) );
  assert (   (check_dominance dom (Array.to_seq [|1;7;1;1;1|])));
  assert (not(check_dominance dom (Array.to_seq [|1;7;1;0;1|])));
)