open InstructionSchedulerTypes
open BNB_heuristic

module Interupter : sig
    type t
    type bnb_interuption = 
        | TimeOut (* time limit is reach *) 
        | NodeOut (* node limit is reach *)
        | TooDeep (* deapth limit is reach *)
    exception BnB_Interupt of bnb_interuption
    
    val empty : t
    val time : float -> t
    val node : int -> t
    val depth : int -> t
    val add : t -> t -> t
end

module type T = sig
    (**
    (mutable) 
    Informations about the execution    
    *)
    type t

    (**
    possible result. A result is unkown in case of interuption (timeout).     
    *)
    type result = 
    | Proven_Optimal of PreprocessProblem.unprocessed_solution * int * int array
    | Proven_Impossible
    | Unkown_Possible of PreprocessProblem.unprocessed_solution * int * int array
    | Unkown

    (**
    Initialised the exectution, given a problem and a vector of maximum register pressure (will be ignored if used without regpress). The optional arguments are : 
    - [choose] : order in witch the branch will be explored (default : critical path)
    - [interupter] : interupter rule (default none). Carefull, a "timeout" start counting from the line its created.
    *)
    val init : ?choose:Strategy.t -> ?interupter:Interupter.t -> (* ?preprocess:PreprocessProblem.t -> *) ?tracker:BranchAndBoundTracker.t -> problem -> int array -> t

    (**
    copy the state of the execution. Does not copy memorised information, instead use a reference to the same memory.
    *)
    val copy : t -> t

    (**
    set the interupter
    *)
    val set_interupter : Interupter.t ->  t -> t

    (**
    set the upper bound, its value might lower durring execution     
    *)
    val set_upper_bound : int -> t-> t

    (**
    set the maximum register pressure allowed 
    *)
    val set_max_regpress : int array -> t -> t

    (**
    compute the result from the given state
    *)
    val compute : t -> result

    (* val compute_regpress : t -> float -> result Seq.t *)
end

module Make(_:BNB_Config.T) : T