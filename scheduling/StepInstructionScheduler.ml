open BTL_BlockOptimizeraux
open InstructionSchedulerTypes

module type T = sig
  type t 

  val do_regpress : bool

  (********)

  val current_time : t -> int
  val current_resource_usage : t -> int array
  val current_schedule : t -> int array
  val current_regpress : t -> int array

  (********)

  val init : InstructionSchedulerTypes.problem -> int array -> t
  val copy : t -> t

  (********)

  val compute_cp : t -> int array 
  (** compute and return the critical path of all instructions. Note that the critical path does not change during execution *)

  (* val compute_min_latency_delay : t -> int array  *)(* compute and return then minimum delay before execution (according to latency) for all instructions *)

  val compute_delay : t -> int array 
  (** compute and return the delay before execution for all variables with regard to already schedule instruction*)

  val compute_advance_delay : t -> int array 
  (** extention of [compute_delay] taking into account direct latency. (compute_advance_delay s).(i) = max (compute_delay s).(i) (max (latency_entering).(i)) *)

  (********)

  val total_makespan : t -> int option (* None if not finish, else Some m with m the total makespan *)
  val possible : t -> bool array option (* Imediately schedulable instructions*)
  val advance_time : t -> unit 
  val schedule_instruction : t -> int -> unit

end

module Base : T = struct
  open BNB_base_scheduler
  type t = BNB_base_scheduler.t
  let do_regpress = false
  let current_time self = self.state.time
  let current_resource_usage self = self.state.resources_usage
  let current_schedule self = self.state.schedule
  let current_regpress _ = Tools.Log.once 1 (fun _ -> Printf.fprintf stderr "WARNING : Base scheduler ignore regress\n" ); [||]
  (**************)
  let init (problem) _ = 
    let nb_instruction = Array.length problem.instruction_usages in
    let succ = Array.make nb_instruction [] in 
    let pred = Array.make nb_instruction [] in 
    List.iter (fun lc ->
      succ.(lc.instr_from) <- (lc.instr_to,lc.latency)::succ.(lc.instr_from);
      pred.(lc.instr_to) <- (lc.instr_from,lc.latency)::pred.(lc.instr_to);
    ) problem.latency_constraints;
    init_raw succ pred problem.resource_bounds problem.instruction_usages
  let copy = copy
  (**************)
  let compute_cp = compute_cp
  let compute_advance_delay        = compute_advance_delay
  let compute_delay                = compute_delay
  let total_makespan               = total_makespan
  let possible                     = possible
  let advance_time                 = advance_time
  let schedule_instruction         = schedule_instruction
end

module BaseRC : T= struct
  (* open Base_scheduler *)
  type t = {
    regpress : int array;
    live_registers : (Registers.reg,unit) Hashtbl.t;
    reference_counting : ((Registers.reg, register_info) Hashtbl.t * ((Registers.reg * mentions_element) list array));
    self : BNB_base_scheduler.t
  }
  let do_regpress = true
  let current_time {self;_} = BNB_base_scheduler.(self.state.time)
  let current_resource_usage {self;_} = BNB_base_scheduler.(self.state.resources_usage)
  let current_schedule {self;_} = BNB_base_scheduler.(self.state.schedule)
  let current_regpress {regpress;_} = regpress

  (**************)
  (*
init_regpress : value ignore -> change to size ?   
  *)
  let init (problem) init_regpress = 
    let nb_instruction = Array.length problem.instruction_usages in
    let succ = Array.make nb_instruction [] in 
    let pred = Array.make nb_instruction [] in 
    List.iter (fun lc ->
      succ.(lc.instr_from) <- (lc.instr_to,lc.latency)::succ.(lc.instr_from);
      pred.(lc.instr_to) <- (lc.instr_from,lc.latency)::pred.(lc.instr_to);
    ) problem.latency_constraints;
    
    let rc = match problem.reference_counting with None -> failwith "reference counting is None for a scheduler using rc" | Some s -> s in
    let rc_by_reg = fst rc in
    let rc_reg = Hashtbl.copy rc_by_reg in
    let regpress = Array.map (fun _ -> 0) init_regpress in
    let live_registers = Hashtbl.create 101 in
    List.iter (fun r -> 
      let {rg_class;_} = match Hashtbl.find_opt rc_by_reg r with Some v -> v | None -> failwith "reference counting incoherence : registrer not in hashtable" in
      Hashtbl.add live_registers r ();
      regpress.(rg_class) <- regpress.(rg_class) + 1
    ) @@ Registers.Regset.elements problem.live_regs_entry;
    (* Printf.printf "[%s]\n" (Tools.Adv_string_conversion.string_of_list_adv (Registers.Regset.elements problem.live_regs_entry) (Registers.adp_string_of_reg)); *)
    {
      self = BNB_base_scheduler.init_raw succ pred problem.resource_bounds problem.instruction_usages;
      live_registers;
      reference_counting = (rc_reg,snd rc);
      regpress;
    }
  let copy {self;reference_counting;regpress;live_registers} = {
    self = BNB_base_scheduler.copy self; 
    live_registers = Hashtbl.copy live_registers;
    reference_counting = (Hashtbl.copy (fst reference_counting),snd reference_counting);
    regpress = Array.copy regpress}
  (**************)
  let compute_cp {self;_} = BNB_base_scheduler.compute_cp self
  let compute_advance_delay {self;_} = BNB_base_scheduler.compute_advance_delay self
  let compute_delay {self;_} = BNB_base_scheduler.compute_delay self
  let total_makespan {self;_} = BNB_base_scheduler.total_makespan self
  let possible {self;_} = BNB_base_scheduler.possible self
  let advance_time {self;_} = BNB_base_scheduler.advance_time self
  let schedule_instruction {self;reference_counting;regpress;live_registers} i = 
    let (by_reg,by_i) = reference_counting in
    List.iter (fun (reg,is_arg) -> 
      let {multiplicity;nb_read;rg_class} = match Hashtbl.find_opt by_reg reg with Some v -> v | None -> failwith "reference counting incoherence : registrer not in hashtable" in
      match is_arg, nb_read with
      (* register created & conssume by i (possible ?)*)
      | (Def,0) -> ()
      (* register conssume by i*)
      | (Arg,m) | (ExtArg,m) when multiplicity = m-> 
        Hashtbl.replace by_reg reg {multiplicity;nb_read=0;rg_class}; 
        Hashtbl.remove live_registers reg;
        regpress.(rg_class) <- regpress.(rg_class) - multiplicity
      (* register used by i*)
      | (Arg,_) | (ExtArg,_) when nb_read > 0 -> Hashtbl.replace by_reg reg {multiplicity;nb_read = nb_read - multiplicity;rg_class}
      
      (* register written by i*)
      | (Def,_) when nb_read > 0 && Hashtbl.mem live_registers reg -> ()
      (* register created by i*)
      | (Def,_) when nb_read > 0 -> 
        Hashtbl.add live_registers reg ();
        regpress.(rg_class) <- regpress.(rg_class) + 1
      (* incoherence *)
      | (Arg,0) | (ExtArg,0) -> failwith "incoherent read number 0 when reading"
      | (_,_) -> failwith "incoherent read number : negative"
    ) by_i.(i);
    BNB_base_scheduler.schedule_instruction self i

  (* let _test () = (
    let dummy_scheduler = BNB_base_scheduler.init_raw [||] [||] [||] [|[||];[||];[||];[||]|] in
    let reg_table = Hashtbl.create 10 in
    Hashtbl.add reg_table (Registers.adp_create 0) {multiplicity=1;nb_read=0;rg_class=0};
    Hashtbl.add reg_table (Registers.adp_create 1) {multiplicity=1;nb_read=1;rg_class=1};
    Hashtbl.add reg_table (Registers.adp_create 2) {multiplicity=1;nb_read=2;rg_class=2};
    Hashtbl.add reg_table (Registers.adp_create 3) {multiplicity=1;nb_read=1;rg_class=0};
    let i_table = [|
      [ (Registers.adp_create 0 , Def) ;
        (Registers.adp_create 1 , Def) ];
      [ (Registers.adp_create 1 , Arg) ;
        (Registers.adp_create 2 , Def);
        (Registers.adp_create 3 , Def) ];
      [ (Registers.adp_create 2 , Arg);
        (Registers.adp_create 3 , Arg) ];
      [ (Registers.adp_create 2 , Arg) ]
    |] in
    let s = {self=dummy_scheduler;reference_counting=(reg_table,i_table);regpress=[|0;0;0|];live_registers=Hashtbl.create 10} in 
    schedule_instruction s 0;
    assert (current_regpress s = [|0;1;0|]);
    schedule_instruction s 1;
    assert (current_regpress s = [|1;0;1|]);
    schedule_instruction s 2;
    assert (current_regpress s = [|0;0;1|]);
    schedule_instruction s 3;
    assert (current_regpress s = [|0;0;0|]);
  ) *)

end

module TC : T = struct
  open BNB_tc_scheduler
  type t = BNB_tc_scheduler.t
  let do_regpress = false

  let current_time self = self.state.time
  let current_resource_usage self = self.state.resources_usage
  let current_schedule self = self.state.schedule.a
  let current_regpress _ = Tools.Log.once 2 (fun _ -> Printf.fprintf stderr "WARNING : TC scheduler ignore regress\n"); [||]

  let init (problem) _ =
    Tools.Log.once 3 (fun () -> Printf.fprintf stderr "WARNING : TC step scheduler assume transitive closure\n");
    let nb_instruction = Array.length problem.instruction_usages in
    let latencys = Array.init nb_instruction (fun _ -> Array.make nb_instruction (-1)) in
    
    List.iter (fun {instr_from;instr_to;latency} -> latencys.(instr_to).(instr_from) <- latency) problem.latency_constraints;
    init_raw latencys problem.resource_bounds problem.instruction_usages

  (********)

  let copy                         = copy
  let compute_cp                   = compute_cp
  (* let compute_prefer_instruction   = compute_prefer_instruction
  let critical_instruction         = critical_instruction *)

  (* let do_dominate                  = do_dominate *)
  let compute_advance_delay        = compute_min_latency_delay
  let compute_delay                = compute_delay
  let total_makespan               = total_makespan
  (* let next                         = next *)
  let possible                     = compute_possible
  let advance_time                 = advance_time
  let schedule_instruction         = schedule_instruction

  (* let runtime_dominance            = do_dominate *)
end