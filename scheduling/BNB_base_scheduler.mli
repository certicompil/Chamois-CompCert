type base = {
  succ : (int * int) list array; (* destination , cost *)
  pred : (int * int) list array; (* predecesor  , cost *)
  resources : int array;
  usages: int array array;
}

type state = {
  (* delay : int array; *)
  resources_usage : int array;
  mutable time : int;
  schedule : int array;
}

type t = {
  base:base;
  state:state;
}

(***************)

val init_raw : (int*int) list array -> (int*int) list array -> int array -> int array array -> t

val copy : t -> t 
(***************)
val compute_cp : t -> int array
val compute_delay : t -> int array
val compute_advance_delay : t -> int array
(***************)

val schedule_instruction : t -> int -> unit
val advance_time : t -> unit  
val total_makespan : t -> int option
val possible : t -> bool array option