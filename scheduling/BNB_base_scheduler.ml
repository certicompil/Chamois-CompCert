type base = {
  succ : (int * int) list array; (* destination , cost *)
  pred : (int * int) list array; (* predecesor  , cost *)
  resources : int array;
  usages: int array array;
}

type state = {
  (* delay : int array; *)
  resources_usage : int array;
  mutable time : int;
  schedule : int array;
}

type t = {
  base:base;
  state:state;
}

(***************)

let for_all3 (f : 'a -> 'b -> 'c -> bool) (a:'a array) (b:'b array) (c:'c array) : bool =
  let flag = ref true in
  Array.iteri (fun i _ ->
    flag := !flag && f a.(i) b.(i) c.(i)
  ) a;
  !flag  

(***************)

let init_raw (succ:(int*int) list array) (pred:(int*int) list array) (resources:int array) (usages:int array array) =
  let base = {succ;pred;resources;usages} in
  let state = {
    resources_usage = Array.map (fun _ -> 0) resources;
    time = 0;
    schedule =  Array.map (fun _ -> -1) usages 
  } in
  {base;state} 

let copy self = {
  base = self.base;
  state = {
    (* delay = Array.copy self.state.delay; *)
    resources_usage = Array.copy self.state.resources_usage;
    time = self.state.time;
    schedule = Array.copy self.state.schedule
  }
}

(***************)

let compute_cp self =
  let succesors = self.base.succ in
  let mem = Array.make (Array.length succesors) None in
  let rec longest_path i =
    match mem.(i) with
    | Some v -> v
    | None ->
    let v = match succesors.(i) with
      | [] -> 0
      | i_succ -> 
        List.fold_left (fun acc (dest,cost) -> max acc (cost + longest_path dest)) 0 i_succ
    in
    mem.(i) <- Some v;
    v
  in
  Array.mapi (fun i _ -> longest_path i) mem

let compute_delay self = 
  Array.mapi (fun i pred -> 
    if self.state.schedule.(i) <> -1 then -1 else
    List.fold_left (fun acc (p,l) -> 
      match self.state.schedule.(p) with
      | -1 -> acc
      | t -> max acc ( t + l - self.state.time )
    ) 0 pred
  ) self.base.pred

let compute_advance_delay self = 
  Array.mapi (fun i pred -> 
    if self.state.schedule.(i) <> -1 then -1 else
    List.fold_left (fun acc (p,l) -> 
      match self.state.schedule.(p) with
      | -1 -> max acc l
      | t -> max acc ( t + l - self.state.time )
    ) 0 pred
  ) self.base.pred

(***************)

let schedule_instruction self i =
  Array.iteri (fun r u -> self.state.resources_usage.(r) <- u + self.base.usages.(i).(r)) self.state.resources_usage;
  self.state.schedule.(i) <- self.state.time;
  (* self.state.delay.(i) <- -1; *)
  (* List.iter (fun (destination,latency) -> self.state.delay.(destination) <- latency) self.base.succ.(i); *)
  ()

let advance_time self = 
  (* Array.iteri (fun i d -> self.state.delay.(i) <- if d>1 then d - 1 else d) self.state.delay; *)
  Array.iteri  (fun i _ -> self.state.resources_usage.(i) <- 0) self.state.resources_usage;
  self.state.time <- self.state.time + 1;
  ()
  
let total_makespan self = 
  let (min_v,max_v) = Array.fold_left (fun (acc_min,acc_max) v -> (min acc_min v,max acc_max v)) (0,0) self.state.schedule in
  if min_v = (-1) then None else (Some (max_v + 1))

let possible self =
  let respect_latency (*and is not scheduled*) = Array.mapi (fun i pred_i ->
    self.state.schedule.(i) = (-1) &&
    List.for_all (fun (j,l) -> 
      let t_j = self.state.schedule.(j) in
        t_j <> -1 && t_j + l <= self.state.time
      ) pred_i
  ) self.base.pred in
  let respect_resource = Array.map (fun usage -> 
    for_all3 (fun u c m -> u+c <= m) usage self.state.resources_usage self.base.resources
  ) self.base.usages in 
  match Array.exists2 (&&) respect_latency respect_resource with
  | true -> Some (Array.map2 (&&) respect_latency respect_resource)
  | false -> None


let _ = (
  (* 0 -0-> 1 -1-> 2 *)
  let self = init_raw [|[(1,0)];[(2,1)];[]|] [|[];[(0,0)];[1,1]|] [|2;1|] [|[|1;0|];[|0;1|];[|1;1|]|] in
  assert (self = {base = {succ = [|[(1, 0)]; [(2, 1)]; []|]; pred = [|[]; [(0, 0)]; [(1, 1)]|]; resources = [|2; 1|]; usages = [|[|1; 0|]; [|0; 1|]; [|1; 1|]|]}; state = {resources_usage = [|0; 0|]; time = 0; schedule = [|-1; -1; -1|]}});
  assert (compute_cp self = [|1;1;0|]);
  assert (compute_delay self = [|0;0;0|]);
  assert (compute_advance_delay self = [|0;0;1|]);
  assert (total_makespan self = None);
  assert (possible self = Some [|true;false;false|]);
  advance_time self;
  (* assert (self.state.resources_usage =[|0; 0|]);
  assert (self.state.time = 1);
  assert (self.state.schedule =[|-1; -1; -1|]); *)
  assert (self.state = {resources_usage = [|0; 0|]; time = 1; schedule = [|-1; -1; -1|]});
  schedule_instruction self 0;
  assert (self.state = {resources_usage = [|1; 0|]; time = 1; schedule = [|1; -1; -1|]});
  assert (compute_delay self = [|-1;0;0|]);
  assert (compute_advance_delay self = [|-1;0;1|]);
  assert (possible self = Some [|false;true;false|]);
  schedule_instruction self 1;
  assert (self.state = {resources_usage = [|1; 1|]; time = 1; schedule = [|1; 1; -1|]});
  assert (compute_delay self = [|-1;-1;1|]);
  assert (compute_advance_delay self = [|-1;-1;1|]);
  assert (possible self = None);
  advance_time self;
  assert (self.state = {resources_usage = [|0; 0|]; time = 2; schedule = [|1; 1; -1|]});
  assert (compute_delay self = [|-1;-1;0|]);
  assert (compute_advance_delay self = [|-1;-1;0|]);
  assert (possible self = Some [|false;false;true|]);
  assert (total_makespan self = None);
  schedule_instruction self 2;
  assert (total_makespan self = Some 3)
)
  
(* let maximum_latency_delay self = 
  let partial_latency = Array.map (fun pred_i -> (*can potentialy take into account hight latency with unschedled instruction*)
    List.fold_left (fun acc (j,l) -> max acc (self.state.schedule.(j) + l - self.state.time)) (-1) pred_i
  ) self.base.pred in
  let lp a b = 0 in
  ()(*TODO : fix latency from the problem (precomputed)*) *)


(* let compute_min_latency_delay self =
  Array.mapi (fun i s -> 
    match s with
    | -1 -> 
      List.fold_left (fun acc (l,j) -> 
        match self.state.schedule.(j) with
        | -1 -> max l acc
        | _ -> acc
      ) self.state.delay.(i) self.base.pred.(i)
    | _ -> -1
  ) self.state.schedule *)


(***********************************)

(* let possible self =
  
 *)

(*
val copy : t -> t

val compute_cp : t -> int array (* compute and return the critical path of all instructions *)
val compute_min_latency_delay : t -> int array (* compute and return then minimum delay before execution (according to latency) for all instructions *)
val compute_prefer_instruction : t -> int array 
(*
  compute for every instruction its prefer instruction i.e the instruction resulting in the maximum latency delay
  (compute_min_latency_delay self).(i) = latency.(i).((compute_prefer_instruction self).(i))
  REVERSE ?
*)

val total_makespan : t -> int option (* None if not finish, else Some m with m the total makespan *)
val possible : t -> bool array option (* Imediately schedulable instructions*)
val advance_time : t -> unit 
(* val next : t -> bool array *) (* filter for possible next instruction *)
val schedule_instruction : t -> int -> unit

val runtime_dominance : t -> int -> int -> bool   
*)