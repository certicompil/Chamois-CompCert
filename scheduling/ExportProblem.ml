open InstructionSchedulerTypes
open BTL_BlockOptimizeraux
open Tools.Adv_string_conversion

(*global id, not reset from one execution to the other*)
let id = ref 0

let file_metadata_id = "TMP_Scheduler_perf_metadata.txt"
let adv_id () =
  let _ = Sys.command ("touch "^file_metadata_id) in
  let in_c = In_channel.open_text file_metadata_id  in
  let nb = match In_channel.input_line in_c with
    |None -> 0
    |Some b -> int_of_string b
  in
  In_channel.close in_c;
  let out_c = Out_channel.open_text file_metadata_id in
  Out_channel.output_string out_c (string_of_int (nb+1));
  Out_channel.close out_c;
  id := nb;
  nb

let reset_id () =
  Sys.remove file_metadata_id

let to_DOT name problem =
  let file = Out_channel.open_gen [Open_wronly;Open_creat;Open_text;Open_trunc] 0o666 (name^".dot") in
  (*
    colorscheme=prgn11
    colorscheme=pastel28
    aliceblue
  *)
  Printf.fprintf file "digraph mygraph{\nnode [style=filled] [colorscheme=pastel28] [fontcolor=black]\n";
  let last_c = ref 1 in
  let color_map = Hashtbl.create 101 in
  List.iter (fun i -> 
    let c = (
    match Hashtbl.find_opt color_map problem.instruction_usages.(i) with
    |None -> 
        let c = !last_c in
        last_c := c + 1;
        Hashtbl.add color_map problem.instruction_usages.(i) c;
        c
    |Some c -> c)  in           
    Printf.fprintf file "i%d [color=\"%d\"]\n" i c
  ) (List.init (Array.length problem.instruction_usages) (fun i->i));
  List.iter (fun lc -> 
    (* Printf.printf "%d %d %d\n" lc.instr_from lc.instr_to lc.latency *)
    (* Printf.printf "%d-(%d)>%d\n" lc.instr_from lc.latency lc.instr_to *)
    Printf.fprintf file "i%d -> i%d [label=%d]\n" lc.instr_from lc.instr_to lc.latency
  ) problem.latency_constraints;

  (*legend*)
  Printf.fprintf file "subgraph legend {\n";
  let pred = ref ("r"^(string_of_int_array ~sep:"_" problem.resource_bounds)) in
  Printf.fprintf file "R%s [shape=box]\n" !pred;
  Hashtbl.iter (fun usage color -> 
    let current = (string_of_int_array ~sep:"_" usage) in
    Printf.fprintf file "R%s [shape=box][color=\"%d\"]\n" current color;
    Printf.fprintf file "R%s -> R%s [color=white]\n" !pred current;
    pred := current
  ) color_map;
  Printf.fprintf file "}";

  Printf.fprintf file "}";
  Out_channel.close file

let check_table_validity problem =
  match problem.reference_counting with
  | None -> ()
  | Some (by_r,by_i) -> 
    let by_r = Hashtbl.copy by_r in
    Array.iteri (fun _ lr -> 
      List.iter (fun (r,do_read) -> 
        match do_read with
        | Def -> () (*TODO ExtArg ???*)
        | Arg | ExtArg -> 
          let {multiplicity;nb_read;rg_class} = Hashtbl.find by_r r in
          Hashtbl.replace by_r r {multiplicity;rg_class;nb_read = nb_read - multiplicity}
      ) lr
    ) by_i;
    Hashtbl.iter (fun _ {nb_read;_} -> 
      assert (nb_read >= 0);
      (* Printf.fprintf stderr "%d " nb_r *)  
    ) by_r

let to_json name problem nr_regs =
  let file = Out_channel.open_gen [Open_wronly;Open_creat;Open_text;Open_trunc] 0o666 (name^".json") in

  let sorted_latency_constraints = List.sort (fun c1 c2 -> 
    match c1,c2 with
    | _ when c1.instr_from <> c2.instr_from -> c1.instr_from - c2.instr_from
    | _ when c1.instr_to <> c2.instr_to -> c1.instr_to - c2.instr_to 
    | _ -> c1.latency - c2.latency
  ) problem.latency_constraints in

  Printf.fprintf file "{";
    Printf.fprintf file "\"resource_bounds\": [  %s ]," (string_of_int_array ~sep:", " problem.resource_bounds);
    Printf.fprintf file "\"instruction_usages\" : [";
        Array.iteri (fun i l -> 
          Printf.fprintf file "[ %s ]\n" (string_of_int_array ~sep:", " l);
          if (i != Array.length problem.instruction_usages - 1) then
              Printf.fprintf file ","
        ) problem.instruction_usages; 
    Printf.fprintf file "],";
    Printf.fprintf file "\"latency_constraints\":[";
    (match sorted_latency_constraints with
    | e::next -> 
        List.iter (fun lc -> 
          Printf.fprintf file "{\"instr_from\":%d,\"instr_to\":%d,\"latency\":%d},\n" lc.instr_from lc.instr_to lc.latency
        ) next;
        Printf.fprintf file "{\"instr_from\":%d,\"instr_to\":%d,\"latency\":%d}]" e.instr_from e.instr_to e.latency
    | [] -> Printf.fprintf file "]");
    
    (match problem.reference_counting with
    | None -> Printf.fprintf file "\n";
    | Some (registers,instructions) -> 

        Printf.fprintf file ",\n";

        check_table_validity problem;
        
        let reg_counter = ref 0 in
        let reg_set = Hashtbl.create 101 in
                
        Seq.iter (fun r -> 
          match Hashtbl.find_opt reg_set r with (* in case multiple binding use this key*)
          | Some _ -> ()
          | None -> 
            let _ = Hashtbl.find registers r in (*verify r is in hashtable ? TODO check ?*)
            Hashtbl.add reg_set r !reg_counter;
            reg_counter := !reg_counter + 1;
            ()
        ) @@ Hashtbl.to_seq_keys registers;

        Printf.fprintf file " \"reference_counting\": {";
        Printf.fprintf file " \"registers\": [";
        (* Hashtbl.iter (fun r (clas, nb_read) -> 
          Printf.fprintf file "{\"key\":%d,\"class\":%d,\"nb_read\":%d},\n" (Hashtbl.find reg_set r) clas nb_read;
        ) registers; *)
        Seq.iter (fun r -> 
          let {nb_read;rg_class;_} = Hashtbl.find registers r in
          Printf.fprintf file "{\"key\":%d,\"class\":%d,\"nb_read\":%d},\n" (Hashtbl.find reg_set r) rg_class nb_read;
        ) @@ Hashtbl.to_seq_keys reg_set;
        Printf.fprintf file "{\"empty\":\"\"}";(*AAAAAAAAAAAAAAAAAAAAAAAAAAAAA*)
        Printf.fprintf file "],\n";
        
        let string_of_mention = function Def -> "\"Def\"" | Arg -> "\"Arg\"" | ExtArg -> "\"ExtArg\"" in

        Printf.fprintf file "\"instructions\" : [%s]"
          (string_of_array_adv ~sep:", \n" instructions (fun l ->
              "[" ^ (string_of_list_adv ~sep:", " l (fun (r,b) -> "[" ^ (string_of_int (Hashtbl.find reg_set r)) ^ "," ^ (string_of_mention b) ^ "]")) ^ "]"
          ))
        ;
        Printf.fprintf file "},\n";

        let live_regs_entry_int_list = List.map (Hashtbl.find reg_set) @@ Registers.Regset.elements problem.live_regs_entry in
        Printf.fprintf file "\"live_regs_entry\": [  %s ],\n" (string_of_list_adv ~sep:", " live_regs_entry_int_list (string_of_int));
        Printf.fprintf file "\"nr_regs\": [  %s ]" (string_of_array_adv ~sep:", " nr_regs (string_of_int));
    );

  Printf.fprintf file "}";
  Out_channel.close file

let to_solution (schedule:int array) : solution = 
  let makespan = 1 + Array.fold_left (max) 0 schedule in
  (Array.append schedule [|makespan|],None)

  let maximum_slot_used times =
    let maxi = ref (-1) in
    for i=0 to (Array.length times)-2
    do
      maxi := max !maxi times.(i)
    done;
    !maxi;;

let check_schedule (problem : problem) ((times,_) : solution) =
  let nr_instructions = Array.length problem.instruction_usages in
  (if Array.length times <> nr_instructions+1
    then failwith
          (Printf.sprintf "check_schedule: %d times expected, got %d"
            (nr_instructions+1) (Array.length times)));
  (if problem.max_latency >= 0 && times.(nr_instructions)> problem.max_latency
    then failwith "check_schedule: max_latency exceeded");
  (Array.iteri (fun i time ->
        (if time < 0
        then failwith (Printf.sprintf "time[%d] < 0" i))) times);
  let slot_resources = Array.init ((maximum_slot_used times)+1)
                          (fun _ -> Array.copy problem.resource_bounds) in
  for i=0 to nr_instructions -1
  do
    let remaining_resources = slot_resources.(times.(i))
    and used_resources = problem.instruction_usages.(i) in
    for resource=0 to (Array.length used_resources)-1
    do
      let after = remaining_resources.(resource) - used_resources.(resource) in
      (if after < 0
        then failwith (Printf.sprintf "check_schedule: instruction %d exceeds resource %d at slot %d" i resource times.(i)));
      remaining_resources.(resource) <- after
    done
  done;
  List.iter (fun ctr ->
      if times.(ctr.instr_to) - times.(ctr.instr_from) < ctr.latency
      then failwith (Printf.sprintf "check_schedule: time[%d]=%d - time[%d]=%d < %d"
                        ctr.instr_to times.(ctr.instr_to)
                        ctr.instr_from times.(ctr.instr_from)
                        ctr.latency)
    ) problem.latency_constraints