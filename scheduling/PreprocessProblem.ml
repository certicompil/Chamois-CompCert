open InstructionSchedulerTypes
(*open BTL_BlockOptimizeraux*)

(********************************************************************************)

type t = int array -> int array
type unprocessed_solution = {
    raw_solution:int array
}

let (dummy:t) = (fun a -> a)

let extend_i_latency (pred:(int*int) list array) (succ:(int*int) list array) (i:int) =
    List.iter (fun (p,l1) -> 
      List.iter (fun (s,l2) -> 
          let l = l1 + l2 in
          match List.find_opt (fun (s_p,_) -> s_p=s) succ.(p) with
          | Some (_,l_p) when l_p >= l -> ()
          | Some (_,_) ->
          succ.(p) <- (s,l1+l2)::(List.filter (fun (s_p,_) -> s_p <> s) succ.(p));
          pred.(s) <- (p,l1+l2)::(List.filter (fun (p_s,_) -> p_s <> p) pred.(s));
          | None -> 
          succ.(p) <- (s,l1+l2)::succ.(p);
          pred.(s) <- (p,l1+l2)::pred.(s);
      ) succ.(i)
    ) pred.(i)
    
let test_extend_i_latency (pred:(int*int) list array) (succ:(int*int) list array) (i:int) = 
    let pred_r = Array.copy pred in
    let succ_r = Array.copy succ in
    extend_i_latency pred_r succ_r i;
    (pred_r,succ_r)

let _ = (
    assert (test_extend_i_latency [|[];[(0,1)];[(1,1)]|] [|[(1,1)];[(2,1)];[]|] 1 = ([|[]; [(0, 1)]; [(0, 2); (1, 1)]|], [|[(2, 2); (1, 1)]; [(2, 1)]; []|]));
)

let extend_latency (pred:(int*int) list array) (succ:(int*int) list array) (i_list:int list) = 
    let pred_r = Array.copy pred in 
    let succ_r = Array.copy succ in
    List.iter (fun i -> extend_i_latency pred_r succ_r i) i_list;
    (pred_r,succ_r)

let _ = (
    assert (extend_latency [|[];[(0,1)];[(1,1)];[(2,1)]|] [|[(1,1)];[(2,1)];[(3,1)];[]|] [1;2] = ([|[]; [(0, 1)]; [(0, 2); (1, 1)]; [(1, 2); (0, 3); (2, 1)]|],[|[(3, 3); (2, 2); (1, 1)]; [(3, 2); (2, 1)]; [(3, 1)]; []|]));
    assert (extend_latency
      [|
          [];
          [];
          [];
          [(0,1);(1,1)];
          [(3,1)];
          [(1,1);(2,1);(3,1);(4,1)];
          [(3,1)];
          [(3,1);(5,1)];
          [(5,1)];
      |] [|
          [(3,1)];
          [(3,1);(5,1)];
          [(5,1)];
          [(4,1);(5,1);(6,1);(7,1)];
          [(5,1)];
          [(7,1);(8,1)];
          [];
          [];
          [];
      |] [3;5] 
      = ([|[]; []; []; [(0, 1); (1, 1)]; [(1, 2); (0, 2); (3, 1)];
      [(1, 2); (0, 2); (2, 1); (3, 1); (4, 1)]; [(1, 2); (0, 2); (3, 1)];
      [(4, 2); (3, 2); (2, 2); (0, 3); (1, 3); (5, 1)];
      [(4, 2); (3, 2); (2, 2); (0, 3); (1, 3); (5, 1)]|],
    [|[(8, 3); (7, 3); (6, 2); (5, 2); (4, 2); (3, 1)];
      [(8, 3); (7, 3); (6, 2); (5, 2); (4, 2); (3, 1)]; [(8, 2); (7, 2); (5, 1)];
      [(8, 2); (7, 2); (4, 1); (5, 1); (6, 1)]; [(8, 2); (7, 2); (5, 1)];
      [(7, 1); (8, 1)]; []; []; []|])
    )
)

let shrink_array (size:int) (to_remove:int list) =
    let list_all = List.init size ((+)0) in
    let to_remove = (List.sort (-) to_remove) in
    let rec remove_all (base:int list) (to_remove:int list) = 
      match base,to_remove with
      | (_,[])
      | ([],_) -> base
      | (hd1::tl1,hd2::_) when hd1 < hd2 -> hd1::(remove_all tl1 to_remove)
      | (hd1::tl1,hd2::tl2) when hd1 = hd2 -> (remove_all tl1 tl2)
      | _ -> failwith "shrink_array : incoherent size / to_remove (remove all)"
    in
    let new_to_old = Array.of_list @@ remove_all list_all to_remove in
    
    let rec skip_all (base:int list) (i:int) (to_remove:int list) =
      match base,to_remove with 
      | [],_ -> []
      | _::tl,[] -> i::(skip_all tl (i+1) to_remove)
      | (hd1::tl1,hd2::_) when hd1 < hd2 -> i::(skip_all tl1 (i+1) to_remove)
      | (hd1::tl1,hd2::tl2) when hd1 = hd2 -> (-1)::(skip_all tl1 (i) tl2)
      | _ -> failwith "shrink_array : incoherent size / to_remove (skip_all)"
    in
    let old_to_new = Array.of_list @@ skip_all list_all 0 to_remove in
    (old_to_new,new_to_old)

let _ = (
    assert (shrink_array 10 [1;5;6;9] = ([|0;-1;1;2;3;-1;-1;4;5;-1|],[|0;2;3;4;7;8|]));
    assert (shrink_array 3 [] = ([|0;1;2|],[|0;1;2|]));
    assert (shrink_array 5 [0;1;2;3;4] = ([|-1; -1; -1; -1; -1|], [||]));
    assert (shrink_array 5 [0;1;3;4] = ([|-1; -1; 0; -1; -1|], [|2|]));
)

(* let filter_0_instructions_restore = ref (fun s -> s) *)
let filter_0_instructions p : problem * t =
  let _ = match p.reference_counting with
  | Some _ -> Tools.Log.once 0 (fun _ -> Printf.printf "WARNING : filter_0_instructions not updated to regpress\n";);
  | None -> () in
  let nb_instruction = Array.length p.instruction_usages in
  let succ = Array.make (nb_instruction+1) [] in 
  let pred = Array.make (nb_instruction+1) [] in 
  List.iter (fun lc ->
    succ.(lc.instr_from) <- (lc.instr_to,lc.latency)::succ.(lc.instr_from);
    pred.(lc.instr_to) <- (lc.instr_from,lc.latency)::pred.(lc.instr_to);
  ) p.latency_constraints;
  
  (*Add a first instruction before every other*)
  succ.(nb_instruction) <- List.init nb_instruction (fun i -> (i,0));
  Array.iteri (fun i p -> pred.(i) <- (nb_instruction,0)::p) pred;
  pred.(nb_instruction) <- [];
  
  let to_remove = 
    List.filter_map (fun (i,b) -> if b then Some i else None) @@ 
    Array.to_list @@ 
    Array.mapi (fun i u -> i,Array.for_all ((=)0) u) p.instruction_usages in

  let (pred,succ) = extend_latency pred succ to_remove in
  (* List.iter (fun i ->
    extend_i_latency pred succ i
  ) to_remove; *)
  (* let 

  let new_latency = List.map (fun (instr_from,instr_to,latency) -> {instr_from;instr_to;latency}) (extend_latency pred succ to_remove) in
  let new_latency_constraints = List.append p.latency_constraints new_latency in *)

  let (old_to_new,new_to_old) = shrink_array (nb_instruction+1) to_remove in 
  (* Printf.printf "wut ? %d ?\n" nb_instruction; *)
  let filter_0_instructions_restore = ( fun s -> 
    (* Printf.printf "lol? %d ?\n" nb_instruction; *)
    let sol = Array.make (nb_instruction+1) (-1) in
    Array.iter2 (fun i v -> 
        sol.(i) <- v
    ) new_to_old s;
    sol.(nb_instruction) <- 0;
    Array.iteri ( fun i v -> 
        if v <> -1 then () else (
          (* 
          Fail if those three conditions are verify
              - i -> j   
              - i > j
              - i and j don't use resources
          Don't appen if 
              - filter_0_instruction is the first preprocess step
              - instruction start in topological order
          *)
          sol.(i) <- List.fold_left (fun acc (p,l) -> 
              let vp = sol.(p) in
              if vp = (-1) then failwith "latency constraints not in topological order" else
              max acc (vp + l)
          ) 0 pred.(i)
        )
    ) sol;
    Array.sub sol 0 nb_instruction
  ) in

  let latency_constraints = 
    List.flatten @@
    List.filter_map (fun i -> i) @@
    Array.to_list @@
    Array.mapi (fun old_p succ ->
        match old_to_new.(old_p) with
        | -1 -> None
        | p -> Some (List.fold_left (fun acc (old_s,l) -> 
          match old_to_new.(old_s) with
          | -1 -> acc
          | s -> {instr_from=p;instr_to=s;latency=l}::acc   
        ) [] succ)
    ) succ in

  (* let latency_constraints = List.filter_map (fun lc -> 
    match old_to_new.(lc.instr_from),old_to_new.(lc.instr_to) with
    | -1,_ | _,-1 -> None
    | instr_from,instr_to -> Some {instr_from;instr_to;latency=lc.latency} 
  ) new_latency_constraints in *)
  let instruction_usages = Array.of_list @@ List.filteri (fun i _ -> old_to_new.(i) <> -1) @@ (Array.to_list p.instruction_usages) in
  let instruction_usages = Array.append instruction_usages [|Array.make (Array.length p.resource_bounds) 0|] in
  {p with
    latency_constraints;
    instruction_usages
  },filter_0_instructions_restore

let transitive_closure p : problem * t=
    let latency_constraints = p.latency_constraints in
    let nb_instruction = Array.length p.instruction_usages in
    let dag = List.map (fun lc -> (lc.instr_from,lc.instr_to,lc.latency)) latency_constraints in
    let (tc_dag,_) = DAG.transitive_closure (dag,nb_instruction) in
    let latency_constraints = List.map (fun (instr_from,instr_to,latency) -> {instr_from;instr_to;latency}) tc_dag in
    assert ((tc_dag,nb_instruction) = (DAG.transitive_closure (tc_dag,nb_instruction)));
    {p with latency_constraints},(fun i->i)

let inc_0_latency p : problem * t =
    let latency_constraints = p.latency_constraints in
    let does_overflow arr1 arr2 arrMax =
      let flag = ref false in
      Array.iteri (fun i vMax -> flag := !flag || arr1.(i) + arr2.(i) > vMax) arrMax;
      !flag
    in
    let latency_constraints = List.map (fun {instr_from;instr_to;latency} -> 
      match latency with 
      |0 when does_overflow p.instruction_usages.(instr_from) p.instruction_usages.(instr_to) p.resource_bounds -> 
          {instr_from;instr_to;latency=1}
      |_ -> {instr_from;instr_to;latency}
    ) latency_constraints in 
    {p with latency_constraints},(fun i->i)

let minimize (p:problem) : problem * t = 
    let (p,_) = transitive_closure p in
    let latency_constraints = p.latency_constraints in
    let nb_instruction = Array.length p.instruction_usages in
    let dag = List.map (fun lc -> (lc.instr_from,lc.instr_to,lc.latency)) latency_constraints in
    let (tc_dag,_) = DAG.minimize (dag,nb_instruction) in
    let latency_constraints = List.map (fun (instr_from,instr_to,latency) -> {instr_from;instr_to;latency}) tc_dag in
    assert (DAG.sort (tc_dag,nb_instruction) = DAG.sort (DAG.minimize (tc_dag,nb_instruction)));
    {p with latency_constraints},(fun i->i)

(* let instrucion_dominance p =
    let nb_instruction = Array.length p.instruction_usages in
    let l_from = Array.make nb_instruction [] in
    let l_to = Array.make nb_instruction [] in
    List.iter (fun lc -> 
      l_from.(lc.instr_from) <- lc::l_from.(lc.instr_from);
      l_to.(lc.instr_to) <- lc::l_to.(lc.instr_to);
    ) p.latency_constraints;
    let dominance p i j =
    in
    p *)

let add_register_latency (p:problem) : problem * t = failwith "Not implemented"
  (* match p.reference_counting with
  | None -> p,(fun a -> a)
  | Some (by_r,by_i) -> 
    (* 
      (r , (write,read)) 
      write : Register.reg option = the eventual instruction creating the register
      read : Register.reg list = exostive list of instruction reading r 
    *)
    let reg_tbl = Hashtbl.create 101 in
    Seq.iter (fun r -> 
      match Hashtbl.find_opt reg_tbl r with
      | Some _ -> ()
      | None -> Hashtbl.add reg_tbl r (None,[])
    ) @@ Hashtbl.to_seq_keys by_r;
    
    Array.iteri (fun i rl ->
      List.iter (fun (r,do_read) ->
        let (write,read) = Hashtbl.find reg_tbl r in
        match do_read with
        | Arg | ExtArg -> Hashtbl.replace reg_tbl r (write,i::read)
        | Def -> assert (write=None); Hashtbl.replace reg_tbl r (Some i,read)
      ) rl
    ) by_i;

    let new_latency_constraints = ref p.latency_constraints in
    Hashtbl.iter (fun _ (write,read) -> 
      match write,read with
      | (_, []) -> ()
      | (None, _) -> ()
      | (Some rw, rl) -> 
        List.iter ( fun rr ->
          new_latency_constraints := {instr_from=rw;instr_to=rr;latency=0}::!new_latency_constraints
        ) rl
    ) reg_tbl;

    {p with latency_constraints = !new_latency_constraints},(fun a -> a) *)


let compute steps p : problem * t = 
    (* Printf.printf "calling preprocess...\n"; *)
    List.fold_left (
      fun (acc_p,acc_r) f -> 
          let (p,r) = f acc_p in
          (p,(fun i -> r @@ acc_r i))
    ) (p,(fun i -> i)) (steps ~filter_0_instructions ~transitive_closure ~minimize ~inc_0_latency)

let _to_solution s (restore:t) = 
    (* Log.p 3 (fun _ -> Printf.printf "pseudo solution : [%s]\n" (string_of_int_array s)); *)
    let s = restore s in
    let ms = 1 + Array.fold_left (max) 0 s in
    let sol = (Array.append s [|ms|]) in
    (* Log.p 3 (fun _ -> Printf.printf "true solution : [%s]\n" (string_of_int_array sol)); *)
    sol

let postprocess (s:unprocessed_solution) (restore:t) = restore s.raw_solution