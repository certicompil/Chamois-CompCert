(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Léo Gourdin        VERIMAG, UGA                   *)
(*           Benjamin Bonneau   ENS, PSL                       *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

(** BTL instruction printer *)

open Printf
open Camlcoq
open Datatypes
open Maps
open BTL
open PrintAST
open PrintVA
open DebugPrint
open BTLtypes
open BTL_Invariants
open BTL_SEsimuref
open PrintOp
open IntPromotionCommon
open RTLcommonaux

(* Printing of BTL code *)

(* options *)
let print_aaddr_annot_opt = ref false
let print_sval_hashcode_opt = ref true

let rec print_list ?(sep=fun pp -> fprintf pp ", ") f pp = function
  | []      -> ()
  | [x]     -> f pp x
  | x :: xs -> f pp x;
               sep pp;
               print_list ~sep f pp xs

let reg pp r = fprintf pp "x%d" (P.to_int r)

let rec regs pp = function
  | [] -> ()
  | [ r ] -> reg pp r
  | r1 :: rl -> fprintf pp "%a, %a" reg r1 regs rl

let ros pp = function
  | Coq_inl r -> reg pp r
  | Coq_inr s -> fprintf pp "\"%s\"" (PrintAST.name_of_qualident s)

let print_succ pp s = fprintf pp "\tsucc %d\n" (P.to_int s)
let print_pref pp pref = fprintf pp "%s" pref

let print_aaddr pp (aaddr : ValueDomain.aval option) =
  if !print_aaddr_annot_opt
  then match aaddr with
  | None -> ()
  | Some aaddr -> fprintf pp " (< %a)" print_aval aaddr

let print_store_num pp = function
  | SNumNone  -> ()
  | SNumInv i -> fprintf pp "#i%d" (P.to_int i)
  | SNumSrc i -> fprintf pp "#%d"  (P.to_int i)

let print_store_info pp sinfo =
  if !print_aaddr_annot_opt
  then begin
    fprintf pp " (%a" print_store_num sinfo.store_num;
    begin match sinfo.store_aaddr with
    | None -> ()
    | Some aaddr -> fprintf pp " < %a" print_aval aaddr
    end;
    fprintf pp ")"
  end

let print_sgn pp sgn =
  if sgn then fprintf pp "s" else fprintf pp "u"

let print_prom_sgns pp (usg,sgn) =
  (if usg <> None then fprintf pp "u");
  (if sgn <> None then fprintf pp "s")

let print_inst_info pp (iinfo, args) =
  match iinfo.inst_promotion with
  | IPromNone   -> ()
  | IPromotableOp prom ->
      fprintf pp " [promotable %a]"
        print_prom_sgns (prom.op_prom0.op_prom_usg, prom.op_prom0.op_prom_sgn)
  | IPromotableCond prom ->
      fprintf pp " [promotable %a]"
        print_prom_sgns (prom.cond_prom0.cond_prom_usg, prom.cond_prom0.cond_prom_sgn)
  | IPromotedOp (op, _, sgn, ret_sgn) ->
      fprintf pp " [promoted %a %a%a]" (print_operation reg) (op, args) print_sgn sgn print_sgn ret_sgn
  | IPromotedCond (cond, _, sgn) ->
      fprintf pp " [promoted %a %a]" (print_condition reg) (cond, args) print_sgn sgn

let rec print_iblock pp is_rec pref ib =
  match ib with
  | Bnop None ->
      print_pref pp pref;
      fprintf pp "??: Bnop None\n"
  | Bnop (Some iinfo) ->
      print_pref pp pref;
      fprintf pp "%d: Bnop\n" iinfo.iinfo_shadow.inumb
  | Bop (op, args, res, iinfo) ->
      print_pref pp pref;
      fprintf pp "%d: Bop: %a = %a%a\n" iinfo.iinfo_shadow.inumb
        reg res (print_operation reg) (op, args)
        print_inst_info (iinfo, args)
  | Bload (trap, chunk, addr, args, dst, iinfo) ->
      print_pref pp pref;
      fprintf pp "%d: Bload: %a = %s[%a%a]%a\n" iinfo.iinfo_shadow.inumb reg dst
        (name_of_chunk chunk) (print_addressing reg) (addr, args)
        print_aaddr iinfo.addr_aval
        print_trapping_mode trap
  | Bstore (chunk, addr, args, src, iinfo) ->
      print_pref pp pref;
      fprintf pp "%d: Bstore: %s[%a%a] = %a\n" iinfo.iinfo_shadow.inumb (name_of_chunk chunk)
        (print_addressing reg) (addr, args)
        print_store_info (BTL.store_info_of_iinfo iinfo) reg src
  | BF (Bcall (sg, fn, args, res, s), iinfo) ->
      print_pref pp pref;
      fprintf pp "%d: Bcall: %a = %a(%a)\n" iinfo.iinfo_shadow.inumb reg res ros fn regs args;
      print_succ pp s
  | BF (Btailcall (sg, fn, args), iinfo) ->
      print_pref pp pref;
      fprintf pp "%d: Btailcall: %a(%a)\n" iinfo.iinfo_shadow.inumb ros fn regs args
  | BF (Bbuiltin (ef, args, res, s), iinfo) ->
      print_pref pp pref;
      fprintf pp "%d: Bbuiltin: %a = %s(%a)\n" iinfo.iinfo_shadow.inumb
        (print_builtin_res reg) res (name_of_external ef)
        (print_builtin_args reg) args;
      print_succ pp s
  | Bcond (cond, args, ib1, ib2, iinfo) ->
      print_pref pp pref;
      fprintf pp "%d: Bcond: (%a) (prediction: %s)%a\n" iinfo.iinfo_shadow.inumb
        (print_condition reg) (cond, args)
        (match iinfo.iinfo_shadow.opt_info with
        | None -> "none"
        | Some true -> "branch"
        | Some false -> "fallthrough")
        print_inst_info (iinfo, args);
      let pref' = pref ^ "  " in
      fprintf pp "%sifso = [\n" pref;
      if is_rec then print_iblock pp is_rec pref' ib1 else fprintf pp "...\n";
      fprintf pp "%s]\n" pref;
      fprintf pp "%sifnot = [\n" pref;
      if is_rec then print_iblock pp is_rec pref' ib2 else fprintf pp "...\n";
      fprintf pp "%s]\n" pref
  | BF (Bjumptable (arg, tbl), iinfo) ->
      let tbl = Array.of_list tbl in
      print_pref pp pref;
      fprintf pp "%d: Bjumptable: (%a)\n" iinfo.iinfo_shadow.inumb reg arg;
      for i = 0 to Array.length tbl - 1 do
        fprintf pp "\t\tcase %d: goto %d\n" i (P.to_int tbl.(i))
      done
  | BF (Breturn None, iinfo) ->
      print_pref pp pref;
      fprintf pp "%d: Breturn\n" iinfo.iinfo_shadow.inumb
  | BF (Breturn (Some arg), iinfo) ->
      print_pref pp pref;
      fprintf pp "%d: Breturn: %a\n" iinfo.iinfo_shadow.inumb reg arg
  | BF (Bgoto s, iinfo) ->
      print_pref pp pref;
      fprintf pp "%d: Bgoto: %d\n" iinfo.iinfo_shadow.inumb (P.to_int s)
  | Bseq (ib1, ib2) ->
      if is_rec then (
        print_iblock pp is_rec pref ib1;
        print_iblock pp is_rec pref ib2)
      else fprintf pp "Bseq...\n"

let print_btl_inst pp ib =
  if !debug_flag then print_iblock pp false "  " ib else ()

let print_btl_block pp ibf n =
  if !debug_flag then (
    fprintf pp "\n";
    fprintf pp "[BTL Liveness] ";
    print_regset ibf.binfo.input_regs;
    fprintf pp "\n";
    fprintf pp "[BTL block %d with bnumb %d]\n" (P.to_int n) ibf.binfo.bnumb;
    print_iblock pp true "  " ibf.entry;
    fprintf pp "\n")
  else ()

let sort_elts ptree = List.sort (fun (k1, _) (k2, _) -> P.compare k2 k1) (PTree.elements ptree)

let print_btl_code pp btl =
  if !debug_flag then (
    fprintf pp "\n";
    let elts = sort_elts btl in
    List.iter
      (fun (n, ibf) ->
        fprintf pp "[BTL Liveness] ";
        print_regset ibf.binfo.input_regs;
        fprintf pp "\n";
        fprintf pp "[BTL block %d with bnumb %d]\n" (P.to_int n) ibf.binfo.bnumb;
        fprintf pp "\n";
        print_iblock pp true "  " ibf.entry;
        fprintf pp "\n")
      elts;
    fprintf pp "\n")
  else ()

let destination : string option ref = ref None

let print_if passno prog =
  match !destination with
  | None -> ()
  | Some f ->
      let oc = open_out (f ^ "." ^ Z.to_string passno) in
      print_btl_code oc prog;
      close_out oc

let print_ireg pp ir =
  if ir.force_input then fprintf pp "input/%d " (P.to_int ir.regof)
  else fprintf pp "last/%d " (P.to_int ir.regof)

let rec print_ireg_list pp = function
  | [] -> ()
  | ir :: lir ->
      print_ireg pp ir;
      print_ireg_list pp lir

let print_rop pp (rop, args) =
  match rop with
  | Rop op -> fprintf pp "Rop [%a]" (print_operation reg) (op, args)
  | Rload (trap, chk, addr, aaddr) ->
      fprintf pp "Rload [%a, %s, %a%a]" print_trapping_mode trap
        (name_of_chunk chk) (print_addressing reg) (addr, args)
        print_aaddr aaddr

let rec lir_to_lr = function [] -> [] | ir :: lir -> ir.regof :: lir_to_lr lir

let print_ival pp = function
  | Ireg ir -> fprintf pp "Ireg %a" print_ireg ir
  | Iop (rop, args) ->
      fprintf pp "Iop [%a, %a]" print_ireg_list args print_rop
        (rop, lir_to_lr args)

let print_istore pp = function
  | Coq_mk_istore (chk, addr, args, sinfo, src) ->
      fprintf pp "%s[%a%a] <- %a"
        (name_of_chunk chk)
        (print_addressing reg) (addr, args)
        print_aaddr sinfo.store_aaddr
        reg src

let print_imem =
  print_list ~sep:(fun pp -> fprintf pp "; ") print_istore

let rec lsval_length = function
  | Snil _ -> 0
  | Scons (_, lsv, _) -> 1 + lsval_length lsv

let fake_args lsv = List.init (lsval_length lsv) (fun _ -> i2p 1)

let print_sval_hc pp hc =
    if !print_sval_hashcode_opt
    then fprintf pp "#%d" hc
    else ()

let name_of_permission = Memtype.(function
  | Freeable -> "Freeable"
  | Writable -> "Readable"
  | Readable -> "Readable"
  | Nonempty -> "Nonempty")

let rec print_sval pp = function
  | Sinput (false, r, hc) ->
      fprintf pp "Sinput%a (%d)"
        print_sval_hc hc (P.to_int r)
  | Sinput (true, r, hc) ->
      fprintf pp "Sinput%a (true, %d)"
        print_sval_hc hc (P.to_int r)
  | Sop (op, lsv, hc) ->
      fprintf pp "Sop%a (%a, %a)"
        print_sval_hc hc
        (print_operation reg) (op, fake_args lsv)
        print_lsval lsv
  | Sfoldr (op, lsv, sv0, _) ->
      let fargs = [i2p 1; i2p 1] in
      fprintf pp "Sfoldr (%a, %a, %a)" (print_operation reg)
        (op, fargs) print_lsval lsv print_sval sv0
  | Sload (sm, trap, chk, addr, lsv, hc) ->
      fprintf pp "Sload%a (%a, %a, %s, %a, %a)"
        print_sval_hc hc
        print_smem sm
        print_trapping_mode trap
        (name_of_chunk chk)
        (print_addressing reg) (addr, fake_args lsv)
        print_lsval lsv
  | SmayUndef (okc, sv, hc) ->
      fprintf pp "SmayUndef%a (%a, %a)"
        print_sval_hc hc
        print_okclause okc
        print_sval sv

and print_lsval pp = function
  | Snil hc -> fprintf pp "%a" print_sval_hc hc
  | Scons (sv, lsv, hc) ->
      fprintf pp "Scons%a (%a, %a)" print_sval_hc hc print_sval sv print_lsval lsv

and print_smem pp = function
  | Sinit hc -> fprintf pp "Sinit%a" print_sval_hc hc
  | Sstore (sm, chk, addr, lsv, sinfo, src, hc) ->
      fprintf pp "Sstore%a (%a, %s, %a, %a, %a%a)" print_sval_hc hc print_smem sm (name_of_chunk chk)
        (print_addressing reg) (addr, fake_args lsv)
        print_lsval lsv print_sval src
        print_store_info sinfo

and print_okclause pp = function
  | OKfalse hc ->
      fprintf pp "OKfalse%a"
        print_sval_hc hc
  | OKalive (sv, hc) ->
      fprintf pp "OKalive%a (%a)"
        print_sval_hc hc
        print_sval sv
  | OKpromotableOp (op, prom, lsv, hc) ->
      fprintf pp "OKpromotableOp%a (%a, %a, %a)"
        print_sval_hc hc
        (print_operation reg) (op, fake_args lsv)
        print_prom_sgns (prom.op_prom0.op_prom_usg, prom.op_prom0.op_prom_sgn)
        print_lsval lsv
  | OKpromotableCond (cond, prom, lsv, hc) ->
      fprintf pp "OKpromotableCond%a (%a, %a, %a)"
        print_sval_hc hc
        (print_condition reg) (cond, fake_args lsv)
        print_prom_sgns (prom.cond_prom0.cond_prom_usg, prom.cond_prom0.cond_prom_sgn)
        print_lsval lsv
  | OKaddrMatch (addr, args, aa, hc) ->
      fprintf pp "OKaddrMatch%a (%a, %a, %a)"
        print_sval_hc hc
        (print_addressing reg) (addr, fake_args args)
        print_lsval args
        print_aval aa
  | OKvalidAccess (perm, chk, addr, args, hc) ->
      fprintf pp "OKloadRange%a (%s, %s, %a, %a)"
        print_sval_hc hc
        (name_of_permission perm)
        (name_of_chunk chk)
        (print_addressing reg) (addr, fake_args args)
        print_lsval args


let rec print_list_sval pp = function
  | [] -> ()
  | sv :: l -> fprintf pp "%a, " print_sval sv; print_list_sval pp l

let print_sreg sreg =
  List.iter
    (fun (n, sv) -> debug "%d |-> %a\n" (P.to_int n) print_sval sv)
    (PTree.elements sreg)

let print_option f pp = function
  | Some x -> fprintf pp "Some %a" f x
  | None   -> fprintf pp "None"

let print_sstate (sst: BTL_SEsimuref.ristate) =
  debug "ris_smem=%a;\n" print_smem sst.ris_smem;
  debug "ris_input_init=%a;\n" (print_option (fun pp -> fprintf pp "%b")) sst.ris_input_init;
  debug "ris_pre=%a;\n" (print_list print_okclause) sst.ris_pre;
  debug "{\nris_sreg =\n";
  print_sreg sst.ris_sreg;
  debug "\n}\n"

let print_sstates sst1 sst2 =
  debug "Symbolic state 1:\n";
  print_sstate sst1;
  debug "Symbolic state 2:\n";
  print_sstate sst2

let print_csasv pp name csasv =
  if csasv.aseq <> []
  then fprintf pp "%s.aseq := [%a]\n"
          name
          (print_list ~sep:(fun pp -> fprintf pp "; ")
            (fun pp (r, iv) -> fprintf pp "%d <- %a" (P.to_int r) print_ival iv)) csasv.aseq;
  if Registers.Regset.cardinal csasv.outputs > Camlcoq.Nat.of_int 0
  then fprintf pp "%s.outputs := [%a]\n"
          name
          (print_list ~sep:(fun pp -> fprintf pp " ")
            (fun pp n -> fprintf pp "%d" (P.to_int n))) (Registers.Regset.elements csasv.outputs)

let print_invariant pp inv =
  print_csasv pp "glue" inv.glue;
  print_csasv pp "hist" inv.history;
  if inv.meminv <> []
  then fprintf pp "meminv := [%a]\n" print_imem inv.meminv

let print_gluemap gm =
  if !debug_flag then
    List.iter
      (fun (n, inv) -> debug "Invariants for block %d\n%a" (P.to_int n) print_invariant inv)
      (sort_elts gm)

module PrintGV = struct
  (* Printing in Graphviz dot format *)
  (* Because we need to escape HTML special characters, the output .GV file need to be preprocessed
     with tools/preprocess_graphviz.gv before being displayed. *)

  let node_code pp co =
    fprintf pp "<VERBATIM>\n";
    print_iblock pp true "" co;
    fprintf pp "</VERBATIM>\n"

  let node_text pp other_pre other_post (ibnum, ib) =
    fprintf pp "<table border=\"1\" cellborder=\"0\" cellspacing=\"0\">\n";
    fprintf pp "<tr><td>Block %d</td></tr>\n" (P.to_int ibnum);
    fprintf pp "<hr/>\n";
    other_pre pp;
    fprintf pp "<tr><td balign=\"left\">\n%a</td></tr>\n" node_code ib.entry;
    other_post pp;
    fprintf pp "</table>\n"

  let node_id pp ibnum =
    fprintf pp "iblock%d" (P.to_int ibnum)

  let node pp ibnum text =
    fprintf pp "%a [ label=<\n%a> ]\n" node_id ibnum text ()

  let node_exits pp (ibnum, ib) =
    fprintf pp "%a -> {%a}\n"
      node_id ibnum
      (print_list ~sep:(fun pp -> fprintf pp " ") node_id)
        (BTLcommonaux.successors_instr_btl ib.entry)

  let graph pp p_node other (btl : BTL.coq_function) =
    fprintf pp "digraph BTL {\n";
    fprintf pp "graph [layout=dot rankdir=TB]\n";
    fprintf pp "node [shape=plain]\n";
    let es = PTree.elements btl.fn_code in
    List.iter (p_node pp) es;
    List.iter (node_exits pp) es;
    fprintf pp "{ rank=source; entry }\n";
    fprintf pp "entry -> %a\n" node_id btl.fn_entrypoint;
    other pp;
    fprintf pp "}\n"

  let btl pp (btl : BTL.coq_function) =
    graph pp (fun pp (ibnum, ib) -> node pp ibnum (fun pp () ->
                node_text pp (fun pp -> ()) (fun pp -> ()) (ibnum, ib)))
             (fun pp -> ()) btl

  let btl_gm pp ((btl, gm) : BTL.coq_function * BTL_Invariants.gluemap) =
    graph pp (fun pp (ibnum, ib) -> node pp ibnum (fun pp () ->
      node_text pp (fun pp ->
        Option.iter
          (fprintf pp "<tr><td balign=\"left\">\n<VERBATIM>\n%a</VERBATIM>\n</td></tr>\n" print_invariant)
          (PTree.get ibnum gm);
        fprintf pp "<hr/>\n"
      ) (fun pp -> ()) (ibnum, ib)))
      (fun pp -> ()) btl

  let to_file (file : string) p btl =
    let f = open_out file in
    p f btl;
    close_out f

end
