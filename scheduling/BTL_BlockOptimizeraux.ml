(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Léo Gourdin        VERIMAG, UGA                   *)
(*           Nicolas Nardino    ENS-Lyon                       *)
(*           Cyril Six          Kalray                         *)
(*           David Monniaux     CNRS, VERIMAG                  *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

(** Auxiliary function of the BTL prepass scheduler oracle *)

open PrintBTL
open BTL
open Machine
open RTLcommonaux
open Registers
open BTLtypes
open BTLcommonaux

(* Number of registers used by a value *)

module SI = Set.Make (Int)

let is_a_cb = function Bcond _ -> true | _ -> false
let is_a_load = function Bload _ -> true | _ -> false

let count_cbs bseq olast indexes =
  let current_cbs = ref SI.empty in
  let cbs_above = Hashtbl.create 100 in
  let update_cbs n ib =
    print_btl_inst stderr ib;
    if is_a_cb ib then current_cbs := SI.add indexes.(n) !current_cbs
    else if is_a_load ib then Hashtbl.add cbs_above indexes.(n) !current_cbs
  in
  Array.iteri (fun n ib -> update_cbs n ib) bseq;
  (match olast with
  | Some last -> update_cbs (Array.length bseq) last
  | None -> ());
  cbs_above

let find_array arr n =
  let index = ref None in
  (try
     Array.iteri
       (fun i n' ->
         match !index with
         | Some _ -> raise Exit
         | None -> if n = n' then index := Some i)
       arr
   with Exit -> ());
  get_some @@ !index

let apply_schedule bseq olast positions =
  let bseq_new = Array.map (fun i -> bseq.(i)) positions in
  (if !config.has_non_trapping_loads && !Clflags.option_fnontrap_loads then
   let fmap n = find_array positions n in
   let seq = Array.init (Array.length positions) (fun i -> i) in
   let fseq = Array.map fmap seq in
   let cbs_above_old = count_cbs bseq olast fseq in
   let cbs_above_new = count_cbs bseq_new olast seq in
   Array.iteri
     (fun n ib ->
       let n' = fseq.(n) in
       match ib with
       | Bload (t, a, b, c, d, e) ->
           (* If the load was trapping and has not been moved above a condition,
              we change it back into a trapping load. *)
           let set_old = Hashtbl.find cbs_above_old n' in
           let set_new = Hashtbl.find cbs_above_new n' in
           if SI.subset set_old set_new then
             if get_some @@ e.iinfo_shadow.opt_info then
               bseq_new.(n') <- Bload (AST.TRAP, a, b, c, d, e)
             else assert !config.has_non_trapping_loads
       | _ -> ())
     bseq);
  let ibl = Array.to_list bseq_new in
  let rec build_iblock = function
    | [] -> failwith "build_iblock: empty list"
    | [ ib ] -> (match olast with Some last -> Bseq (ib, last) | None -> ib)
    | ib1 :: ib2 :: k -> Bseq (ib1, build_iblock (ib2 :: k))
  in
  build_iblock ibl

type mentions_element = Def | Arg | ExtArg
type register_info = {
  multiplicity : int;
  nb_read : int;
  rg_class : int;
}

let reference_counting (seqa : (iblock * Regset.t) array) olast (out_regs : Regset.t)
    (typing : RTLtyping.regenv) :
    (reg, register_info) Hashtbl.t * (reg * mentions_element) list array =
  let retl = Hashtbl.create 42 in
  let retr = Array.make (Array.length seqa + 1) [] in
  (* retr.(i) : (r, b) -> (r', b') -> ...
   * where b = Arg if seen as arg, Dest if seen as dest, and ExtArg if seen
   * as an external arg *)
  List.iter
    (fun reg ->
      let ty = typing reg in
      Hashtbl.add retl reg {
        multiplicity = multiplicity_of_type ty;
        nb_read = multiplicity_of_type ty;      (*add usage for all out registers TODO : check if true*)
        rg_class = Machregsaux.class_of_type ty;
      })
    (Regset.elements out_regs);
  let add_reg reg =
    match Hashtbl.find_opt retl reg with
    | Some {multiplicity;nb_read;rg_class} -> Hashtbl.add retl reg {
        multiplicity;
        nb_read = nb_read + multiplicity;      
        rg_class;
      }
    | None -> 
    let ty = typing reg in
    let nr_regs = multiplicity_of_type ty in
    Hashtbl.add retl reg {
      multiplicity = nr_regs;
      nb_read = nr_regs;
      rg_class = Machregsaux.class_of_type ty;
    }
  in
  let map_arg = List.map (fun r -> (r, Arg)) in
  Array.iteri
    (fun i (ins, liveins) ->
      (match ins with
       | Bop (_, args, dest, _) | Bload (_, _, _, args, dest, _) ->
           List.iter add_reg args;
           retr.(i) <- (dest, Def) :: map_arg args
       | Bcond (_, args, _, _, _) ->
           List.iter add_reg args;
           retr.(i) <- map_arg args
       | Bstore (_, _, args, src, _) ->
           List.iter add_reg args;
           add_reg src;
           retr.(i) <- (src, Arg) :: map_arg args
       | BF (Bcall (_, fn, args, dest, _), _) ->
           List.iter add_reg args;
           retr.(i) <-
             (match fn with
             | Datatypes.Coq_inl reg ->
                 add_reg reg;
                 (dest, Def) :: (reg, Arg) :: map_arg args
             | _ -> (dest, Def) :: map_arg args)
       | BF (Btailcall (_, fn, args), _) ->
           List.iter add_reg args;
           retr.(i) <-
             (match fn with
             | Datatypes.Coq_inl reg ->
                 add_reg reg;
                 (reg, Arg) :: map_arg args
             | _ -> map_arg args)
       | BF (Bbuiltin (_, args, dest, _), _) ->
           let rec bar = function
             | AST.BA r ->
                 add_reg r;
                 retr.(i) <- (r, Arg) :: retr.(i)
             | AST.BA_splitlong (hi, lo) | AST.BA_addptr (hi, lo) ->
                 bar hi;
                 bar lo
             | _ -> ()
           in
           List.iter bar args;
           let rec bad = function
             | AST.BR r -> retr.(i) <- (r, Def) :: retr.(i)
             | AST.BR_splitlong (hi, lo) ->
                 bad hi;
                 bad lo
             | _ -> ()
           in
           bad dest
       | BF (Bjumptable (reg, _), _) | BF (Breturn (Some reg), _) ->
           add_reg reg;
           retr.(i) <- [ (reg, Arg) ]
       | _ -> ());
      retr.(i) <- retr.(i) @ (List.map (fun r ->
        add_reg r;
        (r, ExtArg)) (Regset.elements liveins)))
    seqa;
  (* add mention information about olast *)
  let olast_mention =
    let map_def = List.map (fun reg -> (reg, Def)) in
    let map_arg = List.map (fun reg -> (reg, Arg)) in
    let ld, la = get_olast_dest_args olast in
    map_def ld @ map_arg la in
  List.iter (fun (reg, _) -> add_reg reg) olast_mention;
  retr.(Array.length retr - 1) <-
    olast_mention @ (List.map (fun r -> (r, ExtArg)) (Regset.elements out_regs));
  (* sorting for variable evaluation order *)
  let sort_mentions l =
    let args = List.filter (fun (_, arg) -> arg = Arg) l in
    let defs = List.filter (fun (_, arg) -> arg = Def) l in
    let eargs = List.filter (fun (_, arg) -> arg = ExtArg) l in
    List.concat [args; defs; eargs] in
  let retr = Array.map sort_mentions retr in
  (* print_string "mentions\n";
   * Array.iteri (fun i l ->
   *     print_int i;
   *     print_string ": [";
   *     List.iter (fun (r, b) ->
   *         print_int (Camlcoq.P.to_int r);
   *         print_string ":";
   *         print_string (if b then "a:" else "d");
   *         if b then print_int (snd (Hashtbl.find retl r));
   *         print_string ", "
   *       ) l;
   *     print_string "]\n";
   *     flush stdout;
   *   ) retr; *)
  (retl, retr)

let flatten_blk_basics ibf =
  let ib = ibf.entry in
  let last = ref None in
  let rec traverse_blk ib =
    match ib with
    | BF (_, _) ->
        last := Some ib;
        []
    | Bseq ((Bcond (_, _, _, _, iinfo) as ib1), ib2) -> (
        match iinfo.iinfo_shadow.opt_info with
        | Some _ -> ib1 :: traverse_blk ib2
        | None ->
            last := Some ib;
            [])
    | Bseq (ib1, ib2) -> traverse_blk ib1 @ traverse_blk ib2
    | _ -> [ ib ]
  in
  let ibl = traverse_blk ib in
  (Array.of_list ibl, !last)
