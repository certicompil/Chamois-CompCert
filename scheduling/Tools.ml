module Array_operation = struct
  (**
    if [max src1 src2] exist, ensure [dest] is equal to the max of [src1] [src2] according to [cmp].
    if not, do nothing.
  *)
  let max_in ?(copy=fun (v:'a) -> v) (dest:'a array) (src1:'a array) (src2:'a array) (cmp:'a array -> 'a array -> int) = 
    match cmp src1 src2 with
    | v when v > 0 -> Array.iteri (fun i v -> dest.(i) <- copy v) src1
    | v when v < 0 -> Array.iteri (fun i v -> dest.(i) <- copy v) src2
    | _ -> () 

  let copy_in ?(copy=fun (v:'a) -> v) (dest: 'a array) (src: 'a array) = Array.iteri (fun i v -> dest.(i) <- copy v) src
end

module Adv_string_conversion = struct
  let string_of_array_adv ?(sep="; ") ?(empty="") (a:'a array) (to_string:'a -> string) : string = 
    let str = (Array.fold_left (fun acc e -> acc^sep^(to_string e)) "" a) in
    let sep_size = (String.length sep) in
    if Array.length a = 0 then empty else
    String.sub str sep_size (String.length str - sep_size)
  
  let string_of_int_array ?(sep="; ") ?(empty="") (a:int array) :string = string_of_array_adv ~sep ~empty a (string_of_int)
  
  let string_of_bool_array ?(sep="; ") ?(empty="") (a:bool array) :string = string_of_array_adv ~sep ~empty a (string_of_bool)
  
  let string_of_list_adv ?(sep="; ") ?(empty="") (a:'a list) (to_string:'a -> string) : string = 
    let str = (List.fold_left (fun acc e -> acc^sep^(to_string e)) "" a) in
    let sep_size = (String.length sep) in
    if List.length a = 0 then empty else
    String.sub str sep_size (String.length str - sep_size)
  
  let string_of_seq_adv ?(sep="; ") ?(empty="") (a:'a Seq.t) (to_string:'a -> string) : string = 
    let str = (Seq.fold_left (fun acc e -> acc^sep^(to_string e)) "" a) in
    let sep_size = (String.length sep) in
    if Seq.is_empty a then empty else
    String.sub str sep_size (String.length str - sep_size)
  
  let string_of_option (to_string: 'a -> string) (o:'a Option.t) = match o with None -> "None" | Some s -> "Some "^(to_string s)
end

module Log = struct
  let log_level = ref 0
  let string_indent (i:int) :string = String.make (i*1) '_' 
  let p lvl f = if !log_level>=lvl then f () else ()
  let p_indent level lvl = (if !log_level>=lvl then Printf.printf "%s" (string_indent level));p lvl

  let tokens = Array.init 10 (fun _ -> true)
  let once (id:int) (f:unit -> unit) = 
  match tokens.(id) with
  | true  -> tokens.(id) <- false;f ()
  | false -> ()
end

module Test = struct
  
  let print_adv ?(out_channel=stdout) ?(indent=0) ?(indent_char=' ') (format : ('a, out_channel, unit) format) : 'a =
    match indent with
    | 0 | 1 | 2 | 3 ->
      let indent = String.init (indent*2) (fun _ -> indent_char) in
      Printf.fprintf out_channel "%s" indent;
      Printf.fprintf out_channel format 
    | _ -> (*ignore indent > 3*)
      Printf.ifprintf out_channel format 

  (*************************)

  let time (f: unit -> 'a) : float * 'a = 
    let start_t = Sys.time () in
    let result = f () in
    let end_t = Sys.time () in
    (end_t -. start_t, result)

  (*************************)

  module type Framework = sig
    val name : string
    val execute : unit -> bool 
  end

  let execute (module T : Framework) =
    print_adv "--- Starting test %s ---\n" T.name;
    match T.execute () with
    | true -> print_adv "Ending test %s : success\n\n" T.name;true
    | false -> print_adv "Ending test %s : failure\n\n" T.name;false

  (************************)

  let eq_array (test_name) (arr1) (arr2) =
    print_adv ~indent:1 "test %s (array equality)..." test_name;
    let res = Array.length arr1 = Array.length arr2 && Array.for_all2 (=) arr1 arr2 in
    (match res with
    | true -> print_adv "ok\n"
    | false -> print_adv "fail !\n";print_adv ~indent:2 "[%s] <> [%s]\n" (Adv_string_conversion.string_of_int_array arr1) (Adv_string_conversion.string_of_int_array arr2)
    );
    res

  let eq_int (test_name) (v1) (v2) =
    print_adv ~indent:1 "test %s (int equality)..." test_name;
    let res = (=) v1 v2 in
    (match res with
    | true -> print_adv "ok\n"
    | false -> print_adv "fail !\n";print_adv ~indent:2 "%d <> %d\n" v1 v2
    );
    res

  let eq (test_name:string) (v1:'a) (v2:'a) (eq:'a -> 'a -> bool) (to_string:'a -> string) =
    print_adv ~indent:1 "test %s (int equality)..." test_name;
    let res = eq v1 v2 in
    (match res with
    | true -> print_adv "ok\n"
    | false -> print_adv "fail !\n";print_adv ~indent:2 "%s <> %s\n" (to_string v1) (to_string v2)
    );
    res

  let catch_assert (test_name:string) (f:'a->'b) (v:'a) : bool =
    print_adv "test %s (assert)..." ~indent:1 test_name;
    try 
      let _ = f v in
      print_adv "ok\n";
      true
    with Assert_failure (msg,line,colu) -> 
      print_adv "assert fail !\n";print_adv ~indent:2 "file : %s line : %d : char : %d\n" msg line colu;
      false
end