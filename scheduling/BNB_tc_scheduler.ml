
type base = {
  latency : int array array;
  resources : int array;
  usages: int array array;
  is_free : bool array;
}

(*l must be equal to the number of non -1 in a*)
type array_with_length = {a:int array;mutable l:int}

type state = {
  latency : array_with_length array;
  resources_usage : int array;
  mutable time : int;
  schedule : array_with_length;
}

type t = {
  base : base; (*immutable*)
  state : state;
}

let init_raw (latency:int array array) (resources:int array) (usages:int array array) : t = 
  {
    base = {latency;resources;usages;
      is_free=Array.map (fun rv -> Array.for_all (fun r -> r=0) rv) usages
    };
    state = {
      latency = Array.map (fun a -> {
        a;
        l=(Array.fold_left (fun acc v -> if v=(-1) then acc else acc+1) 0 a)
      }) latency;
      resources_usage = Array.map (fun _ -> 0) resources;
      time = 0;
      schedule = {a=Array.map (fun _ -> -1) usages;l=0};
    }
  }

let copy (self:t) : t =
  {
    base = self.base;
    state = {
      latency = Array.map (fun a -> {
        a=Array.copy a.a;
        l = a.l
      }) self.state.latency;
      resources_usage = Array.copy self.state.resources_usage;
      time = self.state.time;
      schedule = {a=Array.copy self.state.schedule.a;l=self.state.schedule.l};
    }
  }

let check_ressource (self:t) (candidate:int) : bool = 
  let flag = ref true in
  Array.iteri (fun i max_usage -> 
    flag := !flag && self.state.resources_usage.(i) + self.base.usages.(candidate).(i) <= max_usage
  ) self.base.resources;
  !flag

let compute_possible (self:t) =
  let does_change = ref false in
  let possible = Array.map (fun v -> v.l = 0) self.state.latency in
  Array.iteri (fun i is_candidate -> 
    let is_possible = is_candidate && check_ressource self i in
    does_change := !does_change || is_possible; 
    possible.(i) <- is_possible
  ) possible;
  match !does_change with
  | true -> Some possible
  | false -> None

let advance_time (self:t) =
  Array.iteri (fun i _ ->  self.state.resources_usage.(i) <- 0) self.state.resources_usage;
  self.state.time <- self.state.time + 1;
  Array.iteri (fun i1 s_time -> 
    match s_time with
    | -1 ->
      Array.iteri (fun i2 latency -> 
        match self.state.schedule.a.(i2) with
        | -1 -> ()
        | _ when latency = 0 -> failwith "0 latency with already schedule instruction"
        | _ when latency = 1 -> 
          self.state.latency.(i1).a.(i2) <- -1;
          self.state.latency.(i1).l <- self.state.latency.(i1).l - 1
        | _ when latency > 1 -> 
          self.state.latency.(i1).a.(i2) <- latency - 1        
        | _  -> ()
      ) self.state.latency.(i1).a
    |  _ -> ()
  ) self.state.schedule.a

let schedule_instruction (self:t) (instruction:int) = 
  assert (self.state.latency.(instruction).l = 0);
  (*update resource usage*)
  Array.iteri (fun r current_usage -> 
    assert (current_usage + self.base.usages.(instruction).(r) <= self.base.resources.(r));
    self.state.resources_usage.(r) <- current_usage + self.base.usages.(instruction).(r)
  ) self.state.resources_usage;
  (*update schedule*)
  self.state.schedule.a.(instruction) <- self.state.time;
  self.state.schedule.l <- self.state.schedule.l + 1;
  (*"remove" instruction from latency*)
  self.state.latency.(instruction).l <- -1;
  (*update 0 latency from instruction*)
  for i = 0 to Array.length self.state.latency - 1 do
    match self.state.latency.(i).a.(instruction) with
    | 0 ->
      self.state.latency.(i).a.(instruction) <- -1;
      self.state.latency.(i).l <- self.state.latency.(i).l - 1
    | _ -> ()
  done

let total_makespan (self:t) : int option =
  match self.state.schedule.l = (Array.length self.state.schedule.a) with
  | true -> Some (self.state.time + 1)
  | false -> None

let rec next (self:t) : bool array =
  match total_makespan self with
  (*Return all false if no decision is needed*)
  | Some _ -> Array.map (fun _ -> false) self.base.usages
  | None ->
  (*compute possible value in regard to resources and latency*)
  let (possible) = match compute_possible self with
  | None -> 
    advance_time self; 
    next self
  | Some s -> s in
  let must_recompute_possible = ref false in
  (*schedule instructions with no resource usage*)
  Array.iteri (fun i p -> 
    match (p,self.base.is_free.(i)) with
    | false,_ -> ()
    | true,false -> ()
    | true,true -> 
      must_recompute_possible := true;
      schedule_instruction self i
  ) possible;
  if !must_recompute_possible then
    next self
  else
    possible

(*compute minimum delay for every instruction (-1 if already schedule) according to latency constraint alone *)
let compute_min_latency_delay (self:t) : int array =
  Array.map (fun c -> match c.l with
  | -1 -> -1
  |  0 ->  0
  |  _ -> Array.fold_left (max) 0 c.a 
  ) self.state.latency

(*
  compute for every instruction its prefer instruction i.e the instruction resulting in the maximum latency delay
  (compute_min_latency_delay self).(i) = latency.(i).c.((compute_prefer_instruction self).(i))
*)
let compute_prefer_instruction (self:t) : int array =
  Array.mapi (fun i c -> match c.l with
  | -1 -> -1
  |  0 ->  i
  |  _ -> 
    let (_,id,_) = Array.fold_left ( fun (acc_v,acc_id,id) v ->
      if v > acc_v then 
        (v,id,id+1) 
      else 
        (acc_v,acc_id,id+1) 
    ) (-1,0,0) c.a in
    id
  ) self.state.latency

(*
The instruction in the dependence of i resulting in the maximum latency (including partial latency).
If i does not depend on any instruction, -1 is return
*)
let critical_instruction self i : int = 
  let max_v = ref (-1) in
  let max_j = ref (-1) in
  Array.iteri (fun j v ->
    if v > !max_v then (
      max_v := v;
      max_j := j;
    )
  ) self.state.latency.(i).a;
  (* Printf.printf "a:[%s]\n" (Tools.Adv_string_conversion.string_of_array self.state.latency.(i).a); *)
  !max_j


let compute_delay (self:t) = 
  (* Array.mapi (fun i v -> if self.state.schedule.a.(i) <> (-1) then -1 else v) @@  *)
  Array.map (fun ll -> 
    match ll.l with
    | 0 -> 0
    | -1 -> -1
    | _ -> 
      let all_delays = Array.mapi (fun i ll -> if self.state.schedule.a.(i) = (-1) then 0 else ll) ll.a in
      Array.fold_left (max) 0 all_delays
  ) self.state.latency

let compute_cp (self:t) : int array = 
  let cp = Array.map (fun _ -> 0) self.base.latency in
  Array.iteri (fun i _ -> 
    let max_l = ref 0 in
    Array.iteri (fun j _ ->
      max_l := max !max_l (self.base.latency.(j).(i))
    ) self.base.latency;
    cp.(i) <- !max_l
  ) self.base.latency;
  cp

(* let dcp_lower_bound (self:t) : int * int = 
  let cp_max = ref (-2) in
  let cp_arg = ref 0 in
  Array.iteri (fun i _ -> 
    if self.state.schedule.a.(i) = -1 then () else (
      let max_l = ref 0 in
      Array.iteri (fun j _ ->
        max_l := max !max_l (self.state.latency.(j).a.(i))
      ) self.state.latency;
      if !max_l > !cp_max then (
        cp_max := !max_l;
        cp_arg := i
      )
    )
  ) self.state.latency;
  !cp_max,!cp_arg
(*  *)
let compute_cp_i (self:t) : int array = 
  let cpi = Array.map (fun _ -> 0) self.base.latency in
  Array.iteri (fun i _ -> 
    let max_l = ref 0 in
    let max_i = ref i in
    Array.iteri (fun j _ ->
      if !max_l < (self.base.latency.(j).(i)) then (
        max_l := (self.base.latency.(j).(i));
        max_i := j;
      )
    ) self.base.latency;
    cpi.(i) <- !max_i
  ) self.base.latency;
  cpi *)

let do_dominate self i j =
  let do_resource_dom = Array.for_all2 (=) self.base.usages.(i) self.base.usages.(j) in
  let do_latency_dom = ref true in
  Array.iteri (fun k _ -> 
    do_latency_dom := !do_latency_dom && self.base.latency.(k).(i) >= self.base.latency.(k).(j)
  ) self.base.latency;
  do_resource_dom && !do_latency_dom
;;

(****************************************************************************************************************************************)

let test1 () = (
  (*note : diag and upper triangle are always -1 (unless instructions are not ordered)*)
  let schedule = init_raw 
    [|
      [| -1 ; -1 ; -1 |];
      [|  0 ; -1 ; -1 |];
      [|  1 ;  1 ; -1 |];
    |] 
    [|2;1|] 
    [|[|1;0|];[|0;1|];[|1;1|]|] in
  assert (schedule = {
    base =
    {latency = [|[|-1; -1; -1|]; [|0; -1; -1|]; [|1; 1; -1|]|];
     resources = [|2; 1|]; usages = [|[|1; 0|]; [|0; 1|]; [|1; 1|]|];
     is_free = [|false;false;false|]
     ;};
   state =
    {latency =
      [|{a = [|-1; -1; -1|]; l = 0}; {a = [|0; -1; -1|]; l = 1};
        {a = [|1; 1; -1|]; l = 2}|];
     resources_usage = [|0; 0|]; time = 0;
     schedule = {a = [|-1; -1; -1|]; l = 0}}
  });
  assert (compute_cp schedule = [|1;1;0|]);
  assert (compute_min_latency_delay schedule = [|0;0;1|]);
  assert (compute_delay schedule = [|0;0;0|]);
  assert (compute_possible schedule = Some [|true;false;false|]);
  
  advance_time schedule;
  assert (compute_possible schedule = Some [|true;false;false|]);
  assert (total_makespan schedule = None);
  
  schedule_instruction schedule 0;
  assert (compute_min_latency_delay schedule = [|-1;0;1|]);
  assert (compute_delay schedule = [|-1;0;1|]);
  assert (schedule = copy schedule);
  assert (schedule.state.resources_usage.(0)=1 && schedule.state.resources_usage.(1)=0);
  assert (compute_possible schedule = Some [|false;true;false|]);
  
  schedule_instruction schedule 1;
  assert (compute_min_latency_delay schedule = [|-1;-1;1|]);
  assert (compute_delay schedule = [|-1;-1;1|]);
  assert (schedule.state.resources_usage.(0)=1 && schedule.state.resources_usage.(1)=1);
  assert (compute_possible schedule = None);
  
  advance_time schedule;
  assert (compute_min_latency_delay schedule = [|-1;-1;0|]);
  assert (compute_delay schedule = [|-1;-1;0|]);
  assert (compute_possible schedule = Some [|false;false;true|]);
  assert (schedule.state.time = 2);

  schedule_instruction schedule 2;
  assert (total_makespan schedule = Some 3);
)

let test_noResource () = (
  (*note : diag and upper triangle are always -1 (unless instructions are not ordered)*)
  let schedule = init_raw 
  [|
    [| -1 ; -1 ; -1 |];
    [|  1 ; -1 ; -1 |];
    [|  2 ;  1 ; -1 |];
  |] 
  [||] 
  [|[||];[||];[||]|] in
  assert (next schedule = [|false;false;false|]);
  assert (total_makespan schedule = Some 3)
)

let test_dom () = (
  let schedule = init_raw 
    [|
      [| 0 ;  0 ;  0 ;  0 ;  0 ;  0 |];
      [| 0 ;  0 ;  0 ;  0 ;  0 ;  0 |];
      [| 0 ;  0 ;  0 ;  0 ;  0 ;  0 |];
      [| 0 ;  0 ;  0 ;  0 ;  0 ;  0 |];
      [| 1 ;  0 ;  0 ;  0 ;  0 ;  0 |];
      [| 0 ;  1 ;  1 ;  1 ;  0 ;  0 |];
    |] 
    [|2;2|] 
    [|[|1;0|];[|1;0|];[|0;1|];[|1;0|];[|1;1|];[|1;1|]|] in
    assert (do_dominate schedule 3 1);
    assert (not @@ do_dominate schedule 3 0);
    assert (not @@ do_dominate schedule 3 2)
)

;;
test1 ();;
test_noResource ();;