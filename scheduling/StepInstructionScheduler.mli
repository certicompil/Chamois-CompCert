
module type T = sig
  type t 

  val do_regpress : bool

  (********)

  val current_time : t -> int
  val current_resource_usage : t -> int array
  val current_schedule : t -> int array
  val current_regpress : t -> int array

  (********)

  val init : InstructionSchedulerTypes.problem -> int array -> t
  val copy : t -> t

  (********)

  val compute_cp : t -> int array 
  (** compute and return the critical path of all instructions. Note that the critical path does not change during execution *)
  (*TODO : j'aime pas que ça soit ici. C'est pas bo*)

  (* val compute_min_latency_delay : t -> int array  *)(* compute and return then minimum delay before execution (according to latency) for all instructions *)

  val compute_delay : t -> int array 
  (** compute and return the delay before execution for all variables with regard to already schedule instruction*)
  (*TODO : j'aime pas que ça soit ici. C'est pas bo*)

  val compute_advance_delay : t -> int array 
  (** extention of [compute_delay] taking into account direct latency. (compute_advance_delay s).(i) = max (compute_delay s).(i) (max (latency_entering).(i)) *)

  (********)

  val total_makespan : t -> int option (* None if not finish, else Some m with m the total makespan *)
  val possible : t -> bool array option (* Imediately schedulable instructions*)
  val advance_time : t -> unit 
  val schedule_instruction : t -> int -> unit

end

module Base : T 
module TC : T
module BaseRC : T

(* 
type t 

(********)

val current_time : t -> int
val current_resource_usage : t -> int array
val current_schedule : t -> int array

(********)

(* val do_dominate : t -> int -> int -> bool *)

(********)

val init : Adaptator.InstructionSchedulerTypes.problem -> t

val copy : t -> t


val compute_cp : t -> int array 
(** compute and return the critical path of all instructions. Note that the critical path does not change during execution *)

(* val compute_min_latency_delay : t -> int array  *)(* compute and return then minimum delay before execution (according to latency) for all instructions *)

val compute_delay : t -> int array 
(** compute and return the delay before execution for all variables with regard to already schedule instruction*)

val compute_advance_delay : t -> int array 
(** extention of [compute_delay] taking into account direct latency. (compute_advance_delay s).(i) = max (compute_delay s).(i) (max (latency_entering).(i)) *)


(* val compute_prefer_instruction : t -> int array  *)
(*
  compute for every instruction its prefer instruction i.e the instruction resulting in the maximum latency delay
  (compute_min_latency_delay self).(i) = latency.(i).((compute_prefer_instruction self).(i))
  REVERSE ?
*)
(* val critical_instruction : t -> int -> int *)
(*
The instruction in the dependence of i resulting in the maximum latency (including partial latency).
If i does not depend on any instruction, -1 is return
*)

val total_makespan : t -> int option (* None if not finish, else Some m with m the total makespan *)
val possible : t -> bool array option (* Imediately schedulable instructions*)
val advance_time : t -> unit 
val schedule_instruction : t -> int -> unit

(* val runtime_dominance : t -> int -> int -> bool *) *)