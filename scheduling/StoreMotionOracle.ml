(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Benjamin Bonneau   ENS, PSL                       *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

(** Store motion oracle that tries to delay store instructions *)

open AST
open Op
open RTL
open! BTL
open BTLtypes
open BTL_Invariants
open Maps
open Registers
open Camlcoq
open BTLcommonaux
open PrintAST
open PrintOp
open PrintBTL
open Printf

(* Store motion oracle
   The goal of the transformation is to remove redundant stores and to move out of loops
   stores on invariant addresses. The first iteration of the loops need to be unrolled using
   `-funrollsingle`. See test/bonneaub/store_motion for examples.
   Each syntactic store address that appears in the program is associated to an auxiliary register
   ([h]). If useful, stores are replaced by assignment to [h]. We may need to insert latter a store
   using [h] as source.
   The transformation is computed by moving store latter in the program and is done in two steps:
   - During the first step, we use a forward, greatest fixpoint analysis to compute the set 'Delayable'
     of stores that can be delayed up to each program point.
   - In order to avoid needlessly moving stores (which increases the liverange of the auxiliary variables),
     we use a backward, smallest fixpoint analysis to compute the subsets 'Used' that are useful to move.
   The memory invariants are deduced from the set of stores that are delayed at each points.
 *)

(* maximum number of simultaneous candidates (i.e. size of the memory invariant) *)
let max_simult_cand = 16

let print_stats_opt = false
let write_stats_opt = false

type address = {
  a_chunk: memory_chunk;
  a_addr:  addressing;
  a_args:  reg list;
  a_aval:  ValueDomain.aval option;
}

let mk_address a_chunk a_addr a_args a_aval = { a_chunk; a_addr; a_args; a_aval }

let address_disjoint a0 a1 =
  match a0.a_aval, a1.a_aval with
  | Some av0, Some av1 ->
      ValueDomain.vdisjoint av0 (Memdata.size_chunk a0.a_chunk)
                            av1 (Memdata.size_chunk a1.a_chunk)
  | _ -> false

let print_address pp a =
  fprintf pp "%s[%a%a]"
    (name_of_chunk a.a_chunk) (print_addressing reg) (a.a_addr, a.a_args)
    print_aaddr a.a_aval

type cand = {
  c_addr : address;
  c_aux  : reg;
  (* In StPartial, we also propagate the source of each candidate relative to the current iblock *)
  c_src  : store_num_t;
}

let cand_eq c0 c1 = c0.c_addr = c1.c_addr

let cand_dep_reg (r : reg) (c : cand) =
  List.exists (P.(=) r) c.c_addr.a_args

let store_cand c =
  let a = c.c_addr in
  Bstore (a.a_chunk, a.a_addr, a.a_args, c.c_aux,
    { (def_iinfo ()) with addr_aval = a.a_aval; istore_num = c.c_src })

type stores = cand list

let store_list_before : stores -> iblock -> iblock =
  List.fold_right (fun c ib -> Bseq (store_cand c, ib))

type ('a, 'b) join_res =
  | Jdiff  of 'a
  | Jmerge of 'b

let rec split_at n (l : 'a list) : 'a list * 'a list =
  if n <= 0
  then ([], l)
  else match l with
       | [] -> [], []
       | x :: xs -> let xs0, xs1 = split_at (n-1) xs in
                    x :: xs0, xs1

(* l[n0, n1[ *)
let slice n0 n1 (l : 'a list) : 'a list =
  fst (split_at (n1 - n0) (snd (split_at n0 l)))
  

module type St_type = sig
  (* [t] represents an ordered set of delayed stores: a store [a] is after a store [b] ([a > b])
     if [b] must be executed before [a]. *)
  type t
  (* [s] represents an upward-closed subset of a [t]
     (i.e. such that any if a candidate [a] is in [s], any candidate [b] ordered after [a]
     ([b > a]) is also in [s]). *)
  type s
  
  val empty : t
  val block_entry : t -> t
  val empty_subset : t(*0*) -> s(*of 0*)

  val subset : t(*0*) -> s(*of 0*) -> t
  
  val meminv : t -> BTL_Invariants.imem
  val self_inv_annot : t -> meminv_annot_t

  (* Transfer functions *)
  (* Their signatures are of the form:
        (s0 : t) -> {s1 : t & (s s1 -> s s0 * trg_info)}
     where the first arrow is the transfer function for the forward analysis and the second arrow
     is the transfer function for the backward analysis. *)

  (* Transfer function for a store:
     remove the candidate from the set if it was present (use it) and add it after
     all the aliasing elements already present in the set.
     The two first booleans are for statistics: they indicates if the size of the set has been limited and
     if the store overrides a previous store.
     The [stores] need to be added before the instruction.
     The second boolean is true if the store must be changed into a move. *)
  val push : cand -> t(*0*) -> t(*1*) * (bool * bool) * (s(*of 1*) -> s(*of 0*) * stores * bool)
  
  (* Invalidate the candidates satisfying [sp]. *)
  type smash_t = t(*0*) -> t(*1*) * (s(*of 1*) -> s(*of 0*))
  val smash : (cand -> bool) -> smash_t
  val smash_reg : reg -> smash_t
  val smash_all : smash_t

  (* [t_join s0 s1] computes the intersection of [s0] and [s1].
     The result of this intersection is a subset of [s0] and [s1] which is upward closed in [s0] and [s1]
     (with their respective orders) and on which the orders of [s0] and [s1] agree.
     Returns [Jmerge] if the result is [s1]. *)
  val t_join : t(*0*) -> t(*1*) -> (t(*1*), (s(*of 1*) -> s(*of 0*) * meminv_annot_t(*of 1*))) join_res

  (* [s_join s sb1 sb0] computes the union of [sb1] and [sb0]. Returns [Jmerge] if the result is [sb0]. *)
  val s_join : t(*0*) -> s(*of 0*) -> s(*of 0*) -> (s(*of 0*), stores) join_res

  val print : out_channel -> t -> unit
  val print_subset : out_channel -> s -> unit

end

(* Implementation without alias analysis between the stores:
   we assume that all stores alias, each set is totally ordered.
   The operation are typically linear in the size of the sets. *)
module StTotal : St_type = struct
  (* Decreasing list of delayed stores, i.e. from the latest to the oldest. *)
  type t = cand list
  (* An upward closed subset is a prefix of the list. We represent it by its length. *)
  type s = int

  let empty : t = []
  let block_entry (cs : t) = cs
  let empty_subset (_ : t) : s = 0
  
  let subset (l : t) (n : s) = fst (split_at n l)
  
  let meminv : t -> BTL_Invariants.imem =
    List.rev_map (fun c ->
      let a = c.c_addr in
      Coq_mk_istore (a.a_chunk, a.a_addr, a.a_args,
        { store_num   = SNumNone;
          store_aaddr = a.a_aval }, c.c_aux))

  let self_inv_annot (_ : t) = None


  let remove_addr (a : address) (cs0 : t(*0*)) : t(*1*) * bool * (s(*of 1*) -> s(*of 0*) * stores) =
    let rec aux (cs : t) : t * int =
      match cs with
      | [] -> [], 0
      | {c_addr; _} :: cs when c_addr = a -> cs, 1
      | c :: cs -> let cs', n = aux cs in
                   c :: cs', if n > 0 then n + 1 else 0
    in
    let cs1, n = aux cs0 in
    cs1, (n > 0),
    fun n1 -> if n = 0 then n1, []
              else if n1 < n then n, List.rev (slice n1 (n-1) cs0)
              else n1 + 1, []

  let limit_size (cs : t(*0*)) : t(*1*) * bool * (s(*of 1*) -> s(*of 0*)) =
    let cs0, cs1 = split_at max_simult_cand cs in
    cs0, cs1 <> [], fun n1 -> n1

  let push0 (c : cand) (cs0 : t(*0*)) : t(*1*) * (s(*of 1*) -> s(*of 0*) * bool) =
    c :: cs0, fun n1 -> if n1 = 0 then (0, false) else (n1 - 1, true)

  let push (c : cand) (cs0 : t) : t * (bool * bool) * (s -> s * stores * bool) =
    let c = {c with c_src = SNumNone }          in
    let cs1, ov, f0 = remove_addr c.c_addr cs0  in
    let cs2, f1     = push0 c cs1               in
    let cs3, lm, f2 = limit_size cs2            in
    cs3, (lm, ov),
    fun n3 -> let n1, usd = f1 (f2 n3) in
              let n0, str = f0 n1      in
              (n0, str, usd)

  type smash_t = t(*0*) -> t(*1*) * (s(*of 1*) -> s(*of 0*))

  let smash (sp : cand -> bool) (cs0 : t) : t * (s -> s) =
    let rec aux (cs : t) : t =
      match cs with
      | []      -> []
      | c :: _  when sp c -> []
      | c :: cs -> let cs' = aux cs in c :: cs'
    in aux cs0, fun n1 -> n1

  let smash_reg (r : reg) : smash_t =
    smash (cand_dep_reg r)

  let smash_all : smash_t = fun _ ->
    [], (fun n1 -> assert (n1 = 0); 0)

  let t_join : t(*0*) -> t(*1*) -> (t, (s -> s * meminv_annot_t)) join_res =
    let rec aux (pre : cand list) (cs0 : t) (cs1 : t) =
      match cs0, cs1 with
      | cs0, [] -> Jmerge (fun n1 -> n1, None)
      | c0 :: cs0, c1 :: cs1 when cand_eq c0 c1 ->
          aux (c1 :: pre) cs0 cs1
      | _, _ -> Jdiff (List.rev pre)
    in aux []

  let s_join (cs : t) (n0 : s) (n1 : s) : (s, stores) join_res =
    if n0 > n1 then Jdiff n0
    else Jmerge (List.rev (slice n0 n1 cs))

  let print pp cs =
    print_list ~sep:(fun pp -> fprintf pp " ") (fun pp c -> fprintf pp "%d" (P.to_int c.c_aux))
      pp (List.rev cs)
  let print_subset pp n =
    fprintf pp "%d" n

end (* StTotal *)

(* Implementation with an alias analysis between the stores:
   the sets are only partially ordered.
   This implementation is less efficient than the one for the total case. The operations
   are typically quadratic in the size of the sets. *)
module StPartial : St_type = struct
  (* List of delayed stores. The order is the transitive closure of the order of the list between
     aliasing candidates. That is, [a > b] if and only if there is a subsequence:
       a = a_0 ... a_i a_{i+1} ... a_n = b
     such that each pair (a_i, a_{i+1}) aliases. *)
  type t = cand list
  (* The elements of an upward closed subset are represented using a list of booleans. *)
  type s = bool list

  let empty : t = []
  let block_entry (cs : t) =
    let n = List.length cs in
    List.mapi (fun i c -> {c with c_src = SNumInv (P.of_int (n - i))}) cs
  let empty_subset (cs : t) : s = List.map (fun _ -> false) cs
 
  let subset (cs : t) (bs : s) =
    List.(filter_map (fun (b,c) -> if b then Some c else None) (combine bs cs))

  let meminv : t -> BTL_Invariants.imem =
    List.rev_map (fun c ->
      let a = c.c_addr in
      Coq_mk_istore (a.a_chunk, a.a_addr, a.a_args,
        { store_num   = c.c_src;
          store_aaddr = a.a_aval }, c.c_aux))

  let self_inv_annot (cs : t) =
    Some (List.rev_map (fun c -> c.c_src) cs)

  (* If [bs0 - bs] is upward closed in [cs - bs], returns the union of [bs0] and the upward closure of [bs] *)
  let close_subset (cs : t) (bs0 : s) (bs : bool list) : s =
    let rec closure bs rs =
      match rs with
      | [] -> bs
      | (c, (b0, b)) :: rs ->
          closure (b :: bs) (
            if (not b0) && b
            then List.map (fun (c', (b0', b')) ->
                    (c', (b0', b' || not (address_disjoint c.c_addr c'.c_addr)))) rs
            else rs)
    in
    closure [] List.(rev (combine cs (combine bs0 bs)))

  let compensate : t -> s -> s -> stores =
    let rec aux (str : stores) cs bs0 bs1 =
      match cs, bs0, bs1 with
      | [], [], [] -> str
      | c :: cs, b0 :: bs0, b1 :: bs1 ->
          aux (if b0 = b1 then str else (assert (b0 = true); c :: str)) cs bs0 bs1
      | _ -> failwith "compensate: list lengths"
    in aux []

  let remove_addr (a : address) (cs0 : t(*0*)) : t(*1*) * bool * (s(*of 1*) -> s(*of 0*) * stores) =
    let rec aux (cs : t) : t * int =
      match cs with
      | [] -> [], 0
      | {c_addr; _} :: cs when c_addr = a -> cs, 1
      | c :: cs -> let cs', n = aux cs in
                   c :: cs', if n > 0 then n + 1 else 0
    in
    let cs1, n = aux cs0 in
    cs1, (n > 0),
    fun bs1 ->
      if n = 0 then bs1, []
      else let bs1_0, bs_1 = split_at (n-1) bs1 in
           let bs0_0  = bs1_0 @ [true] in
           let cs_0   = fst (split_at n cs0) in
           let bs0_0' = close_subset cs_0 (bs1_0 @ [false]) bs0_0 in
           bs0_0' @ bs_1, compensate cs_0 bs0_0' bs0_0

  let limit_size (cs : t(*0*)) : t(*1*) * bool * (s(*of 1*) -> s(*of 0*)) =
    let cs0, cs1 = split_at max_simult_cand cs in
    cs0, cs1 <> [], fun bs1 -> bs1 @ List.map (fun _ -> false) cs1

  let push0 (c : cand) (cs0 : t(*0*)) : t(*1*) * (s(*of 1*) -> s(*of 0*) * bool) =
    c :: cs0, function
              | b0 :: bs0 -> bs0, b0
              | [] -> failwith "Sp.s shape"

  let push (c : cand) (cs0 : t) : t * (bool * bool) * (s -> s * stores * bool) =
    let cs1, ov, f0 = remove_addr c.c_addr cs0  in
    let cs2, f1     = push0 c cs1               in
    let cs3, lm, f2 = limit_size cs2            in
    cs3, (lm, ov),
    fun n3 -> let n1, usd = f1 (f2 n3) in
              let n0, str = f0 n1      in
              (n0, str, usd)

  type smash_t = t(*0*) -> t(*1*) * (s(*of 1*) -> s(*of 0*))

  let smash (sp : cand -> bool) (cs0 : t) : t * (s -> s) =
    let rec remove (cs : (cand * bool(*rej*)) list) : t * bool list(*rejected subset*) =
      match cs with
      | [] -> [], []
      | (c, true) :: cs ->
          let cs', rej' = remove (List.map (fun (c', r) ->
              c', r || not (address_disjoint c.c_addr c'.c_addr)) cs) in
          cs', true :: rej'
      | (c, false) :: cs ->
          let cs', rej' = remove cs in
          c :: cs', false :: rej'
    in let rec complete (rej : bool list) (bs1 : bool list) : bool list =
      match rej, bs1 with
      | [], [] -> []
      | true  :: rej, bs1      -> false :: complete rej bs1
      | false :: rej, b :: bs1 -> b :: complete rej bs1
      | _ -> failwith "smash::complete"
    in
    let cs1, rej = remove (List.map (fun c -> c, sp c) cs0) in
    cs1, complete rej

  let smash_reg (r : reg) : smash_t =
    smash (cand_dep_reg r)

  let smash_all : smash_t = fun cs ->
    [], fun bs1 -> assert (bs1 = []); List.map (fun _ -> false) cs

  type icand = int * cand
  (* candidate, (source_index, target_index) *)
  type mt_list = (cand * (int * int)) list

  let t_join : t(*0*) -> t(*1*) -> (t, (s -> s * meminv_annot_t)) join_res =
    let rec remove_downward_closure (c : cand) : icand list -> icand list = function
      | [] -> []
      | ic1 :: cs when address_disjoint c.c_addr (snd ic1).c_addr ->
          ic1 :: remove_downward_closure c cs
      | (_, c1) :: cs ->
          remove_downward_closure c (remove_downward_closure c1 cs)
    in let rec find_matching (c : cand) (pre : icand list) : icand list -> (int * icand list) option = function
      | [] -> None
      | (i1, c1) :: cs when cand_eq c c1 ->
          Some (i1, List.rev_append pre cs)
      | c1 :: cs when address_disjoint c.c_addr (snd c1).c_addr ->
          find_matching c (c1 :: pre) cs
      | _ :: _ -> None
    in let rec match_cs (mts : mt_list) (cs0 : icand list) (cs1 : icand list) : (mt_list, mt_list) join_res =
      match cs0, cs1 with
      | [],   []   -> Jmerge mts
      | [], _ :: _ -> Jdiff  mts
      | (i0, c0) :: cs0, cs1 ->
          match find_matching c0 [] cs1 with
          | Some (i1, cs1') -> match_cs ((c0, (i0, i1)) :: mts) cs0 cs1'
          | None            -> match_cs mts (remove_downward_closure c0 cs0) cs1
    in let index_list : t -> icand list = List.mapi (fun i c -> (i, c))
    in
    fun cs0 cs1 -> match match_cs [] (index_list cs0) (index_list cs1) with
    | Jdiff  mts -> Jdiff  (List.rev_map fst mts)
    | Jmerge mts -> Jmerge
      begin
        let idxs = List.rev_map snd mts in
        fun bs1 ->
          let acs0 = Array.of_list cs0                  in
          let acs1 = Array.of_list cs1                  in
          let abs1 = Array.of_list bs1                  in
          let abs0 = Array.make (List.length cs0) false in
          List.iter (fun (i0, i1) ->
              assert (cand_eq acs0.(i0) acs1.(i1));
              abs0.(i0) <- abs1.(i1);
              acs1.(i1) <- acs0.(i0) (* set [c_src] *)
            ) idxs;
          Array.to_list abs0, self_inv_annot (subset (Array.to_list acs1) bs1)
      end

  let s_join (cs : t) (bs1 : s) (bs0 : s) : (s, stores) join_res =
    if List.exists2 (fun b0 b1 -> b1 && not b0) bs0 bs1
    then Jdiff  (List.map2 (||) bs0 bs1)
    else Jmerge (compensate cs bs0 bs1)

  let print pp cs =
    print_list ~sep:(fun pp -> fprintf pp " ") (fun pp c -> fprintf pp "%d" (P.to_int c.c_aux))
      pp (List.rev cs)
  let print_subset pp bs =
    print_list ~sep:(fun pp -> fprintf pp " ") (fun pp -> function true -> fprintf pp "t" | false -> fprintf pp "f")
      pp (List.rev bs)

end (* StPartial *)


let final_succ : BTL.final -> exit list = function
  | Bgoto s | Bcall (_, _, _, _, s) | Bbuiltin (_, _, _, s) -> [s]
  | Bjumptable (_, ls) -> ls
  | Btailcall _ | Breturn _ -> []

let final_smash : BTL.final -> bool = function
  | Bgoto _ | Bjumptable _ -> false
  | Bcall _ | Btailcall  _ | Bbuiltin _ | Breturn _ -> true

let rec traverse_iblock (f : iblock -> iblock) : iblock -> iblock =
  function
  | Bseq (ib1, ib2) ->
      f (Bseq (traverse_iblock f ib1, traverse_iblock f ib2))
  | Bcond (op, args, ib1, ib2, iinfo) ->
      f (Bcond (op, args, traverse_iblock f ib1, traverse_iblock f ib2, iinfo))
  | ib -> f ib

type analyze_stats = {
  mutable nb_stores         : int;
  mutable nb_override       : int;
  mutable nb_moved          : int;
  mutable nb_nodes          : int;
  mutable nb_iter_Delayable : int;
  mutable nb_iter_Used      : int;
  mutable nb_limit          : int;
}

let init_stats nb_nodes =
  { nb_stores = 0; nb_override = 0; nb_moved = 0; nb_nodes;
    nb_iter_Delayable = 0; nb_iter_Used = 0; nb_limit = 0 }
let print_stats pp st =
  fprintf pp "S:%d O:%d M:%d N:%d F:%d B:%d L:%d"
    st.nb_stores st.nb_override st.nb_moved st.nb_nodes st.nb_iter_Delayable st.nb_iter_Used st.nb_limit
let write_stats pp st =
  fprintf pp "%d;%d;%d;%d;%d;%d;%d\n"
    st.nb_stores st.nb_override st.nb_moved st.nb_nodes st.nb_iter_Delayable st.nb_iter_Used st.nb_limit

type ('a, 'b) bblock_exit =
  | BBcont of 'a
  | BBfin  of 'b

module Make (St : St_type) = struct

type  btr3 = unit -> St.t * iblock
type  btr2 = St.s -> btr3
type  btr1 = unit -> St.t * btr2
type  btr0 = St.t -> btr1
type ibtr3 = unit -> iblock
type ibtr2 = St.s -> St.s * ibtr3
type ibtr1 = unit -> ibtr2
type ibtr0 = St.t -> (St.t, St.t) bblock_exit * ibtr1

type analyse_node = {
  (* forward analysis: 'delayable' *)
  mutable an_Delayable      : St.t option; (* at the entry of the iblocks *)
  mutable an_tr1            : btr1 option;
  mutable an_Delayable_exit : St.t option; (* at the exit  of the iblocks *)
  (* backward analysis: 'used' at the end of the iblocks *)
  mutable an_tr2            : btr2;
  mutable an_Used : St.s option;
  (* generation of the target program and invariants *)
  mutable an_tr3            : btr3 option;
}

type analyze_result = {
  aux_tbl : (address, reg) Hashtbl.t;
  an_nds  : analyse_node PTree.t;
  stats   : analyze_stats;
}

(* The BTL code must be composed of basic blocks with synthetic nodes. *)
let transfer_core (btl : BTL.coq_function) : BTL.code * BTL_Invariants.gluemap * analyze_result =
  BTL_SyntheticNodes.compute_surrounding_blocks btl.fn_code btl.fn_entrypoint;
  let res = {
    aux_tbl = Hashtbl.create 32;
    an_nds  = PTree.map (fun _ _ -> {
                an_Delayable      = None;
                an_tr1            = None;
                an_Delayable_exit = None;
                an_tr2            = (fun _ -> failwith "placeholder");
                an_Used           = None;
                an_tr3            = None;
              }) btl.fn_code;
    stats   = init_stats (List.length (PTree.elements btl.fn_code));
  }
  in let get_an (n : node) = Option.get (PTree.get n res.an_nds)
  in let cand_of =
    let next_aux = ref (btl_max_reg btl.fn_code btl.fn_params + 1) in
    fun (a : address) (src : store_num_t) : cand ->
    match Hashtbl.find_opt res.aux_tbl a with
    | Some aux -> {c_addr = a; c_aux = aux; c_src = src}
    | None -> let naux = !next_aux in
              next_aux := naux + 1;
              let aux  = P.of_int naux in
              Hashtbl.add res.aux_tbl a aux;
              {c_addr = a; c_aux = aux; c_src = src}
  in
  let wl : node list ref = ref [] in
  let rec iter (f : node -> unit) : unit =
    match !wl with
    | [] -> ()
    | n :: wl' -> wl := wl'; f n; iter f
  in
  let set_Delayable (nid : node) (s : St.t) : unit =
    let n = get_an nid in
    n.an_Delayable <- Some s;
    n.an_tr1 <- None;
    wl := nid :: !wl
  in let join_Delayable (nid : node) (s : St.t) : (St.t * (St.s -> St.s * meminv_annot_t)) option =
    match (get_an nid).an_Delayable with
    | None    -> set_Delayable nid s; None
    | Some s1 ->
        match St.t_join s s1 with
        | Jmerge f  -> Some (s1, f)
        | Jdiff s1' -> set_Delayable nid s1'; None
  in
  let set_Used (nid : node) (sb : St.s) : unit =
    let n = get_an nid in
    n.an_Used <- Some sb;
    n.an_tr3 <- None;
    wl := nid :: !wl
  in let join_Used (nid : node) (s : St.t) (sb : St.s) : (St.t * stores) option =
    let sb1 = Option.get (get_an nid).an_Used in
    match St.s_join s sb sb1 with
    | Jmerge str  -> Some (St.subset s sb1, str)
    | Jdiff  sb1' -> set_Used nid sb1'; None
  in
  let transfer_block : iblock_info -> btr0 =
  begin
    let transfer_join_out (out : node list) (s : St.t) : unit -> St.t * (St.s -> St.s * meminv_annot_t) =
      let self_st () = (s, fun sb -> (sb, St.self_inv_annot (St.subset s sb))) in
      match out with
      | []  -> self_st
      | [n] -> let f () = join_Delayable n s in
               let _ = f () in
               fun () -> Option.get (f ())
      | out -> List.iter (fun n -> set_Delayable n s) out;
               self_st
    in
    let transfer_join_in (inp : node list) (s : St.t) (sb : St.s) : unit -> St.t * stores =
      match inp with
      | []  -> assert (s = St.empty);
               fun () -> St.empty, []
      | [n] -> let f () = join_Used n s sb in
               let _ = f () in
               fun () -> Option.get (f ())
      | inp -> List.iter (fun n -> set_Used n sb) inp;
               fun () -> St.subset s sb, []
    in
    let rec transfer_iblock (ib : iblock) : ibtr0 =
      let transfer_final (sb_f : St.s -> St.s) : ibtr0 =
        fun s -> BBfin s, fun () sb1 -> (sb_f sb1, fun () -> ib)
      in
      fun s ->
      match ib with
      | BF (fin, _) when final_smash fin ->
          let s1, sb_f = St.smash_all s in
          transfer_final sb_f s1
      | BF (fin, _) ->
          transfer_final (fun sb -> sb) s
      | Bseq (Bcond (_, _, Bnop _, BF (Bgoto out0, _), _), BF (Bgoto out1, _))
      | Bseq (Bcond (_, _, BF (Bgoto out0, _), Bnop _, _), BF (Bgoto out1, _)) ->
          transfer_final (fun sb -> sb) s
      | Bnop _ ->
          BBcont s, fun () sb -> (sb, fun () -> ib)
      | Bop (_, _, dst, _) ->
          let s1, sb_f = St.smash_reg dst s in
          BBcont s1, fun () sb1 -> (sb_f sb1, fun () -> ib)
      | Bload (_, chk, addr, args, dst, iinfo) ->
          let a = mk_address chk addr args iinfo.addr_aval in
          let s1, sb_f = St.smash (fun c -> cand_dep_reg dst c || not (address_disjoint a c.c_addr)) s in
          BBcont s1, fun () sb1 -> (sb_f sb1, fun () -> ib)
      | Bstore (chk, addr, args, src, iinfo) ->
          let c = cand_of (mk_address chk addr args iinfo.addr_aval) iinfo.istore_num in
          let s1, (lm, ov), sb_f = St.push c s in
          if lm then res.stats.nb_limit <- res.stats.nb_limit + 1;
          BBcont s1,
          fun () ->
          res.stats.nb_stores <- res.stats.nb_stores + 1;
          if ov then res.stats.nb_override <- res.stats.nb_override + 1;
          fun sb1 ->
          let (sb0, str, usd) = sb_f sb1 in
          (sb0, fun () ->
           store_list_before str (
             if usd
             then (res.stats.nb_moved <- res.stats.nb_moved + 1;
                   Bop (Omove, [src], c.c_aux, def_iinfo ()))
             else ib))
      | Bseq (ib1, ib2) ->
          let s1, k1 = transfer_iblock ib1 s in
          let s1 = match s1 with
                   | BBcont s1 -> s1
                   | BBfin  _  -> failwith "BTL basic block shape" in
          let s2, k2 = transfer_iblock ib2 s1 in
          s2, fun () ->
          let k1 = k1 () in let k2 = k2 () in
          fun sb2 ->
          let sb1, k2 = k2 sb2 in
          let sb0, k1 = k1 sb1 in
          (sb0, fun () -> Bseq (k1 (), k2 ()))
      | _ -> failwith "BTL basic block shape"
    in
    fun ib s0 ->
      let s0 = St.block_entry s0 in
      let s1, k = transfer_iblock ib.entry s0 in
      let s1 = match s1 with
               | BBfin s1 -> s1
               | BBcont _ -> failwith "BTL basic clock shape" in
      let tr_out = transfer_join_out ib.binfo.successors s1 in
    fun () ->
      let k = k () in
      let s2, tr_out = tr_out () in
      (s2,
    fun sb2 ->
      let sb1, out_annot = tr_out sb2 in
      let sb0, k = k sb1 in
      let k0 = transfer_join_in ib.binfo.predecessors s0 sb0 in
    fun () ->
      let s0, str0 = k0 () in
      let ib = store_list_before str0 (k ()) in
      let ib = traverse_iblock (function
        | BF (fin, iinfo) -> BF (fin, {iinfo with meminv_annot = out_annot})
        | ib -> ib) ib in
      s0, ib)
  end (* transfer_block *)
  in
  (* Forward 'Delayable' analysis *)
  set_Delayable btl.fn_entrypoint St.empty;
  (* The top element (None) is not supported by the transfer functions. It corresponds
     to unreachable nodes. We raise an error if there is such a node. *)
  iter begin fun n ->
    res.stats.nb_iter_Delayable <- res.stats.nb_iter_Delayable + 1;
    let an = get_an n in
    match an.an_tr1 with
    | Some _ -> () (* already updated *)
    | None ->
        an.an_tr1 <- Some (fun _ -> failwith "placeholder");
        let k = transfer_block (Option.get (PTree.get n btl.fn_code))
                               (Option.get an.an_Delayable)
        in if an.an_tr1 <> None then an.an_tr1 <- Some k
  end;

  (* Computation of the transfer functions for the backward analysis *)
  wl := List.map (fun (nid, an) ->
        let (s_exit, tr2) =
          match an.an_tr1 with
          | Some tr1 -> tr1 ()
          | None -> failwith (Printf.sprintf
            "Store motion oracle, tr1 is None at node %d. Unreachable node ?" (P.to_int nid))
        in
        an.an_Delayable_exit <- Some s_exit;
        an.an_tr2            <- tr2;
        an.an_Used           <- Some (St.empty_subset s_exit);
        nid)
      (PTree.elements res.an_nds);

  (* Backward 'Used' analysis *)
  iter begin fun n ->
    res.stats.nb_iter_Used <- res.stats.nb_iter_Used + 1;
    let an = get_an n in
    match an.an_tr3 with
    | Some _ -> () (* already updated *)
    | None ->
        an.an_tr3 <- Some (fun _ -> failwith "placeholder");
        let k = an.an_tr2 (Option.get an.an_Used)
        in if an.an_tr3 <> None then an.an_tr3 <- Some k
  end;
  (* Computation of the target code, liveness gluemap and memory invariants *)
  let trg_gm = PTree.map (fun n ib ->
      let an = get_an n in
      let s, trg = Option.get an.an_tr3 () in
      { ib with entry = trg },
      {
        history = csi_empty;
        glue    = {
          aseq    = [];
          outputs = ib.binfo.input_regs;
        };
        meminv  = St.meminv s;
      }
    ) btl.fn_code
  in

  if print_stats_opt then Printf.eprintf "Stats: %a\n" print_stats res.stats;
  if write_stats_opt
  then begin
    let out = open_out_gen [ Open_append; Open_creat; Open_text ] 0o666 "./store_motion_stats.csv" in
    write_stats out res.stats;
    close_out out
  end;
  BTL_SyntheticNodes.eliminate_synthetic_nodes (PTree.map (fun _ -> fst) trg_gm),
  PTree.map (fun _ -> snd) trg_gm, res

let print_analysis pp ((btl, an) : BTL.coq_function * analyze_result) =
  PrintGV.(
    graph pp
      (fun pp (ibnum, ib) -> node pp ibnum (fun pp () ->
        let an = Option.get (PTree.get ibnum an.an_nds) in
        node_text pp (fun pp ->
          begin match an.an_Delayable with
          | Some s -> fprintf pp "<tr><td>[%a]</td></tr>\n" St.print s
          | None   -> fprintf pp "<tr><td>NONE</td></tr>\n"
          end;
          fprintf pp "<hr/>\n"
        ) (fun pp ->
          fprintf pp "<hr/>\n";
          match an.an_Delayable_exit, an.an_Used with
          | Some s, Some sb -> fprintf pp "<tr><td>[%a]</td></tr>\n" St.print (St.subset s sb)
          | _ -> fprintf pp "<tr><td>NONE</td></tr>\n"
        ) (ibnum, ib)))
      (fun pp ->
        fprintf pp "key [ label=<\n<VERBATIM>\n";
        let ks = List.map (fun (a, r) -> a, P.to_int r) (List.of_seq (Hashtbl.to_seq an.aux_tbl)) in
        let ks = List.sort (fun (_, r0) (_, r1) -> Int.compare r0 r1) ks in
        List.iter (fun (a, r) -> fprintf pp "%d: %a\n" r print_address a) ks;
        fprintf pp "</VERBATIM>\n> ]\n")
  ) btl

let transfer (f : BTL.coq_function) : BTL.code * BTL_Invariants.gluemap =
  let btl_optim, gm, an = transfer_core f in
(*
  PrintGV.to_file "BTL_store_motion.src.GV" print_analysis (f, an);
  PrintGV.to_file "BTL_store_motion.trg.GV" PrintGV.btl_gm ({f with fn_code = btl_optim}, gm);
*)
  btl_optim, gm


end (* Make *)
