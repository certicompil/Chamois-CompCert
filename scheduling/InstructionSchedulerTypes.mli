(** Schedule instructions on a synchronized pipeline
by David Monniaux, CNRS, VERIMAG *)

open BTL_BlockOptimizeraux

(** A latency constraint: instruction number [instr_to] should be scheduled at least [latency] clock ticks before [instr_from].

It is possible to specify [latency]=0, meaning that [instr_to] can be scheduled at the same clock tick as [instr_from], but not before.

[instr_to] can be the special value equal to the number of instructions, meaning that it refers to the final output latency. *)
type latency_constraint = {
  instr_from : int;
  instr_to : int;
  latency : int;
}
                        
(** A scheduling problem.

In addition to the latency constraints, the resource constraints should be satisfied: at every clock tick, the sum of vectors of resources used by the instructions scheduled at that tick does not exceed the resource bounds.
*)
type problem = {
  max_latency : int;
  (** An optional maximal total latency of the problem, after which the problem is deemed not schedulable. -1 means there should be no maximum. *)

  resource_bounds : int array;
  (** An array of number of units available indexed by the kind of resources to be allocated. It can be empty, in which case the problem is scheduling without resource constraints. *)

  live_regs_entry : Registers.Regset.t;
  (** The set of live pseudo-registers at entry. *)

  typing : RTLtyping.regenv;
  (* Register type map. *)

  reference_counting : ((Registers.reg, register_info) Hashtbl.t
                     * ((Registers.reg * mentions_element) list array)) option;
  (** See BTLScheduleraux.reference_counting. *)
  
  instruction_usages: int array array;
  (** At index {i i} the vector of resources used by instruction number {i i}. It must be the same length as [resource_bounds] *)

  latency_constraints : latency_constraint list
  (** The latency constraints that must be satisfied *)
  };;

(** A reg -> reg map that can be returned by scheduling oracles. It defines the register renaming from defined registers
 *  of source block to the new target block. The mappin can be applied to the target block with the help of the
 *  register renaming oracle in BTL_RegisterRenaming *)
type ren_map = (Registers.reg * Registers.reg) Map.Make(Int).t;;

(** Scheduling solution. For {i n} instructions to schedule, and 0≤{i i}<{i n}, position {i i} contains the time to which
 *  instruction {i i} should be scheduled. Position {i n} contains the final output latency.
 *  Optionally returns a new register mapping. *)
type solution = (int array * ren_map option);;
              
(** A scheduling algorithm.
The return value [Some x] is a solution [x].
[None] means that scheduling failed. *)
type scheduler = problem -> solution option;;
