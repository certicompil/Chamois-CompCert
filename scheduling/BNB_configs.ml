let default_regpress_compare a b =
  let amax = Array.fold_left (max) 0 in
  let asum = Array.fold_left (+) 0 in
  match amax a - amax b with
  | v when v <> 0 -> v
  | _ -> (*v = 0*)
  asum a - asum b 

module NORP = struct

  module Raw : BNB_Config.T = struct
    module Scheduler = StepInstructionScheduler.Base
    module UsagePattern = ResourceRelaxation.VolumeApproximation
  
    let preprocess_steps ~filter_0_instructions ~transitive_closure ~minimize ~inc_0_latency = 
      ignore filter_0_instructions;
      ignore transitive_closure;
      ignore minimize;
      ignore inc_0_latency;
      []
  
    let do_resource_cut = false
    let do_simple_memorisaton = false
    let do_memorise_subproblem = false
    let do_subproblem_minimisation = false
    let do_bound_cut = false
  
    (*****************)
    let regpress_compare = default_regpress_compare
  end

  module ReferenceSmall : BNB_Config.T = struct
    include Raw
    
      let preprocess_steps ~filter_0_instructions ~transitive_closure ~minimize ~inc_0_latency = 
        ignore filter_0_instructions;
        ignore transitive_closure;
        ignore minimize;
        ignore inc_0_latency;
        [ 
          filter_0_instructions;
         (*  transitive_closure; *)
          inc_0_latency;
          (* transitive_closure; *)
          (* minimize; *)
        ]
    
      let do_resource_cut = true
      let do_memorise_subproblem = true
      let do_subproblem_minimisation = true
      let do_bound_cut = true
  
    end

  module Reference : BNB_Config.T = struct
  include Raw
  
    let preprocess_steps ~filter_0_instructions ~transitive_closure ~minimize ~inc_0_latency = 
      ignore filter_0_instructions;
      ignore transitive_closure;
      ignore minimize;
      ignore inc_0_latency;
      [ 
        filter_0_instructions;
        (* transitive_closure; *)
        inc_0_latency;
        (* transitive_closure; *)
        (* minimize; *)
      ]
  
    let do_resource_cut = true
    let do_memorise_subproblem = true
    let do_subproblem_minimisation = true
    let do_bound_cut = true

  end

  module NoSPMinimization : BNB_Config.T = struct
    include Reference
    let do_subproblem_minimisation = false
  end

  module SimpleMemorization : BNB_Config.T = struct
    include Reference
    let do_simple_memorisaton = true
    let do_memorise_subproblem = false
    let do_subproblem_minimisation = false
  end

  module NoMemorization : BNB_Config.T = struct
    include Reference
    let do_memorise_subproblem = false
    let do_subproblem_minimisation = false
  end

  module SimpleMemRaw : BNB_Config.T = struct
    include Raw
    let do_simple_memorisaton = true
  end

  module SimpleMemNoRes : BNB_Config.T = struct
    include Reference
    let do_resource_cut = false
    let do_simple_memorisaton = true
    let do_memorise_subproblem = false
    let do_subproblem_minimisation = false
  end

  module NoRes : BNB_Config.T = struct
    include Reference
    let do_resource_cut = false
  end
end

module RP = struct
  module Raw : BNB_Config.T = struct
    module Scheduler = StepInstructionScheduler.BaseRC
    module UsagePattern = ResourceRelaxation.VolumeApproximation
  
    let preprocess_steps ~filter_0_instructions ~transitive_closure ~minimize ~inc_0_latency = 
      ignore filter_0_instructions;
      ignore transitive_closure;
      ignore minimize;
      ignore inc_0_latency;
      []
  
    let do_resource_cut = false
    let do_simple_memorisaton = false
    let do_memorise_subproblem = false
    let do_subproblem_minimisation = false
    let do_bound_cut = false
  
    (*****************)
    let regpress_compare = default_regpress_compare
  end

  module Reference : BNB_Config.T = struct
    include Raw
    let preprocess_steps ~filter_0_instructions ~transitive_closure ~minimize ~inc_0_latency = 
      ignore filter_0_instructions;
      ignore transitive_closure;
      ignore minimize;
      [inc_0_latency]
    
    let do_resource_cut = true
    let do_memorise_subproblem = true
    let do_subproblem_minimisation = true
    let do_bound_cut = true
  end
end

let match_name (name:string) (f:(module BNB_Config.T) -> 'a) : 'a = 
  let process_name = String.lowercase_ascii name in
  match process_name with
  | "raw" -> f (module NORP.Raw)
  |"post" | "postpass" | "reference" | "ref" -> f (module NORP.Reference)
  | "referencesmall" | "refsmall" -> f (module NORP.Reference)
  | "nospminimization" | "nominimization" | "nomin" | "nospmin" -> f (module NORP.NoSPMinimization)
  | "simplespmemorization" | "simplememorization" | "simplemem" | "simplespmem" -> f (module NORP.SimpleMemorization)
  | "nospmemorization" | "nomemorization" | "nomem" | "nospmem" -> f (module NORP.NoMemorization)
  | "rawsimplemem" | "simplememraw" -> f (module NORP.SimpleMemRaw)
  | "simplememnores" -> f (module NORP.SimpleMemNoRes)
  | "nores" -> f (module NORP.NoRes)

  | "rpraw" -> f (module RP.Raw)
  | "prepass" | "pre" | "rpreference" | "rpref" -> f (module RP.Reference)

  | _ -> failwith ("unkown module "^name)

let (let@*@) = match_name