open InstructionSchedulerTypes

type t
type unprocessed_solution = {
    raw_solution:int array
}

val dummy : t

val filter_0_instructions : problem -> problem * t
val transitive_closure : problem -> problem * t
val inc_0_latency : problem -> problem * t
val minimize : problem -> problem * t
val add_register_latency : problem -> problem * t

val compute : (
        filter_0_instructions:  (problem -> problem * t) -> 
        transitive_closure:     (problem -> problem * t) -> 
        minimize:               (problem -> problem * t) -> 
        inc_0_latency:          (problem -> problem * t) -> 
        (problem -> problem * t) list 
    ) -> problem -> problem * t
val postprocess : unprocessed_solution -> t -> int array