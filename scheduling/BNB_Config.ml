open InstructionSchedulerTypes

module type T = sig
  (*
  The implementation of scheduler. Current implementations : 
  with regpress : BaseRC
  without regpress : Base, TC  
  WARNING TC scheduler assume _transitive_closure
  *)
  module Scheduler : StepInstructionScheduler.T
  
  (*
  The implementation of the cutting stock lower bound. Current implementations : Exact, FloatApproximation
  *)
  module UsagePattern : ResourceRelaxation.T
  
  (* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

  (*list of preprocess steps*)
  (*WARNING some module can have assumption on the preprocess step*)
  val preprocess_steps : 
    filter_0_instructions:  (problem -> problem * PreprocessProblem.t) -> 
    transitive_closure:     (problem -> problem * PreprocessProblem.t) -> 
    minimize:               (problem -> problem * PreprocessProblem.t) -> 
    inc_0_latency:          (problem -> problem * PreprocessProblem.t) -> 
    (problem -> problem * PreprocessProblem.t) list 

  val do_resource_cut : bool
  val do_simple_memorisaton : bool
  val do_memorise_subproblem : bool
  val do_subproblem_minimisation : bool
  val do_bound_cut : bool

(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

  val regpress_compare : int array -> int array -> int
end