type t

type node_id

type node_exit_cause = 
  | Leaf of bool (*true iff branch better*)
  | CP_Cut
  | Dom_Cut
  | Mem_Cut
  | Resource_Cut
  | Register_Cut
  | Enumerate

val start : unit -> t

val entering : t -> node_id

val exiting : t -> node_exit_cause -> node_id -> unit

type stat = {
  total_nodes : int;
  leaf : int;
  enumerate : int;

  optimal_nodes : int list;

  critical_path_cut : int;
  dominance_cut : int;
  direct_memorisation_cut : int;
  resource_cut : int;
  register_cut : int;

}
val stat : t -> stat