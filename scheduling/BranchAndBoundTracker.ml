type t = {
  mutable total_nodes : int;
  mutable leaf : int;
  mutable enumerate : int;

  mutable optimal_nodes : int list;

  mutable critical_path_cut : int;
  mutable dominance_cut : int;
  mutable direct_memorisation_cut : int;
  mutable resource_cut : int;
  mutable register_cut : int;

}

type node_id = int

type node_exit_cause = 
  | Leaf of bool (*true iff branch better*)
  | CP_Cut
  | Dom_Cut
  | Mem_Cut
  | Resource_Cut
  | Register_Cut
  | Enumerate

let start () : t = {
  total_nodes = 0;
  leaf = 0;
  enumerate = 0;
  optimal_nodes = [];
  critical_path_cut = 0;
  dominance_cut = 0;
  direct_memorisation_cut = 0;
  resource_cut = 0;
  register_cut = 0;
}

let entering (tracker:t) : node_id = 
  let node_id = tracker.total_nodes in
  tracker.total_nodes <- tracker.total_nodes + 1;
  node_id

let exiting (tracker:t) (cause:node_exit_cause) (node_id:node_id) : unit = 
  match cause with
  | Leaf true -> 
    tracker.leaf <- tracker.leaf + 1;
    tracker.optimal_nodes <- node_id::tracker.optimal_nodes;
  | Leaf false -> tracker.leaf <- tracker.leaf + 1
  | CP_Cut -> tracker.critical_path_cut <- tracker.critical_path_cut + 1
  | Dom_Cut -> tracker.dominance_cut <- tracker.dominance_cut + 1
  | Mem_Cut -> tracker.direct_memorisation_cut <- tracker.direct_memorisation_cut + 1
  | Resource_Cut -> tracker.resource_cut <- tracker.resource_cut + 1
  | Register_Cut -> tracker.register_cut <- tracker.register_cut + 1
  | Enumerate -> tracker.enumerate <- tracker.enumerate + 1

type stat = {
  total_nodes : int;
  leaf : int;
  enumerate : int;

  optimal_nodes : int list;

  critical_path_cut : int;
  dominance_cut : int;
  direct_memorisation_cut : int;
  resource_cut : int;
  register_cut : int;

}
let stat (tracker:t) : stat = 
  let {
    total_nodes;
    leaf;
    enumerate;
    optimal_nodes;
    critical_path_cut;
    dominance_cut;
    direct_memorisation_cut;
    resource_cut;
    register_cut;
  }:t = tracker in 
  {
    total_nodes;
    leaf;
    enumerate;
    optimal_nodes;
    critical_path_cut;
    dominance_cut;
    direct_memorisation_cut;
    resource_cut;
    register_cut;
  }