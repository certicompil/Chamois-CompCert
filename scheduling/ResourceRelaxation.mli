module type T = sig
    type t

    val dummy : t

    val create :  int array array -> int array -> t
    (**
        [create instruction_usage max_usage] return  
    *)

    val lower_bound : bool array -> t -> int
    (**
    [lower_bound filter patterns] compute a lower bound on the makespan of the subproblem containig only instructions [i] such that [filter i] is true
    *)

    val _test : (unit -> unit) list
    (**
    unitary tests for the module
    *) 
end
(**
    Generic type for resource-based relaxation. 
*)

module VolumeApproximation : T
(**
    Compute an approximation based on volume of instruction 
    TODO : ref
*)


module Exact : T
(**
    Try to compute the exact value using a branch and bound method on a cutting stock formulation.
    Cannot be used in practice due to exponantial execution time.
*)
