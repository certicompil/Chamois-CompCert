(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Léo Gourdin        UGA, VERIMAG                   *)
(*           Alexandre Berard   UGA, VERIMAG                   *)
(*           Benjamin Bonneau   ENS, PSL                       *)
(*           David Monniaux     CNRS, VERIMAG                  *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

(** Main interface to communicate with BTL oracles *)

open Maps
open RTLcommonaux
open BTL
open BTLtypes
open Machine
open DebugPrint
open PrintBTL
open BTLcommonaux
open BTL_Liveness
open PrepassSchedulingOracle
open ExpansionOracle
open BTL_RegisterRenaming
open BTL_BlockOptimizeraux
open BTL_IfLiftingOracle
open LazyCodeOracle
open BTL_SyntheticNodes
module IM = Map.Make (Int)

let simple_schedule n btl typing va =
  let ibf = get_some @@ PTree.get n btl in
  let bseq, olast = flatten_blk_basics ibf in
  let seqa = Array.map (fun inst -> (inst, get_liveins inst)) bseq in
  let live_regs_entry = ibf.binfo.input_regs in
  let live_regs_exit = ibf.binfo.s_output_regs in
  let refcount = reference_counting seqa olast live_regs_exit typing in
  (*debug_flag := true;*)
  debug "btl:\n";
  print_btl_block stderr (get_some @@ (PTree.get n btl)) n;
  debug "seqa:\n";
  Array.iter (fun (i, l) ->
    print_btl_inst stderr i;
    debug "livein:";
    List.iter (fun r -> debug " %d" (p2i r)) (Registers.Regset.elements l);
    debug "\n") seqa;
  (*debug_flag := false;*)
  match
    schedule_sequence seqa olast live_regs_entry typing va refcount Default with
  | Some (positions, Some renaming) ->
      let scheduled_ib = apply_schedule bseq olast positions in
      let scheduled_ibf = mk_ibinfo scheduled_ib ibf.binfo in
      let scheduled_btl = PTree.set n scheduled_ibf btl in
      let btl' = apply_renaming_map n scheduled_btl renaming in
      let btl' = restore_blocks n btl' renaming in
      (*debug_flag := true;*)
      debug "\nbefore schedule + renaming:\n";
      print_btl_block stderr (get_some @@ (PTree.get n btl)) n;
      debug "\nafter schedule + renaming:\n";
      print_btl_block stderr (get_some @@ (PTree.get n btl')) n;
      (*debug_flag := false;*)
      Some btl'
  | Some (positions, None) ->
      let new_ib = apply_schedule bseq olast positions in
      let new_ibf = mk_ibinfo new_ib ibf.binfo in
      let btl' = PTree.set n new_ibf btl in
      (*debug_flag := true;*)
      debug "\nbefore schedule:\n";
      print_btl_block stderr (get_some @@ (PTree.get n btl)) n;
      debug "\nafter schedule:\n";
      print_btl_block stderr (get_some @@ (PTree.get n btl')) n;
      (*debug_flag := false;*)
      Some btl'
  | None -> None

let turn_all_loads_nontrap n ibf btl =
  if (not !config.has_non_trapping_loads) || not !Clflags.option_fnontrap_loads
  then btl
  else
    let rec traverse_rec ib =
      match ib with
      | Bseq (ib1, ib2) -> Bseq (traverse_rec ib1, traverse_rec ib2)
      | Bload (t, a, b, c, d, e) ->
          let _opt_info =
            match t with AST.TRAP -> Some true | AST.NOTRAP -> Some false
          in
          e.iinfo_shadow.opt_info <- _opt_info;
          Bload (AST.NOTRAP, a, b, c, d, e)
      | _ -> ib
    in
    let ib' = traverse_rec ibf.entry in
    let ibf' = mk_ibinfo ib' ibf.binfo in
    PTree.set n ibf' btl

let schedule_blk n btl typing ren_map va =
  if !Clflags.option_if_lift
  then if_lift n btl typing va ren_map
  else if !Clflags.option_fprepass
  then simple_schedule n btl typing va
  else None

let schedule_tf (va : bool) btl fi n ibf =
  compute_liveins n ibf btl;
  let code_nt = turn_all_loads_nontrap n ibf btl in
  let code_freg, ren_map = get_freg n code_nt in
  match schedule_blk n code_freg fi.typing ren_map va with
  | Some tbtl -> tbtl
  | None -> btl

let rec apply_optims tf btl fi = function
  | [] -> btl
  | (n, ibf) :: blks -> apply_optims tf (tf btl fi n ibf) fi blks

let btl_expansions_oracle f =
  let btl = f.fn_code in
  let elts = PTree.elements btl in
  find_last_reg_btl elts;
  let btl_exp = apply_optims expanse btl f.fn_info elts in
  let need_dce = update_liveness_btl btl_exp in
  let btl_dce =
    if need_dce then btl_simple_dead_code_elimination btl_exp else btl_exp
  in
  let gm = build_liveness_invariants elts PTree.empty in
  ((btl_dce, f.fn_info), gm)

let btl_int_promotion_oracle (f : BTL.coq_function)
  : (BTL.code * BTL.function_info) * BTL_Invariants.gluemap =
  (*debug_flag := true;
  Printf.fprintf stderr "=================== source program ===================\n%a\n"
    print_btl_code f.fn_code;
  let itv_a = BTL_ZIntervalAnalysis.analyze f in
  let control_points = f.fn_code |> PTree.elements |> List.map fst in
  PrintZIntervalAnalysis.print_analysis_results stderr control_points itv_a;*)
  let btl_optim, gm = IntPromotionOracle.int_promotion_oracle f.fn_code f.fn_entrypoint f.fn_params in
  (*Printf.fprintf stderr "=================== target program ===================\n%a\n"
    print_btl_code btl_optim;*)
  ((btl_optim, f.fn_info), gm)

let btl_lazy_code_oracle f =
  let btl = f.fn_code in
  let btl_optim, gm = lazy_code_oracle btl f.fn_entrypoint in
  let btl_nosynth = eliminate_synthetic_nodes btl_optim in
  ((btl_nosynth, f.fn_info), gm)

let btl_scheduling_oracle f =
  let need_dce = update_liveness_btl f.fn_code in
  let btl_dce =
    if need_dce then btl_simple_dead_code_elimination f.fn_code else f.fn_code
  in
  let elts = PTree.elements btl_dce in
  find_last_reg_btl elts;
  let btl_optim = apply_optims (schedule_tf !Clflags.option_fprepass_alias_abs) btl_dce f.fn_info elts in
  let gm = build_liveness_invariants elts PTree.empty in
  if BTL_IfLiftingOracle.save_stats then BTL_IfLiftingOracle.write_stats ();
  ((btl_optim, f.fn_info), gm)

module StoreMotionOracle1 = StoreMotionOracle.Make(StoreMotionOracle.StTotal)
module StoreMotionOracle2 = StoreMotionOracle.Make(StoreMotionOracle.StPartial)

let btl_store_motion_oracle (f : BTL.coq_function) : (BTL.code * BTL.function_info) * BTL_Invariants.gluemap =
  let need_dce = update_liveness_btl f.fn_code in
  let f =
    if need_dce then {f with fn_code = btl_simple_dead_code_elimination f.fn_code}
    else f
  in
  let btl_optim, gm =
    if !Clflags.option_fstore_motion_alias_abs >= 2
    then StoreMotionOracle2.transfer f
    else StoreMotionOracle1.transfer f
  in ((btl_optim, f.fn_info), gm)

let btl_expansions_rrules () = BTL_SEsimuref.RRexpansions

let btl_lazy_code_rrules  () = BTL_SEsimuref.RRlct (!Clflags.option_flct_sr, !Clflags.option_flct_alias_abs)
let btl_lazy_code_annotate_analysis () = !Clflags.option_flct_alias_abs

let btl_store_motion_rrules () =
  BTL_SEsimuref.RRstoreMotion (!Clflags.option_fstore_motion_alias_abs >= 1,
                               !Clflags.option_fstore_motion_alias_abs >= 2)
let btl_store_motion_annotate_analysis () = !Clflags.option_fstore_motion_alias_abs >= 1

let btl_scheduling_rrules () =
    if !Clflags.option_fprepass_alias_rel || !Clflags.option_fprepass_alias_abs
    then BTL_SEsimuref.RRschedule (!Clflags.option_fprepass_alias_rel, !Clflags.option_fprepass_alias_abs)
    else BTL_SEsimuref.RRnone
let btl_scheduling_annotate_analysis () =
    !Clflags.option_fprepass_alias_abs
