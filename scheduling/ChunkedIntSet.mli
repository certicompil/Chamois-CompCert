type elt = int
type t
val empty : t
val add : int -> t -> t
val singleton : int -> t
val remove : int -> t -> t
val union : t -> t -> t
val inter : t -> t -> t
val disjoint : t -> t -> bool
val diff : t -> t -> t
val elements : t -> int list
val cardinal : t -> int
val is_empty : t -> bool
val mem : elt -> t -> bool
