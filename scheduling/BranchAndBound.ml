open InstructionSchedulerTypes

module Log = Tools.Log
open Tools.Adv_string_conversion
module Tracker = BranchAndBoundTracker
open BNB_heuristic

(********************************************************************************)
(********************************************************************************)

module Interupter = struct
   type interupter_infos = {
      depth : int;
      bnb_tracker : Tracker.t;
   }

   type t = interupter_infos -> unit
   type bnb_interuption = |TimeOut |NodeOut |TooDeep
   exception BnB_Interupt of bnb_interuption

   let empty = fun _ -> ()
   let time (max_time:float) : t = 
      let start = Sys.time () in
      (fun _ -> 
         if Sys.time () > (start +. max_time) then
            raise @@ BnB_Interupt TimeOut
         else 
            ()
      )

   (* let adv_node 
      ?(base_cost=1)
      ?(leaf_cost=0)
      ?(enumerate_cost=(-1))
      ?(critical_path_cut_cost=0)
      ?(dominance_cut_cost=0)
      ?(direct_memorisation_cut_cost=0)
      ?(resource_cut_cost=0)
      ?(register_cut_cost=0)
   (limit:int) : t = (fun {tracker;_} -> 
      let stats = Tracker.stat tracker in 
      let cost = 
         stats.total_nodes * base_cost +
         stats.leaf * leaf_cost + 
         stats.enumerate * enumerate_cost + 
         stats.critical_path_cut * critical_path_cut_cost +
         stats.dominance_cut * dominance_cut_cost +
         stats.direct_memorisation_cut * direct_memorisation_cut_cost +
         stats.resource_cut * resource_cut_cost +
         stats.register_cut * register_cut_cost
      in
      if limit > cost then raise @@ BnB_Interupt NodeOut
   ) *)

   let node (max_nodes:int) : t = (fun {bnb_tracker;_} -> let node_nb = Tracker.((stat bnb_tracker).total_nodes) in if node_nb > max_nodes then raise @@ BnB_Interupt NodeOut)

   let depth (max_deapth:int) : t = (fun {depth;_} -> if depth > max_deapth then raise @@ BnB_Interupt TooDeep)

   let add (it1:t) (it2:t) : t = (fun l -> it1 l;it2 l)
end

module type T = sig
   type t
   type result = 
   | Proven_Optimal of PreprocessProblem.unprocessed_solution * int * int array
   | Proven_Impossible
   | Unkown_Possible of PreprocessProblem.unprocessed_solution * int * int array
   | Unkown
   val init : ?choose:Strategy.t -> ?interupter:Interupter.t -> (* ?preprocess:PreprocessProblem.t -> *) ?tracker:Tracker.t -> problem -> int array -> t
   val copy : t -> t
   val set_interupter : Interupter.t ->  t -> t
   val set_upper_bound : int -> t-> t
   val set_max_regpress : int array -> t -> t
   val compute : t -> result
  (*  val compute_regpress : t -> float -> result Seq.t *)
end


module Make(Config:BNB_Config.T) = struct

   module Scheduler = Config.Scheduler
   module UsagePattern = Config.UsagePattern

   module Dom_interface = struct 

      type t = {
         d : int array;
         rho : int array;
         b : int
      }
   
      type core = {
         dom : Dominance.t;
         (* nb_instruction : int; *)
         (* nb_ressource : int; *)
      }
   
      (* let copy (self:core) : core = {
         dom = Dominance.copy self.dom
      } *)
   
      let dummy_core = {dom=Dominance.create @@ List.to_seq [(0,0)]}
   
      let dummy = {d=[||];rho=[||];b=0}
   
      let dominance_create problem (max_latency:int array) upper_bound : core = 
         (*WARNING change max_latency if schedule_STATE change*)
         if not Config.do_memorise_subproblem then 
            {dom = Dominance.create Seq.empty;(* nb_instruction=0; *)(* nb_ressource=0 *)}
         else
         let dom = Dominance.create (Seq.concat (List.to_seq [
            Seq.init (Array.length problem.instruction_usages) (fun i -> (-1,max_latency.(i))); (*TODO check if OK*)
            Seq.map (fun i -> (0,i)) (Array.to_seq problem.resource_bounds);
            List.to_seq [(-upper_bound,0)]
         ])) in
         {
            dom;
            (* nb_instruction = Array.length (problem.instruction_usages); *)
            (* nb_ressource = Array.length (problem.resource_bounds) *)
         }
         (* Dominance.create (Seq.concat (List.to_seq [
            Seq.init (Array.length problem.instruction_usages) (fun i -> (-1,min_latency.(i))); (*TODO check if OK*)
            Seq.map (fun i -> (0,i * upper_bound) ) (Array.to_seq problem.resource_bounds) (*ressource usage is the "mantis" of the current time *)
         ])) *)
      
      let dominance_check (core:core) (arg:t) : bool =
         if not Config.do_memorise_subproblem then false else
         Dominance.check_dominance core.dom  (Seq.concat (List.to_seq [
            Array.to_seq arg.d;
            Array.to_seq arg.rho;
            List.to_seq [-arg.b];
         ]))
      
      let dominance_add (core:core) (arg:t) : unit =
         if not Config.do_memorise_subproblem then () else
         Dominance.add_dominance core.dom (Seq.concat (List.to_seq [
         Array.to_seq arg.d;
         Array.to_seq arg.rho;
         List.to_seq [-arg.b];
         ]))

      (* let dominance_spring_clean (core:core) : unit = Dominance.clean core.dom *)
      
      (* let c_leaf core =
         {
            d=Array.make core.nb_instruction (-1);
            rho=Array.make core.nb_ressource (0);
            b=0;
         } *)
   
      let c_merge_child (parent:t) (possible:bool array) (c_child_l:t list) =
         let d = Array.mapi (fun i _ ->
            List.fold_left (fun acc v -> max acc v.d.(i)) (-1) c_child_l
         ) parent.d in
         Array.iteri (fun i v ->
            if v then 
               d.(i) <- parent.d.(i)
         ) possible;
         {parent with d}
      (*
      subproblem parent can branch on exactly all variables of possible. 
      c_child_l is an exostive list of the childs causes.
      WARNING parent should not be writen after call
      *)
   
      (* let c_reverse_time (curent:t) (c_next:t) = 
         {
            d = c_next.d;
            rho = curent.rho;
            b = curent.b;
         } *)
      (*
      WARNING curent and c_next should not be writen after call
      *)
   end
   
   (********************************************************************************)

   module Sub_memory_adv : sig
      type t
      val find_opt : t -> (int array * int array * int) -> (int * int array) option
      val add : t -> (int array * int array * int) -> unit
      val create : int -> t
   end = struct
      module DelayTable = Hashtbl.Make(struct
         type t = int array
         let hash = Hashtbl.hash
         let equal a b = Array.for_all2 (=) a b
      end)
      module ResourceSet = Set.Make(struct
         type t = int array
         let compare (* = Stdlib.compare *) a b = Array.fold_left (fun acc (a,b) -> 
            match acc,compare a b with (0,v) | (v,_) -> v
         ) 0 (Array.combine a b)
      end)
      type t = (int*ResourceSet.t) DelayTable.t

      let find_opt (mem:t) ((d,rho,b):int array * int array * int) : (int * int array) option = 
         match DelayTable.find_opt mem d with
         | None -> None
         | Some (b2,_) when b2 < b -> None
         | Some (b2,rhoSet) when b2 > b -> Some (b2,ResourceSet.choose rhoSet)
         | Some (_,rhoSet) -> 
            match ResourceSet.find_opt rho rhoSet with
            | None -> None
            | Some _ -> Some (b,rho)
      
      let add (mem:t) ((d,rho,b):int array * int array * int) : unit = 
         match DelayTable.find_opt mem d with
         | Some (b2,_) when b2 < b -> ()
         | None -> DelayTable.add mem d (b,ResourceSet.singleton rho)
         | Some (b2,_) when b2 > b -> DelayTable.replace mem d (b,ResourceSet.singleton rho)
         | Some (_,rhoSet) -> DelayTable.replace mem d (b,ResourceSet.add rho rhoSet)

      let create (size:int) : t = DelayTable.create size
   end
   
   module Sub_memory = Hashtbl.Make(struct
    type t = int array * int array
    let hash = Hashtbl.hash
    let equal (a1,a2) (b1,b2) = Array.for_all2 (=) a1 b1 && Array.for_all2 (=) a2 b2
   end)
   
   module Register_memory = Hashtbl.Make(struct
      type t = bool array
      let hash = Hashtbl.hash
      let equal a b = not @@ Array.exists2 (<>) a b
   end)
   
   (* module IMap = Map.Make (Int) *)
   
   type result = 
   (* (schedule, makespan, max regpress) *)
   | Proven_Optimal of PreprocessProblem.unprocessed_solution * int * int array
   | Proven_Impossible
   | Unkown_Possible of PreprocessProblem.unprocessed_solution* int * int array
   | Unkown
   
   type t = {
     initial_problem : problem;
     max_regpress : int array;
     usage_patterns : UsagePattern.t;
     problem : problem;
     cp : int array;
     initial_upper_bound : int;
     choose : Strategy.t;
     interupter : Interupter.t;
     tracker : Tracker.t;
   
     start: Scheduler.t;
     dom_memory : Dom_interface.core;
     sub_memory : (int * bool) Sub_memory.t;
     sub_memory_adv : Sub_memory_adv.t;
     register_memory : unit Register_memory.t;
   
     upper_bound : int ref;
     best_schedule : int array;
     best_schedule_regpress : int array;
   }
   
   (* type bnb_return = {
     minimal_instructions_cause : int array
   } *)
   (* information return recurssively by the branch and bound algorithm *)
   
   let copy (info:t) = 
      (* Dom_interface.dominance_spring_clean info.dom_memory; *)
      {
      (* not change during runtime *)
      initial_problem = info.initial_problem;
      max_regpress = info.max_regpress;
      usage_patterns = info.usage_patterns;
      problem = info.problem;
      cp = info.cp;
      choose = info.choose;
      initial_upper_bound = info.initial_upper_bound;
      interupter = info.interupter;
      tracker = Tracker.start ()(*  info.tracker *); (* TODO ?*)
   
      (* modify during runtime *)
      start = Scheduler.copy info.start;
      dom_memory = (* Dom_interface.copy *) info.dom_memory; (**** !!!!!! CHANGE TODO WARNING !!!!!! ****)
      sub_memory = if Config.do_simple_memorisaton then Sub_memory.create 101 else Sub_memory.create 0(* TODO ? Sub_memory.copy info.sub_memory *);
      sub_memory_adv = Sub_memory_adv.create 101;(*TODO modulable*)
      register_memory = Register_memory.create 101;(*  Register_memory.copy info.register_memory; *)
   
      upper_bound = ref !(info.upper_bound);
      best_schedule = Array.copy info.best_schedule;
      best_schedule_regpress = Array.copy info.best_schedule_regpress
   }
   
   let set_upper_bound (bound:int) (info:t) : t = {info with initial_upper_bound = bound; upper_bound = ref bound}
   let set_max_regpress (bounds:int array) (info:t) : t = {info with max_regpress = bounds}
   let set_interupter (interupter:Interupter.t) (info:t) = {info with interupter}
   
   let init 
     ?(choose)
     ?(interupter)
     ?(tracker)
   (initial_problem:problem) (nr_regs:int array) 
    (* CARFULL dom memory is proportional to max latency ! *)
   : t = 
      let problem = initial_problem in
      (* let heuristic = Heuristic.init initial_problem in *)
   
      let initial_upper_bound = if problem.max_latency = -1 then failwith "Instruction scheduling : max latency must be given" else problem.max_latency in (*        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*)
   
      let max_regpress = match Config.Scheduler.do_regpress with
      | false -> [||]
      | true -> nr_regs 
         (*add live registers to current register pressure ? => Ne sers à rien... (fait dans step scheduler)*)
         (* let regpress = (Array.map (( * )0) nr_regs) in
         let rc = match problem.reference_counting with None -> failwith "reference counting is None when optimising regpress" | Some s -> s in
         List.iter (fun r -> 
            let (r_t,_) = match Hashtbl.find_opt (fst rc) r with Some v -> v | None -> failwith "reference counting incoherence : registrer not in hashtable" in
            regpress.(r_t) <- regpress.(r_t) + 1
         ) (Registers.Regset.elements problem.live_regs_entry);
         regpress *)
      in
   
      let start =  Scheduler.init problem max_regpress in
      let initial_cp = Scheduler.compute_cp start (* Heuristic.compute_CP heuristic *) in
      let choose  = match choose with
      | Some f -> f
      | None -> (* Strategy.switch2 @@ *) Strategy.from_order initial_cp (* (Strategy.random_order initial_cp) *) (* (Strategy.reverse_order initial_cp) *)
      in
   
      let dom_memory = if Config.do_memorise_subproblem then Dom_interface.dominance_create problem ( Scheduler.compute_advance_delay start) initial_upper_bound else Dom_interface.dummy_core in 
      let sub_memory = if Config.do_simple_memorisaton then Sub_memory.create 101 else Sub_memory.create 0 in (*TODO 101*)
      let sub_memory_adv = Sub_memory_adv.create 101 in
      let interupter = match interupter with
      | Some f -> f
      | None -> Interupter.empty
      in
      let tracker = match tracker with
      | Some t -> t
      | None -> Tracker.start () in
      (* Log.once (fun _ -> Printf.printf "WARNING : pattern calculation assume all instructions use some resources\n"); *)
      let usage_patterns = if not Config.do_resource_cut then UsagePattern.dummy else
         UsagePattern.create problem.instruction_usages problem.resource_bounds in
      Log.p (2) (fun _ -> Printf.printf "finish init\n";flush stdout);
      {
         initial_problem;
         max_regpress;
         usage_patterns;
         problem;
         cp = initial_cp;
         start;
         choose;
         dom_memory;
         sub_memory;
         sub_memory_adv;
         initial_upper_bound;
         interupter;
         tracker;
         register_memory = Register_memory.create 101;
   
         upper_bound = ref initial_upper_bound;
         best_schedule = Array.copy @@ Scheduler.current_schedule start;
         best_schedule_regpress = Array.copy @@ Scheduler.current_regpress start
      }
   
   
   (* 
      cut based on a relaxation of resource constraints.
      In this case, the minimum schedule is of size critical path + delay.
      (proof without delay in the doc) 
   *)
   let _bound_cut (self: Scheduler.t) (delay: int array) (cp:int array) (upper_bound:int) =
      if not Config.do_bound_cut then None else
      
      (* find instruction with max (delay + cp) *)
      let max_dl = ref (-1) in
      let max_dl_i = ref (-1) in
      Array.iteri (fun i (d,l) -> 
         if d > (-1) && d + l > !max_dl then (
            max_dl := d+l;
            max_dl_i := i 
         )
      ) (Array.combine delay cp);
   
      let current_time = Scheduler.current_time self in
      (* Printf.printf "[%s] %d + %d + 1 >= %d ?\n" (string_of_int_array delay) current_time !max_dl !upper_bound; *)
      match current_time + !max_dl + 1 >= upper_bound with
      | true -> Some !max_dl_i
      | false -> None
   
   (* 
      cut based on memorisation of previous sub-problem.
   *)
   let _dominance_cut (self: Scheduler.t) (delays: int array) (lower_bound:int) (dom_memory:Dom_interface.core) =
      if not Config.do_memorise_subproblem then false else
      let ressources_usage = (Scheduler.current_resource_usage self) in
      Dom_interface.dominance_check dom_memory Dom_interface.{
         d    =  delays;
         rho  =  ressources_usage;
         b    =  lower_bound
      }
   
   (* 
      cut based on direct memorisation of previous sub-problem.
   *)
   let _mem_cut self (mem: (int*bool) Sub_memory.t) (delays: int array) (lower_bound:int) : bool option =
      if not Config.do_simple_memorisaton then None else
      let ressources_usage = (Scheduler.current_resource_usage self) in
      match Sub_memory.find_opt mem (ressources_usage,delays) with
      | None -> None
      | Some (v,_) when v<lower_bound -> None
      | Some (_,b) -> Some b

   (* 
      cut based on direct memorisation of previous sub-problem.
   *)
   let _mem_cut_adv self (mem: Sub_memory_adv.t) (delays: int array) (lower_bound:int) : unit option =
      let ressources_usage = (Scheduler.current_resource_usage self) in
      match Sub_memory_adv.find_opt mem (delays,ressources_usage,lower_bound) with
      | None -> None
      | Some (_) -> Some ()

   
   
   (*
      cut based on a relaxation of latency constraints.
      The bound return must be a lower bound on the cutting stock problem.
   *)
   let _resource_bound_cut (patterns:UsagePattern.t) (_:int array array) (current_schedule: int array) (current_time:int) (upper_bound:int) =
      if not Config.do_resource_cut then false else
      let i_filter = Array.map (fun i -> i=(-1)) current_schedule in
      let bound = UsagePattern.lower_bound i_filter patterns in
      (* Printf.printf "filter : [%s]\n" (string_of_bool_array i_filter);
      Printf.printf "%d + %d >= %d \n" current_time bound upper_bound; *)
      (* Printf.printf "rb : %d + %d >= %d ?\n" current_time bound upper_bound; *)
      current_time + bound >= upper_bound
   
   (*
      cut based on register pression overflow   
   *)
   let _register_cut (self : Scheduler.t) (memory:unit Register_memory.t) (max_regpress:int array) = 
      if not Config.Scheduler.do_regpress then false else
      let i_filter = Array.map (fun e -> e = -1) @@ Scheduler.current_schedule self in
      match Register_memory.mem memory i_filter with
      | true -> true
      | false ->
      let regpress = Scheduler.current_regpress self in
      (* Log.p 5 (fun _ -> Printf.printf "current regpress, max regpress : [%s] [%s]\n" (string_of_int_array regpress) (string_of_int_array max_regpress)); *)
      match Array.exists2 (>) regpress max_regpress with
      | true -> Register_memory.add memory i_filter ();true
      | false -> false
   
   
      
   (*************************)
   
   (* type blocking_constraint = Dom_interface.t option *)
   
   type b_ret = {
      blocking_constraint : Dom_interface.t;
      did_regpress_cut : bool;
      found_better : bool
   }
   
   let return ?(blocking_constraint=Dom_interface.dummy) ?(did_regpress_cut=false) ?(found_better=false) () = {
      blocking_constraint;
      did_regpress_cut;
      found_better
   }
   
   let compute_core (info:t) = 
      (* let next_node_id = ref 0 in *)
      let upper_bound = info.upper_bound in
      let best_schedule = info.best_schedule in
      let best_schedule_regpress = info.best_schedule_regpress in
      (* 
         branch and bound (and more). 
         Update the best schedule as a side effect.
         Return a valid blocking_constraint of the given sub problem. 
      *)
      let rec bnb (interupter_info:Interupter.interupter_infos) (current_max_regpress : int array) (self: Scheduler.t) : b_ret = 
         (* next_node_id := !next_node_id + 1; *)
         let level = Interupter.(interupter_info.depth) in
         let branch_id = Tracker.entering info.tracker in
         info.interupter interupter_info;
         Log.p_indent level (3) (fun _ -> Printf.printf " schedule state : %s\n" (string_of_int_array (Scheduler.current_schedule self)));
         match  Scheduler.total_makespan self with
         (* Leaf worst than previous solution *)
         | Some m when m >= !upper_bound->
            Log.p_indent level (2) (fun _ -> Printf.printf " cutting %d >= %d\n" m !upper_bound); 
            Tracker.exiting info.tracker (Tracker.Leaf false) branch_id;
            return ~blocking_constraint:Dom_interface.{
                  d   = Scheduler.compute_delay self;
                  rho = Scheduler.current_resource_usage self;
                  b   = (!upper_bound - m);
               } ()
         (* Leaf not respecting regpress *)
         | Some _ when _register_cut self info.register_memory info.max_regpress -> 
            Log.p_indent level (2) (fun _ -> Printf.printf " cutting (regpress)\n"); 
            Tracker.exiting info.tracker (Tracker.Leaf false) branch_id;
            return ~did_regpress_cut:true ()
         (* Leaf better than previous solution *)
         | Some m -> 
            Log.p_indent level (2) (fun _ -> Printf.printf " keeping %d < %d\n" m !upper_bound); 
            upper_bound := m;
            Tools.Array_operation.copy_in best_schedule @@ Scheduler.current_schedule self;
            if Config.Scheduler.do_regpress then (
               Tools.Array_operation.copy_in best_schedule_regpress @@ current_max_regpress);
            Tracker.exiting info.tracker (Tracker.Leaf true) branch_id; 
            return 
            ~found_better:true 
            ~blocking_constraint:Dom_interface.{ (*the optimal is updated so this blocking constraint become valid*)
                  d   = Scheduler.compute_delay self;
                  rho = Scheduler.current_resource_usage self;
                  b   = (!upper_bound - m);
               } ()(* (Scheduler.current_schedule self,m) *)
         (* Not a leaf *)
         | None -> 
            let ressources_usage = Scheduler.current_resource_usage self in
            let current_time = Scheduler.current_time self in
            (* let current_upper_bound = !upper_bound in *)

            (* let lp_res = MultiBinPacking.solve_lp (* ~integer:true *)  {info.problem with max_latency = !upper_bound} (Array.map ((=) (-1)) @@ Scheduler.current_schedule self) in
            Printf.printf "%d + %d > %d ?\n" current_time lp_res !upper_bound;flush stdout; *)
            
            (*cuts*)
            Log.p_indent level (3) (fun _ -> Printf.printf " check cut\n");
            let delay = Scheduler.compute_delay self in
            let extended_delay = Scheduler.compute_advance_delay self in
            (* justification for extended delay in doc *)
            match _bound_cut self delay info.cp !upper_bound with 
            |Some id ->
               Log.p_indent level (2) (fun _ -> Printf.printf " bound cut (%d)\n" id);
               Tracker.exiting info.tracker Tracker.CP_Cut branch_id;
               let d = Array.make (Array.length extended_delay) (-1) in
               d.(id) <- extended_delay.(id); (*justification ?*)
               return ~blocking_constraint:Dom_interface.{
                  d;
                  rho = Scheduler.current_resource_usage self;
                  b   = (!upper_bound - current_time);
               } ()
            |None ->
            match _resource_bound_cut info.usage_patterns info.problem.instruction_usages (Scheduler.current_schedule self) current_time !upper_bound with
            | true -> 
               Log.p_indent level (2) (fun _ -> Printf.printf " resource bound cut\n");
               Tracker.exiting info.tracker Tracker.Resource_Cut branch_id;
               return ~blocking_constraint:Dom_interface.{
                  d   = extended_delay;
                  rho = Scheduler.current_resource_usage self;
                  b   = (!upper_bound - current_time);
               } ()
            | false ->
            match _register_cut self info.register_memory info.max_regpress with
            | true -> 
               Log.p_indent level (2) (fun _ -> Printf.printf " register cut [%s]\n" (string_of_int_array info.max_regpress));
               Tracker.exiting info.tracker Tracker.Register_Cut branch_id;
               return ~did_regpress_cut:true ()
               (* failwith "register cut" *)
            | false ->
            match _mem_cut self info.sub_memory extended_delay (!upper_bound - current_time) with
            | Some isRegisterPressure -> 
               Log.p_indent level (2) (fun _ -> Printf.printf " pur mem cut\n");
               Tracker.exiting info.tracker Tracker.Mem_Cut branch_id;
               return ~did_regpress_cut:isRegisterPressure ~blocking_constraint:Dom_interface.{
                  d   = extended_delay;
                  rho = Scheduler.current_resource_usage self;
                  b   = (!upper_bound - current_time);
               } ()
            | None ->
            match _mem_cut_adv self info.sub_memory_adv extended_delay (!upper_bound - current_time) with
            | Some _ -> 
               Log.p_indent level (2) (fun _ -> Printf.printf " pur mem cut adv\n");(*TODO : cause can be generalized*)
               Tracker.exiting info.tracker Tracker.Mem_Cut branch_id;
               return ~did_regpress_cut:false ~blocking_constraint:Dom_interface.{
                  d   = extended_delay;
                  rho = Scheduler.current_resource_usage self;
                  b   = (!upper_bound - current_time);
               } ()
            | None ->
            match _dominance_cut self extended_delay (!upper_bound - current_time) info.dom_memory with
            | true ->
               Log.p_indent level (2) (fun _ -> Printf.printf " dom/mem cut [%s]\n" (string_of_int_array extended_delay));
               Tracker.exiting info.tracker Tracker.Dom_Cut branch_id;
               return ~blocking_constraint:Dom_interface.{
                  d   = extended_delay;
                  rho = Scheduler.current_resource_usage self;
                  b   = (!upper_bound - current_time);
               } () (*TODO minimize -> take the cause of dominance ?*)
            | false ->
            
            (* advance time *)
            match Scheduler.possible self with
            | None -> 
               Log.p_indent level (2) (fun _ -> Printf.printf " advance time\n");
               let current_usage = Array.copy  (Scheduler.current_resource_usage self) in
               Scheduler.advance_time self;
               let next_res = bnb Interupter.{interupter_info with depth=level+1} current_max_regpress self in
               let res = match next_res with
               | {did_regpress_cut=true;_} -> next_res
               | _ when not Config.do_subproblem_minimisation -> next_res
               | {blocking_constraint=next_cause;_} ->
                  assert (not next_res.did_regpress_cut);
                  assert (Array.exists (fun x -> x <> -1) Dom_interface.(next_cause.d)); (* Child should not be a leaf *)
                  let cause_d = Array.map2 (fun a b -> if b <> (-1) && a <> b then (b + 1) else b) extended_delay Dom_interface.(next_cause.d) in
                  let cause = Dom_interface.{ 
                     d = cause_d;
                     rho = current_usage;
                     b = next_cause.b + 1;
                  } in
                  Log.p_indent level (2) (fun _ -> Printf.printf " add sp [%s] [%s] %d\n" Dom_interface.(string_of_int_array cause.d) Dom_interface.(string_of_int_array cause.rho) Dom_interface.(cause.b));
                  Dom_interface.dominance_add info.dom_memory cause; (*TODO : remove ? change ?*)
                  {next_res with blocking_constraint = cause}
               in
               Tracker.exiting info.tracker Tracker.Enumerate branch_id;
               res
            
            (* schedule some instruction *)
            | Some possibles -> 
            let possible_list = info.choose (possibles) in
   
            (* remove dominated instruction => perte de perf, mettre en preprocess*)
            (* let rec aux l =
               match l with
               | [] -> []
               | hd::tl -> 
                  if List.exists (fun i ->
                     Scheduler.runtime_dominance self i hd
                  ) tl then
                     aux tl
                  else
                     hd::(aux tl) 
            in
            let possible_list = aux possible_list in *)
   
            (* schedule when one instruction*)
            (* TODO cause *)
            (* match possible_list with
            | [] -> failwith "no instruction to schedule"
            | i::[] -> 
               Scheduler.schedule_instruction self i;
               bnb (level + 1) self
            | _ ->  *)
   
            (* branch on instructions*)
            Log.p_indent level (2) (fun _ -> Printf.printf " branch\n");

            let list_res = 
               List.map (fun branching_instruction -> 
                  Log.p_indent level (2) (fun _ -> Printf.printf " branch on %d\n" branching_instruction);
                  let child =  Scheduler.copy self in
                  Scheduler.schedule_instruction child branching_instruction;
                  let chind_max_regpress = Array.copy current_max_regpress in
                  if Config.Scheduler.do_regpress then (
                     Tools.Array_operation.max_in chind_max_regpress chind_max_regpress (Scheduler.current_regpress child) Config.regpress_compare;
                  );
                  bnb Interupter.{interupter_info with depth=level+1} chind_max_regpress child
               ) (possible_list) 
            in
            let found_better = List.exists (fun v -> v.found_better) list_res in
            let did_regpress_cut = List.exists (fun v -> v.did_regpress_cut) list_res in
            
            match did_regpress_cut with
            | false -> (* no regpress cut : can memorise the cause and stop without time branching *)
               Log.p_indent level (2) (fun _ -> Printf.printf " don't branch adv time\n");
               Tracker.exiting info.tracker Tracker.Enumerate branch_id;
               (
                  (* match List.length possible_list = 1 with
                  | true -> Log.p_indent level (2) (fun _ -> Printf.printf " no choice, don't memorize\n");return ~found_better ~blocking_constraint:cause ()
                  | false -> *)
                  match Config.do_memorise_subproblem with
                  | true -> 
                     let list_causes = List.map (fun res -> res.blocking_constraint) list_res in 
                     let parent_cause = Dom_interface.{
                        d   = extended_delay;
                        rho = ressources_usage;
                        b   = !upper_bound - current_time
                     } in
                     let cause = match Config.do_subproblem_minimisation with
                        | false -> parent_cause
                        | true -> Dom_interface.c_merge_child parent_cause possibles list_causes 
                     in
                     Dom_interface.dominance_add info.dom_memory cause;
                     Sub_memory_adv.add info.sub_memory_adv (extended_delay,(Scheduler.current_resource_usage self),(!upper_bound - current_time));
                     if Config.do_simple_memorisaton then (
                        Sub_memory.add info.sub_memory ((Scheduler.current_resource_usage self),extended_delay) (!upper_bound - current_time,false)
                     ) else ();
                     return ~found_better ~blocking_constraint:cause ()
                  | false when Config.do_simple_memorisaton -> 
                     Sub_memory.add info.sub_memory ((Scheduler.current_resource_usage self),extended_delay) (!upper_bound - current_time,false);
                     return ~found_better ()
                  | false -> 
                     return ~found_better ()
               )   
            | true -> (* contain a regpress cut : cannot do advance memorisation, must time branch *)
               Log.p_indent level (2) (fun _ -> Printf.printf " branch adv time\n");
               let res_advtime = (
                  let child = Scheduler.copy self in
                  Scheduler.advance_time child;
                  let next_cause_opt = bnb Interupter.{interupter_info with depth=level+1} current_max_regpress child in
                  next_cause_opt
               ) in

               let found_better = found_better || res_advtime.found_better in

               Tracker.exiting info.tracker Tracker.Enumerate branch_id;
               if Config.do_simple_memorisaton then (
                  Sub_memory.add info.sub_memory ((Scheduler.current_resource_usage self),extended_delay) (!upper_bound - current_time,true)
               ) else ();
               return ~found_better ~did_regpress_cut:true ()
      in
   
      (*schedule imediately possible instructions with no resource usage*)
      (* ===> moved to preprocess
      Array.iteri (fun i possible -> 
         match possible with
         | true when (Array.for_all (fun v -> v=0) (info.problem.instruction_usages.(i))) -> 
            Scheduler.schedule_instruction info.start i
         | true -> ()
         | false -> ()
      ) (match Scheduler.possible info.start with | None -> [||] | Some s -> s); *)
      
      let start_regpress = Scheduler.current_regpress info.start in
      let _ = bnb Interupter.{bnb_tracker=info.tracker;depth=0} (start_regpress) info.start in
      (* let _ = match res with
      | Some (res_s,_) -> assert (Array.for_all2 (=) res_s !best_schedule)
      | None -> () in *)
      match !upper_bound = info.initial_upper_bound with
      | true -> None
      | false -> Some (best_schedule,!upper_bound)
   
      (**************************************************************************************************************************************************************************************************************************)
   
      (* let compute_regpress (info:t) =
         let max_regpress_coeff = Array.fold_left (max) 0 info.max_regpress in
         let best_makespan = ref info.initial_upper_bound in
         let compute = (fun i ->
            let updated_info = {(copy info) with 
               initial_upper_bound = !best_makespan;
               max_regpress = Array.map (fun _ -> i) info.max_regpress
            } in
            try 
            match (compute_core updated_info) with
            | None -> Some None
            | Some (s,_) -> 
               best_makespan := 1 + Array.fold_left (max) 0 s;
               let solution = ExportProblem.to_solution @@ Preprocess.postprocess s info.postprocess in
               check_schedule info.initial_problem solution;
               Some (Some solution)
            with | Interupter.BnB_Interupt _ -> 
               Log.p (1) (fun _ -> Printf.printf " optimal not found (interupt)\n");
               None
         ) in
         let result_sequence = Seq.map compute @@ Seq.take (max_regpress_coeff) @@ Seq.ints 1 in
         result_sequence *)
   
      (* let compute_regpress (info:t) (local_timeout:float) =
         let max_regpress_coeff = Array.fold_left (max) 0 info.max_regpress in
         let best_makespan = ref info.initial_upper_bound in
         let compute = (fun i ->
            let updated_info = {(copy info) with 
               interupter = Interupter.add info.interupter (Interupter.time local_timeout);
               initial_upper_bound = !best_makespan;
               max_regpress = Array.map (fun _ -> i) info.max_regpress
            } in
            try 
            match (compute_core updated_info) with
            | None -> Proven_Impossible
            | Some (s,m) -> 
               best_makespan := m;
               let postprocess_schedule = Preprocess.postprocess s info.postprocess in
               let makespan = 1 + Array.fold_left (max) 0 postprocess_schedule in
               Proven_Optimal (postprocess_schedule,makespan,info.best_schedule_regpress)
            with Interupter.BnB_Interupt _ -> 
            match info.initial_upper_bound = !(info.upper_bound) with
            | true -> Unkown
            | false -> 
               let s = (info.best_schedule) in
               let postprocess_schedule = Preprocess.postprocess s info.postprocess in
               let makespan = 1 + Array.fold_left (max) 0 postprocess_schedule in
               best_makespan := makespan;
               Unkown_Possible (postprocess_schedule,makespan,info.best_schedule_regpress)
         ) in
         let result_sequence = Seq.map compute @@ Seq.take (max_regpress_coeff) @@ Seq.ints 1 in
         result_sequence *)
   
      let _compute_simple (info:t) = 
      try match (compute_core info) with
      | None -> 
         Log.p (1) (fun _ -> Printf.printf " no better than heuristic\n");
         Proven_Impossible
      | Some (s,_) -> 
   
         Log.p (1) (fun _ -> Printf.printf " solution : %s\n" (string_of_int_array s));
         (* let postprocess_schedule = Preprocess.postprocess s info.postprocess in *)
         let makespan = 1 + Array.fold_left (max) 0 s in
         Proven_Optimal (PreprocessProblem.{raw_solution=s},makespan,info.best_schedule_regpress)
         with
         |Interupter.BnB_Interupt _ -> 
            Log.p (1) (fun _ -> Printf.printf " optimal not found (interupt)\n");
            match info.initial_upper_bound = !(info.upper_bound) with
            | true -> Unkown
            | false -> 
               let s = (info.best_schedule) in
               (* let postprocess_schedule = Preprocess.postprocess s info.postprocess in *)
               let makespan = 1 + Array.fold_left (max) 0 s in
               Unkown_Possible (PreprocessProblem.{raw_solution=s},makespan,info.best_schedule_regpress)
   
      let compute = _compute_simple
end