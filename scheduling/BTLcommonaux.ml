(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Léo Gourdin        VERIMAG, UGA                   *)
(*           Benjamin Bonneau   ENS, PSL                       *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

(** Common tools for BTL oracles *)

open Maps
open BTL
open Registers
open BTLtypes
open RTLcommonaux
open Datatypes
open Camlcoq
open BTL_Invariants
open AST

(** Shadowfields related functions *)

let undef_node = -1

let mk_iinfo_shadow _inumb _opt_info =
  {
    inumb = _inumb;
    opt_info = _opt_info;
    visited = false;
    liveins = Regset.empty;
  }

let new_iinfo (ii : inst_info) _inumb _opt_info : inst_info =
  BTL.copy_inst_info ii (mk_iinfo_shadow _inumb _opt_info)

let def_iinfo_shadow () =
  {
    inumb = undef_node;
    opt_info = None;
    visited = false;
    liveins = Regset.empty;
  }

let def_iinfo () : inst_info =
  BTL.make_def_inst_info (def_iinfo_shadow ())

let mk_binfo _bnumb _input_regs _s_output_regs _successors _predecessors =
  {
    bnumb = _bnumb;
    visited = false;
    input_regs = _input_regs;
    s_output_regs = _s_output_regs;
    successors = _successors;
    predecessors = _predecessors;
  }

let def_binfo () =
  {
    bnumb = undef_node;
    visited = false;
    input_regs = Regset.empty;
    s_output_regs = Regset.empty;
    successors = [];
    predecessors = [];
  }

let mk_ibinfo _entry _binfo = { entry = _entry; binfo = _binfo }
let mk_finfo _typing _bb_mode = { typing = _typing; bb_mode = _bb_mode }

let mk_function _fn_info _fn_gm _fn_sig _fn_params _fn_stacksize _fn_code
    _fn_entrypoint =
  {
    fn_info = _fn_info;
    fn_gm = _fn_gm;
    fn_sig = _fn_sig;
    fn_params = _fn_params;
    fn_stacksize = _fn_stacksize;
    fn_code = _fn_code;
    fn_entrypoint = _fn_entrypoint;
  }

(** Auxiliary functions on BTL *)

let pget p t = get_some @@ PTree.get p t

let reset_visited_ibf btl =
  List.iter (fun (n, ibf) -> ibf.binfo.visited <- false) (PTree.elements btl)

let apply_over_function btl tf =
  List.iter (fun (n, ibf) -> tf n ibf) (PTree.elements btl)

let tf_ibf_ib f n ibf = f n ibf.entry

let rec reset_visited_ib_rec pc = function
  | Bseq (ib1, ib2) ->
      reset_visited_ib_rec pc ib1;
      reset_visited_ib_rec pc ib2
  | Bcond (_, _, ib1, ib2, iinfo) ->
      reset_visited_ib_rec pc ib1;
      reset_visited_ib_rec pc ib2;
      iinfo.iinfo_shadow.visited <- false
  | Bnop (Some iinfo)
  | Bop (_, _, _, iinfo)
  | Bload (_, _, _, _, _, iinfo)
  | Bstore (_, _, _, _, iinfo)
  | BF (_, iinfo) ->
      iinfo.iinfo_shadow.visited <- false
  | _ -> ()

let get_visited = function
  | Bcond (_, _, _, _, iinfo)
  | Bnop (Some iinfo)
  | Bop (_, _, _, iinfo)
  | Bload (_, _, _, _, _, iinfo)
  | Bstore (_, _, _, _, iinfo)
  | BF (_, iinfo) ->
      iinfo.iinfo_shadow.visited
  | _ -> failwith "get_visited: invalid iblock"

let jump_visit = function
  | Bcond (_, _, _, _, iinfo)
  | Bnop (Some iinfo)
  | Bop (_, _, _, iinfo)
  | Bload (_, _, _, _, _, iinfo)
  | Bstore (_, _, _, _, iinfo)
  | BF (_, iinfo) ->
      if iinfo.iinfo_shadow.visited then true
      else (
        iinfo.iinfo_shadow.visited <- true;
        false)
  | Bseq (_, _) -> false
  | Bnop None -> true

let rec get_inumb skip_goto = function
  | BF (Bgoto s, iinfo) -> if skip_goto then p2i s else iinfo.iinfo_shadow.inumb
  | BF (_, iinfo)
  | Bnop (Some iinfo)
  | Bop (_, _, _, iinfo)
  | Bload (_, _, _, _, _, iinfo)
  | Bstore (_, _, _, _, iinfo)
  | Bcond (_, _, _, _, iinfo) ->
      iinfo.iinfo_shadow.inumb
  | Bseq (ib1, _) -> get_inumb skip_goto ib1
  | _ -> failwith "get_inumb: Bnop None, invalid iblock"

let get_inumb_succ btl skip_goto s = get_inumb skip_goto (pget s btl).entry

let rec successors_instr_btl = function
  | Bseq (ib1, ib2) | Bcond (_, _, ib1, ib2, _) ->
      successors_instr_btl ib1 @ successors_instr_btl ib2
  | BF (Bgoto s, _)
  | BF (Bcall (_, _, _, _, s), _)
  | BF (Bbuiltin (_, _, _, s), _) ->
      [ s ]
  | BF (Bjumptable (_, tbl), _) -> tbl
  | _ -> []

let successors_block ibf =
  let ib = ibf.entry in
  successors_instr_btl ib

let change_final_successor_btl old_s new_s = function
  | BF (Bgoto _, iinfo) -> BF (Bgoto new_s, iinfo)
  | BF (Bcall (s, rind, lr, dst, _), iinfo) ->
      BF (Bcall (s, rind, lr, dst, new_s), iinfo)
  | BF (Bbuiltin (ef, blr, bdst, _), iinfo) ->
      BF (Bbuiltin (ef, blr, bdst, new_s), iinfo)
  | BF (Bjumptable (arg, tbl), iinfo) ->
      let tbl' = List.map (fun s -> if s = old_s then new_s else s) tbl in
      BF (Bjumptable (arg, tbl'), iinfo)
  | _ -> failwith "change_successor_btl: no successors to change"

(** Form a list containing both sources and destination regs of a block *)
let get_regindent = function Coq_inr _ -> [] | Coq_inl r -> [ r ]

let rec get_regs_ib = function
  | Bnop _ -> []
  | Bop (_, args, dest, _) -> dest :: args
  | Bload (_, _, _, args, dest, _) -> dest :: args
  | Bstore (_, _, args, src, _) -> src :: args
  | Bcond (_, args, ib1, ib2, _) -> get_regs_ib ib1 @ get_regs_ib ib2 @ args
  | Bseq (ib1, ib2) -> get_regs_ib ib1 @ get_regs_ib ib2
  | BF (Breturn (Some r), _) -> [ r ]
  | BF (Bcall (_, t, args, dest, _), _) -> dest :: (get_regindent t @ args)
  | BF (Btailcall (_, t, args), _) -> get_regindent t @ args
  | BF (Bbuiltin (_, args, dest, _), _) ->
      AST.params_of_builtin_res dest @ AST.params_of_builtin_args args
  | BF (Bjumptable (arg, _), _) -> [ arg ]
  | _ -> []

let find_last_reg_btl elts =
  reg := 1;
  let rec find_last_reg_btl_rec = function
    | [] -> ()
    | (pc, ibf) :: k ->
        traverse_list reg (get_regs_ib ibf.entry);
        find_last_reg_btl_rec k in
  find_last_reg_btl_rec elts

let plist2IS l = OrdIS.of_list (List.map p2i l)

let rec flatten_iblock_aux ib (k : iblock -> iblock) =
  match ib with
  | Bseq (ib1, ib2) -> flatten_iblock_aux ib1 (fun ib -> Bseq (ib, flatten_iblock_aux ib2 k))
  | _ -> k ib

let flatten_iblock ib = flatten_iblock_aux ib (fun ib -> ib)

(** Invariants map related functions *)

let mk_ir _force_input _regof = { force_input = _force_input; regof = _regof }
let empty_csasv () = { aseq = []; outputs = Regset.empty }

let get_inv_in_gm_or_def n gm =
  match PTree.get n gm with
  | Some inv -> inv
  | None -> { history = empty_csasv (); glue = empty_csasv (); meminv = [] }

let add_outputs_in_gm n gm outputs inv_type =
  let inv = get_inv_in_gm_or_def n gm in
  let inv =
    match inv_type with
    | Gluing ->
        { inv with
          glue =
            {
              aseq = inv.glue.aseq;
              outputs = Regset.union inv.glue.outputs outputs;
            }
        }
    | History ->
        { inv with
          history =
            {
              aseq = inv.history.aseq;
              outputs = Regset.union inv.history.outputs outputs;
            };
        }
  in
  PTree.set n inv gm

let add_aseq_in_gm n gm dst iv inv_type =
  let inv = get_inv_in_gm_or_def n gm in
  let inv =
    match inv_type with
    | Gluing ->
        { inv with
          glue =
            { aseq = (dst, iv) :: inv.glue.aseq; outputs = inv.glue.outputs };
        }
    | History ->
        { inv with
          history =
            {
              aseq = (dst, iv) :: inv.history.aseq;
              outputs = inv.history.outputs;
            };
        }
  in
  PTree.set n inv gm

let build_liveness_invariants elts gm =
  let gm = ref gm in
  List.iter
    (fun (n, ibf) -> gm := add_outputs_in_gm n !gm ibf.binfo.input_regs Gluing)
    elts;
  !gm

(* Dependencies of BTL instructions *)

(* returns the lists of registers read and written by the top level BTL instruction *)
let instr_all_deps : BTL.iblock -> reg list * reg list =
  function
  | Bop (_, args, dst, _) | Bload (_, _, _, args, dst, _) -> (args, [dst])
  | Bstore (_, _, args, src, _) -> (src :: args, [])
  | Bnop _ | Bseq _ -> ([], [])
  | Bcond (_, args, _, _, _) -> (args, [])
  | BF (fin, _) ->
      match fin with
      | Bgoto _ -> ([], [])
      | Breturn res ->
           ((match res with None -> [] | Some res -> [res]), [])
      | Bcall (_, fn, args, dst, _) ->
           ((match fn with Coq_inl r -> r :: args | Coq_inr _ -> args), [dst])
      | Btailcall (_, fn, args) ->
           ((match fn with Coq_inl r -> r :: args | Coq_inr _ -> args), [])
      | Bbuiltin (_, args, dst, _) ->
           (AST.params_of_builtin_args args, AST.params_of_builtin_res dst)
      | Bjumptable (arg, _) -> ([arg], [])

(* return [n] such that all register strictly greater than [n] are unused *)
let btl_max_reg (btl : BTL.code) (params : reg list) : int =
  let ret = ref 0 in
  let acc (r : reg) = ret := max !ret (P.to_int r) in
  let rec iter_iblock (ib : iblock) =
    let src, dst = instr_all_deps ib in
    List.iter acc src; List.iter acc dst;
    match ib with 
    | Bseq (ib0, ib1) | Bcond (_, _, ib0, ib1, _) ->
        iter_iblock ib0; iter_iblock ib1
    | _ -> ()
  in
  List.iter acc params;
  PTree.fold (fun () _ ib -> iter_iblock ib.entry) btl ();
  !ret

(* returns the list of defined and used registers for the last instruction of a btl block *)
let get_olast_dest_args = function
  | Some (Bseq (Bcond (_, args, _, _, _), _)) -> [], args 
  | Some (BF (Breturn (Some arg), _))
  | Some (BF (Bjumptable (arg, _), _)) -> [], [ arg ]
  | Some (BF (Btailcall (_, func, args), _)) ->
      [], (match func with
       | Coq_inl a -> a :: args
       | Coq_inr b -> args)
  | Some (BF (Bcall (_, func, args, dest, _), _)) ->
      [ dest ], (match func with
       | Coq_inl a -> a :: args
       | Coq_inr b -> args)
  | Some (BF (Bbuiltin (_, args, dest, _), _)) ->
      (params_of_builtin_res dest), (params_of_builtin_args args)
  | _ -> [], []
