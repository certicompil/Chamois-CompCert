(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Benjamin Bonneau   ENS, PSL                       *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

open BTL_Invariants
open BTL
open AST
open Op
open Integers
open Maps
open Registers
open Camlcoq
open BTLcommonaux

let rec btl_seq (ibs : iblock list) : iblock =
    match ibs with
    | []   -> Bnop None
    | [ib] -> ib
    | ib :: ibs -> Bseq (ib, btl_seq ibs)

let generate_code : BTL.code -> (int * iblock list) list -> BTL.code =
  List.fold_left (fun c (n, ib) ->
    let n = P.of_int n in
    match PTree.get n c with
    | Some { entry=_; binfo} -> PTree.set n {entry=btl_seq ib; binfo} c
    | None -> failwith "generate_code")

let generate_gm : (int * int list * imem) list -> BTL_Invariants.gluemap =
  List.fold_left (fun gm (n, live, meminv) ->
      PTree.set (P.of_int n) {
        history = csi_empty;
        glue    = {
            aseq    = [];
            outputs = List.fold_left (fun rs r -> Regset.add (P.of_int r) rs) Regset.empty live
        };
        meminv
      } gm
  ) PTree.empty

module Abbrev = struct
  let p = P.of_int
  let i = Z.of_sint
  let f = def_iinfo
  let goto n = BF (Bgoto (p n), f ())
  let synth n0 n1 = (n0, [Bnop (Some (f ())); goto n1])
  let istore dst src =
    Coq_mk_istore (Mint32, Aindexed (i 0), [p dst], {store_num = p 1; store_aaddr = None}, p src)
end

(* test0 *)

(*
void test0(unsigned n, unsigned *p) {
	for (unsigned i = 0; i < n; ++i)
		*p = i;
}
---- unrolled into: ----
void test0(unsigned n, unsigned *p) {
    unsigned i = 0;
    if (n > 0) {
        *p = i;
        i  = 1;
        while (i < n) {
            *p = i;
            ++i;
        }
    }
}
*)

(* source program:
     x1 = p
     x2 = n
     x3 = i

B18 [1 2]
  18: Bop: x3 = Ointconst(0)
  17: Bcond: (x2 <=u 0)
  ifso = [
    3: Bgoto: 2
  ]
  ifnot = [
    ??: Bnop None
  ]
  16: Bgoto: 15

B15 [1 2 3]
  15: Bstore: int32[x1 + 0] = x3
  14: Bop: x3 = Ointconst(1)
  13: Bgoto: 12

B12 [1 2 3]
  -1: Bnop
  12: Bgoto: 11

B11 [1 2 3]
  11: Bcond: (x3 >=u x2)
  ifso = [
    5: Bgoto: 4
  ]
  ifnot = [
    ??: Bnop None
  ]
  10: Bgoto: 9

B9 [1 2 3]
  9: Bstore: int32[x1 + 0] = x3
  8: Bop: x3 = x3 + 1
  7: Bgoto: 6

B6 [1 2 3]
  -1: Bnop
  6: Bgoto: 11

B4 []
  -1: Bnop
  4: Bgoto: 1

B2 []
  -1: Bnop
  2: Bgoto: 1

B1 []
  1: Breturn

*)

let test0_src = Abbrev.([
  18, [Bop (Ointconst (i 0), [], p 3, f ());
       Bcond (Ccompuimm (Cle, i 0), [p 2],
         goto 2,
         Bnop None,
       f ());
       goto 15];
  15, [Bstore (Mint32, Aindexed (i 0), [p 1], p 3, f ());
       Bop (Ointconst (i 1), [], p 3, f ());
       goto 12];
  synth 12 11;
  11, [Bcond (Ccompu Cge, [p 3; p 2],
         goto 4,
         Bnop None,
       f ());
       goto 9];
  9,  [Bstore (Mint32, Aindexed (i 0), [p 1], p 3, f ());
       Bop (Oaddimm (i 1), [p 3], p 3, f ());
       goto 6];
  synth 6 11;
  synth 4 1;
  synth 2 1;
  1,  [BF (Breturn None, f ())]
])

(* x4 = h *)
let test0_trg = Abbrev.([
  18, [Bop (Ointconst (i 0), [], p 3, f ());
       Bcond (Ccompuimm (Cle, i 0), [p 2],
         goto 2,
         Bnop None,
       f ());
       goto 15];
  15, [Bop (Omove, [p 3], p 4, f ());
       Bop (Ointconst (i 1), [], p 3, f ());
       goto 12];
  synth 12 11;
  11, [Bcond (Ccompu Cge, [p 3; p 2],
         goto 4,
         Bnop None,
       f ());
       goto 9];
  9,  [Bop (Omove, [p 3], p 4, f ());
       Bop (Oaddimm (i 1), [p 3], p 3, f ());
       goto 6];
  synth 6 11;
  4,  [Bstore (Mint32, Aindexed (i 0), [p 1], p 4, f ());
       goto 1];
  synth 2 1;
  1,  [BF (Breturn None, f ())]
])

let test0_gm = Abbrev.(
  let mi : imem = [istore 1 4] in
[
  18, [1; 2],    [];
  15, [1; 2; 3], [];
  12, [1; 2; 3], mi;
  11, [1; 2; 3], mi;
  9,  [1; 2; 3], mi;
  6,  [1; 2; 3], mi;
  4,  [],        mi;
  2,  [],        [];
  1,  [],        [];
])

(* test1 *)

(*
void test1(unsigned n, unsigned *p0, unsigned *p1) {
	for (unsigned i = 0; i < n; ++i) {
		*p0 = i;
		*p1 = i;
	}
}
---- unrolled into: ----
void test1(unsigned n, unsigned *p0, unsigned *p1) {
    unsigned i = 0;
    if (n > 0) {
        *p0 = i;
        *p1 = i;
        i   = 1;
        while (i < n) {
            *p0 = i;
            *p1 = i;
            ++i;
        }
    }
}
*)

(* source program:
    x1 = p1
    x2 = p0
    x3 = n
    x4 = 1

B20 [1 2 3]
  20: Bop: x4 = Ointconst(0)
  19: Bcond: (x3 <=u 0)
  ifso = [
    3: Bgoto: 2
  ]
  ifnot = [
    ??: Bnop None
  ]
  18: Bgoto: 17

B17 [1 2 3 4]
  17: Bstore: int32[x2 + 0] = x4
  16: Bstore: int32[x1 + 0] = x4
  15: Bop: x4 = Ointconst(1)
  14: Bgoto: 13

B13 [1 2 3 4]
  -1: Bnop
  13: Bgoto: 12

B12 [1 2 3 4]
  12: Bcond: (x4 >=u x3)
  ifso = [
    5: Bgoto: 4
  ]
  ifnot = [
    ??: Bnop None
  ]
  11: Bgoto: 10

B10 [1 2 3 4]
  10: Bstore: int32[x2 + 0] = x4
  9: Bstore: int32[x1 + 0] = x4
  8: Bop: x4 = x4 + 1
  7: Bgoto: 6

B6 [1 2 3 4]
  -1: Bnop
  6: Bgoto: 12

B4 []
  -1: Bnop
  4: Bgoto: 1

B2 []
  -1: Bnop
  2: Bgoto: 1

B1 []
  1: Breturn
*)

let test1_src = Abbrev.([
  20, [Bop (Ointconst (i 0), [], p 4, f ());
       Bcond (Ccompuimm (Cle, i 0), [p 3],
         goto 2,
         Bnop None,
       f ());
       goto 17];
  17, [Bstore (Mint32, Aindexed (i 0), [p 2], p 4, f ());
       Bstore (Mint32, Aindexed (i 0), [p 1], p 4, f ());
       Bop (Ointconst (i 1), [], p 4, f ());
       goto 13];
  synth 13 12;
  12, [Bcond (Ccompu Cge, [p 4; p 3],
         goto 4,
         Bnop None,
       f ());
       goto 10];
  10, [Bstore (Mint32, Aindexed (i 0), [p 2], p 4, f ());
       Bstore (Mint32, Aindexed (i 0), [p 1], p 4, f ());
       Bop (Oaddimm (i 1), [p 4], p 4, f ());
       goto 6];
  synth 6 12;
  synth 4 1;
  synth 2 1;
  1,  [BF (Breturn None, f ())]
])

(* x5 = h for p0 and p1 *)
let test1_trg = Abbrev.([
  20, [Bop (Ointconst (i 0), [], p 4, f ());
       Bcond (Ccompuimm (Cle, i 0), [p 3],
         goto 2,
         Bnop None,
       f ());
       goto 17];
  17, [Bop (Omove, [p 4], p 5, f ());
       Bop (Ointconst (i 1), [], p 4, f ());
       goto 13];
  synth 13 12;
  12, [Bcond (Ccompu Cge, [p 4; p 3],
         goto 4,
         Bnop None,
       f ());
       goto 10];
  10, [Bop (Omove, [p 4], p 5, f ());
       Bop (Oaddimm (i 1), [p 4], p 4, f ());
       goto 6];
  synth 6 12;
  4,  [Bstore (Mint32, Aindexed (i 0), [p 2], p 5, f ());
       Bstore (Mint32, Aindexed (i 0), [p 1], p 5, f ());
       goto 1];
  synth 2 1;
  1,  [BF (Breturn None, f ())]
])

let test1_gm = Abbrev.(
  let mi : imem = [istore 2 5; istore 1 5] in
[
  20, [1; 2; 3],    [];
  17, [1; 2; 3; 4], [];
  13, [1; 2; 3; 4], mi;
  12, [1; 2; 3; 4], mi;
  10, [1; 2; 3; 4], mi;
  6,  [1; 2; 3; 4], mi;
  4,  [],           mi;
  2,  [],           [];
  1,  [],           [];
])

(* test2 *)

(*
void test2(int *p) {
	*p = 0;
	*p = 1;
}
*)

(* source program:
    x1 = p

B5 [1]
  5: Bop: x4 = Ointconst(0)
  4: Bstore: int32[x1 + 0] = x4
  3: Bop: x3 = Ointconst(1)
  2: Bstore: int32[x1 + 0] = x3
  1: Breturn
*)

let test2_src = Abbrev.([
  5,  [Bop (Ointconst (i 0), [], p 4, f ());
       Bstore (Mint32, Aindexed (i 0), [p 1], p 4, f ());
       Bop (Ointconst (i 1), [], p 3, f ());
       Bstore (Mint32, Aindexed (i 0), [p 1], p 3, f ());
       BF (Breturn None, f ())]
])

(* NOTE: if the two stores were in different blocks, we would have to add a dead auxiliary variable
         for the invariants. *)
let test2_trg = Abbrev.([
  5,  [Bop (Ointconst (i 1), [], p 3, f ());
       Bstore (Mint32, Aindexed (i 0), [p 1], p 3, f ());
       BF (Breturn None, f ())]
])

let test2_gm = [5, [1], []]

(* test3 *)

(*
void test3(int b, int *p) {
	*p = 0;
	if (b) *p = 1;
}
*)

(* source program:
    x1 = p
    x2 = b

B11 [1 2]
  11: Bop: x5 = Ointconst(0)
  10: Bstore: int32[x1 + 0] = x5
  9: Bcond: (x2 !=u 0)
  ifso = [
    6: Bgoto: 5
  ]
  ifnot = [
    ??: Bnop None
  ]
  8: Bgoto: 7

B7 []
  -1: Bnop
  7: Bgoto: 1

B5 [1]
  5: Bop: x4 = Ointconst(1)
  4: Bstore: int32[x1 + 0] = x4
  3: Bgoto: 2

B2 []
  -1: Bnop
  2: Bgoto: 1

B1 []
  1: Breturn
*)

let test3_src = Abbrev.([
  11, [Bop (Ointconst (i 0), [], p 5, f ());
       Bstore (Mint32, Aindexed (i 0), [p 1], p 5, f ());
       Bcond (Ccompuimm (Cne, i 0), [p 2],
         goto 5,
         Bnop None,
       f ());
       goto 7];
  synth 7 1;
  5,  [Bop (Ointconst (i 1), [], p 4, f ());
       Bstore (Mint32, Aindexed (i 0), [p 1], p 4, f ());
       goto 2];
  synth 2 1;
  1,  [BF (Breturn None, f ())]
])

let test3_trg = Abbrev.([
  11, [Bop (Ointconst (i 0), [], p 5, f ()); (* Cannot be moved in block 7 *)
       Bcond (Ccompuimm (Cne, i 0), [p 2],
         goto 5,
         Bnop None,
       f ());
       goto 7];
  7,  [Bstore (Mint32, Aindexed (i 0), [p 1], p 5, f ());
       goto 1];
  5,  [Bop (Ointconst (i 1), [], p 4, f ());
       Bstore (Mint32, Aindexed (i 0), [p 1], p 4, f ());
       goto 2];
  synth 2 1;
  1,  [BF (Breturn None, f ())]
])

let test3_gm = Abbrev.(
  let mi : imem = [istore 1 5] in
[
  11, [1; 2], [];
  7,  [],     mi;
  5,  [1],    mi;
  2,  [],     [];
  1,  [],     [];
])
