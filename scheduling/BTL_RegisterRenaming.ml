(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Alexandre Berard   UGA, VERIMAG                   *)
(*           Léo Gourdin        UGA, VERIMAG                   *)
(*           Benjamin Bonneau   UGA, VERIMAG                   *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

(** BTL register renaming oracle, to be used with if-lifting *)

open Maps
open RTLcommonaux
open BTL
open Registers
open Op
open Datatypes
open AST
open BTLtypes
open BTL_Renumber
open Camlcoq

module IM = Map.Make (Int)
module RM = Map.Make (P)

(* gives a mapping of the form inumb -> reg * reg, where the renaming changes
 * the first reg into the second reg *)
let ssa_renaming_map n btl =
  let new_dest dest m p inumb =
    match RM.find_opt dest m with
    | Some r ->
        let freg = r2pi () in
        RM.add dest freg m, IM.add inumb (dest, freg) p
    | None ->
        RM.add dest dest m, IM.add inumb (dest, dest) p in
  let rec map_dest ib m p =
    let map_dest_f = (fun ib1 ib2 m p ->
      match !Clflags.option_reg_renaming_opt with
      | "forward"  ->
        let m, p = map_dest ib1 m p in
        map_dest ib2 m p
      | "backward" ->
        let m, p = map_dest ib2 m p in
        map_dest ib1 m p
      | s -> failwith ("ssa_renaming_map: unknown reg-renaming option: " ^ s)) in
    match ib with
    | Bseq (ib1, ib2) ->
        map_dest_f ib1 ib2 m p
    | Bcond (cond, args, ifso, ifnot, iinfo) ->
        map_dest_f ifso ifnot m p
    | Bop (op, args, dest, iinfo) ->
        new_dest dest m p iinfo.iinfo_shadow.inumb
    | Bload (trap, chunk, addr, args, dest, iinfo) ->
        new_dest dest m p iinfo.iinfo_shadow.inumb
    | _ -> m, p in
  recompute_inumbs btl n;
  let ibf = get_some @@ PTree.get n btl in
  snd (map_dest ibf.entry RM.empty IM.empty)

(* applies the register map given onto btl node n
 * note: this could give incorrect code that needs to be fixed with restore_blocks *)
let apply_renaming_map n btl ren_map =
  let get_dest inumb p =
    match IM.find_opt inumb p with
    | Some (_, rt) -> rt
    | _ ->
        DebugPrint.debug "access: %d\n" inumb;
        failwith "apply_renaming_map: rename map incorrect" in
  let get_reg r m =
    match RM.find_opt r m with
    | Some r' -> r'
    | None -> r in
  let change_args l m = List.fold_right (fun r l -> (get_reg r m)::l) l [] in
  let change_ren inumb m p =
    match IM.find_opt inumb p with
    | Some (f, t) -> RM.add f t m
    | None -> m in
  let rec ren ib m p =
    match ib with
    | Bseq (ib1, ib2) ->
        let ib1', m = ren ib1 m p in
        let ib2', m = ren ib2 m p in
        Bseq (ib1', ib2'), m
    | Bop (op, args, dest, iinfo) ->
        let args' = change_args args m in
        let dest' = get_dest iinfo.iinfo_shadow.inumb p in
        let m = change_ren iinfo.iinfo_shadow.inumb m p in
        Bop (op, args', dest', iinfo), m
    | Bload (trap, chunk, addr, args, dest, iinfo) ->
        let args' = change_args args m in
        let dest' = get_dest iinfo.iinfo_shadow.inumb p in
        let m = change_ren iinfo.iinfo_shadow.inumb m p in
        Bload (trap, chunk, addr, args', dest', iinfo), m
    | Bstore (chunk, addr, args, src, iinfo) ->
        let args' = change_args args m in
        Bstore (chunk, addr, args', (get_reg src m), iinfo), m
    | Bcond (cond, args, ifso, ifnot, iinfo) ->
        let args' = change_args args m in
        let ifso', m = ren ifso m p in
        let ifnot', m = ren ifnot m p in
        Bcond (cond, args', ifso', ifnot', iinfo), m
    | BF (Bcall (si, fn, args, dest, succ), iinfo) ->
        let args' = change_args args m in
        let fn' =
          match fn with
          | Coq_inl r -> Coq_inl (get_reg r m)
          | _ -> fn in
        BF (Bcall (si, fn', args', dest, succ), iinfo), m
    | BF (Btailcall (si, fn, args), iinfo) ->
        let args' = change_args args m in
        let fn' =
          match fn with
          | Coq_inl r -> Coq_inl (get_reg r m)
          | _ -> fn in
        BF (Btailcall (si, fn', args'), iinfo), m
    | BF (Bjumptable (arg, tbl), iinfo) ->
        let arg' = get_reg arg m in
        BF (Bjumptable (arg', tbl), iinfo), m
    | _ -> ib, m in
  let ibf = get_some @@ PTree.get n btl in
  let ib' = fst (ren ibf.entry RM.empty ren_map) in
  let ibf' = { entry = ib'; binfo = ibf.binfo } in
  PTree.set n ibf' btl

let restore_blocks n btl ren_map =
  let change_ren inumb m p =
    match IM.find_opt inumb p with
    | Some (f, t) -> RM.add f t m
    | None -> m in
  let get_reg r m =
    match RM.find_opt r m with
    | Some r' -> r'
    | None -> r in
  (* restoring registers before exit *)
  let restore_block live_list ib m =
    let rec traverse_live live_list =
      match live_list with
      | l :: next ->
          let l' = get_reg l m in
          if l' = l then
            traverse_live next
          else 
            Bseq (Bop (Omove, l'::[], l, BTLcommonaux.def_iinfo ()), (traverse_live next))
      | [] -> ib in
    traverse_live live_list in
  let rec restore ib m =
    match ib with
    | Bseq (ib1, ib2) ->
        let ib1', m = restore ib1 m in
        let ib2', m = restore ib2 m in
        Bseq(ib1', ib2'), m
    | Bop (op, args, dest, iinfo) ->
        let m = change_ren iinfo.iinfo_shadow.inumb m ren_map in
        Bop (op, args, dest, iinfo), m
    | Bload (trap, chunk, addr, args, dest, iinfo) ->
        let m = change_ren iinfo.iinfo_shadow.inumb m ren_map in
        Bload (trap, chunk, addr, args, dest, iinfo), m
    | Bcond (cond, args, ifso, ifnot, iinfo) ->
        let ifso', m = restore ifso m in
        let ifnot', m = restore ifnot m in
        Bcond (cond, args, ifso', ifnot', iinfo), m
    | BF (Bgoto succ, _) ->
        let ibf_go = get_some @@ PTree.get succ btl in
        restore_block (Regset.elements ibf_go.binfo.input_regs) ib m, m
    | BF (Breturn res, _) ->
        (match res with
        | Some reg -> restore_block (reg::[]) ib m, m
        | None -> ib, m)
    | BF (Bcall (si, fn, args, dest, succ), iinfo) ->
        let ibf_go = get_some @@ PTree.get succ btl in
        let lives = Regset.elements (Regset.remove dest ibf_go.binfo.input_regs) in
        let fn' =
          match fn with
          | Coq_inl r -> Coq_inl (get_reg r m)
          | _ -> fn in
        let ib' = BF (Bcall (si, fn', args, dest, succ), iinfo) in
        restore_block lives ib' m, m
    | BF (Btailcall (si, fn, args), iinfo) ->
        let fn' =
          match fn with
          | Coq_inl r -> Coq_inl (get_reg r m)
          | _ -> fn in
        BF (Btailcall (si, fn', args), iinfo), m
    | BF (Bjumptable (arg, tbl), iinfo) ->
        let arg' = get_reg arg m in
        let rec tbl_union tbl =
          match tbl with
          | succ::next ->
              let ibf_go = get_some @@ PTree.get succ btl in
              Regset.union ibf_go.binfo.input_regs (tbl_union next)
          | [] -> Regset.empty in
        let lives = Regset.elements (tbl_union tbl) in
        let ib' = BF (Bjumptable (arg', tbl), iinfo) in
        restore_block lives ib' m, m
    | BF (Bbuiltin (ef, args, dest, succ), iinfo) ->
        let ibf_go = get_some @@ PTree.get succ btl in
        let builtin_regs = List.fold_left (fun s e -> Regset.add e s) Regset.empty (params_of_builtin_args args) in
        let lives = Regset.elements (Regset.union ibf_go.binfo.input_regs builtin_regs) in
        let ib' = BF (Bbuiltin (ef, args, dest, succ), iinfo) in
        restore_block lives ib' m, m
    | _ -> ib, m in
  let ibf = get_some @@ PTree.get n btl in
  let ib', _ = restore ibf.entry RM.empty in
  let ibf' = { entry = ib'; binfo = ibf.binfo } in
  PTree.set n ibf' btl

let get_freg n btl =
  if not !Clflags.option_reg_renaming then btl, IM.empty else
  let ren_map = ssa_renaming_map n btl in
  let btl' = apply_renaming_map n btl ren_map in
  let btl' = restore_blocks n btl' ren_map in
  btl', ren_map
