module Strategy = struct
  type t = bool array -> int list
  (* let random_order (a:int array) = Array.map (fun _ -> Random.int 1000) a

  let reverse_order = Array.map (fun x -> -x) *)

  let reverse f = (fun filter -> List.rev (f filter))

  let random = (
    fun filter -> 
      List.map (fst) @@
      List.sort (fun (_,a) (_,b) -> b - a) @@
      List.map (fun i -> (i,Random.int 1000)) @@ 
      List.filter_map (fun a->a) @@ 
      Array.to_list @@ 
      Array.mapi (fun i b -> if b then Some i else None) filter
  )

  let from_order order = 
     fun filter -> 
        List.sort (fun a b -> order.(b) - order.(a)) @@
        List.filter (fun i -> filter.(i)) @@ 
        List.init (Array.length filter) (fun i -> i)

  let _switch2 (s:t) = (fun ba -> 
     match s ba with
     |e1::e2::tl -> e2::e1::tl
     |v -> v
  ) 
end