open InstructionSchedulerTypes
open BNB_heuristic

module type T = sig 
    type t

    (**
        choose : critical path first by default
        preprocess : ISORconfig by default
    *)
    val init : ?choose:Strategy.t -> problem -> int array -> t
    (* val compute_CP : t -> int array *)

    (**
        return the schedule. Compute only if alreay computed
    *)
    val compute_schedule : t -> PreprocessProblem.unprocessed_solution

    (**
        return a lower and upper bound on the optimal makespan.
        The upper bound is equal to the schedule makespan.
        Compute only if alreay computed
    *)
    val compute_bounds : t -> int * int

    (**
        return the maximum (?) pression of the scheduling.
        Compute only if alreay computed
    *)
    val compute_regpress : t -> int array


    (*
        return the schedule in the correct format. Compute only if alreay computed
    *)
    (* val compute : t -> solution option *)

end

module Make(_:BNB_Config.T) : T
