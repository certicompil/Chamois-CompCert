(**
    some functions for Directed Acyclic Graphs
*)

(*src dst cost*)
type t = ((int * int * int) list * int)

let rec longest_path_with_memory dag memory i j =
  match memory.(i).(j) with 
    |Some o -> o
    |None ->
    let exiting_i = List.filter (fun (src,_,_) -> src = i) dag in
    let max_path = List.fold_left ( fun acc (_,dst,cost) ->
      max acc (Option.bind (longest_path_with_memory dag memory dst j) (fun x -> Some (x + cost)))
    ) None exiting_i in
    memory.(i).(j) <- Some max_path;
    max_path

let longest_path ((dag,nb_points):t) (i:int) (j:int) =
  let memory = Array.make_matrix nb_points nb_points None in
  Array.iteri (fun i _ -> memory.(i).(i) <- Some (Some 0)) memory;
  longest_path_with_memory dag memory i j


let test_longest_path () =
  assert ((longest_path ([(0,1,1);(1,2,1);(2,3,1)],4) 0 3) = Some 3);
  assert ((longest_path ([(0,1,1);(1,2,1);(2,3,1)],4) 2 1) = None)

let sort ((dag,nb_points):t) : t =
  (List.sort (Stdlib.compare) dag,nb_points)

let transitive_closure ((dag,nb_points):t) : t = 
  let memory = Array.make_matrix nb_points nb_points None in
  Array.iteri (fun i _ -> memory.(i).(i) <- Some (Some 0)) memory;
  let dag_ref = ref [] in
  Array.iteri (fun i _ -> 
    Array.iteri (fun j _ -> 
      match longest_path_with_memory dag memory i j with
      |Some v when i<>j -> dag_ref := (i,j,v)::!dag_ref
      |_-> ()
    ) memory;
  ) memory;
  (!dag_ref,nb_points)

let test_transitive_closure () =
  assert ((sort @@ transitive_closure ([(0,1,1);(1,2,1);(2,3,1)],4)) = ([(0, 1, 1); (0, 2, 2); (0, 3, 3); (1, 2, 1); (1, 3, 2); (2, 3, 1)], 4));
  assert ((sort @@ transitive_closure ([(0,10,1);(0,20,1);(10,11,2);(11,12,1);(12,1,0);(20,21,1);(21,22,2);(22,1,1)],30)) = (
    [ (0,1,5) ; (0, 10, 1) ;(0, 11, 3); (0, 12, 4); (0, 20, 1); (0, 21, 2); (0, 22, 4) ;
    (10, 1, 3); (10, 11, 2); (10, 12, 3); (11, 1, 1); (11, 12, 1);
    (12, 1, 0); (20, 1, 4); (20, 21, 1); (20, 22, 3); (21, 1, 3); (21, 22, 2);
    (22, 1, 1)
    ], 30));
  let _l = List.init 100 (fun i -> (i,i+1,1)) in
  assert (sort @@ transitive_closure (_l,101) = sort @@ transitive_closure @@ transitive_closure (_l,101))

(*assume tc*)
let minimize ((dag,nb_points):t) : t = 
  let filtered_dag = List.filter (fun (i,j,l) ->
    let exiting_i = List.find_all (fun (i2,j2,_) -> i2=i && j2<>j) dag in
    let entering_j = List.find_all (fun (i2,j2,_) -> i2<>i && j2=j) dag in
    let indirect_path_values = List.filter_map ( fun (_,j1,l1) ->
      match List.find_opt (fun (i2,_,_) -> j1=i2) entering_j with
      | None -> None
      | Some (_,_,l2) -> Some (l1+l2)
    ) exiting_i in
    List.for_all (fun l2 -> l2 < l) indirect_path_values
  ) dag in
  (filtered_dag,nb_points)



let test_minimize () = 
  let _l = List.init 100 (fun i -> (i,i+1,1)) in
  assert (sort @@ minimize @@ transitive_closure (_l,101) = sort @@ (_l,101))

;; 
test_longest_path ();;
test_transitive_closure ();;
test_minimize ();;