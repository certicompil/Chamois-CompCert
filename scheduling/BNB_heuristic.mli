(**
    [f filter] is the list of instruction [l] such that [filter.(i) = true] <=> [i in l].
    The order of those instruction in [l] determine the order in wich they will be explore in the branching algorithm.
*)
module Strategy : sig
    type t = bool array -> int list
    val reverse : t -> t
    val random : t
    val from_order : int array -> t
end