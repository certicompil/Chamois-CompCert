# Quick documentation on BTL (last edit 02/08/24)

BTL (stands for “Block Transfer Language”) aims to be an IR dedicated to defensive certification of middle-end
optimizations (before register allocation). It provides a CFG of “loop-free” blocks, where each block is run in
one step emitting at most a single observational event.
The local optimizations (i.e. preserving “locally” the semantics of such blocks) are checked by symbolic
execution with rewriting rules. Global, intra-procedural optimizations (i.e. preserving “globally” the semantics but not always
“locally”) require invariants annotations at each block entry.

Two kinds of oracles are involved with BTL:
- translation oracles, which require a “dupmap” mapping block entries;
- transformation oracles, which require a "gluemap" of invariants to build synchronization points between blocks.

## Features

BTL can support any loop-free block (i.e. all blocks with a single entrypoint),
however, we limit the number of internal joins in the block to keep the
execution time reasonable. Otherwise, the symbolic execution of DAG with
internal joins can become exponential. To select trees without such joins, a
selection method would be to select the maximal loop-free sequence of code,
such that at most the first node has more than one predecessor (except
eventually on exit points). A more complex idea would be to have a threshold to
control the amount of internal joins. For the moment, in practice, we do not
see any use cases where it would be necessary (and more interesting) to select
blocks larger than superblocks.
Some transformations may however transform basic-blocks to superblocks or superblocks to extended-blocks.

BTL features a notion of gluing and history invariants to establish a relation between the source and target blocks to interpret.
See `BTL_Invariants.v`.

See syntax and semantics in file `scheduling/BTL.v`.
Each block is a sequence of instructions (constructed using `Bseq`) terminated by a final `BF` instruction.
An RTL `Inop pc` ending a block’s branch is encoded by `Bseq Bnop (Bgoto pc)` (the same trick appears
for conditions).
The BTL function embeds a tree containing the symbolic invariants (SI) mapping for each block in the function.

Liveness verification is integrated within the verification of gluing invariants, on the target block.

See the whole file structure [here](https://certicompil.gricad-pages.univ-grenoble-alpes.fr/Chamois-CompCert).

## Supported transformations

### Restructurations

- Synthetic nodes' insertion
- Code factorization
- Block selection

### Optimizations
- Superblock scheduling
- Register renaming
- If-lifting (side-exits)
- Rewrites (instructions and memory)
- Local CSE / DCE
- LCT (Lazy Code Transformations)
- Integers' promotion

### Analyses

- Interval
- Value (absolute)
- Liveness

### Security

TODO
