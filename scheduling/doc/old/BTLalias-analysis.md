# Qq idées en vrac pour incorporer une analyse d'alias (locale à chaque bloc pour l'instant) dans l'exécution symbolique

**Rappel** en RTL (et BTL): les opérations sur la mémoire s'effectuent au
travers [deux instructions `Iload` et `Istore`](https://compcert.org/doc/html/compcert.backend.RTL.html#instruction) pour respectivemet
lire/écrire dans la mémoire vers/depuis un registre.

    Iload (chunk:memory_chunk) (addr:addressing) (args:list reg) (dest:reg): instruction
    Istore (chunk:memory_chunk) (addr:addressing) (args:list reg) (src:reg): instruction

Voir: https://compcert.org/doc/html/compcert.backend.RTL.html

Ci-dessus, `addressing` défini les modes d'addressages propres à chaque archi.

Voir par exemple, celle sur [powerpc](https://compcert.org/doc/html/compcert.powerpc.Op.html#addressing) et celle sur [kvx](https://certicompil.gricad-pages.univ-grenoble-alpes.fr/compcert-kvx/html/compcert.kvx.Op.html#addressing).

Pour l'instant, le modèle mémoire de l'exécution symbolique considère
que chaque `Istore` modifie potentiellement l'ensemble de la mémoire
(cf. ci-dessous).

**Idée** On peut probablement affiner le modèle mémoire de l'exécution
symbolique de BTL pour intégrer une analyse d'alias (ça fait un sujet
de thèse !)

Au lieu de représenter la mémoire symbolique comme une séquence de stores "atomiques"

    smem ::= list ((chunk:memory_chunk), (addr:addressing), (args:list_sval), (srce: sval))

On la représenterait comme une séquence de stores "parallèles" (ie. indépendants/disjoints/commutables).

    smem ::= list (para_store) 

    para_store ::= alloc_class |-> list_sval*address_mode*list(offset_map)        // PTree(list_val * address_mode * list(offset_map)
    
    offset_map ::= offset |-> memory_chunk * sval

Et en se basant sur trois fonctions pour décomposer le `addressing` (dépendantes de l'archi):

    get_alloc_class: addressing -> positive       (* alloc_class = positive *)
    get_address_mode: addressing -> address_mode
    get_offset: addressing -> Z                   (* offset = Z *)
    
où `address_mode` est un type "abstrait" défini pour chaque archi ayant juste une égalité décidable.
L'idée c'est bien sur qu'à partir d'un triplet `(alloc_class, address_mode, offset)`, on sait reconstruire le `addressing` correspondant, 
via une surjection (modulo équivalence).


Les `alloc_class` représentent des classes d'allocations *potentiellement* disjointes de blocks mémoires.
Par convention: on a une classe spéciale pour le "tout-venant" (1), les autres étant considérées comme disjointes.
Autrement dit, si un `para_store` donné contient la classe d'allocation "1", c'est la seule possible. 
Sinon, il contient d'autres classes d'allocations qui sont alors disjointes.
Typiquement, la pile + chaque symbole global donne un ensemble fini de classes qu'on peut distinguer. 
Voir les exemples spécifiques à chaque archi ci-dessous.

On peut avoir l'info précises sur la classe d'allocation en regardant `addressing` **après** avoir appliqué un système de 
règles de réécritures sur le couple `(addressing, list_sval)` spécifiques à chaque archi. Par exemple sur riscV: 

    (Aindexed ofs1, [Sop (Oaddrsymbol id ofs2) []]) -> (Aglobal id ofs1+ofs2, [])
    (Aindexed ofs1, [Sop (Oaddrstack ofs) []]) -> (Ainstack id ofs1+ofs2, [])
    (Aindexed ofs1, [Sop (Oaddimm n) [sv1]]) -> (Aindexed ofs+n) [sv1])   // rem: ici, les réécritures continuent récursivement.

A l'intérieur d'un `alloc_class` donné, on ne sait garantir le
caractère disjoint des accès mémoires que sur un couple `(list_sval,
address_mode)` fixé.  Par contre, on peut distinguer les accès en
fonction de `l'offset` et du `memory_chunk`. On conserve de tels accès
disjoints dans un `offset_map` (dont une "bonne" représentation est
discutée plus bas).  Le `list(offset_map)` prend en compte le fait
qu'on peut avoir des écritures qui se recouvrent (en fonction des
`memory_chunk`): elle garde dans ce cas l'ordre séquentiel des
écritures (est-ce que c'est ça qu'on veut ? 
ou alors, dans le cas de recouvrements, on se 
retrouve avec des Vundefs dans les cases mémoires "clobbered" ?)

### Exemple sur riscV:

    Inductive addressing: Type :=
    | Aindexed: ptrofs -> addressing    (**r Address is [r1 + offset] *)
    | Aglobal: ident -> ptrofs -> addressing  (**r Address is global plus offset *)
    | Ainstack: ptrofs -> addressing.   (**r Address is [stack_pointer + offset] *)

Ici, on peut prendre comme `get_alloc_class`:
   
     Aindexed -> 1
     Ainstack -> 2
     Aglobal(ident) -> 2+ident
     
Le type `address_mode` est juste `unit`. Le `get_offset` retourne les `ptrofs`.

### Exemple sur powerpc:

    Inductive addressing: Type :=
    | Aindexed: int -> addressing            (**r Address is [r1 + offset] *)
    | Aindexed2: addressing                  (**r Address is [r1 + r2] *)
    | Aglobal: ident -> ptrofs -> addressing (**r Address is [symbol + offset] *)
    | Abased: ident -> ptrofs -> addressing  (**r Address is [symbol + offset + r1] *)
    | Ainstack: ptrofs -> addressing.        (**r Address is [stack_pointer + offset] *)

Ici, on prendrait comme `get_alloc_class`:
   
     Ainstack -> 2
     Aglobal(ident),Abased(ident) -> 2+ident
     tout le reste   -> 1

Le `address_mode` permet de distinguer les modes d'adressages qui prennet un registre supplémentaire par rapport à la version de base.

    Inductive address_mode: Type :=
    | Default
    | OneMoreRegister

avec: 

    alloc_class=1, Default  -> Aindexed
    alloc_class=1, OneMoreRegister -> Aindexed2
    alloc_class=2+ident, Default  -> Aglobal
    alloc_class=2+ident, OneMoreRegister  -> Abased

### Exemple sur aarch64:

    Inductive addressing: Type :=
    | Aindexed (ofs: int64)                               (**r Address is [r1 + offset] *)
    | Aindexed2                                           (**r Address is [r1 + r2] *)
    | Aindexed2shift (a: amount64)                        (**r Address is [r1 + r2 << a] *)
    | Aindexed2ext (x: extension) (a: amount64)           (**r Address is [r1 + sign-or-zero-ext(r2) << a] *)
    | Aglobal (id: ident) (ofs: ptrofs)                   (**r Address is [global + offset] *)
    | Ainstack (ofs: ptrofs).                             (**r Address is [stack_pointer + offset] *)
     
Ici, on prendrait comme `get_alloc_class`:
   
     Ainstack -> 2
     Aglobal(ident) -> 2+ident
     tout le reste   -> 1

Ci-dessous, on identifie `Aindexed2` avec `(Aindexed2shift 0)`:

    Inductive address_mode: Type :=
    | Mindexed2shift (a: amount64)
    | Mindexed2ext (x: extension) (a: amount64) 
    | Mother

Bon, ici l'analyse d'alias ne prend pas en compte de manière très fine l'info `amount64`. 
**Peut-on faire autrement sans trop compliquer ?** En particulier, si r2=0, alors quelque soit l'amount, on obtient toujours la même adresse. 
Il semble donc difficile de discriminer en fonction de l'amount dans le cas général.

### Exemple sur kvx:

    Inductive addressing: Type :=
    | Aindexed2XS (scale : Z) : addressing (**r Address is [r1 + r2 << scale] *)
    | Aindexed2 : addressing (**r Address is [r1 + r2] *)
    | Aindexed: ptrofs -> addressing    (**r Address is [r1 + offset] *)
    | Aglobal: ident -> ptrofs -> addressing  (**r Address is global plus offset *)
    | Ainstack: ptrofs -> addressing.   (**r Address is [stack_pointer + offset] *)

Ici, on prendrait comme `get_alloc_class`:
   
     Ainstack -> 2
     Aglobal(ident) -> 2+ident
     tout le reste   -> 1

Le `address_mode` (On identifie `Aindexed2` à `(Aindexed2XS 0)`). Même pb avec le "scale" que sur aarch64 pour les histoires de "amount".

    Inductive address_mode: Type :=
    | Mindexed2XS (of scale: Z) 
    | Mother

    
### Exemple sur x86:

    Inductive addressing: Type :=
    | Aindexed: Z -> addressing       (**r Address is [r1 + offset] *)
    | Aindexed2: Z -> addressing      (**r Address is [r1 + r2 + offset] *)
    | Ascaled: Z -> Z -> addressing   (**r Address is [r1 * scale + offset] *)
    | Aindexed2scaled: Z -> Z -> addressing
                                    (**r Address is [r1 + r2 * scale + offset] *)
    | Aglobal: ident -> ptrofs -> addressing (**r Address is [symbol + offset] *)
    | Abased: ident -> ptrofs -> addressing  (**r Address is [symbol + offset + r1] *)
    | Abasedscaled: Z -> ident -> ptrofs -> addressing  (**r Address is [symbol + offset + r1 * scale] *)
    | Ainstack: ptrofs -> addressing. (**r Address is [stack_pointer + offset] *)

Ici, on prendrait comme `get_alloc_class`:
   
     Ainstack -> 2
     Aglobal(ident),Abased(ident),Abasedscaled(ident) -> 2+ident
     tout le reste   -> 1

Le `address_mode` indexé par le scale (on prend `scale = 1` pour les versions sans scale explicite).

    Inductive address_mode: Type :=
    | Default (scale:Z)
    | OneMoreRegister (scale:Z)

### Discussion sur la représentation des `offset_map`

Plusieurs difficultés à gérer:

* recouvrements d'offsets sur des chunks de tailles differentes (les écritures se chevauchent).
* la donnée accédée change de "valeur" si elle est lue avec un chunk
  différent de celui avec lequel elle a été écrite, [même sur des chunk de même taille](https://compcert.org/doc/html/compcert.common.Memdata.html#decode_encode_val).
  Ça ne concerne que l'opération de "load".
  
En prenant en compte [les contraintes d'alignement](https://compcert.org/doc/html/compcert.common.Memdata.html#align_chunk)
et de [taille](https://compcert.org/doc/html/compcert.common.Memdata.html#size_chunk),
on peut décider simplement des recouvrements possibles. Par exemple,
en 64 bits, une écriture `Mfloat64` (donc sur 8 octets) à l'adresse 28
recouvre un autre chunk de taille 8 aux adresses 32 et 24. 
Donc les adresses impactées par `Mfloat64` à l'adresse 28: tous les octets entre les adresses 24 et 35 incluses.
Un `Mint8signed` (sur 1 octet) sur une adresse incluses entre 28 et 31 lui impacte les adresses entre 24 et 35 incluses (en 64 bits).

Voilà une idée de structure de données (ici en `ocaml`) pour les
`offset_map` dans lesquels les offsets sont des entiers signés non
bornés.  En un seul parcours de l'arbre, on peut extraire un
sous-ensemble dont les offsets sont inclus dans un certain range
(opération `get_range`). On pourrait "facilement" aussi retourner le
complémentaire (qui ferait donc l'opération d'effacer un tel sous
ensemble) et aussi coder l'union de deux `offset_map`. 


    (** Our representation of relative offsets into naturals is based on a simple translation: 
        given "MIN" the minimal offsets, number [n] is represented as [n-MIN].
        The interest of this representation: on a fixed number of bits, the usual order is the lexicographic order !

    On -5 bits (including the sign bit):

            -16  ->  00000
             ...
             -4  ->  01100
             -3  ->  01101
             -2  ->  01110
             -1  ->  01111
              0  ->  10000
              1  ->  10001
              2  ->  10010
              3  ->  10011
              4  ->  10100
             ...
              8  ->  11000
             ...
             15  ->  11111

    Remark that this is the usual two's complement representation excepted on the first bit, which is inverted.
    Indeed, MIN = -2^n (for n+1 bits). Thus adding MIN simply inverts the first bit.

    In short:
    - the sign bit is negated w.r.t the usual two's complement representation.
    - the possible sign extension (after the first sign bit) is the usual Ca2 representation.

    Thus, it is very easy to extend such "offsets" on a larger number of bits !
    => we use this, to "extend" offset trees on larger number of bits.
    *)

    (* NB: [offset] is a kind of Zipper structure for list of bits, except the sign bit being treated specially *)
    type 'a offset = {
        hsb: bool list; (* high-significant bits, encoded in LSB, except the first bit that is the sign bit FIXME *)
        lsb: bool list; (* low-significant bits, encode in HSB *)
      } 

    (* given x>=0, returns the list of bits, with LSB first. sign=1 => false represents "0" (ie nonnegative interpretation) *)
    let rec binary sign x =
      if (x=0)
      then []
      else ((x mod 2)=sign)::(binary sign (x/2));;
            
    (* given n >= 0 (could be a Peano natural), l a list of bits with LSB first return the offset encoding such that List.length lsb = n *)
    let get_hsb_lsb signext n l =  (* signext = the usual sign-extension. NB: this is the negation of the sign bit *)
      let rec split n l acc =
        if n = 0 then
          { hsb = (not signext)::l;
            lsb = acc }
        else
          match l with
          | [] -> split (n-1) [] (signext::acc)
          | b::l -> split (n-1) l (b::acc)
      in split n l [];;

    let get_offset n x =
      if x >= 0
      then get_hsb_lsb false n (binary 1 x)
      else get_hsb_lsb true n (binary 0 (-x-1));;  (* we compute the usual Ca2 representation *)


    (* radix-tree by list of bits *)
    type 'a prefixTree =
      Empty
    | Single of 'a
    | Sum of 'a prefixTree * 'a prefixTree;; 

    type 'a offsetMap =
      {
        height: int; (* could be a Peano natural, such that the depth of *all* [Single] is at [height+1] (and depth of [Empty] <= [height+1]) *)
        tree: 'a prefixTree;
      }

    let empty = { height = 0; tree = Empty };; 

      
    let get =
      let rec getrec key ot =
        match key, ot with
        | [], Single x -> x
        | (b::k'), Sum(l, r) ->
           if not b then getrec k' l else getrec k' r
        | _ -> raise Not_found
      in fun off om ->
         let offcode = get_offset om.height off in
         match offcode.hsb with
         | [s] -> getrec (s::offcode.lsb) om.tree 
         | _ -> raise Not_found
      ;;

    let smartSum ot1 ot2 =
      match ot1, ot2 with
      | Empty, Empty -> Empty
      | _, _ -> Sum(ot1, ot2);;

    let add =
      let rec addrec key x ot = 
        match key, ot with
        | [], Empty -> Single x
        | (b::k'), Empty ->
           if not b
           then Sum (addrec k' x Empty, Empty)
           else Sum (Empty, addrec k' x Empty)
        | (b::k'), Sum(l, r) ->
           if not b
           then Sum (addrec k' x l, r)
           else Sum (l, addrec k' x r)
        | [], Single _ -> failwith "already present"
        | _, _ -> failwith "internal error: get_offset vs addrec ?"
      and ext_neg n ot =
        if n=0 then ot else ext_neg (n-1) (Sum(Empty, ot))
      and ext_pos n ot =
        if n=0 then ot else ext_pos (n-1) (Sum(ot, Empty))
      and neutral f ot =
        match ot with
        | Empty -> Empty
        | _ -> f ot
      and addhsb hsb insert_old old fresh =
        match hsb with
        | [] -> failwith "internal error: unexpected hsb" 
        | [b] -> if b then Sum(old,fresh) else Sum(fresh,old)  (* NB: [b] also gives the sign of the added offset *)
        | b::k -> addhsb k insert_old (insert_old old Empty) (if not b then Sum (fresh, Empty) else Sum (Empty, fresh))
      in fun off om ->
         let height = max om.height 1 in
         let offcode = get_offset height off in
         let x = string_of_int off in
         match offcode.hsb with
         | []  -> failwith "internal error: get_offset with empty hsb"
         | [s] -> { height = height; tree = addrec (s::offcode.lsb) x om.tree }
         | s::hsb ->
            let (neg,pos) = (match om.tree with Sum (neg, pos) -> (neg, pos) | Empty -> (Empty, Empty) | _ -> failwith "unexpected Single") in 
            let n = List.length(hsb) in
            let fresh = addrec (offcode.lsb) x Empty in
            {
              height = height + n;
              tree =
                if s then
                  Sum(neutral (ext_neg n) neg, addhsb hsb smartSum pos fresh) 
                else
                  Sum(addhsb hsb (fun o f -> smartSum f o) neg fresh, neutral (ext_pos n) pos)
            }
    ;;

    let get_range =
      (* return the elements above [key] *)
      let rec get_up key ot =
        match key, ot with
        | [], Single x -> ot
        | (b::k'), Sum(l, r) ->
           if not b then smartSum(get_up k' l) r else smartSum Empty (get_up k' r)
        | _ -> Empty
     (* return the elements below [key] *)
     and get_down key ot =
       match key, ot with
       | [], Single x -> ot
       | (b::k'), Sum(l, r) ->
          if not b then smartSum(get_down k' l) Empty else smartSum l (get_down k' r) 
       | _ -> Empty
     (* return the elements in range [down, up] *)
     and get_range down up ot =
      match down, up, ot with
      | [], [], Single x -> ot
      | b1::k1, b2::k2, Sum(l,r) ->
         if b1 > b2 then
           Empty (* up < down *)
         else if not b2 then
           smartSum (get_range k1 k2 l) Empty
         else if b1 then
           smartSum Empty (get_range k1 k2 r)
         else
           smartSum (get_up k1 l) (get_down k2 r)
      | _ -> Empty
     (* trunk useless prefix bits *)
     and resizerec h neg pos =
       if h <= 2 then
         { height = h; tree = Sum(neg,pos) }
       else                 
         match neg, pos with
         | Sum(Empty, neg'), Sum(pos',Empty) -> resizerec (h-1) neg' pos'
         | Sum(Empty, neg'), Empty -> resizerec (h-1) neg' Empty
         | Empty, Sum(pos', Empty) -> resizerec (h-1) Empty pos'
         | _ -> { height = h; tree = Sum(neg,pos) }
     and resize h ot =
       match ot with
       | Empty -> empty
       | Sum(neg,pos) -> resizerec h neg pos
     in fun down up om ->
        let offdown = get_offset om.height down in
        let offup = get_offset om.height up in
        resize om.height (
            match offdown.hsb, offup.hsb with
            | [ds], [os] -> get_range (ds::offdown.lsb) (os::offup.lsb) om.tree
            | _::_, [os] -> get_down (os::offup.lsb) om.tree
            | [ds], _::_ -> get_up (ds::offdown.lsb) om.tree
            | _, _ -> om.tree
          )

    (** tests *)


    let t = ref empty;;
    for i=(-10) to 10  do
      t := add (3*i+5) !t;
    done;;

    let t1 = get_range (-17) 7 !t;;
    let t2 = get_range 15 16 !t;;
    let t3 = get_range 14 16 !t;;

### Utilisation de conteneurs génériques pour dans le raffinement des mémoires symboliques

Remarquons qu'au niveau de BTL_SEtheory, il semble intéressant de
continuer à utiliser une notion "abstraite" de mémoire symbolique
définie comme actuellement (car on n'a pas besoin de décrire les
réécritures sur la mémoire à ce niveau). Et ne faire apparaître la
structure de données plus effective (et les réécritures) que dans un
raffinement.

Il peut être pratique d'utiliser des conteneurs génériques pour
décomposer les preuves et l'implémentation.

Exemple, inspiré des ["rose-trees"](https://mattam82.github.io/Coq-Equations/examples/Examples.RoseTree.html):

    Require Import List.

    Inductive rtree: Type :=
      | Single (x:nat)
      | Bunch (l: list rtree).

    (* my own induction lemma *)
    Lemma rtree_mut_ind (P : rtree -> Prop):
        (forall x, P (Single x)) ->
        (P (Bunch nil)) ->
        (forall r l, P r -> P (Bunch l) -> P (Bunch (r::l))) -> 
        forall r, P r.
    Proof.
      intros Ps Pnil Pcons. fix rtree_mut_ind 1.
      intros [x|l].
      - eapply Ps.
      - generalize l; fix aux 1. clear l.
        intros [|r l].
        + eapply Pnil.
        + eapply Pcons.
          * eapply rtree_mut_ind.
          * eapply aux.
    Qed.

    Definition rmap (f: nat -> nat) :=
      fix rmap (r:rtree) : rtree := 
      match r with
      | Single x => Single (f x)
      | Bunch l => Bunch (List.map rmap l)
      end.

    Lemma rmap_rmap f g r:
      rmap f (rmap g r) = rmap (fun x => f (g x)) r.
    Proof.
      induction r using rtree_mut_ind; simpl in *; auto.
      rewrite IHr.
      inversion IHr0. auto.
    Qed.

Concrètement, il faudrait examiner comment une structure de map générique sur les données associées aux clés peut être (ou pas) utilisée dans un cadre mutuellement inductif (ie avec des données contenant elles-mêmes des maps de façon mutuellement inductive). Donc typiquement, prendre en main [Coq-Equations](https://mattam82.github.io/Coq-Equations) et voir si ça peut aider à garder cette structure de données générique. Enfin, il faudrait voir si on peut en faire une version avec hash-consing (paramétrée par le hash-consing des données). Voir aussi [Coq-elpi](https://hal.inria.fr/hal-01897468).
