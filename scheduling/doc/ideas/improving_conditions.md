# Ideas about conditions in SE


## Comparaison des BDD (modulo réordonnancement des conditions ?)

On peut avoir envie de montrer que les deux blocs ci-dessous sont équivalents (si les dépendences sur les variables le permettent):
```
    if (c1) { i1 } else { i1' }
    if (c2) { i2 } else { i2' }
```
    
et
```
    if (c2) { i2 } else { i2' }
    if (c1) { i1 } else { i1' }
```
Ça revient (en gros) à comparer les BDD:
   
```
    if (c1) { if (c2) {i1;i2} else {i1;i2'} }
    else { if (c2) {i1';i2} else {i1';i2'} }
```
et 
```
    if (c2) { if (c1) {i2;i1} else {i2;i1'} }
    else { if (c1) {i2';i1} else {i2';i1'} }
```
Pour gérer ça, on peut faire des `Ordered BDD`: l'oracle du bloc
transformé annote (certaines) conditions avec des numéros de façon à
ce l'exécution symbolique du bloc transformé produise un `BDD` qui
correspond à celui du bloc d'origine (voir ci-dessous). Cependant, il semble difficile d'appliquer complètement
les techniques de mémoïsation des BDD ayant des booléens sur les
feuilles.  Car ici on veut effectuer une comparaison sur des feuilles
2 à 2 qui n'est pas une égalité, mais une inclusion !
**Principe du réordonnancement de BDD:** l'exécution symbolique du **bloc transformé** réordonne le BDD de
façon à respecter la numérotation: un pére doit avoir un numéro inférieur à
chacun de ses fils (en l'absence de numéro, on ignore les contraintes
de numérotation entre ce noeud est ses voisins). Exemple ci-dessous
avec trois conditions distinctes (pour order `c1 < c2 < c3`):

```
    if (c3) { if (c1) {f1} else {f1'} } else { if (c2) {f2} else {f2'} } 
```
    
est réordonné en
```
    if (c1)
      { if (c2) { if (c3) {f1} else {f2} }
        else { if (c3) {f1} else {f2'} } } 
    else
      { if (c2) { if (c3) {f1'} else {f2} }
        else { if (c3) {f1'} else {f2'} } }
```
**rem:** on ajoute ici un undefined behavior potentiel à cause l'execution de c2 quand c3 est vrai. 
Mais si le **bloc d'origine** est simulé par cet état symbolique réordonné, c'est correct. 
Le bloc transformé a juste supprimé un test inutile...
Bon, à voir si le principe ci-dessus est vraiment utile dans toute sa
généralité. Pour simplifier, on peut aussi restreindre le
réordonnancement du BDD aux cas où il n'y a pas de test redondant
inséré (comme dans l'exemple initial).

## Anciennes idées

Section plus trop à jour, voir plutôt https://gricad-gitlab.univ-grenoble-alpes.fr/veri-arsene/secu-compil/-/blob/main/load_duplication.md?ref_type=heads

### "Skip" de conditions dans les BDD

Ceci est une version simplifiée: comparaison des BDD sans réordonnancement des conditions.

Dans un premier temps (jusqu'à ce qu'on trouve une optimisation où ça pourrait être utile): pas de réordonnacement des BDD.
On autorise juste le BDD du bloc transformé à supprimer des conditions par rapport au bloc d'origine.
Autrement dit, dans la comparaison récursive de `if (c) BDD1 BDD2` avec `if (c') BDD1' BDD2'`:
- soit `c=c'` et on compare récursivement `BDD1` avec `BDD1'` et `BDD2` avec `BDD2'`.
- soit `c<>c'` et  on compare récursivement `BDD1` et `BDD2` avec `if (c') BDD1' BDD2'`
Ce deuxième cas correspond au fait que le test sur `c` dans le bloc d'origine était inutile!

### Propagation de valeurs symbolique de conditions (et élimination de condition) pendant l'execution symbolique

L'exécution symbolique se propageant en `avant`, on peut propager les valeurs des conditions symboliques, qu'on peut utiliser pour éliminer des conditions redondantes 
(et donc limiter l'explosion du nombre de chemin).

Pour rendre ce mécanisme efficace et puissant, on peut guider ce mécanisme de propagation/élimination avec des annotations introduites par les oracles.

- une annotation `bind_to x` qui indique de mémoriser la valeur (soit `true` soit `false` suivant la branche) de la condition symbolique avec le nom `x`
- une annotation `eval_to b proof` qui indique que la condition s'évalue sur la constante `b`, ce qui est prouvable avec la preuve `proof`.

Ici on peut imaginer un langage plus ou moins compliqué pour prouver l'évaluation des conditions. La version la plus simple:

- `eq(x)`  dit simplement que la condition symbolique est syntaxiquement égale celle appelée `x`.
- `eqnot(x)` dit que c'est la négation.

Dans le cas général, on peut introduire tout un système de réécriture pour éliminer les conditions.

On pourrait avoir un système de réécritures dédié à l'élimination de conditions,
En fait, il serait sans doute intéressant de mettre en place un
système de réécriture guidé par oracle pour toutes les instructions
BTL.  Ça permet de concilier "puissance" de l'exécution symbolique et
"efficacité". L'exécution symbolique va pouvoir éventuellement faire
des réécritures compliquées, mais uniquement quand c'est nécessaire.

**Exemple: une `if-conversion` généralisée**
On aimerait montrer que le bloc d'origine:

```
    if (c) {
        x1 = e1
        x2 = e2
        y = e
        x3 = e3
    } else {
        x3 = e3'
        z = e'
        x1 = e1'
        x2 = e2'
    }
```
    
est simulable par le bloc transformé:

```
    x1 = (c?e1:e1')
    x2 = (c?e2:e2')
    x3 = (c?e3:e3')
    if (c) { y = e } else { z = e' }
```

une solution: ajouter une régle de réécriture `x = (c?e:e')` en `if (c) { x=e } else {x=e'}` 
(attention, ce n'est pas une règle de réécriture sur les valeurs
symboliques, mais sur du code BTL lui-même, avant l'exécution
symbolique de ce code).

L'exécution symbolique ouvre alors deux branches `c=true` et
`c=false` sur `x1 = (c?e1:e1')`, puis la propagation/élimination de la
condition symbolique `c` simplifie les conditionnelles sur `x2`, `x3` et `y`/`z`.
Au final, on obtient deux BDD identiques sur cet exemple (sans explosion combinatoire).

**Rem** les mécanismes de propagation/réécritures décrits ci-dessus peuvent être aussi très utile pour la simulation symbolique modulo invariants !


