(**
    given bounds on the possible values of an array, create a structure able to
    - memorise an array 
    - check if a previous array is lower (or equal) for all coordinate
    All sequence must be of the same size.
*)

type t

(**
   create the structure using the given bounds, both included
*)
val create : (int*int) Seq.t -> t

(**
    Return true if a memorise array is lower or equal for all coordinate
*)
val check_dominance : t -> int Seq.t -> bool

(**
    memorise the array
*)
val add_dominance : t -> int Seq.t -> unit

val copy : t -> t

val clean : t -> unit