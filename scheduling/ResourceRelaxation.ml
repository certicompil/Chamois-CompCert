module type T = sig
  type t
  val dummy : t
  val create :  int array array -> int array -> t
  val lower_bound :  bool array -> t -> int
  val _test : (unit -> unit) list
end

module VolumeApproximation : T = struct

  type t = {
    possible_usage:(int array*int) list;
    instructions_usages:int array array
  }
  (*list of (usage, maximum number of elements in parallel for the usage) *)

  let dummy = {possible_usage=[];instructions_usages=[||]}

  let commun_factor (l:int list) : int = 
    match List.fold_left (max) 0 l with
    | 1 -> 1
    | 2 -> 2
    | 3 -> 6
    | 4 -> 12
    | 5 | 6 -> 60
    | 7 -> 420
    | 8 -> 840
    | 9 | 10 -> 2520
    | _ -> failwith "maximum value too high (max 10)"

  let qsomme (l : (int * int) list) : int * int =
    let c_den = commun_factor ((List.map (fun (_,den) -> den)) l) in
    let c_num = List.fold_left (fun acc (num,den) -> acc + num * (c_den / den)) 0 l in
    (c_num,c_den)

  let sum_round_up (l : (int * int) list) : int = 
    match l with
    | [] -> 0
    | _ -> 
    let (num,den) = qsomme l in 
    match num mod den with
    | _ when den=0 -> 0
    | 0 -> num / den
    | _ -> num / den + 1

  let create (instructions_usages : int array array) (max_usage:int array) : t =
     let usages = Array.fold_left (fun acc a1 -> 
       match List.find_opt (fun a2 -> Array.for_all2 (=) a1 a2) acc with
       | Some _ -> acc
       | None -> a1::acc
     ) [] instructions_usages in

     (*remove empty patterns (only needed if not remove during preprocess)*)
     let usages = List.filter (fun a -> Array.exists (fun e -> e>0) a) usages in
   
     let rec enum_max_pattern (elements:int array list) current_usage current_nb =
       match elements with
       | [] -> current_nb
       | hd::tl -> 
         let usage_with_hd = Array.map2 (+) current_usage hd in
         match Array.for_all2 (<=) usage_with_hd max_usage with
         | false ->
           let not_taking_hd = enum_max_pattern tl current_usage current_nb in
           not_taking_hd
         | true -> 
           let taking_hd = enum_max_pattern (hd::tl) (usage_with_hd) (current_nb+1) in
           let not_taking_hd = enum_max_pattern tl current_usage current_nb in
           max taking_hd not_taking_hd
     in
   
     let patterns = List.map (fun usage -> usage,enum_max_pattern usages usage 1) usages in
     {
      possible_usage=patterns;
      instructions_usages;
     }

  let lower_bound (instruction_filter:bool array) (self : t) : int =

  (* Printf.printf "[%s]\n" (Tools.Adv_string_conversion.string_of_array_adv instructions_usages (fun a -> " [" ^ (Tools.Adv_string_conversion.string_of_int_array a) ^ "] "));
  Printf.printf "[%s]\n" (Tools.Adv_string_conversion.string_of_bool_array instruction_filter);
  Printf.printf "[%s]\n" (Tools.Adv_string_conversion.string_of_list_adv patterns (fun (p,n) -> "(["^(Tools.Adv_string_conversion.string_of_int_array p)^"]"^(string_of_int n)^")")); *)

    let patterns_with_nb = List.map (fun (usage,m) -> usage,ref 0,m) self.possible_usage in
    Array.iteri (fun i u -> 
      if instruction_filter.(i) then 
          match List.find_opt (fun (ur,_,_)->Array.for_all2 (=) u ur) patterns_with_nb with
          | Some (_,r,_) -> r := !r + 1
          | None when Array.for_all ((=) 0) u -> ()
          | None -> failwith ("unkown resource consumption : "^(Tools.Adv_string_conversion.string_of_int_array u))
    ) self.instructions_usages;

    (* let float_res = List.fold_left (fun acc (_,r_nb_p,max_p) -> acc +. (float_of_int !r_nb_p) /. (float_of_int max_p)) (0.) patterns_with_nb in
    let res = Float.to_int float_res + (if Float.is_integer float_res then 0 else 1) in *)

    let frac = List.map (fun (_,r_nb_p,max_p) -> !r_nb_p, max_p) patterns_with_nb in
    let res = sum_round_up frac in
    (* Printf.printf "res : %d, float res : %f, is integer : %b \n" res float_res (Float.is_integer float_res);     *)
    res
  ;;

  (*** unitary tests ***)

  let test_qsomme (l:(int * int) list) (expected:int * int) () = 
    assert (qsomme l = expected)
  let tests_qsomme_list = [
    test_qsomme [(1,1);(1,2);(1,3);(1,2);(1,3)] (16,6);
    test_qsomme [(1,1);(1,2);(2,3);(0,2);(1,3)] (15,6);
    test_qsomme [(1,1);(1,2);(1,3);(1,4);(1,5);(1,6)] (147,60);
  ]
  let test_create instructions_usages max_usage possible_usage () = 
    assert ((create instructions_usages max_usage).possible_usage = possible_usage)

  let tests_create_list = [
    ( test_create 
      [|[|0;1|];[|1;1|];[|1;0|];[|0;1|];[|1;1|]|] 
      [|2;1|] 
      [([|1; 0|], 3); ([|1; 1|], 2); ([|0; 1|], 3)]
    );
    ( test_create 
      [|[|1|];[|1|];[|1|]|]
      [|1|]
      [([|1|], 1)]
    ); 
    ( test_create 
      [|[|0;0;1|];[|0;1;0|];[|0;1;1|];[|1;0;0|];[|1;0;1|];[|1;1;0|];[|1;1;1|];[|2;0;0|];[|2;0;1|];[|2;1;0|];[|2;1;1|];|]
      [|2;1;1|]
      [([|2; 1; 1|], 1); ([|2; 1; 0|], 2); ([|2; 0; 1|], 2); ([|2; 0; 0|], 3);([|1; 1; 1|], 2); ([|1; 1; 0|], 3); ([|1; 0; 1|], 3); ([|1; 0; 0|], 4);([|0; 1; 1|], 3); ([|0; 1; 0|], 4); ([|0; 0; 1|], 4)]
    );
    ( test_create 
      [|[|1;1;0;0|];[|1;0;0;1|]|] 
      [|2;2;1;1|]
      [([|1; 0; 0; 1|], 2); ([|1; 1; 0; 0|], 2)]
    );
  ] 
  let test_lower_bound instructions_usages instruction_filter possible_usage expected () =
    assert ((lower_bound instruction_filter {possible_usage;instructions_usages}) = expected)
  
  let tests_lower_bound_list = [
    test_lower_bound 
      [|[|1;1;0;0|];[|1;0;0;1|]|] 
      [|true;true|] 
      [([|1; 0; 0; 1|], 2); ([|1; 1; 0; 0|], 2)] 
      1;
    test_lower_bound 
      [|[|1;1;0;0|];[|1;0;0;1|]|] 
      [|true;false|] 
      [([|1; 0; 0; 1|], 2); ([|1; 1; 0; 0|], 2)] 
      1;
    test_lower_bound 
      [|[|1;1;0;0|];[|1;0;0;1|];[|1;1;0;0|]|] 
      [|true;true;true|] 
      [([|1; 0; 0; 1|], 2); ([|1; 1; 0; 0|], 2)] 
      2;
  ]

  let _test = List.concat [tests_create_list;tests_lower_bound_list;tests_qsomme_list]

end

module Exact : T = struct
  
  (* module Memory = Map.Make(struct
    type t = int array * int array list * int
    let compare (a1,l1,s1) (a2,l2,s2) = 
      match compare a1 a2 with
      | 0 -> (
        match compare (List.length l1) (List.length l2) with
        | 0 -> compare s1 s2
        | v -> v
      )
      | v -> v
  end) *)
  
  module PatternSolver = struct
     let pattern_is_0 (nb_instructions : int array) (pattern:int array) = Array.for_all2 (fun nb_i nb_p -> nb_p=0 || nb_i=0) nb_instructions pattern
     let upper_bound = ref Int.max_int
     (* let memory = ref Memory.empty *)
     (*
     For p in patterns, p.(i) is the number of instructions of type i in p.   
     *)
     let rec mcs_solver (nb_instructions : int array) (patterns : int array list) (size : int) =
       match patterns with
       | [] when Array.exists ((<>)0) nb_instructions -> None
       | _ when size >= !upper_bound -> None
       | [] -> 
         upper_bound := size;
         Some size 
       | p::nxt when pattern_is_0 nb_instructions p -> mcs_solver nb_instructions nxt size
       | p::nxt ->
        (*memorisation*)
        (* 
        let current_upper_bound = !upper_bound in
        match Memory.find_opt (nb_instructions,patterns,size) !memory with
        | Some (_,Some res) -> Some res
        | Some (bound,None) when bound >= current_upper_bound -> None
        | _ -> *)

        (*branching*)
         let do_take = mcs_solver (Array.map2 (fun nb_i nb_p -> max 0 (nb_i - nb_p)) nb_instructions p) patterns (size+1) in
         let dont_take = mcs_solver nb_instructions nxt size in
         let res = match do_take,dont_take with
         | None,_  -> dont_take
         | _, None -> do_take
         | Some s1,Some s2 -> Some (min s1 s2) in
         (* memory := Memory.add (nb_instructions,patterns,size) (current_upper_bound,res) !memory; *)
         res
   
     let solve (nb_instructions : int array) (patterns : int array list) = 
       upper_bound := Int.max_int;
       ignore @@ mcs_solver nb_instructions patterns 0;
       !upper_bound

    let test_mcs_solver nb_instructions patterns size expected () =
      upper_bound := Int.max_int;
      assert (mcs_solver nb_instructions patterns size = expected)

    let tests_mcs_solver_list = [
      test_mcs_solver [|50|] [[|1|]] 0 (Some 50);
      test_mcs_solver [|100;100|] [[|2;0|];[|1;1|]] 0 (Some 100);
      test_mcs_solver [|100;90|] [[|2;0|];[|1;1|]] 0 (Some 95);
      test_mcs_solver [|0;4;1|] [[|1;0;1|];[|0;1;0|]] 0 (Some 5);

    ]
  end 

  type t = {
    (* instructions_nb : int array; *)
    raw_usage : int array list;
    patterns : int array list;
    instructions_usages : int array array;
  }

  let dummy = {raw_usage=[];patterns=[];instructions_usages=[||]}

  let get_raw_usages instructions_usages =
    let raw_usages = Array.fold_left (fun acc a1 -> 
      match List.find_opt (fun a2 -> Array.for_all2 (=) a1 a2) acc with
      | Some _ -> acc
      | None -> a1::acc
    ) [] instructions_usages in
    raw_usages
  (*list of possible usage (each usage apear at most once)*)
  let get_numbered_usage raw_usages instructions_usages =
    let instructions_usages_list = Array.to_list instructions_usages in
     let usages = List.map (fun u ->
       let nb_using = List.length (List.find_all (fun a -> Array.for_all2 (=) a u) instructions_usages_list) in
       (nb_using,u)
     ) raw_usages in 
   
     (*remove empty patterns (only needed if not remove during preprocess)*)
     let usages = List.filter (fun (_,a) -> Array.exists (fun e -> e>0) a) usages in
     Array.of_list usages 
  (*array of (number of use of the usage, usage)*)

  let get_usages instructions_usages = 
     let raw_usages = get_raw_usages instructions_usages in
     get_numbered_usage raw_usages instructions_usages
   
  let get_patterns (usages_array:(int*int array) array) (max_usage:int array)=
     let nb_usages = Array.length usages_array in
     
     let rec aux usage_id current_pattern current_usage=
        match usage_id >= nb_usages with true -> [current_pattern] | false ->
        (*stop when no more usage is left*)
     
        let (usage_nb_max,usage_u) = usages_array.(usage_id) in
        let usage_nb = current_pattern.(usage_id) in
        match usage_nb = usage_nb_max with true -> aux (usage_id+1) current_pattern current_usage | false ->
        (*don't take if already all taken*)
     
        let usage_with_u = Array.map2 (+) current_usage usage_u in
        let pattern_with_u = Array.copy current_pattern in pattern_with_u.(usage_id) <- pattern_with_u.(usage_id) + 1;
        match Array.for_all2 (<=) usage_with_u max_usage with false -> aux (usage_id+1) current_pattern current_usage | true ->
        (*don't take if exead max usage*)
     
        let taking_u = aux usage_id pattern_with_u usage_with_u in
        let not_taking_u = aux (usage_id+1) current_pattern current_usage in
     
        let not_taking_u_filtered = List.filter ( fun p1 -> List.for_all (fun p2 -> Array.exists2 (>) p1 p2) taking_u) not_taking_u in
        List.append taking_u not_taking_u_filtered
     in
     aux 0 (Array.make nb_usages 0) (Array.make (Array.length max_usage) 0)

  let create (instructions_usages : int array array) (max_usage:int array) : t =
     let raw_usage = get_raw_usages instructions_usages in
     let numbered_usage = get_numbered_usage raw_usage instructions_usages in
     (* PatternSolver.memory := Memory.empty; *)
     {
      instructions_usages;
      raw_usage;
      (* instructions_nb = Array.map (fun (i,_) -> i) usages_array; *)
      patterns = get_patterns numbered_usage max_usage;
     }

  

  let lower_bound (instruction_filter:bool array) (patterns : t) =
    let noResource = Array.make (Array.length patterns.instructions_usages.(0)) 0 in

    let raw_usage = patterns.raw_usage in
    let usages = get_numbered_usage raw_usage (Array.map2 (fun b u -> if b then u else noResource) instruction_filter patterns.instructions_usages) in
    let instructions_nb = Array.map (fst) usages in

    (* Printf.printf "instruction usage : [%s] \n" (Tools.Adv_string_conversion.string_of_array_adv instructions_usages (fun a -> "["^Tools.Adv_string_conversion.string_of_int_array a ^"]"));
    Printf.printf "instruction_filter [%s]\n"  (Tools.Adv_string_conversion.string_of_bool_array instruction_filter);
    Printf.printf "instructions_nb [%s]\n"  (Tools.Adv_string_conversion.string_of_int_array instructions_nb); *)
    let res = PatternSolver.solve instructions_nb patterns.patterns in 
    res

  (***unitary tests ***)

  let test_get_usages instructions_usages expected () = 
    assert (get_usages instructions_usages = expected)
  let tests_get_usages_list = [
    (test_get_usages [|[|0;1;0|];[|1;1;1|];[|0;1;0|];[|1;0;0|];[|2;1;1|]|] [|(1, [|2; 1; 1|]); (1, [|1; 0; 0|]); (1, [|1; 1; 1|]); (2, [|0; 1; 0|])|]);
    (test_get_usages [|[|1;1;0;0|];[|1;1;0;0|];[|0;1;1;0|];[|0;1;1;0|];[|0;1;1;0|];[|0;1;1;0|];[|0;0;1;1|]|] [|(1, [|0; 0; 1; 1|]); (4, [|0; 1; 1; 0|]); (2, [|1; 1; 0; 0|])|]);
  ]
  let test_get_patterns usages_array max_usage expected () = 
    assert ((get_patterns usages_array max_usage) = expected)
  let tests_get_patterns_list = [
     (test_get_patterns [| (5,[|2;1;0|]) ; (5,[|2;0;1|]) ; (1,[|1;0;0|]) |] [|3;1;1|] [[|1;0;1|];[|0;1;1|]]);
     (test_get_patterns [| (100,[|1|])|] [|1|] [[|1|]]);
     (test_get_patterns [|(1, [|0; 0; 1; 1|]); (4, [|0; 1; 1; 0|]); (2, [|1; 1; 0; 0|])|] [|1;1;1;1|] [[|1;0;1|];[|0;1;0|]])
  ]
  let _test = List.concat [tests_get_patterns_list;tests_get_usages_list;PatternSolver.tests_mcs_solver_list]
end