open InstructionSchedulerTypes

(** InstructionScheduler scheduling functions and utilities *)

(** Print problem for human readability. *)
val print_problem : out_channel -> problem -> unit;;

(** Get the number of instructions in a problem *)
val get_nr_instructions : problem -> int;;

(** Get the number of resources in a problem *)
val get_nr_resources : problem -> int;;

(* DISABLED
(** Schedule the problem optimally by constraint solving using the Gecode solver. *)
external gecode_scheduler : problem -> solution option
  = "caml_gecode_schedule_instr"
 *)
  
(** Get the number the last scheduling time used for an instruction in a solution.
@return The last clock tick used *)
val maximum_slot_used : int array -> int

(** Validate that a solution is truly a solution of a scheduling problem.
@raise Failure if validation fails *)
val check_schedule : problem -> solution -> unit

(** Schedule the problem using a greedy list scheduling algorithm, from the start.
The first (according to instruction ordering) instruction that is ready (according to the latency constraints) is scheduled at the current clock tick.
Once a clock tick is full go to the next.

@return [Some solution] when a solution is found, [None] if not. *)
val list_scheduler : problem -> solution option


(** Same as list_scheduler, but schedules instructions which decrease
register pressure when it gets too high. *)
val regpres_scheduler : problem -> solution option
val reg_pres_scheduler_old : problem -> solution option
  
(** Schedule the problem using the order of instructions without any reordering. DOES NOT COMPUTE A CORRECT MAKESPAN, the "times" are logical not cycles. *)
val greedy_scheduler : problem -> solution option

val recompute_makespan : problem -> int array -> int option

(** Schedule a problem using a scheduler applied in the opposite direction, e.g. for list scheduling from the end instead of the start. BUGGY *)
val schedule_reversed : scheduler -> problem -> solution option

(** Schedule a problem from the end using a list scheduler. BUGGY *)
val reverse_list_scheduler : problem -> solution option

(** Check that a problem is well-formed.
@raise Failure if validation fails *)
val check_problem : problem -> unit
  
(** Apply a scheduler and validate the result against the input problem.
@return The solution found
@raise Failure if validation fails *)
val validated_scheduler : scheduler -> problem -> solution option;;

(** Get max latency from solution
@return Max latency *)
val get_max_latency : int array -> int;;

(** Get the length of a maximal critical path
@return Max length *)
val maximum_critical_path : problem -> int;;

(** Apply line scheduler then advanced solver
@return A solution if found *)
val cascaded_scheduler : problem -> solution option;;

val show_date_ranges : problem -> unit;;

type pseudo_boolean_problem_type =
  | SATISFIABILITY
  | OPTIMIZATION;;

type pseudo_boolean_mapper
val pseudo_boolean_print_problem : out_channel -> problem -> pseudo_boolean_problem_type -> pseudo_boolean_mapper;;
val pseudo_boolean_read_solution : pseudo_boolean_mapper -> in_channel -> int array;;
val pseudo_boolean_scheduler : pseudo_boolean_problem_type -> problem -> solution option;;

val smt_print_problem : out_channel -> problem -> unit;;

val ilp_print_problem : out_channel -> problem -> pseudo_boolean_problem_type -> pseudo_boolean_mapper;;

val ilp_scheduler : pseudo_boolean_problem_type -> problem -> solution option;;

val mris_scheduler: problem -> bool -> bool -> solution option;;

(** Schedule a problem using a scheduler given by a string name *)
val scheduler_by_name : string -> problem -> solution option;;
