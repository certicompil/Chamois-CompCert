open InstructionSchedulerTypes
open BNB_heuristic
module Preprocess = PreprocessProblem

module type T = sig 
    type t
    val init : ?choose:Strategy.t ->(*  ?preprocess:PreprocessProblem.t -> *) problem -> int array -> t
    val compute_schedule : t -> Preprocess.unprocessed_solution
    val compute_bounds : t -> int * int
    val compute_regpress : t -> int array
    (* val compute : t -> solution option *)
end

module Make(Config:BNB_Config.T) = struct
    module Scheduler = Config.Scheduler
    type t = {
    start :  Scheduler.t;
    (* problem : problem; *)
    (* initial_problem : problem; *)
    choose : Strategy.t;
    (*computed value*)
    mutable schedule : int array option;
    mutable bounds : (int * int) option;
    mutable regpress : int array option;
}

let init ?(choose) initial_problem nr_regs = 
    (*TODO check if MUST DO TC ? *)
    let problem = initial_problem in
    
    let start = Scheduler.init problem nr_regs in

    let choose = match choose with
    | Some s -> s
    | None -> 
        let cp = Scheduler.compute_cp start in
        Strategy.from_order cp (* (Strategy.reverse_order cp) *) (* (Strategy.random_order cp)  *)
    in
    {start;(* problem; *)(* initial_problem; *)choose;schedule=None;bounds=None;regpress=None}

(* let compute_CP self = (Scheduler.compute_cp self.start) *)


let _debug_step i schedule =
    Printf.printf "instruction : %d\n" i;
    Printf.printf "regpress : %s\n" @@ Tools.Adv_string_conversion.string_of_int_array (Scheduler.current_regpress schedule);
    flush stdout;
    let _ = input_line stdin in ()

(*
    copy src into dest if src > dest, else, do nothing.
*)
(* let max_array dest src = 
    match Config.regpress_max dest src = dest with
    | true -> ()
    | false -> Array.iteri (fun i v -> dest.(i) <- v) src *)

let compute_core self = 
    let scheduler = Scheduler.copy self.start in
    let heuristic_ms = ref 0 in
    let flag = ref true in
    let choice_counter = ref 0 in
    let did_choose = ref false in
    let regpress = Array.copy (Scheduler.current_regpress scheduler) in
    while !flag do
      match Scheduler.total_makespan scheduler with
      | Some ms -> 
          heuristic_ms := ms;
          flag := false
      | None -> 
          match Scheduler.possible scheduler with
          | None -> 
            let _ = if !did_choose then (choice_counter := !choice_counter + 1) in
            did_choose := false;
            Scheduler.advance_time scheduler
          | Some possible -> 
                let possible_list = self.choose possible in
                match possible_list with
                | [] -> failwith "an instruction should be possible"
                | i::[] -> 
                    (* _debug_step i scheduler; *)
                    Scheduler.schedule_instruction scheduler i;
                    Tools.Array_operation.max_in regpress regpress (Scheduler.current_regpress scheduler) Config.regpress_compare
                | i::_ -> 
                    (* _debug_step i scheduler; *)
                    did_choose := true;
                    Scheduler.schedule_instruction scheduler i;
                    Tools.Array_operation.max_in regpress regpress (Scheduler.current_regpress scheduler) Config.regpress_compare
    done;
    let heuristic_schedule = Scheduler.current_schedule scheduler in
    let heuristic_ms = !heuristic_ms in
    self.schedule <- Some heuristic_schedule;
    self.bounds <- Some ( heuristic_ms - !choice_counter, heuristic_ms );
    self.regpress <- Some (regpress);
    (heuristic_ms - !choice_counter,heuristic_ms)

let compute_schedule self = 
    match self.schedule with
    | Some v -> Preprocess.{raw_solution=v}
    | None -> 
    let _ = compute_core self in
    Preprocess.{raw_solution=Option.get self.schedule}

let compute_bounds self = 
    match self.bounds with
    | Some v -> v
    | None -> 
    let _ = compute_core self in
    Option.get self.bounds

let compute_regpress self = 
    match self.regpress with
    | Some v -> v
    | None -> 
    let _ = compute_core self in
    Option.get self.regpress

(* let compute self : solution option = 
    let heuristic_schedule =
      match self.schedule with
      | Some s -> s
      | _ -> 
          ignore @@ compute_core self;
          Option.get self.schedule
    in
    let solution = ExportProblem.to_solution @@ Preprocess.postprocess heuristic_schedule self.postprocess (* (Array.append heuristic_schedule [|heuristic_ms|]) *) in
    (* check_schedule self.initial_problem solution; *)
    Some solution *)
end