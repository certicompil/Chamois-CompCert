open Registers
open BTL_BlockOptimizeraux
open RTLtyping

module IM = Map.Make (Int)

type latency_constraint = {
  instr_from : int;
  instr_to : int;
  latency : int;
}

type problem = {
  max_latency : int;
  (** An optional maximal total latency of the problem, after which the problem is deemed not schedulable. -1 means there should be no maximum. *)

  resource_bounds : int array;
  (** An array of number of units available indexed by the kind of resources to be allocated. It can be empty, in which case the problem is scheduling without resource constraints. *)

  live_regs_entry : Regset.t;
  (** The set of live pseudo-registers at entry. *)

  typing : regenv;
  (** Register type map. *)

  reference_counting : ((reg, register_info) Hashtbl.t
                     * ((reg * mentions_element) list array)) option;
  (** See BTL_BlockOptimizeraux.reference_counting. *)

  instruction_usages: int array array;
  (** At index {i i} the vector of resources used by instruction number {i i}. It must be the same length as [resource_bounds] *)

  latency_constraints : latency_constraint list
  (** The latency constraints that must be satisfied *)
}

type ren_map = (reg * reg) IM.t
type solution = (int array * ren_map option)
type scheduler = problem -> solution option

(* type solution = int array
type scheduler = problem -> solution option

type schedule_COMPLET =   {s:int array;m:int}
type schedule_FRAGMENT =  {s:int array;m:int}
type schedule_INCOMPLET = {s:int array}
type schedule_STATE =     {s:int array;r:int array} *)
