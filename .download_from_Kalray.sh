#!/bin/sh
mkdir download
cd download
# sshpass "-p$ sftp -o "StrictHostKeyChecking=no" compcert@ssh.kalray.eu <<EOF
# cd files
# get DEB.tar
# EOF
# tar xf DEB.tar
VERSION=ACE4.8.0 #formerly ACE4.6.0
FILE=$VERSION.tgz
DEBPATH=$VERSION/ACE-acceleration/ubuntu1804/tools/DEB
sshpass "-p$KALRAY_SFTP_PASSWORD" rsync -e 'ssh -v -o "ConnectionAttempts=10" -o "StrictHostKeyChecking=no"' --progress -a certicompil@pressembois.imag.fr:Kalray/$FILE .
tar xvfz $FILE $DEBPATH/
mv $DEBPATH/*.deb .
rm -rf $VERSION
