(* *********************************************************************)
(*                                                                     *)
(*              The Compcert verified compiler                         *)
(*                                                                     *)
(*                 Xavier Leroy, INRIA Paris                           *)
(*                                                                     *)
(*  Copyright Institut National de Recherche en Informatique et en     *)
(*  Automatique.  All rights reserved.  This file is distributed       *)
(*  under the terms of the INRIA Non-Commercial License Agreement.     *)
(*                                                                     *)
(* *********************************************************************)

(** Correctness proof for operator strength reduction. *)

Require Import Coqlib Compopts.
Require Import Integers Floats Values Memory Globalenvs Events.
Require Import Op Registers RTL ZIntervalDomain.
Require Import IntervalpropOp.

Section STRENGTH_REDUCTION.

Variable ge: genv.
Variable sp: block.
Variable ae: AE.t.
Variable e: regset.
Variable m: mem.
Hypothesis MATCH: ematch e ae.

Lemma const_for_result_correct:
  forall a op v,
  const_for_result a = Some op ->
  vmatch v a ->
  exists v', eval_operation ge (Vptr sp Ptrofs.zero) op nil m = Some v' /\ Val.lessdef v v'.
Proof.
  unfold const_for_result. intros.
  destruct (option_val_of_ival a) as [v' | ] eqn:OPTION. 2: discriminate.
  pose proof (option_val_of_ival_sound _ _ H0 _ OPTION).
  subst v'.
  exists v.
  destruct v; try discriminate; inv H; cbn.
  all: split; [try reflexivity | apply Val.lessdef_refl].
Qed.

Lemma op_strength_reduction_correct:
  forall op args vl v,
  vl = map (fun r => AE.get r ae) args ->
  eval_operation ge (Vptr sp Ptrofs.zero) op e##args m = Some v ->
  let (op', args') := op_strength_reduction op args vl in
  exists w, eval_operation ge (Vptr sp Ptrofs.zero) op' e##args' m = Some w /\ Val.lessdef v w.
Proof.
  intros until v. intros VL EVAL.
  unfold op_strength_reduction.
  destruct op.
  all: eexists; split; [eassumption | constructor].
Qed.  

Lemma addr_strength_reduction_correct:
  forall addr args vl res,
  vl = map (fun r => AE.get r ae) args ->
  eval_addressing ge (Vptr sp Ptrofs.zero) addr e##args = Some res ->
  let (addr', args') := addr_strength_reduction addr args vl in
  exists res', eval_addressing ge (Vptr sp Ptrofs.zero) addr' e##args' = Some res' /\ Val.lessdef res res'.
Proof.
  intros until res. intros VL EVAL. rewrite VL.
  unfold addr_strength_reduction.
  destruct addr.
  all: eexists; split; [eassumption | constructor].
Qed.

Lemma cond_strength_reduction_correct:
  forall cond args vl b,
    vl = map (fun r => AE.get r ae) args ->
    eval_condition cond e##args m = Some b ->
    let (cond', args') := cond_strength_reduction cond args vl in
    eval_condition cond' e##args' m = Some b.
Proof.
  intros until b. intros VL EVAL. cbn.
  unfold cond_strength_reduction.
  destruct cond; eassumption.
Qed.

End STRENGTH_REDUCTION.
