#!/bin/bash
# @author gourdinl
# Script to compute profiling results for multiple version of CompCert

CCOMPS=(
  "/home/yuki/Work/VERIMAG/Compcert_one"
  "/home/yuki/Work/VERIMAG/Compcert_two"
  "/home/yuki/Work/VERIMAG/Compcert_three"
)

FCTS=(
  '.*function_equiv_checker$;.*WF.*function_checker$;.*Live.*function_checker$'
  '.*check_symbolic_simu$'
  '.*check_symbolic_simu$;.*liveness_checker$'
)

for ((i = 0; i < ${#CCOMPS[@]}; i++)); do
  ./prof_ci_tests.sh ${CCOMPS[$i]} ${FCTS[$i]}
done
