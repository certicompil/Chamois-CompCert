[![](http://www-verimag.imag.fr/~boulme/pub/chamois.png) ![](http://www-verimag.imag.fr/~boulme/pub/poster_OOPSLA23-thumb.jpg)](http://www-verimag.imag.fr/~boulme/pub/poster_OOPSLA23.pdf)

# Chamois CompCert

Chamois CompCert is a fork of CompCert, the formally verified C compiler.
For an up-to-date presentation, see [our OOPSLA'23 paper](https://hal.science/hal-04102940). 

## Overview
The CompCert C verified compiler is a compiler for a large subset of the
C programming language that generates code for the PowerPC, ARM, x86 and
RISC-V processors.

The distinguishing feature of CompCert is that it has been formally
verified using the Coq proof assistant: the generated assembly code is
formally guaranteed to behave as prescribed by the semantics of the
source C code.

For more information on CompCert (authors, supported platforms, supported C
features, installation instructions, using the compiler, etc), please
refer to the [Web site](https://compcert.org/) and especially
the [user's manual](https://compcert.org/man/).

## Chamois version

Chamois CompCert is a version of CompCert with additions from [Verimag](https://www-verimag.imag.fr) and previously from [Kalray](https://www.kalrayinc.com/) (a chamois is a fast and agile caprine, very common in French Alps). 
Main features:

* A companion benchmarking toolkit provided on [the Chamois-Benchs repository](https://gricad-gitlab.univ-grenoble-alpes.fr/certicompil/chamois-benchs).
* A backend for the Coolidge VLIW KVX processor.
* Postpass scheduling passes for KVX and ARMv8 (aarch64) along with a
  preprocessing peephole optimizer.
* Improved subexpression elimination: two passes CSE2 and CSE3. Both go through
  loops and feature a small alias analysis.
* A generic prepass scheduling optimizer with a multi-purpose preprocessing
  front-end: rewritings, register renaming, if-lifting and some generic code
  transformations such as loop-rotation, loop-unrolling, or tail-duplication.
* A pass of Lazy Code Transformations (Lazy Code Motion and Lazy Strength Reduction).
* A profiling system: see [`OPTIM_PROFILING.md`](OPTIM_PROFILING.md) for details.
* Static branch prediction.
  _The branch prediction is basic, it annotates each `Icond` node by an `option bool`. A `Some true` annotation indicates we predict the branch will be taken. `Some false` indicates the fallthrough case is predicted. `None` indicates we could not predict anything, and are not sure about which control will be preferred._
* `-ftracelinearize` uses the branch prediction information to linearize LTL basic blocks in a slightly better way (in the `Linearize` phase).
* "Canaries" for stack protection (`-fstack-protector`, `-fstack-protector-all`) on x86(-64), RISC-V, ARM and AArch64
* And some experimental features that are work in progress.

_Please refer to the resources listed below for more information._
	
The people responsible for this version are

* Sylvain Boulmé (Grenoble-INP, Verimag)
* David Monniaux (CNRS, Verimag)
* Léo Gourdin (UGA, Verimag)
* Basile Pesin (UGA, Verimag)
* Benjamin Bonneau (ENS-PSL, UGA, Verimag)
* Alexandre Bérard (Framatome, UGA, Verimag).

and formerly

* Cyril Six (Phd 2018-2021, Kalray)

with contributions of:

* Justus Fasse (M2R UGA 2021, now at KU Leuven).
* Pierre Goutagny and Nicolas Nardino (L3 ENS-Lyon 2021).

## Installing

Please follow the instructions in [`INSTALL.md`](INSTALL.md)

## Main papers and docs on Chamois CompCert

* [The documentation of the Chamois CompCert, generated from the Coq sources, for the KVX backend](https://certicompil.gricad-pages.univ-grenoble-alpes.fr/Chamois-CompCert).
* [Formally Verifying Optimizations with Block Simulations](https://hal.science/hal-04102940) an OOPSLA'23 paper, by Gourdin, Bonneau, Boulmé, Monniaux and Bérard.
* [Testing a Formally Verified Compiler](https://hal.science/hal-04096390/) a TAP'23 paper, by Monniaux, Gourdin, Boulmé and Lebeltel.
* [The Trusted Computing Base of the CompCert Verified Compiler](https://hal.science/hal-03541595/), an ESOP'22 paper, by Monniaux and Boulmé.
* [Lazy Code Transformations in a Formally Verified Compiler](https://hal.science/hal-04108775/), a ICOOOLPS'23 paper, by Gourdin.
* [Formal Validation of Intra-Procedural Transformations by Defensive Symbolic Simulation](https://lgourd.in/defense.html), Phd Thesis of Léo Gourdin in 2023.
* [A 5-minutes video](http://www-verimag.imag.fr/~boulme/videos/poster-oopsla20.mp4) by Cyril Six, presenting the postpass scheduling and the KVX backend
(also on [YouTube if you need subtitles](https://www.youtube.com/watch?v=RAzMDS9OVSw)).
* [Optimized and formally-verified compilation for a VLIW processor](https://hal.archives-ouvertes.fr/tel-03326923), Phd Thesis of Cyril Six in 2021.

## More papers...

* [Certified and Efficient Instruction Scheduling](https://hal.archives-ouvertes.fr/hal-02185883), an OOPSLA'20 paper, by Six, Boulmé and Monniaux.
* [Formally Verified Defensive Programming (efficient Coq-verified computations from untrusted ML oracles) -- Chapters 1 to 3](https://hal.archives-ouvertes.fr/tel-03356701), Habilitation Thesis of Sylvain Boulmé in 2021.
* [Simple, Light, Yet Formally Verified, Global Common Subexpression Elimination and Loop-Invariant Code Motion](https://hal.archives-ouvertes.fr/hal-03212087), a LCTES'21 paper, by Monniaux and Six.
* [Formally Verified Superblock Scheduling](https://hal.archives-ouvertes.fr/hal-03200774), a CPP'22 paper, by Six, Gourdin, Boulmé, Monniaux, Fasse and Nardino.
* [Code Transformations to Increase Prepass Scheduling Opportunities in CompCert](https://www-verimag.imag.fr/~boulme/CPP_2022/FASSE-Justus-MSc-Thesis_2021.pdf), MSc Thesis of Justus Fasse in 2021
and the [report](https://www-verimag.imag.fr/~boulme/CompCert_reports/Berard_Alexandre_M1report_2022.pdf) by Alexandre Bérard (M1 MOSIG internship 2022) of its adaptation to BTL.
* [Register-Pressure-Aware Prepass-Scheduling for CompCert](https://www-verimag.imag.fr/~boulme/CPP_2022/NARDINO-Nicolas-BSc-Thesis_2021.pdf), BSc Thesis of Nicolas Nardino in 2021.
* [Formally verified postpass scheduling with peephole optimization for AArch64](https://www.lirmm.fr/afadl2021/papers/afadl2021_paper_9.pdf), a short AFADL'21 paper, by Gourdin.
* The [BTL short documentation](scheduling/doc/BTLquickdoc.md).

## License
CompCert is not free software.  This non-commercial release can only
be used for evaluation, research, educational and personal purposes.
A commercial version of CompCert, without this restriction and with
professional support and extra features, can be purchased from
[AbsInt](https://www.absint.com).  See the file `LICENSE` for more
information.

## Copyright
The CompCert verified compiler is Copyright Institut National de
Recherche en Informatique et en Automatique (INRIA) and 
AbsInt Angewandte Informatik GmbH.

The additions are Copyright Grenoble-INP, CNRS and Kalray.

## Contact
General discussions on CompCert take place on the
[compcert-users@inria.fr](https://sympa.inria.fr/sympa/info/compcert-users)
mailing list.

For inquiries on the commercial version of CompCert, please contact
info@absint.com

For inquiries on the Verimag-specific additions, contact the researchers.
