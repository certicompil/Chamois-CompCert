stages:
  - build
  - test

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  # Number of parallel execution **in each docker**
  NJOBS: 2
  # Docker-in-docker stuff
  DOCKER_TLS_CERTDIR: "/certs"
  REGISTRY: "gricad-registry.univ-grenoble-alpes.fr/sixcy/compcert"
  WD: "/wd"
  # Kalray tools specific variables
  KALRAY_SFTP_PASSWORD: $KALRAY_SFTP_PASSWORD
  KALRAY_SFTP_PW_FILE: "kalray_pw.secret"
  KALRAY_VERSION: "ACE4.8.0"
  # Csmith binary and libs
  CSMITH_BIN: "$WD/csmith/bin/csmith"
  CSMITH_INC: "-I $WD/csmith/include/csmith-2.3.0/"
  # Yarpgen binary
  YARPGEN_BIN: "$WD/yarpgen-1.1/yarpgen"
  # CCG binary
  CCG_BIN: "$WD/ccg/ccg"
  # Default execution timeout, can be overwritten by jobs
  TIMEOUT: "10s"
  # Binaries for Intel SDE
  SDE32: "$WD/sde/sde"
  SDE64: "$WD/sde/sde64"
  # RISC-V 32 config
  RV32_DIR: "riscv32"
  # OPTS1 more ccomp options for the official CompCert tests
  # Currently it is used for the ARM mthumb option
  OPTS1: ""
  # OPTS2 more ccomp options for Csmith, currently for KVX only (stack size)
  OPTS2: ""
  DOCKERTAG: "moretests"
  
.build_script: &build_script
  - make -j "$NJOBS"
  - make -j "$NJOBS" clightgen

.common_test_simu: &common_test_simu
  - make -j 1 -C test SIMU="$SIMU" EXECUTE="$SIMU" CCOMPOPTS="$OPTS0 $OPTS1" all test 
  - make -j "$NJOBS" -C test/other_testsuites/paranoia test CCOMPOPTS="$OPTS0 $OPTS1" EXECUTE="$SIMU"
  - mv $WD/c-testsuite test/other_testsuites/c-testsuite/
  - make -j "$NJOBS" -C test/other_testsuites/c-testsuite/ CCOMPOPTS="$OPTS0 $OPTS1" EXECUTE="$SIMU"
  - mv $WD/compile test/other_testsuites/gcc/
  - mv $WD/execute test/other_testsuites/gcc/
  - make -j "$NJOBS" -C test/other_testsuites/gcc CCOMPOPTS="$OPTS0 $OPTS1" EXECUTE="$SIMU" test
  # The below might helps to generate the platform.info file
  - (cd test/random/csmith && $CSMITH_BIN -v)
  - ulimit -s65536 && make -j "$NJOBS" -C test/random/csmith TARGET_CC="$TCC" EXECUTE="timeout $TIMEOUT $SIMU" CCOMPOPTS="-static $OPTS0 $OPTS2" TARGET_CFLAGS="-static" CSMITH_INCLUDES="$CSMITH_INC" CSMITH="$CSMITH_BIN"
  - mv $YARPGEN_BIN test/random/yarpgen/
  - ulimit -s65536 && make -j "$NJOBS" -C test/random/yarpgen TARGET_CC="$TCC" EXECUTE="timeout $TIMEOUT $SIMU" $BITS
  - mkdir -p test/ccg/ccg && mv $CCG_BIN test/ccg/ccg/
  - make -j "$NJOBS" -C test/random/ccg

.intel_sde_test: &intel_sde_test
  - echo "$SDE"
  - make --debug=verbose -C test test EXECUTE="$SDE"

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH == "chamois" || $CI_COMMIT_BRANCH == "kvx-work" || $CI_COMMIT_BRANCH == "kvx-work-ssa" || $CI_COMMIT_BRANCH == "kvx-work-velus" || $CI_COMMIT_BRANCH == "master"

.common_64:
  image: $REGISTRY/ccomp_verimag_dimg_64:$DOCKERTAG
  stage: test

.common_32:
  image: $REGISTRY/ccomp_verimag_dimg_32:$DOCKERTAG
  stage: test

.common_kvx:
  image: $REGISTRY/ccomp_verimag_dimg_kvx:$DOCKERTAG
  stage: test

docker_build:
  image: docker:latest
  stage: build
  services:
    - docker:dind
  script:
    - docker login -u $DEPLOY_TOKEN_USER -p $DEPLOY_TOKEN_PASSWORD $CI_REGISTRY
    - cd docker
    - docker build -t ccomp_verimag_dimg_base:$DOCKERTAG ./ccomp_verimag_docker_base/
    - docker build --build-arg WD=$WD --build-arg NJOBS=$NJOBS --build-arg RV32_DIR=$RV32_DIR -t ccomp_verimag_dimg_64:$DOCKERTAG ./ccomp_verimag_docker_64/
    - docker build -t ccomp_verimag_dimg_32:$DOCKERTAG ./ccomp_verimag_docker_32/
    - echo $KALRAY_SFTP_PASSWORD > ccomp_verimag_docker_kvx/$KALRAY_SFTP_PW_FILE
    - docker build --build-arg KALRAY_SFTP_PW_FILE=$KALRAY_SFTP_PW_FILE --build-arg KALRAY_VERSION=$KALRAY_VERSION -t ccomp_verimag_dimg_kvx:$DOCKERTAG ./ccomp_verimag_docker_kvx/
    - docker tag ccomp_verimag_dimg_64:$DOCKERTAG $REGISTRY/ccomp_verimag_dimg_64:$DOCKERTAG
    - docker tag ccomp_verimag_dimg_32:$DOCKERTAG $REGISTRY/ccomp_verimag_dimg_32:$DOCKERTAG
    - docker tag ccomp_verimag_dimg_kvx:$DOCKERTAG $REGISTRY/ccomp_verimag_dimg_kvx:$DOCKERTAG
    - docker push $REGISTRY/ccomp_verimag_dimg_64:$DOCKERTAG
    - docker push $REGISTRY/ccomp_verimag_dimg_32:$DOCKERTAG
    - docker push $REGISTRY/ccomp_verimag_dimg_kvx:$DOCKERTAG
  rules:
    - changes:
        - docker/**/*
      when: always

check_admitted:
  extends: .common_64
  script:
    - ./config_x86_64.sh
    - make check-admitted-full

test_x86_64:
  extends: .common_64
  variables:
    TCC: 'gcc'
    SDE: '$SDE64 -cet -- '
  script:
    - ./config_x86_64.sh
    - *build_script
    - *common_test_simu
    - *intel_sde_test

test_ia32:
  extends: .common_32
  variables:
    TCC: 'gcc -m32'
    BITS: 'BITS=32'
    SDE: '$SDE32 -cet -- '
  script:
    - ./config_ia32.sh
    - *build_script
    - *common_test_simu
    - *intel_sde_test

test_aarch64:
  extends: .common_64
  variables:
    SIMU: 'qemu-aarch64 -L /usr/aarch64-linux-gnu'
    TCC: 'aarch64-linux-gnu-gcc'
  script:
    - ./config_aarch64.sh
    - *build_script
    - *common_test_simu

test_armv6:
  extends: .common_64
  variables:
    SIMU: 'qemu-arm -L /usr/arm-linux-gnueabi'
    TCC: 'arm-linux-gnueabi-gcc'
    BITS: 'BITS=32'
  script:
    - ./config_armv6.sh
    - *build_script
    - *common_test_simu

test_arm:
  extends: .common_64
  variables:
    SIMU: 'qemu-arm -L /usr/arm-linux-gnueabi'
    TCC: 'arm-linux-gnueabi-gcc'
    BITS: 'BITS=32'
    OPTS1: '-marm'
    OPTS2: '-marm'
  script:
    - ./config_arm.sh
    - *build_script
    - *common_test_simu

test_armhf:
  extends: .common_64
  variables:
    SIMU: 'qemu-arm -L /usr/arm-linux-gnueabihf'
    TCC: 'arm-linux-gnueabihf-gcc'
    BITS: 'BITS=32'
    OPTS1: '-marm'
    OPTS2: '-marm'
  script:
    - ./config_armhf.sh
    - *build_script
    - *common_test_simu

test_arm_thumb:
  extends: .common_64
  variables:
    SIMU: 'qemu-arm -L /usr/arm-linux-gnueabi'
    TCC: 'arm-linux-gnueabi-gcc'
    BITS: 'BITS=32'
    OPTS1: '-mthumb'
    OPTS2: '-mthumb'
  script:
    - ./config_arm.sh
    - *build_script
    - *common_test_simu

test_armhf_thumb:
  extends: .common_64
  variables:
    SIMU: 'qemu-arm -L /usr/arm-linux-gnueabihf'
    TCC: 'arm-linux-gnueabihf-gcc'
    BITS: 'BITS=32'
    OPTS1: '-mthumb'
    OPTS2: '-mthumb'
  script:
    - ./config_armhf.sh
    - *build_script
    - *common_test_simu

test_ppc:
  extends: .common_64
  variables:
    SIMU: 'qemu-ppc -L /usr/powerpc-linux-gnu -cpu 7400'
    TCC: 'powerpc-linux-gnu-gcc'
    BITS: 'BITS=32'
  script:
    - qemu-ppc --version
    - ./config_ppc.sh
    - *build_script

test_ppc64:
  extends: .common_64
  variables:
    SIMU: 'qemu-ppc -L /usr/powerpc-linux-gnu -cpu 7400'
  script:
    - ./config_ppc64.sh
    - *build_script

test_rv64:
  extends: .common_64
  variables:
    SIMU: 'qemu-riscv64 -L /usr/riscv64-linux-gnu'
    TCC: 'riscv64-linux-gnu-gcc'
  script:
    - ./config_rv64.sh
    - *build_script
    - *common_test_simu

test_rv32:
  extends: .common_64
  variables:
    SIMU: 'qemu-riscv32 -L $WD/$RV32_DIR/sysroot'
    TCC: 'riscv32-unknown-linux-gnu-gcc'
    BITS: 'BITS=32'
  script:
    - ./config_rv32.sh
    - *build_script
    - *common_test_simu

test_kvx:
  extends: .common_kvx
  variables:
    SIMU: 'kvx-cluster -- '
    TCC: 'kvx-cos-gcc'
    BINRC: '/opt/kalray/accesscore/kalray.sh'
    TIMEOUT: '45s'
    OPTS2: '-Wl,--defsym=USER_STACK_SIZE=0x80000'
  before_script:
    - rm -rf flocq
  script:
    - source $BINRC
    - ./config_kvx.sh --use-external-Flocq
    - *build_script
    - *common_test_simu

pages:
  extends: .common_kvx
  variables:
    BINRC: '/opt/kalray/accesscore/kalray.sh'
  before_script:
    - rm -rf flocq
  script:
    - source $BINRC
    - ./config_kvx.sh --use-external-Flocq
    - *build_script
    - make documentation
    - mkdir public
    - cp -rf doc/* public/
    - ./tools/fix_html_date.sh doc/index-chamois-kvx.html > public/index.html
    - rm public/index-chamois-kvx.html
  artifacts:
    paths:
    - public
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: always

test_macos:
  tags:
    - macos
  stage: test
  extends: .common_64
  script:
    - opam init
    - opam install -y coq menhir
    - eval $(opam env)
    - ./config_macos_aarch64.sh
    - *build_script
    - make -j 1 -C test SIMU="$SIMU" EXECUTE="$SIMU" CCOMPOPTS="$OPTS0 $OPTS1" all test 
    - make -j "$NJOBS" -C test/other_testsuites/paranoia test CCOMPOPTS="$OPTS0 $OPTS1" EXECUTE="$SIMU"
    
include:
  - template: Jobs/SAST-IaC.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml
  